let colors = {
    'grey-darkest': '#3d4852',
    'grey-darker': '#606f7b',
    'grey-dark': '#8795a1',
    'grey': '#b8c2cc',
    'grey-light': '#dae1e7',
    'grey-lighter': '#f1f5f8',
    'transparent': 'transparent',
    'regal-blue': '#000080', 

   
};
module.exports = {
  future: {
     removeDeprecatedGapUtilities: true,
     purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: colors,
    },
    listStyleType: {
      none: 'none',
      disc: 'disc',
      decimal: 'decimal',
      square: 'square',
      roman: 'lower-roman',
      }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms'),
  ],
}

