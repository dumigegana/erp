<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
      Schema::connection('mysql')->create('results', function (Blueprint $table) {
        $table->id();
        $table->foreignId('registration_id');
        $table->double('course_w', 5, 2)->nullable();
        $table->double('exam', 5, 2)->nullable();
        $table->boolean('is_pass')->default(false);
        $table->boolean('is_publish')->default(false);
        $table->string('remark')->nullable(); 
        $table->softDeletes();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('results', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('results');
    }
}
