<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('departments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('faculty_id')->nullable();
            $table->foreignId('hod_id')->nullable();
            $table->string('code')->nullable()->unique(); 
            $table->string('name')->unique(); 
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('departments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('departments');
    }
}
