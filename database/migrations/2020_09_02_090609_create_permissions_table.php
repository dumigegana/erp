<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('permissions', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        $permissions = [
            'dashboard.access',
            'admin.access',
            'staff.access',
            'academic.access',  
            'academic.admin',
            'academic.hod',
            'academic.dean',
            'academic.registry',
            'users.access',
            'users.admin',
            'roles.access',
            'roles.admin',            
            'CRUD.access',
            'CRUD.admin',
            'admissions.access',
            'admissions.admin',
            'bursary.access',
            'bursary.admin',
            'hod.admin',
            'hr.admin',
            'dean.admin',
            'check.sheet.access',
            'check.sheet.admin',
            'programmes.access',
            'programmes.admin',
            'departments.access',
            'departments.admin',
            'faculties.access',
            'faculties.admin',
            'registration.access',
            'registration.admin',
            'gsu.tutor',
        ];

        foreach($permissions as $permission) {
            $perm = new App\Permission;
            $perm->slug = $permission;
            $perm->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('permissions', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('permissions');
    }
}
 