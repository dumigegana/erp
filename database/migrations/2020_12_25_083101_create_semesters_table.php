<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('semesters', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->date('reg_from')->nullable();
            $table->date('reg_to')->nullable();
            $table->boolean('active')->default('0');           
            $table->boolean('billed')->default('0');           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::connection('mysql')->table('semesters', function (Blueprint $table) {
        //     $table->dropSoftDeletes();
        // });
        Schema::connection('mysql')->dropIfExists('semesters');
    }
}
