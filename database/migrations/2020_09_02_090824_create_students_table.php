<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('gsu_id')->unique();
            $table->foreignId('cohort_id');
            $table->foreignId('scholarship_id')->nullable();
            $table->foreignId('programme_id');
            $table->foreignId('part_id')->default(1);
            $table->boolean('disability');
            $table->string('sex'); 
            $table->boolean('imprt')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('students', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('students');
    }
}
