<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('description')->nullable(); 
            $table->softDeletes();
            $table->timestamps();
        });

        $roles = [
            'Admin',
            'Staff',
            'Lecturer',
            'Student',
            'Dean',
            'Hod',
            'Admissions',
            'Registry',
        ];

        foreach($roles as $role) {
            $rol = new App\Role;
            $rol->name = $role;
            $rol->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('roles', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('roles');
    }
}
