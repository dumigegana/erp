<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTertiaryInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('tertiary_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('applicant_id');
            $table->string('institution');
            $table->string('programme');
            $table->string('class');
            $table->string('certificat_loc');
            $table->string('transcript_loc')->nullable();
            $table->string('period');
            $table->string('awaded');
            $table->softDeletes();
            $table->timestamps();
        });
    }
     
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('tertiary_infos', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('tertiary_infos');
    }
}
