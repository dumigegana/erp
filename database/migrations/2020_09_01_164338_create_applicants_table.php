<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('applicants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id')->nullable();
            $table->foreignId('cohort_id')->nullable();
            $table->foreignId('entry_type_id')->nullable();
            $table->foreignId('study_program_id')->nullable();
            $table->foreignId('programme_id')->nullable();
            $table->boolean('terms_agree')->default(0);
            $table->string('first_name');
            $table->string('middle_names')->nullable();
            $table->string('surname');
            $table->string('previous_surname')->nullable();
            $table->string('email');
            $table->string('home_address')->nullable();
            $table->string('sex');
            $table->string('tittle')->nullable();
            $table->string('marital_status');
            $table->string('national_id');
            $table->string('nationality')->nullable();
            $table->string('citizenship')->nullable();
            $table->boolean('perm_res_zim')->default(0);
            $table->string('permit')->nullable();
            $table->string('zim_res_period')->nullable();
            $table->string('cell_number');
            $table->string('home_phone')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->boolean('has_dis')->default(0);
            $table->foreignId('disability_id')->nullable();
            $table->string('prospective_sponsor')->nullable();
            $table->string('referee_1')->nullable();
            $table->string('referee_2')->nullable();
            $table->string('ref1_address')->nullable();
            $table->string('ref2_address')->nullable();
            $table->string('birth_certificate')->nullable();
            $table->string('birth_cert_no')->nullable();
            $table->string('id_card')->nullable();
            $table->string('marriage_certificate')->nullable();
            $table->string('cv')->nullable();
            $table->string('admissions_outcome')->default('Pending');
            $table->string('admissions_comment')->nullable();
            $table->string('fdean_outcome')->default('Pending');
            $table->string('fdean_comment')->nullable();
            $table->string('hod_outcome')->default('Pending');
            $table->string('hod_comment')->nullable();
            $table->string('bursar_outcome')->default('Pending');
            $table->string('bursar_comment')->nullable();
            $table->bigInteger('hod_by')->nullable()->unsigned();
            $table->dateTime('hod_at')->nullable();
            $table->bigInteger('dean_by')->nullable()->unsigned();
            $table->dateTime('dean_at')->nullable();                       
            $table->bigInteger('bursar_by')->nullable()->unsigned();
            $table->dateTime('bursar_at')->nullable();
            $table->bigInteger('admissions_by')->nullable()->unsigned();
            $table->dateTime('admissions_at')->nullable();
            $table->dateTime('outcome_by')->nullable();
            $table->boolean('imprt')->default(false);
            $table->softDeletes();
            $table->timestamps();            
            $table->dateTime('outcome_at')->nullable();
        });
    }
        

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('applicants', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('applicants');
    }
}
