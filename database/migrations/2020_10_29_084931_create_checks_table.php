<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */ 
  public function up()
  {
    Schema::connection('mysql')->create('checks', function (Blueprint $table) {
      $table->id();
      $table->foreignId('applicant_id');
      $table->integer('step')->default(0);
      $table->boolean('complete')->default(false);
      $table->string('personal')->default('Incomplete');
      $table->string('academic')->default('Incomplete');
      $table->string('files')->default('Incomplete');
      $table->string('outcomes')->default('Incomplete');
      $table->string('status')->default('Applicant');
      $table->softDeletes();
      $table->timestamps(); 
    });
     for($i = 1; $i <= 430; $i++) {
        $check = new App\Check;
            $check->applicant_id = $i;
            $check->step = 7;
            $check->complete = true;
            $check->personal = "Complete";
            $check->academic = "Complete";
            $check->files = "Complete";
            $check->outcomes = "Complete";
            $check->status = "Student";
            $check->save();
     }    
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
        Schema::connection('mysql')->table('checks', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
      Schema::connection('mysql')->dropIfExists('checks');
  }
}
