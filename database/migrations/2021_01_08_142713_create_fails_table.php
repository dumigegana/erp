<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('fails', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sem_course_id');
            $table->foreignId('course_id'); 
            $table->foreignId('fail_result_id'); 
            $table->foreignId('repeat_result_id'); 
            $table->boolean('repeated')->default(false); 
            $table->softDeletes();
            $table->timestamps(); 
        });
    }    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('fails', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('fails');
    }
}
