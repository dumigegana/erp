<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('mysql')->create('registrations', function (Blueprint $table) {
        $table->id(); 
        $table->foreignId('part_id');
        $table->foreignId('student_id');
        $table->foreignId('sem_course_id');
        $table->foreignId('fail_id')->nullable();
        $table->boolean('fin_clearnc')->default('0');
        $table->boolean('approved')->default('0');
        $table->string('status')->default('Current');        
        $table->softDeletes(); 
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('registrations', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('registrations');
    }
}
