<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('sem_courses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('course_part_id');
            $table->foreignId('semester_id');
            $table->foreignId('weight_id')->default(1);
            $table->foreignId('staff_id');
            $table->boolean('lect_submit')->default(0);
            $table->boolean('dept_aproved')->default(0);
            $table->boolean('faclt_aproved')->default(0);
            $table->boolean('acad_aproved')->default(0);
            $table->integer('state')->default(10);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('sem_courses', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('sem_courses');
    }
}
