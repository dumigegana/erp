<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('gsu_id');
            $table->string('fullname');
            $table->string('program')->nullable();
            $table->double('amount', 30, 2)->nullable();
            $table->string('phone')->nullable();
            $table->string('status')->nullable();
            $table->string('channel')->nullable();
            $table->string('reference')->unique();
            $table->string('merchanttrace')->unique()->nullable();
            $table->string('hash')->nullable();
            $table->string('pollurl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('payments');
    }
}
