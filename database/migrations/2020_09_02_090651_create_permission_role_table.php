<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('permission_role', function (Blueprint $table) {
            $table->id();
            $table->foreignId('permission_id');
            $table->foreignId('role_id'); 
            $table->softDeletes();
            $table->timestamps();
        });
        foreach(App\Permission::all() as $perm) {
            $rel = new App\Permission_Role;
            $rel->permission_id = App\Permission::permission('id', $perm->id)->id;
            $rel->role_id = App\Role::role_m('name', 'admin')->id;
            $rel->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('permission_role', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::connection('mysql')->dropIfExists('permission_role');
    }
}
