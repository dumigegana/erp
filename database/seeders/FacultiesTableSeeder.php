<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Faculty;
class FacultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('faculties');
        $faculties = array(
            ['name' => 'NON ACADEMIC SECTIONS ', 'code' => 'None'],
            ['name' => 'FACULTY OF  LIFE SCIENCES', 'code' => 'HLS'],
            ['name' => 'FACULTY OF ENGINEERING AND THE ENVIRONMENT', 'code' => 'HEEN'],
        );

        // Loop through each faculty above and create the record for them in the database
        foreach ($faculties as $faculty)
        {
            Faculty::create($faculty);
        }
    }
}
