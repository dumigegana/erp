<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Cohort;
class CohortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('cohorts');
        $cohorts = array(
            ['name' => 'Before 2020', 'bulletin_id' => 1],
            ['name' => '2020 Intake', 'bulletin_id' => 2],
            ['name' => '2021 Intake', 'bulletin_id' => 2],

        );

        // Loop through each cohort above and create the record for them in the database
        foreach ($cohorts as $cohort)
        {
            Cohort::create($cohort);
        }
    }
}
