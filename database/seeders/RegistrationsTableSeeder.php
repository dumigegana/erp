<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Registration;

class RegistrationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory('App\Registration', 20)->create();
        Registration::factory()->count(20)->create();
    }
}
