<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Bulletin;
class BulletinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('bulletins');
        $bulletins = array(
            ['name' => 'Bulletin 1', 'year_range' => 'Before 2020'],
            ['name' => 'Bulletin 2', 'year_range' => '2020-2023'],           

        );

        // Loop through each cohort above and create the record for them in the database
        foreach ($bulletins as $bulletin)
        {
            Bulletin::create($bulletin);
        }
    }
}
