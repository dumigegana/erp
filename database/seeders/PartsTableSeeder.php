<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Part;
class PartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('parts');
        $parts = array(
            ['part' =>  'PRE', 'semester' => 'PRE'],
            ['part' =>  1, 'semester' => 1],
            ['part' =>  1, 'semester' => 2],
            ['part' =>  2, 'semester' => 1],
            ['part' =>  2, 'semester' => 2],
            ['part' =>  3, 'semester' => 1],
            ['part' =>  3, 'semester' => 2],
            ['part' =>  4, 'semester' => 1],
            ['part' =>  4, 'semester' => 2],
            ['part' =>  5, 'semester' => 1],
            ['part' =>  5, 'semester' => 2],

        );

        // Loop through each part above and create the record for them in the database
        foreach ($parts as $part)
        {
            Part::create($part);
        }
    }
}
