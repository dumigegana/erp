<?php

         namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Staff;
use Illuminate\Support\Facades\DB;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         Model::unguard();
        DB::connection('mysql')->table('staffs');
        $staffs = array(
            ['user_id' => 1, 'gsu_number' => 99999999, 'title' => 'Dr', 'sex' => 'Male', 'department_id'=>1, 'position' => 'Senior Lecture', 'teach' => 1],
            ['user_id' => 4, 'gsu_number' => 99999999, 'title' => 'Dr', 'sex' => 'Male', 'department_id'=>1, 'position' => 'Facculty Dean', 'teach' => 1],
            ['user_id' => 5, 'gsu_number' => 99999999, 'title' => 'Dr', 'sex' => 'Male', 'department_id'=>1, 'position' => 'Facculty Dean', 'teach' => 1],
            ['user_id' => 6, 'gsu_number' => 99999999, 'title' => 'Dr', 'sex' => 'Male', 'department_id'=>2, 'position' => 'HOD Agriculture', 'teach' => 1],
            ['user_id' => 7, 'gsu_number' => 99999999, 'title' => 'Dr', 'sex' => 'Male', 'department_id'=>3, 'position' => 'HOD Mining', 'teach' => 1],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($staffs as $staff)
        {
            Staff::create($staff);
        }
    }
}


 