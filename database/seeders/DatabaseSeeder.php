<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(UsersTableSeeder::class);
      $this->call(StaffsTableSeeder::class);
      $this->call(StudentsTableSeeder::class);        
      $this->call(ApplicantsTableSeeder::class);
      $this->call(Role_UsersTableSeeder::class);
      $this->call(Entry_typesTableSeeder::class);
      $this->call(FacultiesTableSeeder::class);
      $this->call(Study_programsTableSeeder::class);
      $this->call(SubjectsTableSeeder::class);
      $this->call(DisabilitiesTableSeeder::class);
      $this->call(DepartmentsTableSeeder::class);
      $this->call(ProgrammesTableSeeder::class);
      $this->call(GradesTableSeeder::class);
      $this->call(CohortsTableSeeder::class);
      $this->call(CoursesTableSeeder::class);
      $this->call(PartsTableSeeder::class);
      $this->call(CoursePartsTableSeeder::class);
      $this->call(SemCoursesTableSeeder::class);
      $this->call(SemestersTableSeeder::class);
      $this->call(DeansTableSeeder::class);
      $this->call(WeightsTableSeeder::class);
      $this->call(HodsTableSeeder::class);
      $this->call(BulletinsTableSeeder::class);
      $this->call(BulletinCoursesTableSeeder::class);
      $this->call(OldresultsTableSeeder::class);
      // Disable all mass assignment restrictions
    //   Model::unguard();
    //   $this->call(SemCoursesTableSeeder::class);
    //   $this->call(RegistrationsTableSeeder::class);
    //   // Re enable all mass assignment restrictions
    //   Model::reguard();
    }
}
