<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Grade;
class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('grades');
        $grades = array(
            ['grade' => 'Fail', 'type' => 'GSU', 'lower' =>  0, 'upper' =>  49.5, 'points' =>  0],
            ['grade' => 'P', 'type' => 'GSU', 'lower' =>   49.5, 'upper' =>  54.5, 'points' =>  2],
            ['grade' => '2.2', 'type' => 'GSU', 'lower' =>  54.5, 'upper' =>  64.5, 'points' =>  3],
            ['grade' => '2.1', 'type' => 'GSU', 'lower' =>  64.5, 'upper' =>  74.5, 'points' =>  4],
            ['grade' => '1', 'type' => 'GSU', 'lower' =>  74.5, 'upper' =>  100, 'points' =>  5],
            ['grade' => 'A', 'type' => 'O', 'points' =>  1],
            ['grade' => 'B', 'type' => 'O', 'points' =>  2],
            ['grade' => 'C', 'type' => 'O', 'points' =>  3],
            ['grade' => 'D', 'type' => 'O', 'points' =>  0],
            ['grade' => 'E', 'type' => 'O', 'points' =>  0],
            ['grade' => 'U', 'type' => 'O', 'points' =>  0],
            ['grade' => 'A', 'type' => 'A', 'points' =>  5],
            ['grade' => 'B', 'type' => 'A', 'points' =>  4],
            ['grade' => 'C', 'type' => 'A', 'points' =>  3],
            ['grade' => 'D', 'type' => 'A', 'points' =>  2],
            ['grade' => 'E', 'type' => 'A', 'points' =>  1],
            ['grade' => 'U', 'type' => 'A', 'points' =>  0],
        );

        // Loop through each grade above and create the record for them in the database
        foreach ($grades as $grade)
        {
            Grade::create($grade);
        }
    }
}
