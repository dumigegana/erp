<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Semester;
class SemestersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Model::unguard();
    DB::connection('mysql')->table('semesters');
    $semesters = array(
      ['name' =>  '2021 Conventional Sem I', 'reg_from' => '2021-06-14', 'reg_to' => '2022-03-26', 'active'=>1],
      ['name' =>  '2021 Block Sem I', 'reg_from' => '2021-04-07', 'reg_to' => '2021-04-26'],
      ['name' =>  '2021 Parallel Sem I', 'reg_from' => '2021-02-26', 'reg_to' => '2021-03-26'],
      ['name' =>  '2021 Conventional Sem II', 'reg_from' => '2021-09-26', 'reg_to' => '2021-10-26'],

    );

    // Loop through each semester above and create the record for them in the database
    foreach ($semesters as $semester)
    {
        Semester::create($semester);
    }
  }
}
