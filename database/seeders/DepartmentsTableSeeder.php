<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Department;
class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('departments');
        $departments = array(
            ['faculty_id' => '2', 'name' => 'General'],
            ['faculty_id' => '2', 'name' => 'Animal Science'],
            ['faculty_id' => '2', 'name' => 'Crop Science'],
            ['faculty_id' => '3', 'name' => 'Metallurgical Engineering'],
            ['faculty_id' => '3', 'name' => 'Mining Engineering'],
            ['faculty_id' => '3', 'name' => 'Geomatics and Surveying'],

        );

        // Loop through each department above and create the record for them in the database
        foreach ($departments as $department)
        {
            Department::create($department);
        }
    }
}
