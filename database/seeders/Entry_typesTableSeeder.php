<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Entry_type;
class Entry_typesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('entry_types');
        $entry_types = array(
            ['type' => 'Normal Entry', 'description' => 'Normal Entry into Block Release Programmes Through A\' Level Results or Equivalent'], 
            ['type' => 'Special Entry', 'description' => 'Special Entry Through Diploma Results or other Qualifications'], 
            ['type' => 'Mature Entry', 'description' => ''], 

        );

        // Loop through each entry_type above and create the record for them in the database
        foreach ($entry_types as $entry_type)
        {
            Entry_type::create($entry_type);
        }
    }
}
