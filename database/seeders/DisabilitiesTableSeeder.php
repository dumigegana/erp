<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Disability;
class DisabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('disabilities');
        $disabilities = array(
            ['code' =>  '01', 'description' => 'Intellectual Disabilities'],
            ['code' =>  '02', 'description' => 'HEARING IMPAIRMENT'],
            ['code' =>  '03', 'description' => 'DEAFNESS'],
            ['code' =>  '04', 'description' => 'SPEECH/LANGAUGE IMPAIRMENT'],
            ['code' =>  '05', 'description' => 'VISUAL IMPAIRMENT'],
            ['code' =>  '06', 'description' => 'EMOTIONAL DISTURBANCE'],
            ['code' =>  '07', 'description' => 'ORTHOPEDIC IMPAIRMENT'],
            ['code' =>  '08', 'description' => 'OTHER HEALTH IMPAIRMENT'],
            ['code' =>  '09', 'description' => 'SPECIFIC LEARNING DISABILITY'],
            ['code' =>  '10', 'description' => 'MULTIPLE DISABILITIES'],
            ['code' =>  '11', 'description' => 'DEAF/BLINDNESS'],
            ['code' =>  '12', 'description' => 'TRAUMATIC BRAIN INJURY'],
            ['code' =>  '13', 'description' => 'AUTISM'],
            ['code' =>  '14', 'description' => 'DEVELOPMENTAL DELAY'],


        );

        // Loop through each disability above and create the record for them in the database
        foreach ($disabilities as $disability)
        {
            Disability::create($disability);
        }
    }
}
