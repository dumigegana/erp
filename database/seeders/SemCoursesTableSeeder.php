<?php

    namespace Database\Seeders;

    use Illuminate\Database\Seeder;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\DB;
    use App\SemCourse;

    class SemCoursesTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Model::unguard();
            DB::connection('mysql')->table('sem_courses');
            $sem_courses = array(
              ['course_part_id' => 176, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 86, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 159, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 110, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 107, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 145, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 100, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 93, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 90, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 85, 'semester_id' => 1, 'staff_id' => 4],
              ['course_part_id' => 88, 'semester_id' => 1, 'staff_id' => 4],
            );

            // Loop through each semester above and create the record for them in the database
            foreach ($sem_courses as $sem_course)
            {
                SemCourse::create($sem_course);
            }
        }
    }
