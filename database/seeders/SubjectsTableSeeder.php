<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Subject;
class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('subjects');
        $subjects = array(
            ['type' =>  'OA', 'name' => 'Accounting'],
            ['type' =>  'O', 'name' => 'Accounts'],
            ['type' =>  'OA', 'name' => 'Accounts, Principles of'],
            ['type' =>  'OA', 'name' => 'Ancient History'],
            ['type' =>  'A', 'name' => 'Ancient History and Literature'],
            ['type' =>  'OA', 'name' => 'Applied Mechanics'],
            ['type' =>  'O', 'name' => 'Applied Statistics'],
            ['type' =>  'OA', 'name' => 'Art'],
            ['type' =>  'OA', 'name' => 'Art and Craft (AEC)'],
            ['type' =>  'OA', 'name' => 'Bible Knowledge'],
            ['type' =>  'OA', 'name' => 'Biology'],
            ['type' =>  'OA', 'name' => 'Bookkeeping and Accounting'],
            ['type' =>  'OA', 'name' => 'Botany'],
            ['type' =>  'O', 'name' => 'Building Supplies'],
            ['type' =>  'OA', 'name' => 'Business Studies/Management'],
            ['type' =>  'OA', 'name' => 'Chemistry'],
            ['type' =>  'O', 'name' => 'Commerce'],
            ['type' =>  'OA', 'name' => 'Computer Studies'],
            ['type' =>  'A', 'name' => 'Computer Science'],
            ['type' =>  'O', 'name' => 'Computing Studies'],
            ['type' =>  'O', 'name' => 'Craft and Design'],
            ['type' =>  'OA', 'name' => 'Divinity'],
            ['type' =>  'O A', 'name' => 'Drama and Theatre Arts (AEB) Economic and Political Studies'],
            ['type' =>  'O', 'name' => 'Economic and Public Affairs'],
            ['type' =>  'A', 'name' => 'Economic and Social History'],
            ['type' =>  'A', 'name' => 'Economic Geography'],
            ['type' =>  'OA', 'name' => 'Economic History'],
            ['type' =>  'O', 'name' => 'Economic Principles'],
            ['type' =>  'OA', 'name' => 'Economics'],
            ['type' =>  'A', 'name' => 'Electronic Systems (AEB)'],
            ['type' =>  'O', 'name' => 'Electricity and Electronics'],
            ['type' =>  'O', 'name' => 'Elementary Physiology'],
            ['type' =>  'O', 'name' => 'Elements of Sociology'],
            ['type' =>  'OA', 'name' => 'Engineering Drawing'],
            ['type' =>  'OA', 'name' => 'Engineering Science'],
            ['type' =>  'O', 'name' => 'English Language'],
            ['type' =>  'OA', 'name' => 'English Literature'],
            ['type' =>  'O', 'name' => 'Environmental Biology (AEB)'],
            ['type' =>  'OA', 'name' => 'Environmental Studies'],
            ['type' =>  'OA', 'name' => 'fashion and Fabrics / dress Textiles'],
            ['type' =>  'OA', 'name' => 'Food and Nutrition / Food Science'],
            ['type' =>  'OA', 'name' => 'French'],
            ['type' =>  'O', 'name' => 'French Literature Cambridge'],
            ['type' =>  'OA', 'name' => 'French Studies'],
            ['type' =>  'OA', 'name' => 'General Mathematics'],
            ['type' =>  'OA', 'name' => 'General Principles of English Law'],
            ['type' =>  'OA', 'name' => 'General Science'],
            ['type' =>  'OA', 'name' => 'Geography'],
            ['type' =>  'OA', 'name' => 'Geology'],
            ['type' =>  'A', 'name' => 'Government and Political Studies/Politics'],
            ['type' =>  'A', 'name' => 'Government Economics and Commerce'],
            ['type' =>  'OA', 'name' => 'Health Science'],
            ['type' =>  'OA', 'name' => 'History'],
            ['type' =>  'OA', 'name' => 'History, Ancient'],
            ['type' =>  'OA', 'name' => 'History of Art'],
            ['type' =>  'O', 'name' => 'History and Appreciation of Music'],
            ['type' =>  'OA', 'name' => 'Home of Economics'],
            ['type' =>  'OA', 'name' => 'Human biology'],
            ['type' =>  'OA', 'name' => 'Law'],
            ['type' =>  'OA', 'name' => 'Mathematics'],
            ['type' =>  'A', 'name' => 'Mathematics/Applied'],
            ['type' =>  'OA', 'name' => 'Mathematics, Pure'],
            ['type' =>  'O', 'name' => 'Metalwork'],
            ['type' =>  'OA', 'name' => 'Music'],
            ['type' =>  'OA', 'name' => 'Ndebele'],
            ['type' =>  'OA', 'name' => 'Physical Science'],
            ['type' =>  'OA', 'name' => 'Physics'],
            ['type' =>  'O', 'name' => 'Physics with Chemistry'],
            ['type' =>  'OA', 'name' => 'Political Studies'],
            ['type' =>  'O', 'name' => 'Portuguese'],
            ['type' =>  'O', 'name' => 'Principles of Economics'],
            ['type' =>  'OA', 'name' => 'Psychology'],
            ['type' =>  'OA', 'name' => 'Religious Studies'],
            ['type' =>  'O', 'name' => 'Rural Biology'],
            ['type' =>  'OA', 'name' => 'Shona'],
            ['type' =>  'OA', 'name' => 'Social Science'],
            ['type' =>  'OA', 'name' => 'Sociology'],
            ['type' =>  'OA', 'name' => 'Statistics'],
            ['type' =>  'O', 'name' => 'Surveying'],
            ['type' =>  'OA', 'name' => 'Technical Drawing'],
            ['type' =>  'O', 'name' => 'Technical Graphics'],
            ['type' =>  'O', 'name' => 'Woodwork'],
            ['type' =>  'OA', 'name' => 'Zoology'],

        );

        // Loop through each subject above and create the record for them in the database
        foreach ($subjects as $subject)
        {
            Subject::create($subject);
        }
    }
}
