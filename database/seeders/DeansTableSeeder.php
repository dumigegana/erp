<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Dean;
class DeansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        Model::unguard();
        DB::connection('mysql')->table('deans');
        $deans = array(
            ['faculty_id' => 2, 'staff_id' => 2, 'active' => 1, 'from_date' => '2019-01-01'],
            ['faculty_id' => 3, 'staff_id' => 3, 'active' => 1, 'from_date' => '2019-01-01'],

        );

        // Loop through each dean above and create the record for them in the database
        foreach ($deans as $dean)
        {
            Dean::create($dean);
        }
    }
}
