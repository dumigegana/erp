<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Programme;
class ProgrammesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('programmes');
        $programmes = array(
            ['department_id' => '2', 'name' => 'Bachelor of Science Honours Degree in Animal Science', 'code' => 'LAS'],
            ['department_id' => '3', 'name' => 'Bachelor of Science Honours Degree in Crop Science', 'code' => 'LCS'],
            ['department_id' => '4', 'name' => 'Bachelor of Engineering Honours Degree in Metallurgical Engineering', 'code' => 'EMR'],
            ['department_id' => '5', 'name' => 'Bachelor of Engineering Honours Degree in Mining Engineering', 'code' => 'EMI'],
            ['department_id' => '6', 'name' => 'Bachelor of Engineering Degree in Geomatics and Surveying', 'code' => 'EGS'],

        );

        // Loop through each programme above and create the record for them in the database
        foreach ($programmes as $programme)
        {
            Programme::create($programme);
        }
    }
}
