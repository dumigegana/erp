<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Study_program;
class Study_programsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('study_programs');
        $study_programs = array(
            ['type' => 'CONVENTIONAL', 'description' => ''], 
            ['type' => 'PARALLEL', 'description' => ''], 
            ['type' => 'BLOCK', 'description' => ''],
            ['type' => 'NOT STATED', 'description' => ''],

        );

        // Loop through each study_program above and create the record for them in the database
        foreach ($study_programs as $study_program)
        {
            Study_program::create($study_program);
        }
    }
}
