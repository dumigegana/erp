<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Weight;
class WeightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('weights');
        $weights = array(
            ['course_w' => 0.25, 'exam' => 0.75],
            ['course_w' => 0.30, 'exam' => 0.70],
            ['course_w' => 0, 'exam' => 1],
            ['course_w' => 1, 'exam' => 0],
            ['course_w' => 0.50, 'exam' => 0.50],

        );

        // Loop through each Weight above and create the record for them in the database
        foreach ($weights as $weight)
        {
            Weight::create($weight);
        }
    }
}
