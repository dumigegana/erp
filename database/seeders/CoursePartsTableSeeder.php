<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\CoursePart;
class CoursePartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */ 
    public function run()
    {
        Model::unguard();
        DB::connection('mysql')->table('course_parts');
        $course_parts = array(
            ['part_id'=>3, 'course_id' => 201, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 200, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 200, 'programme_id' =>1,],
            ['part_id'=>9, 'course_id' => 197, 'programme_id' =>1,],
            ['part_id'=>9, 'course_id' => 195, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 193, 'programme_id' =>2,],
            ['part_id'=>8, 'course_id' => 192, 'programme_id' =>2,],
            ['part_id'=>7, 'course_id' => 190, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 189, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 188, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 187, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 186, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 185, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 184, 'programme_id' =>2,],
            ['part_id'=>5, 'course_id' => 182, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 182, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 181, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 180, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 179, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 178, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 178, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 177, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 176, 'programme_id' =>2,],
            ['part_id'=>3, 'course_id' => 175, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 175, 'programme_id' =>2,],
            ['part_id'=>3, 'course_id' => 174, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 174, 'programme_id' =>2,],
            ['part_id'=>3, 'course_id' => 173, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 173, 'programme_id' =>2,],
            ['part_id'=>3, 'course_id' => 172, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 172, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 171, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 171, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 170, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 170, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 169, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 169, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 168, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 168, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 167, 'programme_id' =>2,],
            ['part_id'=>9, 'course_id' => 166, 'programme_id' =>1,],
            ['part_id'=>9, 'course_id' => 165, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 164, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 164, 'programme_id' =>2,],
            ['part_id'=>8, 'course_id' => 163, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 162, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 162, 'programme_id' =>2,],
            ['part_id'=>8, 'course_id' => 161, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 161, 'programme_id' =>1,],
            ['part_id'=>8, 'course_id' => 161, 'programme_id' =>2,],
            ['part_id'=>8, 'course_id' => 160, 'programme_id' =>1,],
            ['part_id'=>9, 'course_id' => 159, 'programme_id' =>1,],
            ['part_id'=>7, 'course_id' => 158, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 157, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 156, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 155, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 154, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 153, 'programme_id' =>1,],
            ['part_id'=>5, 'course_id' => 152, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 151, 'programme_id' =>2,],
            ['part_id'=>4, 'course_id' => 150, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 149, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 148, 'programme_id' =>1,],
            ['part_id'=>4, 'course_id' => 147, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 146, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 146, 'programme_id' =>2,],
            ['part_id'=>3, 'course_id' => 145, 'programme_id' =>1,],
            ['part_id'=>3, 'course_id' => 145, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 143, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 143, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 142, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 142, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 141, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 141, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 140, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 140, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 139, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 139, 'programme_id' =>2,],
            ['part_id'=>2, 'course_id' => 138, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 137, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 137, 'programme_id' =>2,],
            ['part_id'=>11, 'course_id' => 136, 'programme_id' =>3,],
            ['part_id'=>11, 'course_id' => 135, 'programme_id' =>3,],
            ['part_id'=>11, 'course_id' => 134, 'programme_id' =>3,],
            ['part_id'=>10, 'course_id' => 133, 'programme_id' =>3,],
            ['part_id'=>10, 'course_id' => 132, 'programme_id' =>3,],
            ['part_id'=>10, 'course_id' => 131, 'programme_id' =>3,],
            ['part_id'=>11, 'course_id' => 130, 'programme_id' =>3,],
            ['part_id'=>11, 'course_id' => 129, 'programme_id' =>3,],
            ['part_id'=>9, 'course_id' => 128, 'programme_id' =>3,],
            ['part_id'=>7, 'course_id' => 127, 'programme_id' =>3,],
            ['part_id'=>7, 'course_id' => 126, 'programme_id' =>3,],
            ['part_id'=>7, 'course_id' => 125, 'programme_id' =>3,],
            ['part_id'=>7, 'course_id' => 124, 'programme_id' =>3,],
            ['part_id'=>7, 'course_id' => 123, 'programme_id' =>3,],
            ['part_id'=>6, 'course_id' => 122, 'programme_id' =>3,],
            ['part_id'=>6, 'course_id' => 121, 'programme_id' =>3,],
            ['part_id'=>6, 'course_id' => 120, 'programme_id' =>3,],
            ['part_id'=>6, 'course_id' => 119, 'programme_id' =>3,],
            ['part_id'=>6, 'course_id' => 118, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 117, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 116, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 115, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 114, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 113, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 112, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 111, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 110, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 109, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 108, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 107, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 106, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 105, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 104, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 103, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 102, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 101, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 100, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 99, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 98, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 97, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 96, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 95, 'programme_id' =>3,],
            ['part_id'=>10, 'course_id' => 91, 'programme_id' =>4,],
            ['part_id'=>10, 'course_id' => 90, 'programme_id' =>4,],
            ['part_id'=>10, 'course_id' => 89, 'programme_id' =>4,],
            ['part_id'=>9, 'course_id' => 86, 'programme_id' =>4,],
            ['part_id'=>7, 'course_id' => 85, 'programme_id' =>4,],
            ['part_id'=>7, 'course_id' => 84, 'programme_id' =>4,],
            ['part_id'=>7, 'course_id' => 83, 'programme_id' =>4,],
            ['part_id'=>7, 'course_id' => 82, 'programme_id' =>4,],
            ['part_id'=>7, 'course_id' => 81, 'programme_id' =>4,],
            ['part_id'=>6, 'course_id' => 80, 'programme_id' =>4,],
            ['part_id'=>6, 'course_id' => 79, 'programme_id' =>4,],
            ['part_id'=>6, 'course_id' => 78, 'programme_id' =>4,],
            ['part_id'=>6, 'course_id' => 77, 'programme_id' =>4,],
            ['part_id'=>6, 'course_id' => 76, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 75, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 75, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 74, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 74, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 73, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 73, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 72, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 72, 'programme_id' =>3,],
            ['part_id'=>5, 'course_id' => 71, 'programme_id' =>4,],
            ['part_id'=>5, 'course_id' => 71, 'programme_id' =>3,],
            ['part_id'=>4, 'course_id' => 70, 'programme_id' =>4,],
            ['part_id'=>4, 'course_id' => 69, 'programme_id' =>4,],
            ['part_id'=>4, 'course_id' => 68, 'programme_id' =>4,],
            ['part_id'=>4, 'course_id' => 67, 'programme_id' =>4,],
            ['part_id'=>4, 'course_id' => 66, 'programme_id' =>4,],
            ['part_id'=>4, 'course_id' => 66, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 65, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 65, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 64, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 64, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 63, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 63, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 62, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 62, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 61, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 61, 'programme_id' =>3,],
            ['part_id'=>3, 'course_id' => 60, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 60, 'programme_id' =>4,],
            ['part_id'=>3, 'course_id' => 60, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 59, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 59, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 58, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 58, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 57, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 57, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 56, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 56, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 55, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 55, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 54, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 54, 'programme_id' =>3,],
            ['part_id'=>10, 'course_id' => 49, 'programme_id' =>5,],
            ['part_id'=>10, 'course_id' => 48, 'programme_id' =>5,],
            ['part_id'=>10, 'course_id' => 47, 'programme_id' =>5,],
            ['part_id'=>10, 'course_id' => 46, 'programme_id' =>5,],
            ['part_id'=>9, 'course_id' => 44, 'programme_id' =>5,],
            ['part_id'=>7, 'course_id' => 43, 'programme_id' =>5,],
            ['part_id'=>7, 'course_id' => 42, 'programme_id' =>5,],
            ['part_id'=>7, 'course_id' => 41, 'programme_id' =>5,],
            ['part_id'=>7, 'course_id' => 40, 'programme_id' =>5,],
            ['part_id'=>7, 'course_id' => 39, 'programme_id' =>5,],
            ['part_id'=>6, 'course_id' => 38, 'programme_id' =>5,],
            ['part_id'=>6, 'course_id' => 37, 'programme_id' =>5,],
            ['part_id'=>6, 'course_id' => 36, 'programme_id' =>5,],
            ['part_id'=>6, 'course_id' => 35, 'programme_id' =>5,],
            ['part_id'=>6, 'course_id' => 34, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 33, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 32, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 31, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 30, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 29, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 28, 'programme_id' =>5,],
            ['part_id'=>5, 'course_id' => 27, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 26, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 25, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 24, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 23, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 22, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 21, 'programme_id' =>5,],
            ['part_id'=>4, 'course_id' => 20, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 19, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 18, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 17, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 16, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 15, 'programme_id' =>5,],
            ['part_id'=>3, 'course_id' => 14, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 13, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 12, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 11, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 10, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 9, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 8, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 7, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 6, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 5, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 4, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 3, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 2, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 1, 'programme_id' =>5,],
            ['part_id'=>2, 'course_id' => 1, 'programme_id' =>4,],
            ['part_id'=>2, 'course_id' => 1, 'programme_id' =>3,],
            ['part_id'=>2, 'course_id' => 1, 'programme_id' =>1,],
            ['part_id'=>2, 'course_id' => 1, 'programme_id' =>2,],
        );

        // Loop through each course_part above and create the record for them in the database
        foreach ($course_parts as $course_part)
        {
            CoursePart::create($course_part);
        }
    }
}
