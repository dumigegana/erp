<?php

         namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
Use App\Hod;
class HodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        Model::unguard();
        DB::connection('mysql')->table('hods');
        $hods = array( 
            ['department_id' => 4, 'staff_id' => 4,'active' => 1,'from_date' => '2019-01-01'],
            ['department_id' => 2, 'staff_id' => 5,'active' => 1,'from_date' => '2019-01-01'],

        );

        // Loop through each hod above and create the record for them in the database
        foreach ($hods as $hod)
        {
            Hod::create($hod);
        }
    }
}
