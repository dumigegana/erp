<?php

namespace Database\Factories;

use App\SemCourse;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SemCourseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SemCourse::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

		 // $course_ps = App\CoursePart::pluck('id')->toArray();
		 // $semesters = App\Semester::pluck('id')->toArray();
	    return [
	        // 'course_part_id' => $faker->randomElement($course_ps),
	        // 'semester_id' =>  $faker->randomElement($semesters),
	        'staff_id' => 1,
	    ];
    }
}
 