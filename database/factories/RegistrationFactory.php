<?php

namespace Database\Factories;

use App\Models\Registration;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RegistrationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Registration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	$students = App\Student::pluck('id')->toArray();
		 $sem_courses = App\SemCourse::pluck('id')->toArray();
	    return [
	        'student_id' => $faker->randomElement($students),
	        'sem_course_id' => $faker->randomElement($sem_courses),
	        'part_id' => function (array $sem_course) {
	            return App\SemCourse::find($sem_course['sem_course_id'])->course_part->part_id;
	        },
	    ];
	} 