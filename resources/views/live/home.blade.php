@extends('layouts.stud')
@section('content')
<div class="flex flex-col">
  <!-- Card Sextion Starts Here -->
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">

    <!--Profile Picture-->
    <div class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/5 lg:w-1/5">
    	@if (empty(Auth::user()->profile_picture))
	<img src="{{URL::asset('/img/avatar.png')}}"style="float:left;width:200px;height:200px;" >
	@else
	<img src="{{URL::asset('profile/'. auth()->user()->profile_picture)}}" alt="{{Auth::user()->fullname }}" style="float:left;width:200px;height:200px;"> 
	@endif
    </div>
    <!--Horizontal form-->
    <div class="mb-2 bg-white border-solid border-grey-light rounded border shadow-sm w-full md:w-2/5 lg:w-2/5">      
      <div class="p-3">
        <table class="table-fixed">          
          <tbody>
            <tr>
              <td class="font-bold text-blue-500">Name</td>
              <td class="text-yellow-600">: {{Auth::user()->fullname}}</td>
            </tr>
            @if(ERP::isStudent(auth()->user()))
            <tr>
              <td class="font-bold text-blue-500">ID</td>
              <td class="uppercase text-yellow-600">: {{Auth::user()->student->gsu_id}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Program</td>
              <td class="text-yellow-600">: {{Auth::user()->student->programme->code}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Program</td>
              <td class="text-yellow-600">: {{Auth::user()->student->programme->name}}</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <!--/Horizontal form-->

    <!--Underline form-->
    <div class="mb-2 md:mx-2 lg:mx-2 bg-white border-solid border-gray-200 rounded border shadow-sm w-full md:w-2/5 lg:w-2/5">
      <div class="p-3">
        <table class="table-fixed">
          <tbody>
             @if(ERP::isStudent(auth()->user()))
                  @if(auth()->user()->applicant)
            <tr>
              <td class="font-bold text-blue-500">Sex</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->sex}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Marital Status</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->marital_status}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Nationality</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->nationality}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">National ID</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->national_id}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Sponsor</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->prospective_sponsor}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Date of Birth</td>
              <td class="text-yellow-600">: {{Auth::user()->student->applicant->date_of_birth}}</td>
            </tr>
            @endif
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <!--/Underline form-->
  </div>
  <!-- /Cards Section Ends Here -->
   <!--Grid Form-->
 @if($cRegs->count() > 0)
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 my-5">
    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
      <div class="bg-blue-200 px-2 py-3 text-xl font-bold text-center border-solid border-gray-200 border-b">
          Current Courses
      </div>
      <div class="p-3">
        <table class="table-responsive w-full rounded">
          <thead>
            <tr>
              <th class=" bg-purple-600 text-white border w-1/7 px-4 py-2">Code</th>
              <th class=" bg-purple-600 text-white border w-1/4 px-4 py-2">Course Name</th>
              <th class=" bg-purple-600 text-white border w-1/6 px-4 py-2">Lecture</th>
              <th class=" bg-purple-600 text-white border w-1/6 px-4 py-2">Status</th>
              <th class=" bg-purple-600 text-white border w-1/6 px-4 py-2">Course Work</th>
            </tr>
          </thead>
          <tbody>
            @foreach($cRegs as $cReg)
            <tr>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$cReg->sem_course->course_part->course->code}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$cReg->sem_course->course_part->course->name}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$cReg->sem_course->staff->user->fullname}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold"> @if($cReg->sem_course->status == true) Approved @else Pending @endif</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">@if($cReg->result){{$cReg->result->course_w}}@endif</td>
            </tr>
            @endforeach              
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @else
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 my-5">
    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
      <div class="bg-blue-200 px-2 py-3 text-xl font-bold text-center border-solid border-gray-200 border-b">
          No Courses Registered
      </div>
    </div>
  </div>@endif
  <!--/Grid Form-->
</div>
@endsection


       