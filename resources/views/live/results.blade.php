@extends('layouts.stud')
@section('breadcrumb')
    <b class="text-xl text-yellow-500">MY RESULTS</b>
@endsection
@section('content')
<div class="flex flex-col">
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 my-5">
    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
      <div class="p-3">
        <table class="table-auto w-full rounded">
          <thead>
            <tr>
              <th class="border px-4 py-2 bg-purple-600 text-white font-bold">Code</th>
              <th class="border px-4 py-2 bg-purple-600 text-white font-bold">Course Name</th>
              <th class="border px-4 py-2 bg-purple-600 text-white font-bold">Mark</th>
              <th class="border px-4 py-2 bg-purple-600 text-white font-bold">Class</th>
              <th class="border px-4 py-2 bg-purple-600 text-white font-bold">Part</th>
            </tr>
          </thead>
          <tbody>
            @foreach($semesters as $semester)
            <tr>
              <td class=" bg-blue-200"></td>
              <td class="px-4 py-2 bg-blue-200 uppercase font-bold text-center">{{$semester->name}}</td>
              <td class=" bg-blue-200"></td>
              <td class=" bg-blue-200"> </td>
              <td class=" bg-blue-200"></td>
            </tr>
            @foreach($semester->sem_courses as $sem_course)
            @foreach($sem_course->registrations as $registration)
            @if($registration->result)
            @if($registration->id % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-blue-100'; @endphp @endif
            <tr class="{{$bg}}">
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$sem_course->course_part->course->code}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$sem_course->course_part->course->name}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$registration->result->overall}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold"> {{$registration->result->grade}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$registration->result->course_w}}</td>
            </tr>
            @endif
            @endforeach 
            @endforeach
            @endforeach
            @if(auth()->user()->student->imprt)
            
              @foreach($parts as $part)
            <tr>
              <td class=" bg-blue-200"></td>
              <td class="px-4 py-2 bg-blue-200 uppercase font-bold text-center">Part {{$part->part}}:{{$part->semester}} Results</td>
              <td class=" bg-blue-200"></td>
              <td class=" bg-blue-200"> </td>
              <td class=" bg-blue-200"></td>
            </tr>
            @foreach($part->oldresults as $result)
             @if($result->id % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-blue-100'; @endphp @endif
            <tr class="{{$bg}}">
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$result->course->code}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$result->course->name}} </td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$result->overall}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$result->grade}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$result->part->part}} : {{$result->part->semester}}</td>
            </tr>
            @endforeach
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!--/Grid Form-->
</div>
@endsection


       