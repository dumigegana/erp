@extends('layouts.stud')
@section('breadcrumb')
    <b class="text-xl text-yellow-500">MY COURSES</b>
@endsection
@section('content')
<div class="flex flex-col">
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 my-5">
    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
      <div class="bg-blue-200 uppercase px-2 py-3 text-xl font-bold text-center border-solid border-gray-200 border-b">
          Current Courses
      </div>
      <div class="p-3">
        <table class="table-responsive w-full rounded">
          <thead>
            <tr>
              <th class="border w-1/8 px-4 py-2 bg-purple-600 text-white">Part</th>
              <th class="border w-2/8 px-4 py-2 bg-purple-600 text-white">Code</th>
              <th class="border w-5/8 px-4 py-2 bg-purple-600 text-white">Course Name</th>
              <th class="border w-5/8 px-4 py-2 bg-purple-600 text-white">Status</th>
            </tr>
          </thead> 
          <tbody>
            @foreach($course_ps as $course_p)
            @if($course_p->course)
            @if($course_p->course->hasBulletin($bulletin))
            @if($course_p->id % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-blue-100'; @endphp @endif
            <tr class="{{$bg}}">
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course_p->part->part}}:{{$course_p->part->semester}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course_p->course->code}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course_p->course->name}}</td>
              <td class="border text-center pl-8 font-bold">
               @if(in_array($course_p->course_id, $repeats))<x-zondicon-close class="h-5 w-5 text-red-500"/> @endif
                @if($result_keys->contains($course_p->course_id))<x-zondicon-checkmark class="h-5 w-5 text-green-500 "/> @endif
               </td>
            </tr>
            @endif 
            @endif 
            @endforeach              
          </tbody> 
        </table>
      </div>
    </div>
  </div>
  <!--/Grid Form-->
</div>
@endsection


       