@extends('layouts.stud')
@section('breadcrumb')
    <b class="text-xl text-yellow-500">Registration</b>
@endsection
@section('content')
@livewire('student.registration.register', ['stat' => $stat])
@endsection


       