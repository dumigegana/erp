@extends('layouts.app')
@section('content')
@if ($applicant->check->step > 3)
@livewire('apply.data', ['applicant' => $applicant, 'step' => $applicant->check->step])
@else
@livewire('apply.enrol', ['applicant' => $applicant, 'step' => $applicant->check->step])
@endif
@endsection


       