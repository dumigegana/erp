@extends('layouts.guest')
@section('title', 'Login')
@section('content')
<div class="max-w-xl mt-20 p-10 bg-white rounded shadow-xl">
  <form method="POST" action="{{ route('login') }}">        
    @csrf
    <p class="text-gray-800 font-medium text-center text-lg font-bold">Login</p>
    <div class="">
      <label class="block text-sm text-gray-00" for="username">Username</label>
      @error('username')<span class="text-sm text-red-500">{{$message}}</span>@enderror
      <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="username" name="username" type="text" required="" placeholder="User Name" aria-label="username">
    </div>
    <div class="mt-2">
      <label class="block text-sm text-gray-600" for="password">Password</label>
      @error('password')<span class="text-sm text-red-500 ml-5">{{$message}}</span>@enderror
      <input class="w-full px-5  py-1 text-gray-700 bg-gray-200 rounded" id="password" name="password" type="password" required="" placeholder="*******" aria-label="password">
    </div>
    <center>
      <button class="px-4 mt-5 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Login</button>
    </center>
  </form>
  <div class="space-x-48 pl-5">
    <div
      class="inline-block right-0 align-baseline"
      x-data="{ 'isDialogOpen': false }"
      @keydown.escape="isDialogOpen = false"
    > 

      <a href="#" class="font-bold text-sm text-500 hover:text-blue-800" @click="isDialogOpen = true">Reset Password</a>

        <!-- overlay -->
      <div
          class="overflow-auto"
          style="background-color: rgba(0,0,0,0.5)"
          x-show="isDialogOpen"
          :class="{ 'absolute inset-0 z-10 flex items-center justify-center': isDialogOpen }"
      >
        <!-- dialog -->
        <div
           class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6 relative"
           x-show="isDialogOpen"
           @click.away="isDialogOpen = false"
        >
          
              
          <button class="absolute top-0 right-0 pr-3" type="button" @click="isDialogOpen = false">✖</button> 

          <form method="POST" action="{{route('rec_signup')}}">
            @csrf
            <div class="flex justify-between items-center pb-3">
              <p class="block text-lg text-gray-600">Enter your GSU Username</p>
            </div>

            <!-- content -->
            <input type="hidden" name="type" value="2">
            @error('username')<span class="text-sm text-red">{{$message}}</span>@enderror
            <input class="w-full px-5 py-1 border-2 border-gray-600 text-gray-800 bg-gray-200 rounded" name="username">
            <!--Footer-->
            <div class="flex justify-end pt-2">
              <button class="modal-close px-4 mt-5 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit" @click="isDialogOpen = false">Submit</button>
            </div>
          </form>
        </div><!-- /dialog -->
      </div><!-- /overlay -->        
    </div>
    <div
          class="inline-block right-0 align-baseline"  
          x-data="{ 'isDialogOpen': false }"
          @keydown.escape="isDialogOpen = false"
    > 

      <a href="#" class="font-bold text-sm text-500 hover:text-blue-800" @click="isDialogOpen = true">Sign Up</a>

        <!-- overlay -->
      <div
          class="overflow-auto"
          style="background-color: rgba(0,0,0,0.5)"
          x-show="isDialogOpen"
          :class="{ 'absolute inset-0 z-10 flex items-center justify-center': isDialogOpen }"
      >
        <!-- dialog -->
        <div
             class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6 relative"
             x-show="isDialogOpen"
             @click.away="isDialogOpen = false"
        >
            
                
          <button class="absolute top-0 right-0 pr-3" type="button" @click="isDialogOpen = false">✖</button> 

          <form method="POST" action="{{route('rec_signup')}}">
            @csrf
            <div class="flex justify-between items-center pb-3">
              <p class="block text-lg text-gray-600">Enter your GSU Username</p>
            </div>

            <!-- content --> 
            <input type="hidden" name="type" value="1">
            @error('username')<span class="text-sm text-red">{{$message}}</span>@enderror
            <input class="w-full px-5 py-1 border-2 border-gray-600 text-gray-800 bg-gray-200 rounded" name="username">
            <!--Footer-->
            <div class="flex justify-end pt-2">
              <button class="modal-close px-4 mt-5 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit" @click="isDialogOpen = false">Submit</button>
            </div>
          </form>
        </div><!-- /dialog -->
      </div><!-- /overlay -->
    </div>

  </div>
</div>
@endsection