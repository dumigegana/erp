<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GSU</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
    <tr>
      <td style="padding: 10px 0 30px 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
          <tr>
            <td align="center" bgcolor="#3d68ff" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
              <img src="https://lh3.googleusercontent.com/W2nepKPihrsgPw3tJFrgmI8k1WW_x4fRnyjMiODtW4qk65qvihpx0NeMmX8rnY5X2eSM4UO7Q2EforcLBPW5g8CbG4niveJVDO3VmKrK1UV48wGIMOTMvPxWfL3R5kXg-h648-sd=w2400" alt="GSU Logo" width="230" height="230" style="display: block;" />
            </td>
          </tr>
          <tr>
            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                    <b>Gwanda State University</b>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                    <p>Your email address was used to apply to Gwanda State University. You are gettting this email to make sure it was you. If you recognise this action <a href="{{ route('LIVE::code', ['code' => $code]) }}">confirm </a> your action, else you may disregard this mail.</p>
                      	
                  </td>
                </tr>
                <tr>
                  <td style="padding: 0px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                    <p>Please do not reply to this message. Replies to this message are routed to an unmonitored mailbox. For any inquiries, please contact our Admissions office on this address admissions@gsu.ac.zw</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td bgcolor="#400040" style="padding: 30px 30px 30px 30px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
                &reg; ERP 2020<br/>
                <p style="color: #ffffff;">Excellence Through Esprit De Corps</p>
            </td>
            <td align="right" width="25%">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                    <a href="#" style="color: #ffffff;">
                      <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
                    </a>
                  </td>
                  <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                  <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                    <a href="#" style="color: #ffffff;">
                      <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                    </a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
            