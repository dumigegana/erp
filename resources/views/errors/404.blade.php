@extends('layouts.guest')
@section('title', '404 Error')
@section('content')
      <div class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
       @auth
       @if(auth()->user()->hasRole('Staff'))
        <p class="text-gray-800 font-medium text-center text-xl font-bold">404 - Not Found</p>
         <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded ml-24" type="submit"><a href="{{ url('/') }}">Take Me Back To Home</a></button>
      @elseif(auth()->user()->hasRole('Student'))
      <p class="text-gray-800 font-medium text-center text-xl font-bold">404 - Not Found</p>
         <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded ml-24" type="submit"><a href="{{ route('LIVE::home') }}">Take Me Back To Home</a></button>
         @endif
      @endauth 
      @guest     
        <p class="text-gray-800 font-medium text-center text-xl font-bold">404 - Not Found</p>
         <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded ml-24" type="submit"><a href="{{ route('welcome') }}">Take Me Back To Home</a></button>
      @endguest
      </div>
@endSection