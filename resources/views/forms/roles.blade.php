<div class="w-full px-4 flex flex-wrap">
  @foreach($roles as $role)
  <div class="w-1/3 flex-none mb-6">
    <label class="switch relative inline-block w-16 h-10 mr-4">
      @if(isset($user)) 
      <input type="checkbox" autocomplete="off" value="{{ $role->id }}" wire:model="data.{{ $role->name }}" @if($user->hasRole($role->name)) checked @endif >
      <span class="slider cursor-pointer inset-0 absolute round rounded-full"></span>
      <div class="ml-3 text-gray-700 font-medium">
       {{ $role->name }}
      </div>
      @endif
    </label>
  </div>
 @endforeach
</div>