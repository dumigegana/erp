@section('css')
  .dot {
    top: -.25rem;
    left: -.25rem;
    transition: all 0.3s ease-in-out;
    }

  .toggle__dot {
    transform: translateX(175%);
    background-color: #6495ED;
  }
.toggle__line {
  background-color: #AED6F1;
}
@endsection
    <?php $found = false; ?>
    <?php $counter = 0; ?>            
  <div class="w-full px-4 flex flex-wrap">
    @foreach($permissions as $perm)
        <?php $found = true ?>
    <div class="w-1/3 flex-none mb-6">

<!-- <div class="flex items-center justify-center w-full mb-24"> -->
  
  <!-- Toggle Button -->
  <label 
    for="toogle{{$perm->id}}"
    class="flex items-center tracking-wide"
  >
    <!-- toggle -->
    <div class="relative" 
      <?php
        if(isset($role)){
            if($role->hasPermission($perm->slug)) {
                echo "x-data=\"{ show: true }\" ";
            }
            else {
              echo "x-data=\"{ show: false }\" ";
            }
        }
      ?>
      >
      <!-- input -->
      <!-- <input type="hidden" value="0"  name="{{-- $perm->id --}}">      -->
      <input 
      <?php
            $disabled = false;
            if(isset($role)){
                if($role->hasPermission($perm->slug)) {
                    echo "checked ";
                }
                if($role->su) {
                    if($perm->su) {
                        $disabled = true;
                     }
                }
            }
            // if(!$disabled and (!$perm->assignable and !ERP::loggedInUser()->su)){
            //     $disabled = true;
            // }
            // if($disabled) {
            //     echo "disabled ";
            // }
        ?>
         id="toogle{{$perm->id}}" type="checkbox" class="hidden" value="1"  name="{{ $perm->id }}" />
      <!-- line -->
      <div
        class="w-10 h-2 bg-gray-200 rounded-full shadow-inner"
        @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__line': show }"
      ></div>
      <!-- dot -->
      <div
        class="dot absolute w-4 h-4 bg-white border-2 border-gray-300 rounded-full shadow inset-y-0 left-0"
         @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__dot': show }"
      ></div>
    </div>
    <!-- label -->
    <div
      class="ml-3 text-gray-700 font-medium"
    >
       {{ ERP::permissionName($perm->slug) }}
    </div>
  </label>
</div>
 @endforeach
 </div>
      @if(!$found)
          <div class="col-md-6 col-lg-4 down-spacer">
              <p>No permissions found</p>
          </div>
      @endif