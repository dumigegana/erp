<div class="w-full px-4 flex flex-wrap">
  @foreach($roles as $role)
  <div class="w-1/3 flex-none mb-6">
<!-- <div class="flex items-center justify-center w-full mb-24"> -->  
  <!-- Toggle Button -->
    <label for="toogle_{{$role->id}}" class="flex items-center tracking-wide">
    <!-- toggle -->
    <div class="relative" 
      <?php
        if(isset($user)){
            if($user->hasRole($role->name)) {
                echo "x-data=\"{ show: true }\" ";
            }
            else {
              echo "x-data=\"{ show: false }\" ";
            }
        }
      ?>
      >
      <p>ghhhhhj</p>
      <!-- input --> 
      <!-- <input type="hidden" value="0"  name="{{-- $role->id --}}">      -->
      @if(isset($user))      
      <input id="toogle_{{$role->id}}" type="checkbox" class="hidden" name="{{ $role->id }}" wire:model="data.{{ $role->id }}" @if($user->hasRole($role->name)) checked @endif>     
      @endif
    <div class="w-10 h-2 bg-gray-200 rounded-full shadow-inner"
    @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__line': show }">
    
    </div>
      <!-- dot -->
      <div
        class="dot absolute w-4 h-4 bg-white border-2 border-gray-300 rounded-full shadow inset-y-0 left-0"
         @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__dot': show }"
      ></div>
    </div>
    <!-- label -->
    <div
      class="ml-3 text-gray-700 font-medium"
    >
       {{ $role->name }}
    </div>
  </label>
</div>
 @endforeach
 </div>