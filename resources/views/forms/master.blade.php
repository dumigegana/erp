@section('css')
  .dot {
    top: -.25rem;
    left: -.25rem;
    transition: all 0.3s ease-in-out;
    }

  .toggle__dot {
    transform: translateX(175%);
    background-color: #6495ED;
  }
.toggle__line {
  background-color: #AED6F1;
}
@endsection
@if(!$fields)
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <div class="text-blue-200 text-sm font-bold">  
        {{ trans('erp.form_no_fields_title') }}
        </div>
        <p>{{ trans('erp.form_no_fields_subtitle') }}</p>
    </div>
@endif
 
@foreach($fields as $field)
  <?php
   $error = false;
   $warning = false;

   $code_script = false;
   $editor_script = false;
   $relation = false;

   # Setup the value
   $empty_value = false;
   if(isset($empty)) {
       foreach($empty as $emp) {
           if($field == $emp) {
               $empty_value = true;
           }
       }
   }
   if($empty_value or !isset($row)) {
       $value = "";
   } else {
       $value = $row->$field;

       # Check if the value needs to be decrypted
       $decrypt = false;
       foreach($encrypted as $encrypt) {
           if($field == $encrypt) {
               if($value != ''){
                   $decrypt = true;
               }
           }
       }
       if($decrypt) {
           try {
               $value = Crypt::decrypt($value);
           } catch (Exception $e) {
               $error = trans('erp.decrypt_fail');
           }
       }

       # Check if it's a hashed value, to display it empty
       $hashed_value = false;
       foreach($hashed as $hash) {
           if($field == $hash) {
               $hashed_value = true;
           }
       }
       if($hashed_value) {
           $value = "";
       }
   }

   $show_field = str_replace('_', ' ', ucfirst($field));
   $show_field_id = str_replace('id', ' ', ucfirst($show_field));


   $type = Schema::getColumnType($table, $field);


   # Set the input type
   if($type == 'string') {
       $input_type = "text";
   } elseif($type == 'integer') {
       $input_type = "number";
   } else {
       $input_type = "text";
   }

   # Check if it needs to be masked
   foreach($masked as $mask) {
       if($mask == $field) {
           $input_type = "password";
       }
   }

   # Check if it's a code
   foreach($code as $cd) {
       if($cd == $field) {
           $code_script = true;
       }
   }

   # Check for the editor values
   foreach($wysiwyg as $ed) {
       if($ed == $field) {
           $editor_script = true;
       }
   }

   # Check if it's a relation
   if(array_key_exists($field, $relations)) {
       $relation = true;
   }

   ?>

   @if($editor_script or $code_script)      
   <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
    @if($editor_script)
        <textarea  name="{{ $field }}" wire:model.lazy="data.{{ $field }}" value="{{ old($field)}}" class="ckeditor appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">{{ $value }}</textarea>
    @elseif($code_script)
        <pre class="code" value="{{ old($field) }}" id="{{ $field }}-code">{{ $value }}</pre>
        <textarea hidden name="{{ $field }}" wire:model.lazy="data.{{ $field }}" id="{{ $field }}" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">{{ $value }}</textarea><br>
        <script>
            var editor = ace.edit("{{ $field }}-code");
            editor.getSession().on('change', function(){
                $("#{{ $field }}").val(editor.getSession().getValue());
            });
        </script>
    @endif   

    @elseif($relation)
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field_id }}</label>
        <select  name="{{ $field }}" wire:model.lazy="data.{{ $field }}" id="{{ $field }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
          
            @foreach($relations[$field]['data'] as $relation_data)
                <?php $relation_value = $relations[$field]['value']; $relation_show = $relations[$field]['show']; ?>
                <option <?php if($value == $relation_data->$relation_value){ echo "selected"; } ?> value="{{ $relation_data->$relation_value }}">{{ $relation_data->$relation_show }}</option>
            @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
              <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
              </svg>
            </div>
        @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 
    @else

       @if($type == 'string')             
                   
                  
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
          <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" placeholder="{{ $show_field }}" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
            @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
                   </div>

              
           @elseif($type == 'integer')

               <!-- INTEGER COLUMN -->
               <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
                      <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" placeholder="{{ $show_field }}" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                    </div>
                    @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        
           @elseif($type == 'datetime')

               
          <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
            <input type="datetime-local" id="{{ $field }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" placeholder="{{ $show_field }}" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
              @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

           @elseif($type == 'date')

               
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
            <input type="date" id="{{ $field }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" placeholder="{{ $show_field }}" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
              @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

           @elseif($type == 'boolean')
                <!-- BOOLEAN COLUMN --> 

             <!-- Toggle Button -->
                <label 
                  for="toogle{{$field}}"
                  class="flex items-center tracking-wide"
                >
                  <!-- toggle -->
                  <div class="relative" x-data="{ show: false }">
                    <!-- input -->
                  <input type="hidden" value="0"  name="{{ $field }}" wire:model.lazy="data.{{ $field }}">     
                    <input id="toogle{{$field}}" type="checkbox" class="hidden" value="1"  name="{{ $field }}" wire:model.lazy="data.{{ $field }}" />
                    <!-- line -->
                    <div
                      class="w-10 h-2 bg-gray-200 rounded-full shadow-inner"
                      @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__line': show }"
                    ></div>
                    <!-- dot -->
                    <div
                      class="dot absolute w-4 h-4 bg-white border-2 border-gray-300 rounded-full shadow inset-y-0 left-0"
                       @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__dot': show }"
                    ></div>
                  </div>
                  <!-- label -->
                  <div
                    class="ml-3 text-gray-700 font-medium"
                  >
                     {{ $field }}
                  </div>
                </label>


                   @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

           @elseif($type == 'text')

        <!-- TEXT COLUMN -->
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
          <textarea placeholder="{{ $show_field }}" value="{{ old($field) }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" rows="3" id="{{ $field }}">{{ $value }}</textarea>
            @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 
           @else

         <!-- ALL OTHER COLUMN -->
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }}</label>
          <input type="{{ $input_type }}"  id="{{ $field }}" name="{{ $field }}" wire:model.lazy="data.{{ $field }}" placeholder="{{ $show_field }}" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                    @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div>

           @endif
       @foreach($confirmed as $confirm)
           @if($field == $confirm)
               @if($type == 'string')


                   <!-- STRING CONFIRMATION -->
                   <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
                 <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }} Confimation</label>
                      <input type="{{ $input_type }}"  id="{{ $field }}_confirmation" name="{{ $field }}_confirmation" placeholder="{{ $show_field }} confirmation" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                     @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

               @elseif($type == 'integer')

                   <!-- INTEGER COLUMN CONFIRMATION -->
                   <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">>
                        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }} {{ trans('erp.confirmation') }}</label>
                        <input type="{{ $input_type }}"  id="{{ $field }}_confirmation" name="{{ $field }}_confirmation" placeholder="{{ $show_field }} confirmation" value="{{ old($field) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                         @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 
                   </div>
                 </p><br>

               @elseif($type == 'boolean')

                   <!-- BOOLEAN CONFIRMATION -->
                       <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
                           <input <?php if(old($field)){ echo "checked='checked'"; } ?> type="checkbox" tabindex="0" class="hidden" name="{{ $field }}_confirmation" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                           <label class="w3-green">{{ $show_field }} {{ trans('erp.confirmation') }}</label>
                        @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

               @elseif($type == 'text') 

                   <!-- TEXT COLUMN -->
                   <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
                       <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }} {{ trans('erp.confirmation') }}</label>
                       <textarea placeholder="{{ $show_field }}" value="{{ old($field) }}" name="{{ $field }}_confirmation" rows="3" id="{{ $field }}_confirmation" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">{{ $value }}</textarea>
                        @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
        </div> 

               @else

               <!-- ALL OTHER COLUMN CONFIRMATION -->
               <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">{{ $show_field }} {{ trans('erp.confirmation') }}</label>
                <input style = "width:60%"  type="{{ $input_type }}"  id="{{ $field }}_confirmation" name="{{ $field }}_confirmation" placeholder="{{ $show_field }} confirmation" value="{{ old($field) }}"  class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
              @error('data.'.$field)       
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
              @enderror
          </div> 

                 @endif

             @endif
         @endforeach

     @endif

@endforeach
@section('js')
    @if(isset($cc_id) and isset($cc_value))
        <script>
            $("#{{ $cc_id }}_dropdown").dropdown("set selected", "{{ $cc_value }}");
            $("#{{ $cc_id }}_dropdown").dropdown("refresh");
        </script>
    @endif
@endsection
