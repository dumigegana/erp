@extends('layouts.guest')
@section('title', 'SignUp')
@section('content')
<form class="max-w-3xl flex-grow self-end mt-10 p-10 pb-5 bg-white rounded shadow-xl" method="POST"  enctype="multipart/form-data>
  @foreach ($errors->all() as $error)
    <p class="text-red-500 text-sm italic">{{ $error }}</p>
  @endforeach
  @csrf
  <input type="hidden" name="code" value="{{$id}}">
    <p class="text-gray-800 font-medium text-center text-lg font-bold">Register</p>
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label class="block text-sm text-gray-00" for="first_name">First Name</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('first_name') }}" name="first_name" type="text" placeholder="Name" aria-label="First Name">
      </div>
      <div class="w-full md:w-1/3 px-3">
          <label class="block text-sm text-gray-00" for="middle_names">Middle Names</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('middle_names') }}" name="middle_names" type="text" placeholder="Middle Names" aria-label="Middle Name">
      </div>
      <div class="w-full md:w-1/3 px-3">
          <label class="block text-sm text-gray-00" for="surname">Surname</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('surname') }}" name="surname" type="text" placeholder="Surname" aria-label="Surname">
          @error('data.surname')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label class="block text-sm text-gray-00" for="password">Password</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="password" type="password" aria-label="Password">
          @error('data.surname')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="w-full md:w-1/3 px-3">
          <label class="block text-sm text-gray-00" for="password_confirmation">Confirm Password</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="password_confirmation" type="password" aria-label="Confirm Password">
      </div>
      <div class="w-full md:w-1/3 px-3">
          <label class="block text-sm text-gray-00" for="cell_number">Cell Number</label>
          <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('cell_number') }}" name="cell_number" type="text" placeholder="+263798765432" aria-label="Cell Number">
          @error('data.surname')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label class="block text-sm text-gray-00"
                 for="grid-tittle">
              Tittle
          </label>
          <div class="relative">
              <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="title">
                <option @if(old('title') == 'Mr') selected @endif value="Mr">Mr</option>
                <option @if(old('title') == 'Mrs') selected @endif value="Mrs">Mrs</option>
                <option @if(old('title') == 'MISS') selected @endif value="MISS">MISS</option>
                <option @if(old('title') == 'MS') selected @endif value="MS">MS</option>
                <option @if(old('title') == 'DR') selected @endif value="DR">DR</option>
                <option @if(old('title') == 'PS') selected @endif value="PS">PS</option>
                <option @if(old('title') == 'REV') selected @endif value="REV">REV</option>
                <option @if(old('title') == 'SR') selected @endif value="SR">SR</option>
                <option @if(old('title') == 'Prof') selected @endif value="Prof">Prof</option>
              </select>
              <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                </svg>
              </div>
          </div>
      </div>
      <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label class="block text-sm text-gray-00" for="grid-sex">
              Sex
          </label>
          <div class="relative">
              <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="sex">
                <option @if(old('sex') == 'Male') selected @endif value="Male">Male</option>
                <option @if(old('sex') == 'Female') selected @endif value="Female">Female</option>
              </select>
              <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                </svg>
              </div>
          </div>
      </div>
      <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
        <label class="block text-sm text-gray-00"
               for="grid-state">
            Department
        </label>
        <div class="relative">
          <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="department_id" >
               @foreach ($departments as $dep)
               @php $id=$dep->id@endphp
            <option @if(old('department_id') == $id) selected @endif value="{{$dep->id}}">{{$dep->name}}</option>
            @endforeach
              </select>
              <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                </svg>
              </div>
          </div>
      </div>
    </div>
   <div class="flex flex-wrap -mx-3 mb-6">
    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
        <label class="block text-sm text-gray-00" for="gsu_number">Employment Number</label>
        <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('gsu_number') }}" name="gsu_number" type="text"  aria-label="Employment Number">
    </div>
    <div class="w-full md:w-1/3 px-3">
        <label class="block text-sm text-gray-00" for="position">Designation</label>
        <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('position') }}" name="position" type="text"  aria-label="Designation">
    </div>

    <div class="w-full md:w-1/3 px-3">
        <label class="block text-sm text-gray-00" for="profile_picture">Profile Picture <i>(Optional)</i></label>
        <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="profile_picture" type="file" placeholder="Country" aria-label="Profile Picture ">
    </div>
  </div>
  <div class="flex justify-between pl-5">
    <div class="mt-4 inline-block right-0 align-baseline">
        <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Register</button>
    </div>
    <a class="inline-block place-self-end  align-baseline font-bold text-sm text-500 hover:text-blue-800" href="{{url('/')}}">
          Already have an account ?
      </a>
   </div>   
</form>
@endsection
