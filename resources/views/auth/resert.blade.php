@extends('layouts.guest')
@section('title', 'Login')
@section('content')
      <form class="max-w-xl mt-20 p-10 bg-white rounded shadow-xl" method="POST" action="{{ route('reset',['id'=>$id]) }}">        
        @csrf
        <input type="hidden" name="code" value="{{$id}}">
        <p class="text-gray-800 font-medium text-center text-lg font-bold">Login</p>
        <div class="">
          <label class="block text-sm text-gray-00" for="password">Password</label>
          @error('password')<span class="text-sm text-red">{{$message}}</span>@enderror
          <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="password" name="password" type="password" required="" placeholder="User Name" aria-label="password">
        </div>
        <div class="mt-2">
          <label class="block text-sm text-gray-600" for="password_confirmation">Confirm Password</label>
          @error('password_confirmation')<span class="text-sm text-red">{{$message}}</span>@enderror
          <input class="w-full px-5  py-1 text-gray-700 bg-gray-200 rounded" id="password_confirmation" name="password_confirmation" type="password" required="" placeholder="*******" aria-label="password_confirmation">
        </div>
        <center>
          <button class="px-4 mt-5 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Reset</button>
        </center>
      </form>
@endsection