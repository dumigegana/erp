<!doctype html>
<html lang="en">

<head>
  <title>GSU ERP| @yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Gwanda,University,State,Tertiary,Gwanda State University,Zimbabwe University,Gwanda University">
    <!-- Css -->
   <link href="{{ URL::to( 'css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="shortcut icon" href="{{URL::asset('/img/icon.png')}}">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <style>
  .login{
    background: url({{URL::asset('/img/bg.jpeg')}})
  }
   @yield('css')
  </style>  
</head>
<body class="h-screen font-sans login bg-cover">
<div class="container mx-auto flex flex-1 justify-center items-center">
  <div class="w-full max-w-full">
        <div class="leading-loose flex justify-center ">
     @yield('content')
    </div>
  </div>
</div>
@include('sweetalert::alert')
<script src="{{ URL::to( 'js/alpine.js') }}" defer></script>
</body>

</html>


