<!DOCTYPE html>
<html>
<head>
	<title>GSU Enrolment</title>	
	<link href="{{ URL::to( 'css/app.css') }}" rel="stylesheet">
	<!-- <link href="{{-- URL::to( 'css/dropzone.min.css') --}}" rel="stylesheet"> -->
	<!-- <link href='https://fonts.googleapis.com/css?family=Bungee sShade' rel='stylesheet'> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
  <meta name="keywords" content="Enrolment, Gwanda,University,State,Tertiary,Gwanda State University,Zimbabwe University,Gwanda University">
  
  <link rel="shortcut icon" href="{{URL::asset('/img/icon.png')}}">
 <link href="{{ URL::to( 'css/sweetalert2.css') }}" rel="stylesheet">
	<style type="text/css">
		/*basic reset*/
* {margin: 0; padding: 0;}

html {
	height: 100%;
	/*Image only BG fallback*/
	background: rgba(54,137,231,0.88);
	background: -moz-linear-gradient(left, rgba(54,137,231,0.88) 0%, rgba(121,160,241,0.88) 25%, rgba(220,194,255,0.94) 62%, rgba(255,255,255,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(54,137,231,0.88)), color-stop(25%, rgba(121,160,241,0.88)), color-stop(62%, rgba(220,194,255,0.94)), color-stop(100%, rgba(255,255,255,1)));
	background: -webkit-linear-gradient(left, rgba(54,137,231,0.88) 0%, rgba(121,160,241,0.88) 25%, rgba(220,194,255,0.94) 62%, rgba(255,255,255,1) 100%);
	background: -o-linear-gradient(left, rgba(54,137,231,0.88) 0%, rgba(121,160,241,0.88) 25%, rgba(220,194,255,0.94) 62%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(left, rgba(54,137,231,0.88) 0%, rgba(121,160,241,0.88) 25%, rgba(220,194,255,0.94) 62%, rgba(255,255,255,1) 100%);
	background: linear-gradient(to right, rgba(54,137,231,0.88) 0%, rgba(121,160,241,0.88) 25%, rgba(220,194,255,0.94) 62%, rgba(255,255,255,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3689e7', endColorstr='#ffffff', GradientType=1 );
}	
	h1 {
		color: #000057;
		position: relative top 10px;
		text-align: center;
		font-size: 30px;
		padding: 0px;
		font-family: 'Bungee Shade';
	}
	
	h2 {
		color: #000057;
		margin: auto;
		text-align: center;
		font-size: 30px;
		padding: 0px;
		font-family: 'Chivo';
	}

	.wrapper {
	  width: 100%;
	  font-family: "Helvetica";
	  font-size: 14px;
	  /*border: 1px solid #ccc;*/
	}

	.StepProgress {
	  position: relative;
	  padding-left: 65px;
	  list-style: none;
	  color: #fff;
	}

	.StepProgress::before {
	  display: inline-block;
	  content: "";
	  position: absolute;
	  top: 0;
	  left: 35px;
	  width: 10px;
	  height: 100%;
	  border-left: 5px solid #ccc;
	}

	.StepProgress-item {
	  position: relative;
	  counter-increment: list;
	}

	.StepProgress-item:not(:last-child) {
	  padding-bottom: 50px;
	}

	.StepProgress-item::before {
	  display: inline-block;
	  content: "";
	  position: absolute;
	  left: -30px;
	  height: 100%;
	  width: 10px;
	}

	.StepProgress-item::after {
	  content: "";
	  display: inline-block;
	  position: absolute;
	  top: 0;
	  left: -37px;
	  width: 22px;
	  height: 22px;
	  border: 5px solid #ccc;
	  border-radius: 50%;
	  background-color: #fff;
	}

	.StepProgress-item.is-done::before {
	  border-left: 5px solid #00cc00;
	}

	.StepProgress-item.is-done::after {
	  content: "✔";
	  font-size: 15px;
	  top: -4px;
	  left: -40px;  
	  width: 26px;
	  height: 26px;
	  color: #fff;
	  text-align: center;
	  border: 5px solid #00cc00;
	  background-color: #00cc00;
	}

	.StepProgress-item.current::before {
	  border-left: 5px solid #cccc00;
	}

	.StepProgress-item.current::after {
	  content: counter(list);
	  padding-top: 1px;
	  width: 26px;
	  height: 26px;
	  top: -4px;
	  left: -40px;
	  font-size: 12px;
	  text-align: center;
	  color: #00cc00;
	  border: 3px solid #00cc00;
	  background-color: white;
	}

	.StepProgress strong {
	  display: block;
	}

	img.resize {
      max-width:50%;
      max-height:50%;
		   margin: 20px 40px 40px 40px;
    }
     .error {
     	color: red;
     	font-size: 12px;
     }
     /* Taiwind tool tip style*/
     .tooltip .tooltip-text {
			  visibility: hidden;
			  text-align: center;
			  padding: 2px 6px;
			  position: absolute;
			  z-index: 100;
			}
			.tooltip:hover .tooltip-text {
			  visibility: visible;
			}
	[x-cloak] {
			display: none !important;
		}
			@yield('css')
		</style>
	    
	     @laravelPWA   
	</head>
	<body>
  <script src="{{ URL::to('/js/sweetalert2@9.js') }}"></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
 <script>
   const SwalModal = (icon, title, html) => {
      Swal.fire({
        icon,
        title,
        html
      })
    }

    const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
      Swal.fire({
        icon,
        title,
        html,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText,
        reverseButtons: true,
    }).then(result => {
        if (result.value) {
            return livewire.emit(method, params)
        }

        if (callback) {
            return livewire.emit(callback)
        }
    })
    }

    const SwalAlert = (icon, title, timeout = 7000) => {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: timeout,
        onOpen: toast => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
          icon,
          title
        })
    }

    document.addEventListener('DOMContentLoaded', () => { 
        this.livewire.on('swal:modal', data => {
            SwalModal(data.icon, data.title, data.text)
        })

        this.livewire.on('swal:confirm', data => {
            SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data.params, data.callback)
        })

        this.livewire.on('swal:alert', data => {
            SwalAlert(data.icon, data.title, data.timeout)
        }) 
    })
    </script>
		
			 @yield('content')   
		@livewireScripts
		 <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->
	  <script src="{{ URL::to( 'js/alpine.js') }}" defer></script>
	  <!-- <script src="{{-- URL::to( 'js/dropzone.min.js') --}}" defer></script> -->
	</body>
</html>