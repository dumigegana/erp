<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="keywords" content="Gwanda,University,State,Tertiary,Gwanda State University,Zimbabwe University,Gwanda University">
  <!-- Css -->
 <link href="{{ URL::to( 'css/app.css') }}" rel="stylesheet">
 <link rel="shortcut icon" href="{{URL::asset('/img/icon.png')}}">
  <style>
    @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');
    .font-family-karla { font-family: karla; }
    .bg-sidebar { background: #3d68ff; }
    .cta-btn { color: #3d68ff; }
    .upgrade-btn { background: #1947ee; }
    .upgrade-btn:hover { background: #0038fd; }
    .active-nav-link { background: #1947ee; }
    .nav-item:hover { background: #1947ee; }
    .account-link:hover { background: #3d68ff; }
    [x-cloak] {
      display: none !important;
    }
    @yield('css')
    
  </style>
   @yield('head')
  <link href="{{ URL::to( 'css/sweetalert2.css') }}" rel="stylesheet">
  <title>GSU</title>
  {{--@laravelPWAs --}}
</head>

<body class="bg-gray-100 font-family-karla flex">
  <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
  <script src="{{ URL::to('/js/sweetalert2@9.js') }}"></script>
 <script>
   const SwalModal = (icon, title, html) => {
      Swal.fire({
        icon,
        title,
        html
      })
    }

    const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
      Swal.fire({
        icon,
        title,
        html,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText,
        reverseButtons: true,
    }).then(result => {
        if (result.value) {
            return livewire.emit(method, params)
        }

        if (callback) {
            return livewire.emit(callback)
        }
    })
    }

    const SwalAlert = (icon, title, timeout = 7000) => {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: timeout,
        onOpen: toast => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
          icon,
          title
        })
    }

    document.addEventListener('DOMContentLoaded', () => { 
        this.livewire.on('swal:modal', data => {
            SwalModal(data.icon, data.title, data.text)
        })

        this.livewire.on('swal:confirm', data => {
            SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data.params, data.callback)
        })

        this.livewire.on('swal:alert', data => {
            SwalAlert(data.icon, data.title, data.timeout)
        }) 
    })
    </script>
  <aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
  	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
    <div class="p-6">
      <a href="{{-- route('ERP::dashboard') --}}" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">
        <img class="h-20 w-17" src="{{URL::asset('/img/logo.png')}}" alt="Logo">
      </a>
    </div>
    <nav class="text-white text-base font-semibold pt-3">
      <!-- Dropdown Nav -->
      <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">
        <div x-data="{show: false}" @click.away="show = false">
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-lg">Registration</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-blue-300">
             @if(!session()->has('sage'))
            <a href="{{ route('LIVE::register', ['state' => 1]) }}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Registration</a>
            @endif
            <a href="{{ route('LIVE::check') }}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Check Sheet</a>
            <a href="{{ route('LIVE::results') }}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Results</a>
          </div>
          <div x-data="{show: false}" @click.away="show = false">
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-lg">Examination</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-blue-300">
            <a href="{{-- route('ERP::students') --}}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Exam time table</a>
            <a href="{{-- route('ERP::applicants') --}}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Arrangement</a>
          </div>
        </div>
         
        <div x-data="{show: false}" @click.away="show = false">
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-lg">Accommodation</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-blue-300">
            <a href="{{-- route('ERP::students') --}}" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Book Room</a>
        </div>
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Useful Links</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="http://gsu.ac.zw/" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">GSU Website</a>
            <a href="https://mail.google.com/a/students.gsu.ac.zw" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">Email</a>
            <a href="http://classroom.google.com/" class="block pl-10 pr-2 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">E-Learning</a>
          </div>
        </div>    
        
    </nav>      
  </aside>

  <div class="relative w-full flex flex-col h-screen overflow-y-hidden">
    <!-- Desktop Header -->
    <header class="w-full flex items-center bg-white py-2 px-6 hidden sm:flex">
      <div class="w-1/2">
             @yield('breadcrumb')
      </div>
         @auth
      <div class="relative w-1/2 flex justify-end"> 
          <button class="realtive z-10 h-8 border-2 border-blue-300 rounded-md hover:bg-blue-300 hover:text-white focus:border-gray-300 focus:outline-none text-yellow-600"><a href="{{route('LIVE::paynow') }}">Pay Online</a></button>
          
        <div x-data="{ isOpen: false }" class="pl-8 mr-20">
          <button @click="isOpen = !isOpen" class="realtive z-10 h-8 w-8 rounded-full overflow-hidden border-2 border-gray-300 hover:border-gray-400 focus:border-gray-300 focus:outline-none"> {{-- Auth::user()->username --}}
              <img src="{{URL::asset('/img/avatar.png')}}" alt="Profile Picture">
          </button>
          <button x-show="isOpen" @click="isOpen = false" class="h-full w-full fixed inset-0 cursor-default"></button>
          <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-4">
            <a href="{{ route('LIVE::home') }}" class="block pl-10 pr-2 py-2 account-link hover:text-white">Account</a>
            <a href="#" class="block pl-10 pr-2 py-2 account-link hover:text-white">Support</a>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="block pl-10 pr-2 py-2 account-link hover:text-white">Log out</a>
          </div>
        </div>
      </div>
      @endauth
    </header>

    <!-- Mobile Header & Nav -->
    <header x-data="{ isOpen: false }" class="w-full bg-sidebar py-5 px-6 sm:hidden">
      <div class="flex items-center justify-between">
        <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
        <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
          <i x-show="!isOpen"><x-zondicon-menu class="h-6 w-6"/></i>
          <i x-show="isOpen"><x-zondicon-close class="h-6 w-6"/></i>
        </button>
      </div>

      <!-- Dropdown Nav -->
      <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">   
        <a href="index.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-tachometer-alt mr-3"></i>
            Dashboard
        </a>
        <a href="blank.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-sticky-note mr-3"></i>
            Blank Page
        </a>
        <a href="tables.html" class="flex items-center active-nav-link text-white py-2 pl-4 nav-item">
            <i class="fas fa-table mr-3"></i>
            Tables
        </a>
        <a href="forms.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-align-left mr-3"></i>
            Forms
        </a>
        <a href="tabs.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-tablet-alt mr-3"></i>
            Tabbed Content
        </a>
        <a href="calendar.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-calendar mr-3"></i>
            Calendar
        </a>
        <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-cogs mr-3"></i>
            Support
        </a>
        <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-user mr-3"></i>
            My Account
        </a>
        <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"  class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-sign-out-alt mr-3"></i>
            Log Out
        </a>
        
      </nav>
      <!-- <button class="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-br-lg rounded-bl-lg rounded-tr-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center">
          <i class="fas fa-plus mr-3"></i> New Report
      </button> -->
    </header>

    <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
        <main class="w-full flex-grow p-6">
        @yield('content')                
        </main>

        <footer class="w-full bg-white text-right p-4">
            <a target="_blank" href="https://www.gsu.ac.zw.com" class="underline"></a>
        </footer>
    </div>
    
  </div>

 {{-- @include('sweetalert::alert') --}}
@livewireScripts
<!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js" defer></script> -->
<script src="{{ URL::to( 'js/alpine.js') }}" defer></script>
@yield('js')

</body>
</html>