<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="keywords" content="Gwanda,University,State,Tertiary,Gwanda State University,Zimbabwe University,Gwanda University">
  <!-- Css -->
 <link href="{{ URL::to( 'css/app.css') }}" rel="stylesheet"> 
  <link rel="shortcut icon" href="{{URL::asset('/img/icon.png')}}">
 <link href="{{ URL::to( 'css/sweetalert2.css') }}" rel="stylesheet">
 @yield('links')
  <style>
    @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');
    .font-family-karla { font-family: karla; }
    .bg-sidebar { background: #1A2E56; }
    .bg-logo { background: #C8C9F2; }
    .bg-main { background: #eaeafa; }
    .bg-drop { background: #75B9F0; }
    .text-drop { color: #1A2E56; }
    .cta-btn { color: #3d68ff; }
    .upgrade-btn { background: #21517E; }
    .upgrade-btn:hover { background: #0038fd; }
    .bg-nav-link { background: #21517E; }
    .nav-item:hover { background: #21517E; }
    .account-link:hover { background: #3d68ff; }
    [x-cloak] {
      display: none !important;
    }
    @yield('css')
    
  </style>
   @yield('head')
  <title>GSU</title>
  @laravelPWAs 
</head>

<body class="bg-gray-100 font-family-karla flex">
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
  <script src="{{ URL::to('/js/sweetalert2@9') }}"></script>
 <script>
   const SwalModal = (icon, title, html) => {
      Swal.fire({
        icon,
        title,
        html
      })
    }

    const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
      Swal.fire({
        icon,
        title,
        html,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText,
        reverseButtons: true,
    }).then(result => {
        if (result.value) {
            return livewire.emit(method, params)
        }

        if (callback) {
            return livewire.emit(callback)
        }
    })
    }

    const SwalAlert = (icon, title, timeout = 7000) => {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: timeout,
        onOpen: toast => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
          icon,
          title
        })
    }

    document.addEventListener('DOMContentLoaded', () => { 
        this.livewire.on('swal:modal', data => {
            SwalModal(data.icon, data.title, data.text)
        })

        this.livewire.on('swal:confirm', data => {
            SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data.params, data.callback)
        })

        this.livewire.on('swal:alert', data => {
            SwalAlert(data.icon, data.title, data.timeout)
        }) 
    })
    </script>
  <aside class="relative bg-sidebar h-screen w-56 hidden sm:block shadow-xl">
  	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
    <div class="pl-10 py-3 bg-logo">
      <a href="{{-- route('ERP::dashboard') --}}" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">
        <img class="h-28 w-24" src="{{URL::asset('/img/logo.png')}}" alt="Logo">
      </a>
    </div>
    <nav class="text-white text-base font-semibold pt-3">
      <!-- Dropdown Nav -->
      <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Manage Users</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="{{ route('ERP::users') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">User Accounts</a>
            <a href="{{ route('ERP::roles') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Roles</a>
            <a href="{{ route('ERP::permissions') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Permissions</a>
          </div>
        </div>
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Registration</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="{{ route('ERP::students') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Students List</a>
            <a href="{{ route('ERP::applicants') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Applications</a>
            <a href="{{ route('ERP::reg') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Course Registration</a>
            <a href="{{ route('ERP::s_class') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Grades</a>
          </div>
        </div>
        
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Academics</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="{{ route('ERP::faculties') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Faculties</a>
            <a href="{{ route('ERP::departments') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Departments</a>
            <a href="{{ route('ERP::programmes') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Programs</a>
            <a href="{{ route('ERP::courses') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Courses</a>
            <a href="{{ route('ERP::course_parts') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Program Courses</a>
            <a href="{{ route('ERP::s_class') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Classes</a>
            <a href="#" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Time Table</a>
          </div>
        </div>
        
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Admissions</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="{{ route('ERP::applicants') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Applications</a>
            <a href="{{ route('ERP::subjects') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Entry Subjects</a>
            <a href="{{ route('ERP::cohorts') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Cohorts</a>
            <a href="{{ route('ERP::disabilities') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Disability</a>
            <a href="{{ route('ERP::entry_types') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Entry types</a>
          </div>
        </div>
        
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Records</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>
            <a href="{{ route('ERP::semesters') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Semesters</a>
            <a href="{{ route('ERP::sem_courses') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Semester Courses</a>
          </div>
        </div> 
        <div x-data="{show: false}" @click.away="show = false" x-cloak>
          <button @click="show = ! show" class="block px-1 focus-within:text-gray-600 py-2 w-full items-center text-white hover:opacity-100 nav-item">
            <div class="flex">
              <span class="pl-4 text-base">Human Resource</span>
            </div>
          </button>
          <div x-show="show" class="mt-2 w-full text-lx bg-drop" x-cloak>            
            <a href="{{ route('ERP::deans') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Deans</a>
            <a href="{{ route('ERP::hods') }}" class="block pl-10 pr-2 py-1 text-drop hover:bg-blue-400 hover:text-white">Department Heads</a>
          </div>
        </div>
        
        
        
    </nav>
      <a href="{{ route('ERP::CRUD') }}" class="absolute w-full upgrade-btn bottom-0 active-nav-link text-white flex items-center justify-center opacity-75 py-4">
          Settings
      </a>
  </aside>

  <div class="relative w-full flex flex-col h-screen overflow-y-hidden">
    <!-- Desktop Header -->
    <header class="w-full flex items-center bg-white py-1 px-6 hidden sm:flex">
      <div class="w-1/2">
             @yield('breadcrumb')
      </div>
         @auth
      <div x-data="{ isOpen: false }" class="w-1/2 flex justify-end" x-cloak>
        <button @click="isOpen = !isOpen" class="realtive z-10 h-8 w-8 rounded-full overflow-hidden border-2 border-gray-300 hover:border-gray-400 focus:border-gray-300 focus:outline-none"> {{-- Auth::user()->username --}}
            <img src="{{URL::asset('/img/avatar.png')}}" alt="Profile Picture">
        </button>
        <button x-show="isOpen" @click="isOpen = false" class="h-full w-full fixed inset-0 cursor-default"></button>
        <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-8 z-10 " x-cloak>
          <a href="{{-- route('ERP::profile') --}}" class="block pl-10 pr-2 py-1 account-link hover:text-white">Account</a>
          <a href="#" class="block pl-10 pr-2 py-1 account-link hover:text-white">Support</a>
          <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="block pl-10 pr-2 py-1 account-link hover:text-white">Log out</a>
        </div>
      </div>
      @endauth
    </header>

    <!-- Mobile Header & Nav -->
    <header x-data="{ isOpen: false }" class="w-full bg-sidebar py-5 px-6 sm:hidden">
      <div class="flex items-center justify-between">
        <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
        <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
          <i x-show="!isOpen"><x-zondicon-menu class="h-6 w-6"/></i>
          <i x-show="isOpen"><x-zondicon-close class="h-6 w-6"/></i>
        </button>
      </div>

      <!-- Dropdown Nav -->
      <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">   
        <a href="index.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-tachometer-alt mr-3"></i>
            Dashboard
        </a>
        <a href="blank.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-sticky-note mr-3"></i>
            Blank Page
        </a>
        <a href="tables.html" class="flex items-center active-nav-link text-white py-2 pl-4 nav-item">
            <i class="fas fa-table mr-3"></i>
            Tables
        </a>
        <a href="forms.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-align-left mr-3"></i>
            Forms
        </a>
        <a href="tabs.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-tablet-alt mr-3"></i>
            Tabbed Content
        </a>
        <a href="calendar.html" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-calendar mr-3"></i>
            Calendar
        </a>
        <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-cogs mr-3"></i>
            Support
        </a>
        <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-user mr-3"></i>
            My Account
        </a>
        <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"  class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
            <i class="fas fa-sign-out-alt mr-3"></i>
            Log Out
        </a>
        
      </nav>
      <!-- <button class="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-br-lg rounded-bl-lg rounded-tr-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center">
          <i class="fas fa-plus mr-3"></i> New Report
      </button> -->
    </header>

    <div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
        <main class="w-full flex-grow px-6 py-1 bg-main">
        @yield('content')                
        </main>

        <footer class="w-full bg-white text-right p-4">
            <a target="_blank" href="https://www.gsu.ac.zw" class="underline"></a>
        </footer>
    </div>
    
  </div>

 @include('sweetalert::alert')
<!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.0/dist/alpine.min.js" defer></script> -->
<script src="{{ URL::to( 'js/alpine.js') }}" defer></script>
@yield('js')

</body>
</html>