<div class="flex">
	<aside class="relative h-screen w-64 hidden sm:block" >
		<img class="resize" src="{{URL::asset('/img/logo.png')}}" alt="GSU Logo">  
		<div class="wrapper" id="prog">
			<ul class="StepProgress">
				<?php $sp1 = ''; $sp2 = ''; $sp3 = ''; $sp4 = ''; $sp5 = ''; $sp6 = ''; $sp7 = ''; ?>
				@isset($step)
					@if($step == 1)       
							 @php $sp1 = "current"; @endphp
						@elseif($step == 2)
						@php $sp1 = "is-done"; 
								$sp2 = "current"; @endphp
						@elseif($step == 3)
							 @php $sp1 = "is-done";
								$sp2 = "is-done";
								$sp3 = "current"; @endphp
						@elseif($step == 4)
						 @php   $sp1 = "is-done";
								$sp2 = "is-done"; 
								$sp3 = "is-done";
								$sp4 = "current"; @endphp
						@elseif($step == 5)
							@php  $sp1 = "is-done";
								$sp2 = "is-done";
								$sp3 = "is-done";
								$sp4 = "is-done";
								$sp5 = "current"; @endphp 
						@elseif($step == 6)
							@php  $sp1 = "is-done";
								$sp2 = "is-done";
								$sp3 = "is-done";
								$sp4 = "is-done";
								$sp5 = "is-done";
								$sp6 = "current"; @endphp   
						@elseif($step == 7)
							@php  $sp1 = "is-done";
								$sp2 = "is-done";
								$sp3 = "is-done";
								$sp4 = "is-done";
								$sp5 = "is-done";
								$sp6 = "is-done";
						@endif               
				@endisset
				<li class="StepProgress-item {{ $sp1 }}"><strong>-Create Account</strong></li>
				<li class="StepProgress-item {{ $sp2 }}"><strong>-Personal Details</strong></li>
				<li class="StepProgress-item {{ $sp3 }}"><strong>-Application Details</strong></li>
				<li class="StepProgress-item {{ $sp4 }}"><strong>-O/A Level Details</strong></li>
				@if($data['entry_type_id'] > 1)
				<li class="StepProgress-item {{ $sp5 }}"><strong>-Professional</strong></li>
				<li class="StepProgress-item {{ $sp6 }}"><strong>-Employment History</strong></li>
				@endif
			</ul>
		</div>
	</aside>
	<div class="relative w-full flex flex-col h-screen overflow-y-hidden">
		<div class="w-full h-screen overflow-x-hidden border-t flex flex-col">
			<main class="w-full flex-grow p-3">
				<div class="sm:w-full rounded overflow-hidden">   
					<h1>Gwanda State University</h1>  
					<div class="w-4/5 bg-white my-10 mx-20 p-4" >   
					 @if($step == 1)
					 @include('livewire.enrol.step1')
						@elseif($step == 2)
					 @include('Livewire.enrol.step2')
						@elseif($step == 3)
					 @include('Livewire.enrol.step3')
						@elseif($step == 4)
					 @include('Livewire.enrol.step4')
						@elseif($step == 5)
					 @include('Livewire.enrol.step5')
						@elseif($step == 6)
					 @include('Livewire.enrol.step6')
						@elseif($step == 7)
					 @include('Livewire.enrol.step7')
						@elseif($step == 0)

						@section('css')
						@endsection

						@if (session()->has('message'))
						<div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
							<div class="flex">
								<div class="py-1"><svg class="fill-current h-7 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
								<div>
									<p class="text-sm"> {{ session('message') }}</p>
								</div>
							</div>
						</div>
							@endif
						<h2 class="text-xl font-bold text-blue-800 my-5">Welcome Statement </h2>
						<button wire:click="increment" class="w-full mt-4 p-4 bg-white text-pink-500 font-bold border-4 rounded-lg border-pink-500 cursor-pointer transition-all duration-700 hover:bg-pink-500 hover:text-white hover:border-transparent">Start Here to Apply</button><br>
            <div x-data="{ 'isDialogOpen': false }" @keydown.escape="isDialogOpen = false"> 

			    		<button class="w-full mt-4 p-4 bg-white text-blue-500 font-bold border-4 rounded-lg border-blue-600 cursor-pointer transition-all duration-700 hover:bg-blue-600 hover:text-white hover:border-transparent" @click="isDialogOpen = true">Continue with my Application</button><br>

		            <!-- overlay -->
		          <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="isDialogOpen" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': isDialogOpen }" x-cloak>
		            <!-- dialog -->
		            <div
                 class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6 relative"
                 x-show="isDialogOpen"
                 @click.away="isDialogOpen = false"
		            >
		                
		                    
		              <button class="absolute top-0 right-0 pr-3" type="button" @click="isDialogOpen = false">✖</button> 

		              <form wire:submit.prevent="other">
		                <div class=" items-center pb-3">
											<p class="text-2xl font-bold text-blue-400 mb-5">Enter your email address</p>
											

											<!-- content -->
											<input class="appearance-none block w-full text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white border-blue-300" wire:model.lazy="data.email">
											<!--Footer-->
											<div class="flex justify-end pt-2">
												<button class="modal-close px-4 bg-blue-400 p-3 rounded-lg text-white hover:bg-indigo-400" type="submit" @click="isDialogOpen = false">Submit</button>
											</div>
		                </div>
		              </form>
		            </div><!-- /dialog -->
		          </div><!-- /overlay -->
		        </div>
						<div x-data="{ 'isModalOpen': false }" @keydown.escape="isModalOpen = false"> 

					    <button class="w-full mt-4 p-4 bg-white text-purple-500 font-bold border-4 rounded-lg border-purple-500 cursor-pointer transition-all duration-700 hover:bg-purple-500 hover:text-white hover:border-transparent" @click="isModalOpen = true">Track My Application</button><br>

			            <!-- overlay -->
		          <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="isModalOpen" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': isModalOpen }" x-cloak>
		            <!-- dialog -->
		            <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6 relative" x-show="isModalOpen" @click.away="isModalOpen = false">

		              <button class="absolute top-0 right-0 pr-3" type="button" @click="isModalOpen = false">✖</button> 

		              <form wire:submit.prevent="other">
		                <div class="items-center pb-3">
											<p class="text-2xl font-bold text-blue-400 mb-5">Enter your email address</p>
											<!-- content -->
											<input class="appearance-none block w-full text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white border-blue-300" wire:model.lazy="data.email">
											<!--Footer-->
											<div class="flex justify-end pt-2">
												<button class="modal-close px-4 bg-blue-400 p-3 rounded-lg text-white hover:bg-indigo-400" type="submit" @click="isModalOpen = false">Submit</button>
											</div>
		                </div>
		              </form>
		            </div><!-- /dialog -->
		          </div><!-- /overlay -->
		        </div>

					</div>
					@endif
				</div>
			</main> 
		</div>
		</div>
	</div>

