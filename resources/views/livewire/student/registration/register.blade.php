<div class="flex flex-col">
  <!-- Card Section Starts Here -->
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">

    <!--Profile Picture-->
    <div class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/5 lg:w-1/5">
    	@if (empty(Auth::user()->profile_picture))
	<img src="{{URL::asset('/img/avatar.png')}}"style="float:left;width:200px;height:200px;" >
	@else
	<img src="{{URL::asset('profile/'. auth()->user()->profile_picture)}}" alt="{{Auth::user()->fullname }}" style="float:left;width:200px;height:200px;"> 
	@endif
    </div>
    <!--Horizontal form-->
    <div class="mb-2 bg-white border-solid border-grey-light rounded border shadow-sm w-full md:w-2/5 lg:w-2/5">	    
	    <div class="p-3">
        <table class="table-fixed">          
          <tbody>
            <tr> 
              <td class="font-bold text-blue-500">Name</td>
              <td class="text-yellow-600">: {{Auth::user()->fullname}}</td>
            </tr>
            @if(ERP::isStudent(auth()->user()))
            <tr>
              <td class="font-bold text-blue-500">ID</td>
              <td class="uppercase text-yellow-600">: {{Auth::user()->student->gsu_id}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Program</td>
              <td class="text-yellow-600">: {{Auth::user()->student->programme->code}}</td>
            </tr>
            <tr>
              <td class="font-bold text-blue-500">Program</td>
              <td class="text-yellow-600">: {{Auth::user()->student->programme->name}}</td>
            </tr>
            @endif
          </tbody> 
        </table>
      </div>
    </div>
    <!--/Horizontal -->

    <!--Underline form-->
    <div class="mb-2 md:mx-2 lg:mx-2 bg-white border-solid border-gray-200 rounded border shadow-sm w-full md:w-2/5 lg:w-2/5">
      <p class="font-bold text-blue-500 ml-5 mt-5">BALANCE: <span class="text-yellow-600">ZW ${{number_format($bal->DCBalance * -1, 2)}}</span></p>
      <div class="p-3">
        <div class="md:w-4/20 px-3 mb-2 mt-5 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-5">Select Semester</label>
        <select  name="semester_id" wire:model.lazy="semester_id" class=" appearance-none block w-full text-yello-00 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:loading.class="border-grey-200" wire:change="$emit('updateSem')">
        	<option value="" class="text-yellow-500">Select</option>
            @foreach($semesters as $semester)
                <option value="{{$semester->id}}" @if(old('semester_id') == $semester->id) selected @endif >{{ $semester->name }}</option>
            @endforeach
        </select>
        
      {{--  @if($error)
       
            <p class="text-red-500 text-sm italic">
                {{ $error }}
            </p>
        @endif --}}
        </div> 
      </div>
    </div>
    <!--/Underline form-->
  </div>
  <!-- /Cards Section Ends Here -->
   <!--Grid Form-->
@if($stat == 2)
  <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 my-5">
    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
      <div class="bg-gray-200 px-2 py-3 text-xl font-bold text-center border-solid border-gray-200 border-b">
          Current Continuous Assessment
      </div>
      <div class="p-3">
        <table class="table-responsive w-full rounded">
          <thead>
            <tr>
              <th class="border w-1/6 px-4 py-2 bg-purple-600 text-white">Code</th>
              <th class="border w-1/4 px-4 py-2 bg-purple-600 text-white">Course Name</th>
              <th class="border w-1/5 px-4 py-2 bg-purple-600 text-white">Lecture</th>
              <th class="border w-1/5 px-4 py-2 bg-purple-600 text-white">Semester</th>
              <th class="border w-1/6 px-4 py-2 bg-purple-600 text-white">Part</th>
            </tr>
          </thead>
          <tbody>
          	@foreach($semcourses as $course)
            <tr>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course->course_part->course->code}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course->course_part->course->name}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course->staff->user->fullname}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course->semester->name}}</td>
              <td class="border px-4 py-2 text-blue-800 font-bold">{{$course->course_part->part->part}}:{{$course->course_part->part->semester}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <button wire:click="register"  wire:loading.class="bg-gray" class="w-2/5 mt-4 p-4 bg-white border-4 rounded-lg border-blue-300 cursor-pointer transition-all duration-700 hover:bg-blue-800 hover:text-white hover:border-transparent">Register these courses.</button>        
      </div>
    </div>
  </div>
  <!--/Grid Form-->
  @endif
</div>
</div>
