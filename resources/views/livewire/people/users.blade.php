<div>
  @if($updateMode)
     @include('Livewire.people.users.update')
  @elseif($createMode)
     @include('Livewire.people.users.create')
@else
<br>
<div class="topnav">
  <a class="w3-btn themef hover" wire:click="create()">Add</a>
  <input type="text" placeholder="Search..">
</div>
  <table class="w3-table w3-striped w3-bordered">
   <tr>
     <th>Name</th>
     <th>Phone</th>
     <th>File</th>
     <th>Options</th>
   </tr>
  @foreach($data as $row)
   <tr>
     <td>{{$row->first_name}}</td>
     <td>{{$row->last_name}}</td>
     <td>{{$row->email}}</td>
     <td>
       <button wire:click="edit({{$row->id}})" class="w3-btn themef hover w3-center" >Edit</button>
       <button wire:click="destroy({{$row->id}})" class="w3-btn themef hover w3-center" >Delete</button>
     </td>
   </tr>
   @endforeach
 </table>
 @endif
</div>