<div
    x-data="
        {
             isEditing: false,
             isName: '{{ $isName }}',
             focus: function() {
                const textInput = this.$refs.textInput;
                textInput.focus();
                textInput.select();
             }
        }
    "
    x-cloak
>
    <div class="p-2" x-show=!isEditing>
        <span
            x-on:click="isEditing = true; $nextTick(() => focus())"
        >@if($origName){{ $origName }}@else Pending @endif</span>
    </div>
    <div x-show=isEditing class="flex flex-col">
        <form class="flex">

            <input shadowless
                          type="text"
                          class="border-0 truncate focus:border-lh-yellow focus:ring focus:ring-lh-yellow focus:ring-opacity-50 h-7 rounded text-sm"
                          placeholder="100 characters max."
                          x-ref="textInput"
                          wire:model.lazy="newName"
                          x-on:keydown.enter="isEditing = false"
                          x-on:keydown.escape="isEditing = false"
                          wire:keydown.enter="save"
            />
            <button type="button" class="pl-2 focus:outline-none" title="Cancel" x-on:click="isEditing = false"><x-zondicon-close class="h-4 w-4"/></button>
            <button
                type="submit"
                class="pl-1 focus:outline-none text-green-700"
                title="Save"
                x-on:click="isEditing = false"
                wire:click.prevent="save"
            ><x-zondicon-checkmark class="h-4 w-4"/></button>
        </form>
        <small class="text-xs text-green-700">Enter to save, Esc to cancel</small>
    </div>
</div>