<div>
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Enter New Head of Department </p>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Name</label>
        @error('data.staff_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="data.staff_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>
        @foreach($staffs as $staff)
        <option @if(old('staff_id') == $staff->id) selected @endif value="{{$staff->id}}">{{ $staff->user->fullname }}</option>
        @endforeach
      </select>
    </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Department</label>
        @error('data.department_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="data.department_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>
        @foreach($departments as $department)
        <option @if(old('department_id') == $department->id) selected @endif value="{{$department->id}}">{{$department->code}}: {{$department->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-2">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">From Date</label>
        <input type="date" wire:model.lazy="data.from_date" name="from_date" value="{{ old('from_date') }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.from_date')       
          <p class="text-red-500 text-sm italic">
              {{ $message }}
          </p>
            @enderror
      </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-2">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">From Date</label>
        <input type="date" wire:model.lazy="data.to_date" name="to_date" value="{{ old('to_date') }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.to_date')       
          <p class="text-red-500 text-sm italic">
              {{ $message }}
          </p>
            @enderror
      </div>
        
    <!--Footer-->
    <div class="flex justify-center mb-4 pt-2">
      <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
    </div>
  </form>
</div>