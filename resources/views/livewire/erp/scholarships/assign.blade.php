<div>
  <!--Title-->
  <form wire:submit.prevent="confirm" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Add Student to {{$scholarship->name}} Scholarship</p>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0 relative">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Student</label>
        @error('data.staff_id') <span class="error">{{ $message }}</span> @enderror
      <Select wire:model.lazy="stud_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" placeholder="Enter Student ID">
        <option value="">Select</option>
      @foreach($studs as $stud)
      <option value="{{$stud->id}}">{{$stud->gsu_id}} {{$stud->user->fullname}}</option>
      @endforeach
      </Select>

    </div>
        
    <!--Footer-->
    <div class="flex justify-center mb-4 pt-2">
      <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
    </div>
  </form>
</div>