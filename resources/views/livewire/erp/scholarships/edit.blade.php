<div>
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4">
      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Name</label>
        <input type="text" wire:model.lazy="data.name" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.name')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Description</label>
        <input type="text" wire:model.lazy="data.description" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.description')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <!--Footer-->
      <div class="flex justify-center mb-4 pt-2">
        <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
      </div>
  </form>
</div>