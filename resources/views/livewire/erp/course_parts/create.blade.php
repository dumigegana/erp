<div>
  <!--Title-->
  <form wire:submit.prevent="check" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Enter New Program Course</p>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Part</label>
        @error('data.part_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="data.part_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>
        @foreach($parts as $part)
        <option @if(old('part_id') == $part->id) selected @endif value="{{$part->id}}">{{ $part->part }}:{{ $part->semester }}</option>
        @endforeach
      </select>
    </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Program</label>
        @error('data.programme_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="data.programme_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>
        @foreach($programmes as $programme)
        <option @if(old('programme_id') == $programme->id) selected @endif value="{{$programme->id}}">{{$programme->code}}: {{$programme->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Course</label>
        @error('data.course_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="data.course_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>
        @foreach($courses as $cos)
        <option @if(old('course_id') == $cos->id) selected @endif value="{{$cos->id}}">{{ $cos->code}}: {{ $cos->name }}</option>
        @endforeach
      </select>
    </div>
    <!--Footer-->
    <div class="flex justify-center mb-4 pt-2">
      <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
    </div>
  </form>
</div>