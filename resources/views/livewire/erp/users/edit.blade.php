
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Edit User</p>
    <div class="flex flex-wrap -mx-3 my-6">
    	<div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block text-sm text-grey-darker" for="first_name">First Name</label>
        <input wire:model.lazy="data.first_name" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('first_name') }}" name="first_name" type="text" >
        @error('data.first_name')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror 
      </div>
      <div class="w-full md:w-1/2 px-3">
        <label class="block text-sm text-grey-darker" for="middle_names">Middle Names</label>
        <input wire:model.lazy="data.middle_names" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('middle_names') }}" name="middle_names" type="text">@error('data.middle_names')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full md:w-1/2 px-3">
          <label class="block text-sm text-grey-darker" for="surname">Surname</label>
          <input wire:model.lazy="data.surname" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-2 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"  value="{{ old('surname') }}" name="surname" type="text">
          @error('data.surname')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="w-full md:w-1/2 px-3">
        <label class="block text-grey-darker text-sm">User name</label>
        <input type="text" wire:model.lazy="data.username" name="username" value="{{ old( 'username') }}" class=" appearance-none block w-full text-grey-darker bg-grey-200 border border-grey-200 rounded py-2 px-4 pl-8 leading-tight focus:outline-none focus:bg-white">
         @error('data.username')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
    </div>
      
      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block text-grey-darkers text-sm">Cell Number</label>
        <input type="text" wire:model.lazy="data.cell_number" name="cell_number" value="{{ old( 'cell_number') }}" class=" appearance-none block w-full text-grey-darker border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.cell_number')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>

      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block text-grey-darker text-sm">Profile Picture</label>
        <input type="text" wire:model.lazy="data.profile_picture" name="profile_picture" value="{{old('profile_picture')}}" class=" appearance-none block w-full text-grey-darker border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.profile_picture')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="relative flex flex-row">
	      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
	     		<label 
	            for="toogle_active"
	            class="flex items-center "
	          >
	            <!-- toggle -->
	            <div class="relative" 
	            <?php
		          if($user->active == true){
		                echo "x-data=\"{ show: true }\" ";
		            }
		            else {
		              echo "x-data=\"{ show: false }\" ";
		            }		        
		      	?> >
		      	@if($user->active)
	              <!-- input -->
	              <input checked type="checkbox" class="hidden" value="1"  name="active" wire:model="data.active" />
	              @else
	              <input type="checkbox" class="hidden" value="1"  name="active" wire:model="data.active" />
	             @endif
	              <!-- line -->
	              <div
	                class="w-10 h-2 bg-gray-200 rounded-full shadow-inner"
	                @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__line': show }"
	              ></div>
	              <!-- dot -->
	              <div
	                class="dot absolute w-4 h-4 bg-white border-2 border-gray-300 rounded-full shadow inset-y-0 left-0"
	                 @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__dot': show }"
	              ></div>
	            </div>
	            <!-- label -->
	            <div
	              class="ml-3 text-gray-700 font-medium"
	            >
	               Activate
	            </div>
	          </label>
	      </div>
	      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0 absolute right-0">
	     		<label 
	            for="toogle_su"
	            class="flex items-center "
	          >
	            <!-- toggle -->
	            <div class="relative" <?php
		          if($user->su == true){
		                echo "x-data=\"{ show: true }\" ";
		            }
		            else {
		              echo "x-data=\"{ show: false }\" ";
		            }		        
		      	?> >
	              <!-- input -->
	              <input <?php
	            if($user->su == true){
	                    echo "checked ";
	                }
	        ?> type="checkbox" class="hidden" value="1"  name="su" wire:model="data.su" />
	              <!-- line -->
	              <div
	                class="w-10 h-2 bg-gray-200 rounded-full shadow-inner"
	                @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__line': show }"
	              ></div>
	              <!-- dot -->
	              <div
	                class="dot absolute w-4 h-4 bg-white border-2 border-gray-300 rounded-full shadow inset-y-0 left-0"
	                 @click="show = !show" :aria-expanded="show ? 'true' : 'false'" :class="{ 'toggle__dot': show }"
	              ></div>
	            </div>
	            <!-- label -->
	            <div
	              class="ml-3 text-gray-700 font-medium"
	            >
	               Super User
	            </div>
	          </label>

	             @error('data.su')       
	        <p class="text-red-500 text-sm italic">
	            {{ $message }}
	        </p>
	        @enderror
	      </div>
	    </div>
         <!--Footer-->
      <div class="flex justify-center mb-4 pt-2">
        <button class="bg-white border border-blue-200 font-bold text-grey-darker p-1 rounded-lg hover:text-white hover:bg-indigo-400">Update</button>
      </div>
  </form>
