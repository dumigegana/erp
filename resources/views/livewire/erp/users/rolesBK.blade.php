<div class="min-w-full">  
  @section('css')
    .dot {
      top: -.25rem;
      left: -.25rem;
      transition: all 0.3s ease-in-out;
      }

      .toggle__dot {
        transform: translateX(175%);
        background-color: #6495ED;
      }
    .toggle__line {
      background-color: #AED6F1;
    }

    .slider {
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 1rem;
      width: 1rem;
      left: 2px;
      bottom: 3px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }
    input:checked + .slider {
      background-color: #2196F3;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
      -webkit-transform: translateX(42px);
      -ms-transform: translateX(42px);
      transform: translateX(42px);
    }
    .slider.round:before {
      border-radius: 50%;
    }
  @endsection
  <div class="py-2 flex flex-row mb-4 min-w-full">
    <div class="w-1/6">              
      <button class="bg-transparent text-gray-400 font-semibold border border-gray-400 py-2 px-2 rounded "><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>         
    </div>
    <div class="relative w-5/6 flex flex-row-reverse ">
      
      <div class="text-gray-800 relative order-1">
        <input type="hidden" placeholder="Search" class="bg-white p-1 rounded-full text-sm focus:outline-none">
        <button class="absolute inset-y-0 right-0 mx-2">
          <x-zondicon-search class="h-4 w-4"/>
        </button>
      </div>
    </div>
  </div>
  <div class="bg-white overflow-auto">
    <form method="Post" action="{{ route('ERP::user_roles') }}" class="px-4" x-cloak> 
      @csrf
      <input type="hidden" name="id" value="{{ $user->id }}">
        <p class="text-blue-500 text-xl font-bold text-center mb-6 mt-1">Select Roles</p>
        <div class="w-full px-4 flex flex-wrap">
          @foreach($roles as $role)
          <div class="w-1/3 flex-none mb-6">
            <label class="switch relative inline-block w-16 h-5 mr-4">
              @if(isset($user)) 
              <input type="checkbox" autocomplete="off" name="{{$role->id}}" @if($user->hasRole($role->name)) checked @endif >
              <span class="slider cursor-pointer inset-0 absolute round rounded-full"></span>
              <div class="ml-3 mt-7 text-gray-700 font-medium">
               {{ $role->name }}
              </div>
              @endif
            </label>
          </div>
         @endforeach
        </div>
        <br>
      <div class="flex justify-center mb-4 pt-2" x-cloak>
        <button type="submit" class="bg-white border border-blue-200 font-bold text-grey-darker p-1 rounded-lg hover:text-white hover:bg-indigo-400" @click="roleModal = false">Update</button>
      </div>
    </form>
  </div> 
</div>