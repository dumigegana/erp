<div class="w-full rounded overflow-hidden shadow-lg ">
  @if (session()->has('message'))
  <div class="w-full">
    <center><p class="text-green-700 font-bold ">{{ session('message') }}</p></center>
  </div>
    @endif

  <div 
    x-data="{
      openTab: 1,
      activeClasses: 'border-l border-t border-r rounded-t text-blue-700',
      inactiveClasses: 'text-blue-500 hover:text-blue-800'
    }" 
    class="p-6"
  >
    <ul class="flex content-end flex-wrap border-b ">
      <li class="flex-1"></li>
      <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="-mb-px Accept-1">
        <a :class="openTab === 1 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">General Profile </a>
      </li>
      <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="Accept-1">
        <a :class="openTab === 2 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">Academic</a>
      </li>
      <li @click="openTab = 3" :class="{ '-mb-px': openTab === 3 }" class="Accept-1">
        <a :class="openTab === 3 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">References</a>
      </li>
      <li @click="openTab = 4" :class="{ '-mb-px': openTab === 4 }" class="Accept-1">
        <a :class="openTab === 4 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">Assessment</a>
      </li>
    </ul>
    <div class="w-full pt-8">
      <div x-show="openTab === 1">        
        {{--@if ($addSubjects == false)--}}
        @unless (session()->has('message'))
        <center><p class="text-xl font-bold text-blue-800">Personal Details </p></center>
        @endunless 
        <center>
          <form wire:submit.prevent="saveGen">
            <div class="mt-4">
                @php $personal = $applicant->check->personal @endphp
                <label class="inline-flex items-center">
                  <input type="radio" class="form-radio text-green-500" wire:model.lazy="general" value="Complete & Correct">
                   @if($personal == 'Complete & Correct')  
                  <span class="ml-2 font-bold text-xl text-green-500">Complete & Correct</span>
                  @else
                  <span class="ml-2">Complete & Correct</span>
                  @endif 
                </label>
                <label class="inline-flex items-center ml-6">
                  <input type="radio" class="form-radio text-red-500" wire:model.lazy="general" value="Incomplete">
                   @if($personal == 'Incomplete')  
                  <span class="ml-2 font-bold text-xl text-red-500">Incomplete</span>
                  @else
                  <span class="ml-2">Incomplete</span>
                  @endif 
                </label>
                <label class="inline-flex items-center ml-6">
                  <input type="radio" class="form-radio text-red-500" @if($personal == 'Incorrect') checked @endif wire:model.lazy="general" value="Incorrect">
                   @if($personal == 'Incorrect')  
                  <span class="ml-2 font-bold text-xl text-red-500">Incorrect</span>
                  @else
                  <span class="ml-2">Incorrect</span>
                  @endif 
                </label>
                <label class="inline-flex items-center ml-6">
                  <button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white py px-1 border border-blue-500 hover:border-transparent rounded">
                  Submit</button>
                </label>
            </div>
          </form>
        </center>

        <div class="flex mb-4">
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  p-8" >
            <table class="table-fixed">           
              <tbody>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Tittle:</td>
                  <td class="text-blue-700">: {{ $applicant->tittle }}</td>          
                </tr>
                @if($applicant->previous_surname)  
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Previous Surname</td>
                  <td class="text-blue-700">: {{ $applicant->previous_surname }}</td>          
                </tr>
                @endif
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Email</td>
                  <td class="text-blue-700">: {{ $applicant->email }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Address</td>
                  <td class="text-blue-700">: {{ $applicant->home_address }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Sex</td>
                  <td class="text-blue-700">: {{ $applicant->sex }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">National Id</td>
                  <td class="text-blue-700">: {{ $applicant->national_id }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Marital Status</td>
                  <td class="text-blue-700">: {{ $applicant->marital_status }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Mariage Certificate</td>
                  <td class="text-blue-700">: {{ $applicant->marital_status }}</td>          
                </tr>
              </tbody> 
            </table>
          </div>
             
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  p-8" >
            <table class="table-fixed">           
              <tbody>
                @if($applicant->permit)  
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Permit</td>
                  <td class="text-blue-700">: {{ $applicant->permit }}</td>          
                </tr>
                @endif
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Cell number</td>
                  <td class="text-blue-700">: {{ $applicant->cell_number }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Home Phone</td>
                  <td class="text-blue-700">: {{ $applicant->home_phone }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Date of birth</td>
                  <td class="text-blue-700">: {{ $applicant->date_of_birth }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Place of birth</td>
                  <td class="text-blue-700">: {{ $applicant->place_of_birth }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Birth certificate #</td>
                  <td class="text-blue-700">: {{ $applicant->birth_cert_no }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Prospective Sponsor</td>
                  <td class="text-blue-700">: {{ $applicant->prospective_sponsor }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Nationality</td>
                  <td class="text-blue-700">: {{ $applicant->nationality }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Citizenship</td>
                  <td class="text-blue-700">: {{ $applicant->citizenship }}</td>          
                </tr>                
              </tbody> 
            </table>
          </div> 
        </div>
        <div> 
          <center>
            <button wire:click="birth()" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Birth Certificate</button>
            <button wire:click="nId" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">National ID</button>
            <button wire:click="mariage" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Mariage Certificate</button>
          </center>
        </div> 
        <div class="px-4">
         @if($isBirth  == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->birth_certificate) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
         @if($isId  == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->id_card) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
         @if($isMariage == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->marriage_certificate) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
        </div>
      </div>
      <div x-show="openTab === 2">
        @unless (session()->has('message'))
        <center><p class="text-xl font-bold text-blue-800">GCE Results Details</p></center>
        @endunless 
        <center>
        <form wire:submit.prevent="saveAcademic">
          <div class="mt-4">
            <div class="mt-2">
              @php $acc = $applicant->check->academic @endphp
              <label class="inline-flex items-center">
                <input type="radio" class="form-radio text-green-500" wire:model.lazy="academic" value="Complete & Correct">
                @if($acc == 'Complete & Correct')  
                <span class="ml-2 font-bold text-xl text-green-500">Complete & Correct</span>
                @else
                <span class="ml-2">Complete & Correct</span>
                @endif 
              </label>
              <label class="inline-flex items-center ml-6">
                <input type="radio" class="form-radio text-red-500" wire:model.lazy="academic" value="Incomplete">                
                 @if($acc == 'Incomplete')  
                <span class="ml-2 font-bold text-xl text-red-500">Incomplete</span>
                @else
                <span class="ml-2">Incomplete</span>
                @endif 
              </label>
              <label class="inline-flex items-center ml-6">
                <input type="radio" class="form-radio text-red-500" wire:model.lazy="academic" value="Incorrect">
                 @if($acc == 'Incorrect')  
                <span class="ml-2 font-bold text-xl text-red-500">Incorrect</span>
                @else
                <span class="ml-2">Incorrect</span>
                @endif 
              </label>
              <label class="inline-flex items-center ml-6">
                <button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white py px-1 border border-blue-500 hover:border-transparent rounded">
                Submit</button>
              </label>
            </div>
          </div>
        </form></center>        
           @foreach($applicant->school_certificates as $cert)    
        <div class="flex mb-4">
          <div class="w-2/5 rounded overflow-hidden shadow-lg pl-5 pr-20 pt-8">
            <p class="text-xl text-blue-800 text-center">{{ $cert->exam_body }}</p>
            <p class="text-ms text-blue-800 text-center font-bold underline">{{ $cert->level }}'Level => {{ $cert->awaded_month }}</p> <br>
            <table class="table-fixed">           
              <tbody>
                @foreach($cert->school_results as $result)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">{{ $result->subject->name }} </td>
                  <td class="text-yellow-700 pl-6">: {{ $result->grade->grade }}</td>          
                </tr>
                @endforeach 
              </tbody> 
            </table>
          </div>
          <div class="w-3/5 rounded overflow-hidden shadow-lg pl-5 pt-8 pr-2" >            
          <div style="height:400px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div style="width:900px; height:700px">
              <img src="{{ asset('storage/'.$cert->file_loc) }}" alt="test image"/>
            </div>
          </div><br>
          </div> 
        </div>
        @endforeach 
        @if($applicant->tertiary_infos)      
           @foreach($applicant->tertiary_infos as $tertiary)    
        <div class="flex mb-4">
          <div class="w-2/5 rounded overflow-hidden shadow-lg pl-2 pr-6 pt-8" >
            <table class="table-fixed">           
              <tbody>
                <tr>
                  <td class="text-xl text-blue-800">{{ $tertiary->institution }}</td>
                  <td class="text-xl text-blue-800">{{ $tertiary->awaded }}</td>
                </tr>
                <tr>
                  <td class="text-xl text-blue-800">{{ $tertiary->programme }}</td>
                  <td class="text-xl text-blue-800">{{ $tertiary->class }}</td>
                </tr>
                @foreach($tertiary->tertiary_subjects as $subject)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold"> {{ $subject->subject }} </td>
                  <td class="w-1/4"></td>         
                </tr>
                @endforeach 
              </tbody> 
            </table>
          </div>
          <div class="w-3/5 rounded overflow-hidden shadow-lg pt-8 pl-5 pr-2" >            
          <div style="height:400px; width:100%; overflow-y:scroll; overflow-x: scroll;">     
            <div style="width:900px; height:700px">
              <img src="{{ asset('storage/'.$tertiary->certificat_loc) }}" alt="test image"/>
            </div>
          </div><br>
          </div> 
        </div>
        @endforeach
        @endif
      </div>
      <div x-show="openTab === 3">  
        <center>
          <form wire:submit.prevent="saveReffs">
            <div class="mt-4">
              <div class="mt-2">
                 @php $reff = $applicant->check->files @endphp
                <label class="inline-flex items-center">
                  <input type="radio" class="form-radio text-red-500"  wire:model.lazy="reffs" value="Complete & Correct">
                  @if($reff == 'Complete & Correct')  
                <span class="ml-2 font-bold text-xl text-green-500">Complete & Correct</span>
                @else
                <span class="ml-2">Complete & Correct</span>
                @endif 
                </label>
                <label class="inline-flex items-center ml-6">
                  <input type="radio" class="form-radio text-red-500" wire:model.lazy="reffs" value="Incomplete">
                   @if($reff == 'Incomplete')  
                <span class="ml-2 font-bold text-xl text-red-500">Incomplete</span>
                @else
                <span class="ml-2">Incomplete</span>
                @endif 
              </label>
                <label class="inline-flex items-center ml-6">
                  <input type="radio" class="form-radio text-red-500" wire:model.lazy="reffs" value="Incorrect">
                   @if($reff == 'Incorrect')  
                <span class="ml-2 font-bold text-xl text-red-500">Incorrect</span>
                @else
                <span class="ml-2">Incorrect</span>
                @endif 
                </label>
                <label class="inline-flex items-center ml-6">
                  <button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white py px-1 border border-blue-500 hover:border-transparent rounded">
                  Submit</button>
                </label>
                
              </div>
            </div>
          </form>
        </center>      
        @if(count($applicant->work_experiences) > 0)  
        <center><p class="text-xl font-bold text-blue-800">Work Experience</p> </center> <br>   
        <div class=" mb-4">
          <div class="w-full rounded overflow-hidden shadow-lg p-8" >
            <table class="table-auto w-full">           
              <tbody>
                @foreach($applicant->work_experiences as $work)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">: {{ $work->occupation }}</td>
                  <td class="text-blue-700">: {{ $work->name_of_employer }}</td>
                  <td class="text-blue-700">: {{ $work->from_date }}</td>         
                  <td class="w-1/7"> </td>         
                  <td class="text-blue-700">: {{ $work->to_date }} </td>         
                </tr>
                @endforeach
              </tbody> 
            </table>
          </div>
        </div>
         @endif 
        <center><p class="text-xl font-bold text-blue-800">Refferences</p> </center> <br>   
        <div class=" mb-4">
          <div class="w-full rounded overflow-hidden shadow-lg p-8" >
            <table class="table-auto w-full">           
              <tbody>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">: {{ $applicant->referee_1 }}</td>
                  <td class="text-blue-700">: {{ $applicant->ref1_address }}</td>       
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">: {{ $applicant->referee_2 }}</td>
                  <td class="text-blue-700">: {{ $applicant->ref2_address }}</td>       
                </tr>
              </tbody> 
            </table>
          </div>
        </div>
        <div class="flex mb-4">
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  p-8" >
            <table class="table-fixed">           
              <tbody>               
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Entry Type</td>
                  <td class="text-blue-700">: {{ $applicant->entry_type->type }}</td>
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Study program</td>
                  <td class="text-blue-700">: {{ $applicant->study_program->type }}</td>
                </tr>
                 @foreach($applicant->programmes as $program)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">{{ $program->code }}</td>
                  <td class="text-blue-700">: {{ $program->name }}</td>
                </tr>
                @endforeach
              </tbody> 
            </table>
          </div>
             
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  p-8" >
            <table class="table-fixed">           
              <tbody>                
                 @if(!$applicant->perm_res_zim)  
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Residence:</td>
                  <td class="text-blue-700">: Temporal Zim Resident</td>          
                </tr>               
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Permit</td>
                  <td class="text-blue-700">: {{ $applicant->permit }}</td>          
                </tr>
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Period</td>
                  <td class="text-blue-700">: {{ $applicant->zim_res_period }}</td>          
                </tr>
                @else
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Residence</td>
                  <td class="text-blue-700">: Permanent Zim Resident</td>          
                </tr>
                @endif
                @foreach($applicant->attended_schools as $school)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">{{$school->name}}</td>
                  <td class="text-blue-700">: {{ $school->from_year }} - {{ $school->to_year }} </td>
                </tr>
                @endforeach
                @if($applicant->has_dis)
                <tr>
                  <td class="uppercase tracking-wide text-blue-800 text-sm font-bold">Disability</td>
                  <td class="text-blue-700">: {{ $applicant->disability->description }}</td>           
                </tr>
                
                @endif               
              </tbody> 
            </table>
          </div> 
        </div>
        <div> 
          <center>
            <button wire:click="cv()" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Curriculum Vitae</button>
          </center>
        </div> 
        <div class="px-4">
         @if($isCv  == true)
          <div style="height:1200px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->cv) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
        </div>

      </div>
      <div x-show="openTab === 4">
        @unless (session()->has('message'))
        <center><p class="text-xl font-bold text-blue-800">Assessment Page</p></center>
        @endunless 
        <div class="mb-4 mx-20 mb-3 rounded overflow-hidden shadow-lg pl-5 pr-10 pt-5">
          <p class="text-xl text-blue-800 text-center font-bold underline">{{ $applicant->prefered_prog }}</p> <br>
          <table class="table-fixed w-full mb-5">  
            <thead class="bg-gray-800 text-white">
              <tr class="w-full">
                <th class="w-2/6 text-left py-3 px-4 uppercase font-semibold text-sm">Office</th>
                <th class="w-1/6 text-left py-3 px-4 uppercase font-semibold text-sm">Outcome</th>
                <th class=" w-3/6 text-left py-3 px-4 uppercase font-semibold text-sm">Comments</th>
              </tr>
            </thead>         
            <tbody wire:poll.2500ms>
              <tr>
                <td class="w-2/6 uppercase tracking-wide text-blue-800 text-sm font-bold">Admissions</td>
                <td class="w-1/6">{{ $applicant->admissions_outcome}}</td>
                <td class="w-3/6">{{ $applicant->admissions_comment }}</td>         
              </tr> 
              <tr>
                <td class="w-2/6  uppercase tracking-wide text-blue-800 text-sm font-bold">Bursary</td>
                <td class="w-1/6">{{ $applicant->bursar_outcome}}</td>
                <td class="w-3/6">{{ $applicant->admissions_comment }}</td>         
              </tr> 
              <tr>
                <td class="w-2/6 uppercase tracking-wide text-blue-800 text-sm font-bold">Head of Department</td>
                <td class="w-1/6">{{ $applicant->hod_outcome}}</td>
                <td class="w-3/6">{{ $applicant->hod_comment }}</td>         
              </tr> 
              <tr>
                <td class="w-2/6  uppercase tracking-wide text-blue-800 text-sm font-bold">Dean</td>
                <td class="w-1/6">{{ $applicant->fdean_outcome}}</td>
                <td class="w-3/6">{{ $applicant->fdean_comment }}</td>         
              </tr> 
            </tbody> 
          </table>
        </div>
        @if($applicant->check->complete == true)
        <div class="mb-4 mx-20 mb-10 rounded overflow-hidden shadow-lg pl-5 pr-10 pt-5">
          <p class="text-xl text-blue-800 text-center font-bold underline">Feedback</p> <br>
          @if(auth()->user()->hasPermission('applicants.admin') && auth()->user()->hasPermission('admissions.admin'))         
          <form class="w-full " wire:submit.prevent="saveAdmissions">
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="admissions_outcome">Outcome</label>
                <select wire:model="admissions_outcome" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                  
                  <option @if(old('admissions_outcome') == 'Accept') selected @endif value="Accept">Accept</option>
                  <option @if(old('admissions_outcome') == 'Decline') selected @endif value="Decline" class="text-red-700">Decline</option>
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div> 
               @error('admissions_outcome') <span class="error text-red-800">{{ $message }}</span> @enderror 
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="admissions_comment">
                  Comments
                </label>
                <textarea class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" wire:model.lazy="admissions_comment" type="text"> </textarea>
              </div>
              <div class="w-full md:w-1/4 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="Action">
                  Action
                </label>
                <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hove:text-white p-2 border border-blue-500 hover:border-transparent rounded">Submit</button> 
              </div>
            </div>
          </form>
             @endif
             @if(auth()->user()->hasPermission('applicants.admin') && auth()->user()->hasPermission('bursary.admin'))         
          <form class="w-full " wire:submit.prevent="saveBussary">
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="bursar_outcome">Outcome</label>
                <select wire:model="bursar_outcome" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                  <option @if(old('bursar_outcome') == 'Accept') selected @endif value="Accept">Accept</option>
                  <option @if(old('bursar_outcome') == 'Decline') selected @endif value="Decline" class="text-red-700">Decline</option>
                </select> 
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div>
               @error('bursar_outcome') <span class="error text-red-800">{{ $message }}</span> @enderror 
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="bursar_comment">
                  Comments
                </label>
                <textarea class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" wire:model.lazy='bursar_comment' type="text"> </textarea>
              </div>
              <div class="w-full md:w-1/4 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="Action">
                  Action
                </label>
                <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hove:text-white p-2 border border-blue-500 hover:border-transparent rounded">Submit</button> 
              </div>
            </div>
          </form>
             @endif
             @if(auth()->user()->hasPermission('applicants.admin') && auth()->user()->hasPermission('hod.admin'))         
          <form class="w-full " wire:submit.prevent="saveHod">
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="hod_outcome">Outcome</label>
                <select wire:model="hod_outcome" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                  <option @if(old('hod_outcome') == 'Accept') selected @endif value="Accept">Accept</option>
                  <option @if(old('hod_outcome') == 'Decline') selected @endif value="Decline" class="text-red-700">Decline</option>
                </select> 
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div>
               @error('hod_outcome') <span class="error text-red-800">{{ $message }}</span> @enderror 
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="hod_comment">
                  Comments
                </label>
                <textarea class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" wire:model.lazy="hod_comment" type="text"> </textarea>
              </div>
              <div class="w-full md:w-1/4 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="Action">
                  Action
                </label>
                <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hove:text-white p-2 border border-blue-500 hover:border-transparent rounded">Submit</button> 
              </div>
            </div>
          </form>           
             @endif
             @if(auth()->user()->hasPermission('applicants.admin') && auth()->user()->hasPermission('dean.admin'))         
          <form class="w-full " wire:submit.prevent="saveDean">
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="fdean_outcome">Outcome</label>
                <select wire:model="fdean_outcome" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                  <option @if(old('fdean_outcome') == 'Accept') selected @endif value="Accept">Accept</option>
                  <option @if(old('fdean_outcome') == 'Decline') selected @endif value="Decline" class="text-red-700">Decline</option>
                </select> 
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div>
               @error('fdean_outcome') <span class="error text-red-800">{{ $message }}</span> @enderror 
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="fdean_comment">
                  Comments
                </label>
                <textarea class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" wire:model.lazy="fdean_comment" type="text"> </textarea>
              </div>
              <div class="w-full md:w-1/4 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="Action">
                  Action
                </label>
                <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hove:text-white p-2 border border-blue-500 hover:border-transparent rounded">Submit</button> 
              </div>
            </div>
          </form>
          @endif
        </div>
        <div class="mb-4 mx-20 mb-10 rounded overflow-hidden shadow-lg pl-5 pr-10 pt-5">
          <p class="text-xl text-red-800 text-center font-bold underline">Admit/Decline Applicant</p> <br>
           @if(auth()->user()->hasPermission('applicants.admin') )
           @if(auth()->user()->hasPermission('admissions.admin'))
          <form wire:submit.prevent="saveOutcome">
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="status">Final Outcome</label>
                <select wire:model="status" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                  <option @if(old('status') == 'Admit') selected @endif value="Admit">Admit</option>
                  <option @if(old('status') == 'Reject') selected @endif value="Reject" class="text-red-700">Reject</option>
                  </select>
                  <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>
                </div>
                <div class="w-full md:w-3/4 px-3">
                  <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="programme">Change Programme</label>
                  <select wire:model="programme" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
                    @foreach($programmes as $programme)
                    @if (old('programme') == $programme->id)
                    <option value= "{{$programme->id}}" selected>{{$programme->name}}</option>
                    @else
                    <option value= "{{$programme->id}}">{{$programme->name}}</option>
                    @endif
                    @endforeach
                  </select>
                  <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>
                </div>
              </div>      
              <center>           
                <label class="inline-flex items-center ml-6">
                  <button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white p-2 border border-blue-500 hover:border-transparent rounded">
                  Submit</button>
                </label>
              </center>
          </form>
           @endif
           @endif
        </div>
        @else 
        <div class="mb-4 mx-20 mb-10 rounded overflow-hidden shadow-lg pl-5 pr-10 pt-5">
          <p class="text-xl text-blue-800 text-center font-bold underline">Incomplete application, May not be evaluated</p> <br>
        </div>
        @endif         
      </div> 
    </div> 
  </div>   
</div>