<div class="max-w-full flex-grow self-end">
  <!--Title-->
  <form wire:submit.prevent="check" class="px-4">
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Semester Fees Settings</p>
    <div class="flex flex-wrap mx-3">
      <div class="md:w-3/10 px-3 mb-2 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-4">Semester</label>
          @error('data.semester_id') <span class="error">{{ $message }}</span> @enderror
        <select wire:model.lazy="semester_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
          <option selected>-- select --</option>
          @foreach($semesters as $semester)
          <option @if(old('semester_id') == $semester->id) selected @endif value="{{$semester->id}}">{{ $semester->name }}</option>
          @endforeach
        </select> 
      </div>
      <div class="md:w-1/5 px-3 mb-2 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-4">Total fee</label>
          @error('data.fee') <span class="error">{{ $message }}</span> @enderror
        <input wire:model.lazy="fee" type="number" step=".01" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">        
      </div>
      <div class="md:w-1/5 px-3 mb-2 mt-1 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-4">Required Red %</label>
          @error('registration') <span class="error">{{ $message }}</span> @enderror
        <input wire:model.lazy="registration" type="number" step=".01" min="0" max="100" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-wh.ite focus:border-blue-300">
      </div>
      <div class="md:w-2/9 px-3 mb-2 mt-1 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-4">Required Results %</label>
          @error('results') <span class="error">{{ $message }}</span> @enderror
        <input wire:model.lazy="results" type="number" step=".01" min="0" max="100" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-wh.ite focus:border-blue-300">
      </div>
    </div>
    <div class="grid grid-cols-4 gap-4">
      @foreach($programmes as $programme)
     <div class="flex mt-4">
        <label class="flex items-center">
          <input type="checkbox" class="form-checkbox" wire:model.lazy="progs.{{ $programme->id }}" value="{{$programme->id}}">
          <span class="ml-2">{{$programme->name}}</span>
        </label>
      </div>
      @endforeach
    </div>
    
    <!--Footer-->
    <div class="flex justify-center mb-4 pt-2">
      <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
    </div>
  </form>
</div>