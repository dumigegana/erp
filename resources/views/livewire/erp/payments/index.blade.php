<div class="min-w-full"> 
  <div class="py-4 flex flex-row mb-4 min-w-full">
    <div class="w-1/6"> 
      <button class="bg-transparent text-gray-400 font-semibold border border-gray-400 py-2 px-2 rounded "><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
     </div>
    <div class="relative w-5/6 flex flex-row-reverse ">
      <div class="relative text-gray-800 mr-4 order-4">
        <select wire:model="orderBy" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-1 rounded leading-tight pr-5 pl-2 focus:outline-none focus:bg-white focus:border-yellow-500">
          <option value="id">Defaulty</option>
          <option value="gsu_id">Student number</option>
          <option value="created_at">Date</option>
          <option value="fullname">Name</option>
          <option value="method">Payment Method</option>
          <option value="ref_no">Refernce number</option>
          <!-- <option value="parts.fullname">Part</option> -->
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>        
      </div> 
      <div class="relative text-gray-800 mr-4 order-3">
        <select wire:model="orderAsc" class="bg-white border border-blue-100 text-blue-800 py-1 pr-4 pl-2 rounded leading-tight focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none">
          <option value="asc">Ascending</option>
          <option value="desc">Descending</option>
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 p-1 text-grey-darker leading-tight mt-1 ml-3">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>        
      </div>
      <div class="relative text-gray-800 mr-4 order-2">
        <select wire:model="perPage" class="bg-white border border-blue-100 text-blue-800 py-1 pl-2 pr-2 rounded leading-5 focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none ">
          <option>10</option>
          <option>25</option>
          <option>50</option>
          <option>75</option>
          <option>100</option>
          <option>200</option>
        </select> 
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>          
      </div> 
      <div class="text-gray-800 relative order-1">
        <input type="search" wire:model.debounce.300ms="search" placeholder="Search" class="bg-white p-1 rounded-full text-sm focus:outline-none">
        <button class="absolute inset-y-0 right-0 mx-2">
          <x-zondicon-search class="h-4 w-4"/>
        </button>
      </div>
    </div>
  </div>
<div class="bg-white overflow-auto">
  <table class="min-w-full bg-white">
    <thead class="bg-sidebar text-white">
      <tr>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Student Number</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Full Name</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Program</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Paid By</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Reference</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">purpose</th>        
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Payment Method</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Date</th>
      </tr>
    </thead>
     @if (count($payments)>0)
    <tbody class="text-gray-700 divide-y-2 divide-red-100 divide-dotted">
       @foreach($payments as $payment)
      @if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
        <td class="text-left py-1 px-4">{{ $payment->gsu_id }}</td>
        <td class="text-left py-1 px-4">{{ $payment->fullname }}</td>
        <td class="text-left py-1 px-4">{{ $payment->program}}</td>        
        <td class="text-left py-1 px-4">{{ $payment->by_name }}</td>
        <td class="text-left py-1 px-4">{{ $payment->ref_no}}</td>
        <td class="text-left py-1 px-4">{{ $payment->purpose}}</td>
        <td class="text-left py-1 px-4">{{ $payment->method}}</td>
        <td class="text-left py-1 px-4">{{ $payment->created_at}}</td>        
      </tr>
      @endforeach
    </tbody>
    @endif
  </table> 
   
 {!! $payments->links('vendor.pagination.tailwind_live') !!} 
  </div>
</div>  