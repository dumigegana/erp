<div class="min-w-full">   
@section('css')
.slider {
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 1rem;
  width: 1rem;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}
input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}
input:checked + .slider:before {
  -webkit-transform: translateX(43px);
  -ms-transform: translateX(43px);
  transform: translateX(43px);
}
.slider.round:before {
  border-radius: 50%;
}
@endsection
  <div class="py-4 flex flex-row mb-4 min-w-full">
  	<div class="w-1/6">
          @if(auth()->user()->hasPermission('cohorts.admin'))
	    <div x-data="{ 'showModal': false }" @eModal.window="showModal = true" @keydown.escape="showModal = false"  x-cloak>
	      <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-0 hover:border-transparent rounded "><a href="#" @click.prevent="showModal = true" wire:click="create"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
	        <!-- overlay -->
	      <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
	              <!--Dialog-->
	        <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg relative text-left " x-show="showModal" @click.away="showModal = false" >
  
            <button class="absolute top-0 right-0 mr-2 text-red-500" type="button" @click="showModal = false">✖</button> 
	      @include('livewire.erp.cohorts.create')
	        </div>
	      </div>
	    </div>
	    @else    
	    <button class="bg-transparent text-gray-400 font-semibold border border-gray-400 py-2 px-2 rounded "><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
       @endif 
    </div>

    <div class="relative w-5/6 flex flex-row-reverse ">
	    <div class="relative text-gray-800 mr-4 order-4">
        <select wire:model="orderBy" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-1 rounded leading-tight pr-5 pl-2 focus:outline-none focus:bg-white focus:border-yellow-500">
	        <option value="name">Name</option>
	        <option value="active">Active</option>
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>        
	    </div> 
	    <div class="relative text-gray-800 mr-4 order-3">
	      <select wire:model="orderAsc" class="bg-white border border-blue-100 text-blue-800 py-1 pr-4 pl-2 rounded leading-tight focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none">
	        <option value="asc">Ascending</option>
	        <option value="desc">Descending</option>
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 p-1 text-grey-darker leading-tight mt-1 ml-3">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>        
	    </div>
	    <div class="relative text-gray-800 mr-4 order-2">
	      <select wire:model="perPage" class="bg-white border border-blue-100 text-blue-800 py-1 pl-2 pr-2 rounded leading-5 focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none ">
	        <option>10</option>
	        <option>25</option>
	        <option>50</option>
	        <option>75</option>
	        <option>100</option>
	        <option>200</option>
        </select>	
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
          <x-zondicon-cheveron-down class="h-4 w-4"/>
        </div>          
	    </div> 
	    <div class="text-gray-800 relative order-1">
	      <input type="search" wire:model.debounce.300ms="search" placeholder="Search" class="bg-white p-1 rounded-full text-sm focus:outline-none">
	      <button class="absolute inset-y-0 right-0 mx-2">
	        <x-zondicon-search class="h-4 w-4"/>
	      </button>
	    </div>
	  </div>
  </div>
<div class="bg-white overflow-auto">
  <table class="min-w-full bg-white">
    <thead class="bg-sidebar text-white">
      <tr>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Name</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Active</th>
        <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Options</th>
      </tr>
    </thead>
     @if (count($cohorts)>0)
    <tbody class="text-gray-700 divide-y-2 divide-red-100 divide-dotted">
       @foreach($cohorts as $c@if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}"><tr class="theme-text">
        <td class="text-left py-1 px-4">{{ $cohort->name }} </td>
        <td class="text-left py-1 px-4">{{ $cohort->active }} </td>
        <td class="text-left py-1 px-4 flex flex-row"> 
        	 @if(auth()->user()->hasPermission('cohorts.admin'))
	    <div x-data="{ 'showModal': false }" @keydown.escape="showModal = false">
	      <button class="bg-transparent hover:bg-yellow-800 text-yellow-800 font-semibold hover:text-white p-0 hover:border-transparent rounded mr-4 "><a href="#" @click="showModal = true" wire:click="edit({{$cohort->id}})"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
	        <!-- overlay -->
	      <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }" x-cloak>
	              <!--Dialog-->
	        <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg relative text-left " x-show="showModal" @click.away="showModal = false" >
  
            <button class="absolute top-0 right-0 mr-2 text-red-500" type="button" @click="showModal = false">✖</button> 
	      @include('livewire.erp.cohorts.edit')
	        </div>
	      </div>
	    </div>
	    @else    
	    <button class="bg-transparent text-gray-400 font-semibold border border-gray-400 py-2 px-2 rounded "><a href="#"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
       @endif 
        @if(auth()->user()->hasPermission('cohorts.admin'))    
	    <button class="bg-transparent hover:bg-red-500 text-red-500 font-semibold hover:text-white p-0 hover:border-transparent rounded" wire:click.prevent="delete({{$cohort->id}})"><a href="#"><x-zondicon-trash class="h-4 w-4"/></a></button>
       @endif
        </td>
      </tr>
      @endforeach
    </tbody>
    @endif
  </table> 
   
 {!! $cohorts->links('vendor.pagination.tailwind_live') !!} 
	</div>
</div>
 @section('js')
 <script>
// window.addEventListener('eModal', event => {

//     // alert('Name updated to: '+ event.detail.open);
// })
</script>
@endsection
</div> 