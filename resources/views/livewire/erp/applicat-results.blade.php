<div class="w-full rounded overflow-hidden shadow-lg ">
	 @if (session()->has('message'))
	<div class="w-full">
	  <center><p class="text-green-700 font-bold ">{{ session('message') }}</p></center>
	</div>
   @endif
  <div 
		x-data="{
		  openTab: 1,
		  activeClasses: 'border-l border-t border-r rounded-t text-blue-700',
		  inactiveClasses: 'text-blue-500 hover:text-blue-800'
			}" 
			class="p-6"
		  >
		<ul class="flex border-b">
		  <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="-mb-px mr-1">
			<a :class="openTab === 1 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">
			  O/A'LEVEL
			</a>
		  </li>
		  <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="mr-1">
			<a :class="openTab === 2 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">TERTIARY</a>
		  </li>
		  <li @click="openTab = 3" :class="{ '-mb-px': openTab === 3 }" class="mr-1">
			<a :class="openTab === 3 ? activeClasses : inactiveClasses" class="bg-gray-100 inline-block py-2 px-4 font-semibold" href="#">WORK EXPERIENCE</a>
		  </li>
		</ul>
		<div class="w-full pt-4">
		  <div x-show="openTab === 1">  
			  @unless ($row->check->step < 3)    	
				@if ($addSubjects == false)
				  @unless (session()->has('message'))
				<center><p class="text-xl font-bold text-blue-800">General Certificate of Education (GCE)</p></center>
			  @endunless 
			  <center><p class="italic">(Upload one file at a time, Starting with O'level certificate.)</p> </center> 
			  <form wire:submit.prevent="save">	  
				  <div class="flex mb-4">
					<div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8">
						<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-birth_cert_no">Date Awarded:</label>	        
					  <input type="month" placeholder="eg 2020-10" wire:model.lazy="awaded_date" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('awaded_date') }}"> <p class="text-blue-500 text-xs italic">Year and month in this format 2021-11 (for Nov 2021).</p>
						  
				   
						<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-birth_cert_no">Examination Body:</label>
						<input type="text" wire:model.lazy="exam_body" value="{{ old('exam_body') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="(e.g. ZIMSEC, Cambridge ...">  
				  </div> 
					   
				  <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8" >
						<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-birth_cert_no">Level / Award:</label>
						<select type="text" wire:model.lazy="level" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">	
							<option>select</option>						
							<option @if(old('level') == 'O') selected @endif value="O">O'Level</option>
							<option @if(old('level') == 'A') selected @endif value="A">A'Level</option>
							<option @if(old('level') == 'M') selected @endif value="M">Matric</option>
						</select>		
										 
						<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-certiicate">Certificate:</label>
						<input type="file" name="file_loc" wire:model.lazy="file_loc" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('file_loc') }}">        
						@error('file_loc') <span class="error text-red-800">{{ $message }}</span> @enderror 
						 
						</div> 
					</div>
					<center><button type="submit" class="bg-transparent hover:bg-red-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Submit</button></center>
				
					</form>
					@endif
					@if ($file_loc && $addSubjects)
					<div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">			
						<div class="w-full h-full">
						<img src="{{ $file_loc->temporaryUrl() }}" alt="test image" />
						</div>
					</div>
					<div class="w-full bg-blue-500 text-white rounded mr-20 mx-20 mt-8 h-10">
						<center><p class="text-xl font-bold text-blue-800">List in chronological order, all subjects and results ( including failures ) in the above certificate. </p></center>
					</div>
					<div class="w-full rounded overflow-hidden shadow-lg">		   
					<form class="w-full">
					  <div class="flex flex-wrap mx-20 mb-2">
							<div class="md:w-7/16 px-3 mb-2 md:mb-0">
							  <label class="block uppercase tracking-wide text-gray-700 text-xl font-bold mb-2" for="grid-subject-id">
								Subject
							  </label>
							  <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" wire:model.lazy="subject_id.0">	
									<option>select</option>								
									@foreach ($subjects as $sub)
									<option value="{{ $sub->id }}"> {{ $sub->name }}</option>
									@endforeach
							  </select>
							  @error('subject_id.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
							</div>
							<div class="md:w-3/8 px-3">
							  <label class="block uppercase tracking-wide text-gray-700 text-xl font-bold mb-2" for="grid-grade-id">
								Grade
							  </label>
							  <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="grade_id.0">
									<option>select</option>
									@foreach ($grades as $grad)
									<option value="{{ $grad->id }}"> {{ $grad->grade }}</option>
									@endforeach
								</select>
						  		@error('grade_id.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
							</div>
							<div class="md:w-3/16 px-3">
								<label class="block uppercase tracking-wide text-gray-700 text-xl font-bold mb-2" for="grid-add">
								Action
							  </label>
						   	<button class="bg-green-500 hover:bg-green-700 text-white py-1 px-4 rounded" wire:click.prevent="add({{$i}})"><x-zondicon-add-solid class="h-6 w-6"/></button>
							</div>
					  </div>
			 
						 @foreach($inputs as $key => $value)
						<div class="flex flex-wrap mx-20 mx-3 mb-2">
							<div class="md:w-7/16 px-3 mb-2 md:mb-0">
							  <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="subject_id.{{ $value }}">
									<option>select</option>
									@foreach ($subjects as $sub)
									<option value="{{ $sub->id }}"> {{ $sub->name }}</option>
								  @endforeach
							  </select>
							  @error('subject_id.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
							</div>
							<div class="md:w-3/8 px-3">
							  <select class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="grade_id.{{ $value }}">
									<option>select</option>
									@foreach ($grades as $grad)
									<option value="{{ $grad->id }}"> {{ $grad->grade }}</option>
									@endforeach
						  	</select>
						  	
						  		@error('grade_id.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
							</div>
							<div class="md:w-3/16 px-3">
						    <button class="bg-red-500 hover:bg-red-700 text-white py-1 px-4 rounded" wire:click.prevent="remove({{$key}})"><x-zondicon-trash class="h-6 w-6"/></button>
							</div>
						</div>
						@endforeach
					  <center><button type="button" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" wire:click.prevent="storeSubjects()">Save</button></center>
					  <br>
					</form> 
				</div>
				@endif
				@endunless
		  </div>
		  <div x-show="openTab === 2"> 
				@unless ($row->check->step < 4 && $row->entry_type->id > 1)
						@unless (session()->has('message'))
				<center><p class="text-xl font-bold text-blue-800">TERTIARY INSTITUTIONS ATTENDED</p></center>
				  @endunless 
				<center><p class="italic">..</p> </center> 
			  <form class="" wire:submit.prevent="saveTertiary">	  
					<div class="flex mb-4">
						<div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8">
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-date-awaded">Date Awarded:</label>	        
						  <input type="month" placeholder="eg 2020-10" wire:model.lazy="awaded" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('awaded') }}"> 
						  
						  <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-institution">Institution:</label>	        
						  <input type="text" placeholder="GSU, NUST ..." wire:model.lazy="institution" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('institution') }}" placeholder="GSU, Nust..."> 							  
					   
						  <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-programm">Programme of Study:</label>
							<input type="text" wire:model.lazy="programme" value="{{ old('programme') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="e.g. ND, BA, BSC ...">  						
						   
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-period">Period of Study:</label>
							<input type="text" wire:model.lazy="period" value="{{ old('period') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="2008-2013">
					  </div> 
					  <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8" >
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-classificatio">Degree / Deploma Classification:</label>
							<input type="text" wire:model.lazy="class" value="{{ old('class') }}" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outlnine-none focus:shadow-outline" placeholder="(IF APPLICABLE)">  
						 
							 
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-certificate">Certificate:</label>
							<input type="file" wire:model.lazy="certificat_loc" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('certificat_loc') }}">        
							@error('certificat_loc') <span class="error text-red-800">{{ $message }}</span> @enderror 
							 
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Transcript:</label>
							<input type="file" wire:model.lazy="transcript_loc" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ old('transcript_loc') }}">        
							@error('transcript_loc') <span class="error text-red-800">{{ $message }}</span> @enderror 
						  
						<div class="flex mb-4 mr-3" >
							<div class="md:w-4/20 px-3 mb-2 md:mb-0">
								<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-subject">
							   Major Subjects:
							  </label>
							  <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="subject.0">
							  @error('subject.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
							</div>
							<div class="md:w-2/20 px-3">
								<label class="block uppercase tracking-wide mt-8 text-gray-700 text-xs font-bold mb-2" for="grid-action">
								Action
							  </label>
						   <button class="bg-green-500 hover:bg-green-700 text-white py-1 px-4 rounded" wire:click.prevent="add({{$i}})"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></button>
						</div>
					</div>
							@foreach($inputs as $key => $value)
					<div class="flex mb-4 mr-3">
						<div class="md:w-4/20 px-3 mb-2 md:mb-0">
						  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="subject.{{ $value }}" type="text">
					  @error('subject.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-2/20 px-3">
					   <button class="bg-red-500 hover:bg-red-700 text-white py-1 px-4 rounded" wire:click.prevent="remove({{$key}})"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button>
					</div>
				</div>
						@endforeach
				</div> 
			</div>
					<center><button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Submit</button></center>
					
				</form>
						@endunless
			  </div>
		  <div x-show="openTab === 3"> 
			  @unless ($row->check->step < 5)      	  
				  @unless (session()->has('message'))
				<center><p class="text-xl font-bold text-blue-800">WORK EXPERIENCE / EMPLOYMENT HISTORY</p></center>
			   @endunless 
			  <center><p class="italic">..</p> </center> 
			  <form  wire:submit.prevent="saveWork">	  
					<div class="flex mb-4 mr-3" >
						<div class="md:w-4/20 px-3 mb-2 md:mb-0">
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-from_date">
							From Month & Year:
						  </label>
						  <input type="month" placeholder="eg 2020-10"  class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="from_date.0">
						  @error('from_date.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-4/20 px-3">
						  <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-to_date">
							To Month & Year
						  </label>
						  <input  type="month" placeholder="eg 2020-10" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="to_date.0">
					  @error('to_date.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						  <div class="md:w-5/20 px-3">
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-occupation">
							Occupation:
							</label>
							<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="occupation.0">
						  @error('occupation.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-5/20 px-3">
						  <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer">
							Name of Employer
						  </label>
						  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="name_of_employer.0">
					  @error('name_of_employer.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-2/20 px-3">
							<label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-action">
							Action
						  </label>
					   <button class="bg-green-500 hover:bg-green-700 text-white py-1 px-4 rounded" wire:click.prevent="add({{$i}})"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></button>
						</div>
				  </div>
				 
						@foreach($inputs as $key => $value)
					<div class="flex mb-4 mr-3">
						<div class="md:w-4/20 px-3 mb-2 md:mb-0">
						  <input type="month" placeholder="eg 2020-10" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="from_date.{{ $value }}">
						  @error('from_date.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-4/20 px-3">
						  <input type="month" placeholder="eg. 2020-10" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="to_date.{{ $value }}">
					 	 @error('to_date.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-5/20 px-3">
						  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="occupation.{{ $value }}">
						  @error('occupation.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-5/20 px-3">
						  <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="name_of_employer.{{ $value }}">
					  	@error('name_of_employer.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
						</div>
						<div class="md:w-2/20 px-3">
					    <button class="bg-red-500 hover:bg-red-700 text-white py-1 px-4 rounded" wire:click.prevent="remove({{$key}})"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button>
						</div>
					</div>
					@endforeach
					<center><button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Submit</button></center>
					
				</form>
					@endunless
		  </div> 
		</div>
  </div>	
</div>
