<div>
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4 x-cloack"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Edit Semester Course</p>
      <p class="block uppercase tracking-wide text-purple-500 text-sm mb-2 mt-8">Part {{$sem_course->course_part->part->part}}:{{$sem_course->course_part->part->semester}} <span class="text-red-500 font-bold">{{$sem_course->course_part->programme->code}}</span> {{$sem_course->course_part->course->name}}</p>
        @error('part_id') <span class="error">{{ $message }}</span> @enderror
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Semester</label>
        @error('semester_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="semester_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        @foreach($semesters as $semester)
        <option @if(old('semester_id') == $semester->id) selected @endif value="{{$semester->id}}">{{$semester->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Lecturer</label>
        @error('staff_id') <span class="error">{{ $message }}</span> @enderror
      <select wire:model.lazy="staff_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        @foreach($staffs as $staff)
        <option @if(old('staff_id') == $staff->id) selected @endif value="{{$staff->id}}">{{ $staff->user->fullname}}</option>
        @endforeach
      </select>
    </div>
       <!--Footer-->
    <div class="flex justify-center mb-4 pt-2">
      <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Update</button>
    </div>
  </form>
</div>