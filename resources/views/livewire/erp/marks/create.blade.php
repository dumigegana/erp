<div
    x-data="
        {
             isCreate: false,
             focus: function() {
                const textInput = this.$refs.textInput;
                textInput.focus();
                textInput.select();
             }
        }
    "
    x-cloak
>
    <div class="p-2" x-show=!isCreate>
        <span
            x-on:click="isCreate = true; $nextTick(() => focus())"
        >Pending</span>
    </div>
    <div x-show=isCreate class="flex flex-col">
        <form class="flex">

            <input shadowless
              type="number" min="0" step=".01" max="100"
              class="border-0 truncate focus:border-lh-yellow focus:ring focus:ring-lh-yellow focus:ring-opacity-50 h-7 rounded text-sm"
              x-ref="textInput"
              wire:model.lazy="mark"
              x-on:keydown.enter="isCreate = false"
              x-on:keydown.escape="isCreate = false"
              wire:keydown.enter="save"
            />
            <button type="button" class="pl-2 focus:outline-none" title="Cancel" x-on:click="isCreate = false"><x-zondicon-close class="h-4 w-4"/></button>
            <button
                type="submit"
                class="pl-1 focus:outline-none text-green-700"
                title="Save"
                x-on:click="isCreate = false"
                wire:click.prevent="save"
            ><x-zondicon-checkmark class="h-4 w-4"/></button>
        </form>
        <small class="text-xs text-green-700">Enter to save, Esc to cancel</small>
    </div>
</div>