<div class="min-w-full"> 
  <div class="py-4 flex flex-row w-1/2">
    <div class="static left-0 top-0 mt-3 flex flex-row ">
          @if(auth()->user()->hasPermission('academic.access')) 
      <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold mr-2 hover:text-white py-2 px-2 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::export', ['id' =>encrypt($semCos->id)]) }}"><x-zondicon-download class="h-4 w-4"/></a></button>


      <div x-data="{ tooltip: false }" class="inline-flex">
        <div x-on:mouseover="tooltip = true" x-on:mouseleave="tooltip = false">
          <div x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
            <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-2 border border-red-500 hover:border-transparent rounded"><a href="#" @click="showModal = true"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
              <!-- overlay -->
            <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                    <!--Dialog-->
              <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6" x-show="showModal" @click.away="showModal = false" >
              <button class="relative top-0 right-0 pr-3 mb-0" type="button" @click="showModal = false">✖</button>             
            @livewire('erp.marks.add', ['semCos_id' => $semCos->id])
              </div>
            </div>
          </div>
        </div>
        <div class="relative" x-cloak x-show.transition.origin.top="tooltip">
          <div class="absolute top-0 ml-10 z-10 w-32 p-2 text-sm leading-tight theme-text transform -translate-x-1/2 -translate-y-full bg-yellow-400 rounded-lg shadow-lg">
           Course Weights
          </div>
          <svg class="absolute z-10 w-10 h-6 text-yellow-400 transform -translate-x-10 -translate-y-3 fill-current stroke-current" width="8" height="8">
            <rect x="12" y="-20" width="10" height="8" transform="rotate(45)" />
          </svg>
        </div>
      </div>
      <div x-data="{ 'showImport': false }" @keydown.escape="showImport = false" x-cloak>
        <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-2 border border-red-500 hover:border-transparent rounded"><a href="#" @click="showImport = true"><x-zondicon-upload class="h-4 w-4"/></a></button>
          <!-- overlay -->
        <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showImport" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showImport}">
                <!--Dialog-->
          <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6" x-show="showImport" @click.away="showImport = false" >
          <button class="relative top-0 right-0 pr-3 mb-0 text-red-500" type="button" @click="showImport = false">✖</button>             
         <form action="{{ route('ERP::import') }}" method="POST" enctype="multipart/form-data" class="ml-2 p-8">                @csrf
          <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-grey-darker font-bold mb-1" for="grid-password">
                    Weight
                </label>
                <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="weight_id">
                    <option class="font-bold" disabled>Course work : Exam</option>
                    @foreach($weights as $weight)
                        <option value="{{$weight->id}}">Course work {{ $weight->course_w * 100}} : Exam {{$weight->exam * 100}}</option>
                    @endforeach
                </select>
                  <p class="text-grey-dark text-xs italic"></p>
              </div>
          </div>
            <div class="items-center">
              <label class="flex flex-column items-center px-2 py-2 text-purple-500 rounded shadow-lg tracking-wide uppercase border border-purple-500 cursor-pointer hover:bg-purple-500 hover:text-white" x-data="{ files: null }">
                <x-zondicon-upload class="h-4 w-4"/>
                <span class="ml-2 text-base leading-normal"  x-text="files ? files.map(file => file.name).join(', ') : 'Select single file...'"></span>
                <input type='file' name="file" class="hidden" x-on:change="files = Object.values($event.target.files)"/>
              </label>
            </div>
            <button type="submit" class="p-2 bottom-0 right-0 my-6 text-red-500 rounded shadow-lg tracking-wide uppercase border border-red-500 cursor-pointer hover:bg-red-500 hover:text-white">Submit</button>
          </form>
          </div>
        </div>
      </div>   
      
      @else    
      <button class="bg-transpare:t text-gray-400 font-semibold py-2 px-2 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
       @endif 
    </div>
    <div class="py-4 flex flex-row absolute right-0">
    @if($state >= $semCos->state) 
        <button type="submit" class="border border-grey-600 h-8 rounded px-4 bg-grey-100  text-grey-600 mr-10 mb-2"><a href="#">
          Submitted</a></button>
        @else
        <button type="submit" class="border border-green-600 h-8 rounded px-4 bg-grey-200 text-green-600 mr-10 mb-2 hover:bg-green-600 hover:text-white"><a href="{{ route('ERP::lect_submit', ['state' => encrypt($state), 'name' =>encrypt($semCos->id * $state)]) }}">
          Submit</a>
          @endif
        </button>
      <div class="text-gray-600 mr-6">
        <input type="search" name="serch" placeholder="Search" class="bg-white h-8 px-2 pr-10 rounded-full text-sm focus:outline-none">
        <!-- <button type="submit" class="absolute right-1  mt-2 mr-10">
          <x-zondicon-search class="h-4 w-4"/>
        </button> -->
      </div>
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">ID</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Full Name</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Course work ({{$semCos->weight->course_w*100}}%)</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Exam ({{$semCos->weight->exam *100}}%)</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Overall</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Grade</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Action</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($semCos->registrations as $reg)
        <tr class="theme-text">
          <td class="text-left py-1 px-4 uppercase">{{ $reg->student->gsu_id }} </td>
          <td class="text-left py-1 px-4">{{ $reg->student->user->fullname }} </td>
          @if($reg->result)
          <td class="text-left py-1 px-4"> @livewire('dry.edit-field', ['model' => '\App\Result', 'entity' => $reg->result, 'field' => "course_w"])</td>
          <td class="text-left py-1 px-4">@livewire('dry.edit-field', ['model' => '\App\Result', 'entity' => $reg->result, 'field' => "exam"])</td>
          <td class="text-left py-1 px-4">{{ $reg->result->overall }} </td>
          <td class="text-left py-1 px-4">{{ $reg->result->grade }} </td>
          @else 
          <td class="text-left py-1 px-4">
          @livewire('erp.marks.create', ['reg_id' => $reg->id, 'state' => "course_w"])
          </td>
          <td class="text-left py-1 px-4"> 
            @livewire('erp.marks.create', ['reg_id' => $reg->id, 'state' => "exam"])
           </td>
          <td class="text-left py-1 px-4">Pending </td>
          <td class="text-left py-1 px-4">Pending </td>
         </div>
          @endif
          <td class="text-left py-1 px-4"> 
            <button class="bg-transparent text-grey-300 font-semibold py-1 px-2 border border-grey-300 rounded"><x-zondicon-edit-pencil class="h-4 w-4"/></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>