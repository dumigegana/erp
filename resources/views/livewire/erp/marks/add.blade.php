<form wire:submit.prevent="submit">
	<p class="text-blue-500 text-xl font-bold text-center pt-0 pb-5"> {{$sem_cos->course_part->course->code}}: {{$sem_cos->course_part->course->name}}</p>
  <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full px-3">
          <label class="block  tracking-wide text-grey-darker font-bold mb-1" for="grid-password">
              Weight
          </label>
          <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" wire:model.lazy="weight_id">
                  <option class="font-bold" disabled>Course work : Exam</option>
                  @foreach($weights as $weight)
                      <option value="{{$weight->id}}">Course work {{ $weight->course_w * 100}} : Exam {{$weight->exam * 100}}</option>
                  @endforeach
              </select>
          <p class="text-grey-dark text-xs italic"></p>
      </div>
  </div>
	  
		<!--Footer-->
	<div class="flex justify-end pt-2">
		<button class="px-4 bg-blue-400 p-3 rounded-lg text-white hover:bg-indigo-400" x-on:click="showModal = false" >Submit</button>
	</div>	
</form>
