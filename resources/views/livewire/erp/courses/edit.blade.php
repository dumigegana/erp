<div>
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Edit Course</p>    
    <input type="hidden"  wire:model="row_id" value="{{$course->id}}">
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Course Code</label>
        <input type="text" wire:model.lazy="data.code" value="{{ old( $course->code) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.code')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Name</label>
        <input type="text" wire:model.lazy="data.name" value="{{ old( $course->name) }}" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.name')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="md:w-4/20 px-3 mb-2 mt- md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Department</label>
        <select wire:model.lazy="data.department_id" class="block w-full text-blue-700 border border-blue-200 py-1 px-6 mb-3 rounded leading-tight focus:outline-none focus:bg-white">
          
           <option value="">Select</option>
           @foreach($departments as $department)
           <option @if(old('department_id') == $department->id) selected @endif value="{{$department->id}}">{{$department->name}} </option>
           @endforeach
        </select>          
          @error('data.department_id')         
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
          @enderror
        </div>
      
         <!--Footer-->
      <div class="flex justify-center mb-4 pt-2">
        <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Update</button>
      </div>
  </form>
</div>