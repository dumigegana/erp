<form method="Post" action="{{ route('ERP::user_roles') }}" class="px-4" x-cloak> 
  @csrf
  <input type="hidden" name="id" value="{{ $user->id }}">
    <p class="text-blue-500 text-xl font-bold text-center mb-6 mt-1">Select Roles</p>
    @if($roleEdit == 1)
    <div class="w-full px-4 flex flex-wrap">
      @foreach($roles as $role)
      <div class="w-1/3 flex-none mb-6">
        <label class="switch relative inline-block w-16 h-5 mr-4">
          @if(isset($user)) 
          <input type="checkbox" autocomplete="off" name="{{$role->id}}" @if($user->hasRole($role->name)) checked @endif >
          <span class="slider cursor-pointer inset-0 absolute round rounded-full"></span>
          <div class="ml-3 mt-7 text-gray-700 font-medium">
           {{ $role->name }}
          </div>
          @endif
        </label>
      </div>
     @endforeach
    </div>
    @endif
    <br>
  <div class="flex justify-center mb-4 pt-2" x-cloak>
    <button type="submit" class="bg-white border border-blue-200 font-bold text-grey-darker p-1 rounded-lg hover:text-white hover:bg-indigo-400" @click="roleModal = false">Update</button>
  </div>
</form>
