<div>
  <!--Title-->
  <form wire:submit.prevent="store" class="px-4"> 
    <p class="text-blue-500 text-xl font-bold text-center mt-1">Enter New Subject</p>
      <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Name</label>
        <input type="text" wire:model.lazy="data.name" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('data.name')       
            <p class="text-red-500 text-sm italic">
                {{ $message }}
            </p>
            @enderror
      </div>
      <div class="md:w-4/20 px-3 mb-2 mt- md:mb-0">
        <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Level</label>
        <select wire:model.lazy="data.type" class="block w-full text-blue-700 border border-blue-200 py-1 px-6 mb-3 rounded leading-tight focus:outline-none focus:bg-white">
          
           <option value="">Select</option>
           <option value="O">Ordinary Level</option>
           <option value="A">Advanced Level</option>
           <option value="OA">Both O & A'Level</option>
        </select>          
          @error('data.type')         
              <p class="text-red-500 text-sm italic">
                  {{ $message }}
              </p>
          @enderror
        </div>
      <!--Footer-->
      <div class="flex justify-center mb-4 pt-2">
        <button class="bg-white border border-blue-200 text-blue-400 p-1 rounded-lg hover:text-white hover:bg-indigo-400">Submit</button>
      </div>
  </form>
</div>