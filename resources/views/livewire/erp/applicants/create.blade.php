@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::applicants') }}" class="text-xl text-blue-700 mx-4 underline"><i class="fa fa-user-secret"></i>Applicants</a>
  <a class="w3-bar-item">Create Applicant</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'Applicants')
@section('content')

<form method="POST" enctype="multipart/form-data">  
        @csrf        
  @if ($errors->any())
    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
       @foreach ($errors->all() as $error)
      <span class="block sm:inline">{{ $error }}</span>
      <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
        <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
      </span>
      @endforeach
    </div>
@endif
  <div class="m-4 rounded overflow-hidden shadow-lg">
   <div class="flex mb-4">
      <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg mr5 pl-5 pb-2 pr-5">
        <div class="md:w-4/20 px-3 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="grid-tittle">TITTLE</label>
          <select type="text" name="tittle" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
            <option value="">Select</option>
            <option @if(old('tittle') == 'Mr') selected @endif value="Mr">Mr</option>
            <option @if(old('tittle') == 'Mrs') selected @endif value="Mrs">Mrs</option>
            <option @if(old('tittle') == 'MISS') selected @endif value="MISS">MISS</option>
            <option @if(old('tittle') == 'MS') selected @endif value="MS">MS</option>
            <option @if(old('tittle') == 'DR') selected @endif value="DR">DR</option>
            <option @if(old('tittle') == 'PS') selected @endif value="PS">PS</option>
            <option @if(old('tittle') == 'REV') selected @endif value="REV">REV</option>
            <option @if(old('tittle') == 'SR') selected @endif value="SR">SR</option>
            <option @if(old('tittle') == 'Prof') selected @endif value="Prof">Prof</option>
          </select> 
          
         @error('tittle') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        @include('forms/master')
        <div class="md:w-4/20 px-3 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-birth_cert_no">Birth Certificate Number</label>
          <input type="text" name="birth_cert_no" value="{{ old('birth_cert_no') }}" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
         @error('birth_cert_no') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-marital_status">Marital Status</label>
          <select type="text" name="marital_status" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
            <option value="">Select</option>
            <option @if(old('marital_status') == 'Married') selected @endif value="Married">Married</option>
            <option @if(old('marital_status') == 'Single') selected @endif value="Single">Single</option>
            <option @if(old('marital_status') == 'Devoced') selected @endif value="Devoced">Devoced</option>
            <option @if(old('marital_status') == 'Widowed') selected @endif value="Widowed">Widowed</option>
          </select> 
           
         @error('marital_status') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-nationality">Nationality</label>
          <select type="text" name="nationality" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
            <option value="">Select</option>
              <option @if(old('nationality') == 'zimbabwean') selected @endif value="zimbabwean">Zimbabwean</option>
              <option @if(old('nationality') == 'afghan') selected @endif value="afghan">Afghan</option>
              <option @if(old('nationality') == 'albanian') selected @endif value="albanian">Albanian</option>
              <option @if(old('nationality') == 'algerian') selected @endif value="algerian">Algerian</option>
              <option @if(old('nationality') == 'american') selected @endif value="american">American</option>
              <option @if(old('nationality') == 'andorran') selected @endif value="andorran">Andorran</option>
              <option @if(old('nationality') == 'angolan') selected @endif value="angolan">Angolan</option>
              <option @if(old('nationality') == 'antiguans') selected @endif value="antiguans">Antiguans</option>
              <option @if(old('nationality') == 'argentinean') selected @endif value="argentinean">Argentinean</option>
              <option @if(old('nationality') == 'armenian') selected @endif value="armenian">Armenian</option>
              <option @if(old('nationality') == 'australian') selected @endif value="australian">Australian</option>
              <option @if(old('nationality') == 'austrian') selected @endif value="austrian">Austrian</option>
              <option @if(old('nationality') == 'azerbaijani') selected @endif value="azerbaijani">Azerbaijani</option>
              <option @if(old('nationality') == 'bahamian') selected @endif value="bahamian">Bahamian</option>
              <option @if(old('nationality') == 'bahraini') selected @endif value="bahraini">Bahraini</option>
              <option @if(old('nationality') == 'bangladeshi') selected @endif value="bangladeshi">Bangladeshi</option>
              <option @if(old('nationality') == 'barbadian') selected @endif value="barbadian">Barbadian</option>
              <option @if(old('nationality') == 'barbudans') selected @endif value="barbudans">Barbudans</option>
              <option @if(old('nationality') == 'batswana') selected @endif value="batswana">Batswana</option>
              <option @if(old('nationality') == 'belarusian') selected @endif value="belarusian">Belarusian</option>
              <option @if(old('nationality') == 'belgian') selected @endif value="belgian">Belgian</option>
              <option @if(old('nationality') == 'belizean') selected @endif value="belizean">Belizean</option>
              <option @if(old('nationality') == 'beninese') selected @endif value="beninese">Beninese</option>
              <option @if(old('nationality') == 'bhutanese') selected @endif value="bhutanese">Bhutanese</option>
              <option @if(old('nationality') == 'bolivian') selected @endif value="bolivian">Bolivian</option>
              <option @if(old('nationality') == 'bosnian') selected @endif value="bosnian">Bosnian</option>
              <option @if(old('nationality') == 'brazilian') selected @endif value="brazilian">Brazilian</option>
              <option @if(old('nationality') == 'british') selected @endif value="british">British</option>
              <option @if(old('nationality') == 'bruneian') selected @endif value="bruneian">Bruneian</option>
              <option @if(old('nationality') == 'bulgarian') selected @endif value="bulgarian">Bulgarian</option>
              <option @if(old('nationality') == 'burkinabe') selected @endif value="burkinabe">Burkinabe</option>
              <option @if(old('nationality') == 'burmese') selected @endif value="burmese">Burmese</option>
              <option @if(old('nationality') == 'burundian') selected @endif value="burundian">Burundian</option>
              <option @if(old('nationality') == 'cambodian') selected @endif value="cambodian">Cambodian</option>
              <option @if(old('nationality') == 'cameroonian') selected @endif value="cameroonian">Cameroonian</option>
              <option @if(old('nationality') == 'canadian') selected @endif value="canadian">Canadian</option>
              <option @if(old('nationality') == 'cape verdean') selected @endif value="cape verdean">Cape Verdean</option>
              <option @if(old('nationality') == 'central african') selected @endif value="central african">Central African</option>
              <option @if(old('nationality') == 'chadian') selected @endif value="chadian">Chadian</option>
              <option @if(old('nationality') == 'chilean') selected @endif value="chilean">Chilean</option>
              <option @if(old('nationality') == 'chinese') selected @endif value="chinese">Chinese</option>
              <option @if(old('nationality') == 'colombian') selected @endif value="colombian">Colombian</option>
              <option @if(old('nationality') == 'comoran') selected @endif value="comoran">Comoran</option>
              <option @if(old('nationality') == 'congolese') selected @endif value="congolese">Congolese</option>
              <option @if(old('nationality') == 'costa rican') selected @endif value="costa rican">Costa Rican</option>
              <option @if(old('nationality') == 'croatian') selected @endif value="croatian">Croatian</option>
              <option @if(old('nationality') == 'cuban') selected @endif value="cuban">Cuban</option>
              <option @if(old('nationality') == 'cypriot') selected @endif value="cypriot">Cypriot</option>
              <option @if(old('nationality') == 'czech') selected @endif value="czech">Czech</option>
              <option @if(old('nationality') == 'danish') selected @endif value="danish">Danish</option>
              <option @if(old('nationality') == 'djibouti') selected @endif value="djibouti">Djibouti</option>
              <option @if(old('nationality') == 'dominican') selected @endif value="dominican">Dominican</option>
              <option @if(old('nationality') == 'dutch') selected @endif value="dutch">Dutch</option>
              <option @if(old('nationality') == 'east timorese') selected @endif value="east timorese">East Timorese</option>
              <option @if(old('nationality') == 'ecuadorean') selected @endif value="ecuadorean">Ecuadorean</option>
              <option @if(old('nationality') == 'egyptian') selected @endif value="egyptian">Egyptian</option>
              <option @if(old('nationality') == 'emirian') selected @endif value="emirian">Emirian</option>
              <option @if(old('nationality') == 'equatorial guinean') selected @endif value="equatorial guinean">Equatorial Guinean</option>
              <option @if(old('nationality') == 'eritrean') selected @endif value="eritrean">Eritrean</option>
              <option @if(old('nationality') == 'estonian') selected @endif value="estonian">Estonian</option>
              <option @if(old('nationality') == 'ethiopian') selected @endif value="ethiopian">Ethiopian</option>
              <option @if(old('nationality') == 'fijian') selected @endif value="fijian">Fijian</option>
              <option @if(old('nationality') == 'filipino') selected @endif value="filipino">Filipino</option>
              <option @if(old('nationality') == 'finnish') selected @endif value="finnish">Finnish</option>
              <option @if(old('nationality') == 'french') selected @endif value="french">French</option>
              <option @if(old('nationality') == 'gabonese') selected @endif value="gabonese">Gabonese</option>
              <option @if(old('nationality') == 'gambian') selected @endif value="gambian">Gambian</option>
              <option @if(old('nationality') == 'georgian') selected @endif value="georgian">Georgian</option>
              <option @if(old('nationality') == 'german') selected @endif value="german">German</option>
              <option @if(old('nationality') == 'ghanaian') selected @endif value="ghanaian">Ghanaian</option>
              <option @if(old('nationality') == 'greek') selected @endif value="greek">Greek</option>
              <option @if(old('nationality') == 'grenadian') selected @endif value="grenadian">Grenadian</option>
              <option @if(old('nationality') == 'guatemalan') selected @endif value="guatemalan">Guatemalan</option>
              <option @if(old('nationality') == 'mguinea-bissauanale') selected @endif value="guinea-bissauan">Guinea-Bissauan</option>
              <option @if(old('nationality') == 'guinean') selected @endif value="guinean">Guinean</option>
              <option @if(old('nationality') == 'guyanese') selected @endif value="guyanese">Guyanese</option>
              <option @if(old('nationality') == 'haitian') selected @endif value="haitian">Haitian</option>
              <option @if(old('nationality') == 'herzegovinian') selected @endif value="herzegovinian">Herzegovinian</option>
              <option @if(old('nationality') == 'honduran') selected @endif value="honduran">Honduran</option>
              <option @if(old('nationality') == 'hungarian') selected @endif value="hungarian">Hungarian</option>
              <option @if(old('nationality') == 'icelander') selected @endif value="icelander">Icelander</option>
              <option @if(old('nationality') == 'indian') selected @endif value="indian">Indian</option>
              <option @if(old('nationality') == 'indonesian') selected @endif value="indonesian">Indonesian</option>
              <option @if(old('nationality') == 'iranian') selected @endif value="iranian">Iranian</option>
              <option @if(old('nationality') == 'iraqi') selected @endif value="iraqi">Iraqi</option>
              <option @if(old('nationality') == 'irish') selected @endif value="irish">Irish</option>
              <option @if(old('nationality') == 'israeli') selected @endif value="israeli">Israeli</option>
              <option @if(old('nationality') == 'italian') selected @endif value="italian">Italian</option>
              <option @if(old('nationality') == 'ivorian') selected @endif value="ivorian">Ivorian</option>
              <option @if(old('nationality') == 'jamaican') selected @endif value="jamaican">Jamaican</option>
              <option @if(old('nationality') == 'japanese') selected @endif value="japanese">Japanese</option>
              <option @if(old('nationality') == 'jordanian') selected @endif value="jordanian">Jordanian</option>
              <option @if(old('nationality') == 'kazakhstani') selected @endif value="kazakhstani">Kazakhstani</option>
              <option @if(old('nationality') == 'kenyan') selected @endif value="kenyan">Kenyan</option>
              <option @if(old('nationality') == 'kittian and nevisian') selected @endif value="kittian and nevisian">Kittian and Nevisian</option>
              <option @if(old('nationality') == 'kuwaiti') selected @endif value="kuwaiti">Kuwaiti</option>
              <option @if(old('nationality') == 'kyrgyz') selected @endif value="kyrgyz">Kyrgyz</option>
              <option @if(old('nationality') == 'laotian') selected @endif value="laotian">Laotian</option>
              <option @if(old('nationality') == 'latvian') selected @endif value="latvian">Latvian</option>
              <option @if(old('nationality') == 'lebanese') selected @endif value="lebanese">Lebanese</option>
              <option @if(old('nationality') == 'liberian') selected @endif value="liberian">Liberian</option>
              <option @if(old('nationality') == 'libyan') selected @endif value="libyan">Libyan</option>
              <option @if(old('nationality') == 'liechtensteiner') selected @endif value="liechtensteiner">Liechtensteiner</option>
              <option @if(old('nationality') == 'macedonian') selected @endif value="lithuanian">Lithuanian</option>
              <option @if(old('nationality') == 'luxembourger') selected @endif value="luxembourger">Luxembourger</option>
              <option @if(old('nationality') == 'macedonian') selected @endif value="macedonian">Macedonian</option>
              <option @if(old('nationality') == 'malagasy') selected @endif value="malagasy">Malagasy</option>
              <option @if(old('nationality') == 'malawian') selected @endif value="malawian">Malawian</option>
              <option @if(old('nationality') == 'malaysian') selected @endif value="malaysian">Malaysian</option>
              <option @if(old('nationality') == 'maldivan') selected @endif value="maldivan">Maldivan</option>
              <option @if(old('nationality') == 'malian') selected @endif value="malian">Malian</option>
              <option @if(old('nationality') == 'maltese') selected @endif value="maltese">Maltese</option>
              <option @if(old('nationality') == 'marshallese') selected @endif value="marshallese">Marshallese</option>
              <option @if(old('nationality') == 'mauritanian') selected @endif value="mauritanian">Mauritanian</option>
              <option @if(old('nationality') == 'mauritian') selected @endif value="mauritian">Mauritian</option>
              <option @if(old('nationality') == 'mexican') selected @endif value="mexican">Mexican</option>
              <option @if(old('nationality') == 'micronesian') selected @endif value="micronesian">Micronesian</option>
              <option @if(old('nationality') == 'moldovan') selected @endif value="moldovan">Moldovan</option>
              <option @if(old('nationality') == 'monacan') selected @endif value="monacan">Monacan</option>
              <option @if(old('nationality') == 'mongolian') selected @endif value="mongolian">Mongolian</option>
              <option @if(old('nationality') == 'moroccan') selected @endif value="moroccan">Moroccan</option>
              <option @if(old('nationality') == 'mosotho') selected @endif value="mosotho">Mosotho</option>
              <option @if(old('nationality') == 'motswana') selected @endif value="motswana">Motswana</option>
              <option @if(old('nationality') == 'mozambican') selected @endif value="mozambican">Mozambican</option>
              <option @if(old('nationality') == 'namibian') selected @endif value="namibian">Namibian</option>
              <option @if(old('nationality') == 'nauruan') selected @endif value="nauruan">Nauruan</option>
              <option @if(old('nationality') == 'nepalese') selected @endif value="nepalese">Nepalese</option>
              <option @if(old('nationality') == 'new zealander') selected @endif value="new zealander">New Zealander</option>
              <option @if(old('nationality') == 'ni-vanuatu') selected @endif value="ni-vanuatu">Ni-Vanuatu</option>
              <option @if(old('nationality') == 'nicaraguan') selected @endif value="nicaraguan">Nicaraguan</option>
              <option @if(old('nationality') == 'nigerien') selected @endif value="nigerien">Nigerien</option>
              <option @if(old('nationality') == 'north korean') selected @endif value="north korean">North Korean</option>
              <option @if(old('nationality') == 'northern irish') selected @endif value="northern irish">Northern Irish</option>
              <option @if(old('nationality') == 'norwegian') selected @endif value="norwegian">Norwegian</option>
              <option @if(old('nationality') == 'omani') selected @endif value="omani">Omani</option>
              <option @if(old('nationality') == 'pakistani') selected @endif value="pakistani">Pakistani</option>
              <option @if(old('nationality') == 'palauan') selected @endif value="palauan">Palauan</option>
              <option @if(old('nationality') == 'panamanian') selected @endif value="panamanian">Panamanian</option>
              <option @if(old('nationality') == 'papua new guinean') selected @endif value="papua new guinean">Papua New Guinean</option>
              <option @if(old('nationality') == 'paraguayan') selected @endif value="paraguayan">Paraguayan</option>
              <option @if(old('nationality') == 'peruvian') selected @endif value="peruvian">Peruvian</option>
              <option @if(old('nationality') == 'polish') selected @endif value="polish">Polish</option>
              <option @if(old('nationality') == 'portuguese') selected @endif value="portuguese">Portuguese</option>
              <option @if(old('nationality') == 'qatari') selected @endif value="qatari">Qatari</option>
              <option @if(old('nationality') == 'romanian') selected @endif value="romanian">Romanian</option>
              <option @if(old('nationality') == 'russian') selected @endif value="russian">Russian</option>
              <option @if(old('nationality') == 'rwandan') selected @endif value="rwandan">Rwandan</option>
              <option @if(old('nationality') == 'saint') selected @endif value="saint lucian">Saint Lucian</option>
              <option @if(old('nationality') == 'salvadoran') selected @endif value="salvadoran">Salvadoran</option>
              <option @if(old('nationality') == 'samoan') selected @endif value="samoan">Samoan</option>
              <option @if(old('nationality') == 'san marinese') selected @endif value="san marinese">San Marinese</option>
              <option @if(old('nationality') == 'sao tomean') selected @endif value="sao tomean">Sao Tomean</option>
              <option @if(old('nationality') == 'saudi') selected @endif value="saudi">Saudi</option>
              <option @if(old('nationality') == 'scottish') selected @endif value="scottish">Scottish</option>
              <option @if(old('nationality') == 'senegalese') selected @endif value="senegalese">Senegalese</option>
              <option @if(old('nationality') == 'serbian') selected @endif value="serbian">Serbian</option>
              <option @if(old('nationality') == 'seychellois') selected @endif value="seychellois">Seychellois</option>
              <option @if(old('nationality') == 'sierra leonean') selected @endif value="sierra leonean">Sierra Leonean</option>
              <option @if(old('nationality') == 'singaporean') selected @endif value="singaporean">Singaporean</option>
              <option @if(old('nationality') == 'slovakian') selected @endif value="slovakian">Slovakian</option>
              <option @if(old('nationality') == 'slovenian') selected @endif value="slovenian">Slovenian</option>
              <option @if(old('nationality') == 'solomon islander') selected @endif value="solomon islander">Solomon Islander</option>
              <option @if(old('nationality') == 'somali') selected @endif value="somali">Somali</option>
              <option @if(old('nationality') == 'south african') selected @endif value="south african">South African</option>
              <option @if(old('nationality') == 'south korean') selected @endif value="south korean">South Korean</option>
              <option @if(old('nationality') == 'spanish') selected @endif value="spanish">Spanish</option>
              <option @if(old('nationality') == 'sri lankan') selected @endif value="sri lankan">Sri Lankan</option>
              <option @if(old('nationality') == 'sudanese') selected @endif value="sudanese">Sudanese</option>
              <option @if(old('nationality') == 'surinamer') selected @endif value="surinamer">Surinamer</option>
              <option @if(old('nationality') == 'swazi') selected @endif value="swazi">Swazi</option>
              <option @if(old('nationality') == 'swedish') selected @endif value="swedish">Swedish</option>
              <option @if(old('nationality') == 'swiss') selected @endif value="swiss">Swiss</option>
              <option @if(old('nationality') == 'syrian') selected @endif value="syrian">Syrian</option>
              <option @if(old('nationality') == 'taiwanese') selected @endif value="taiwanese">Taiwanese</option>
              <option @if(old('nationality') == 'tajik') selected @endif value="tajik">Tajik</option>
              <option @if(old('nationality') == 'tanzanian') selected @endif value="tanzanian">Tanzanian</option>
              <option @if(old('nationality') == 'thai') selected @endif value="thai">Thai</option>
              <option @if(old('nationality') == 'togolese') selected @endif value="togolese">Togolese</option>
              <option @if(old('nationality') == 'tongan') selected @endif value="tongan">Tongan</option>
              <option @if(old('nationality') == 'trinidadian or tobagonian') selected @endif value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
              <option @if(old('nationality') == 'tunisian') selected @endif value="tunisian">Tunisian</option>
              <option @if(old('nationality') == 'turkish') selected @endif value="turkish">Turkish</option>
              <option @if(old('nationality') == 'tuvaluan') selected @endif value="tuvaluan">Tuvaluan</option>
              <option @if(old('nationality') == 'ugandan') selected @endif value="ugandan">Ugandan</option>
              <option @if(old('nationality') == 'ukrainian') selected @endif value="ukrainian">Ukrainian</option>
              <option @if(old('nationality') == 'uruguayan') selected @endif value="uruguayan">Uruguayan</option>
              <option @if(old('nationality') == 'uzbekistani') selected @endif value="uzbekistani">Uzbekistani</option>
              <option @if(old('nationality') == 'venezuelan') selected @endif value="venezuelan">Venezuelan</option>
              <option @if(old('nationality') == 'vietnamese') selected @endif value="vietnamese">Vietnamese</option>
              <option @if(old('nationality') == 'welsh') selected @endif value="welsh">Welsh</option>
              <option @if(old('nationality') == 'yemenite') selected @endif value="yemenite">Yemenite</option>
              <option @if(old('nationality') == 'zambian') selected @endif value="zambian">Zambian</option>
          </select>  
          
         @error('nationality') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-citizenship">Citizenship</label>
          <select type="text" name="citizenship" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
             <option value="">Select</option>
             <option @if(old('citizenship') == 'Zimbabwe') selected @endif value="Zimbabwe">Zimbabwe</option>
             <option @if(old('citizenship') == 'Afganistan') selected @endif value="Afganistan">Afghanistan</option>
             <option @if(old('citizenship') == 'Albania') selected @endif value="Albania">Albania</option>
             <option @if(old('citizenship') == 'Algeria') selected @endif value="Algeria">Algeria</option>
             <option @if(old('citizenship') == 'American Samoa') selected @endif value="American Samoa">American Samoa</option>
             <option @if(old('citizenship') == 'Andorra') selected @endif value="Andorra">Andorra</option>
             <option @if(old('citizenship') == 'Angola') selected @endif value="Angola">Angola</option>
             <option @if(old('citizenship') == 'Anguilla') selected @endif value="Anguilla">Anguilla</option>
             <option @if(old('citizenship') == 'Antigua & Barbuda') selected @endif value="Antigua & Barbuda">Antigua & Barbuda</option>
             <option @if(old('citizenship') == 'Argentina') selected @endif value="Argentina">Argentina</option>
             <option @if(old('citizenship') == 'Armenia') selected @endif value="Armenia">Armenia</option>
             <option @if(old('citizenship') == 'Aruba') selected @endif value="Aruba">Aruba</option>
             <option @if(old('citizenship') == 'Australia') selected @endif value="Australia">Australia</option>
             <option @if(old('citizenship') == 'Austria') selected @endif value="Austria">Austria</option>
             <option @if(old('citizenship') == 'Azerbaijan') selected @endif value="Azerbaijan">Azerbaijan</option>
             <option @if(old('citizenship') == 'Bahamas') selected @endif value="Bahamas">Bahamas</option>
             <option @if(old('citizenship') == 'Bahrain') selected @endif value="Bahrain">Bahrain</option>
             <option @if(old('citizenship') == 'Bangladesh') selected @endif value="Bangladesh">Bangladesh</option>
             <option @if(old('citizenship') == 'Barbados') selected @endif value="Barbados">Barbados</option>
             <option @if(old('citizenship') == 'Belarus') selected @endif value="Belarus">Belarus</option>
             <option @if(old('citizenship') == 'Belgium') selected @endif value="Belgium">Belgium</option>
             <option @if(old('citizenship') == 'Belize') selected @endif value="Belize">Belize</option>
             <option @if(old('citizenship') == 'Benin') selected @endif value="Benin">Benin</option>
             <option @if(old('citizenship') == 'Bermuda') selected @endif value="Bermuda">Bermuda</option>
             <option @if(old('citizenship') == 'Bhutan') selected @endif value="Bhutan">Bhutan</option>
             <option @if(old('citizenship') == 'Bolivia') selected @endif value="Bolivia">Bolivia</option>
             <option @if(old('citizenship') == 'Bonaire') selected @endif value="Bonaire">Bonaire</option>
             <option @if(old('citizenship') == 'Bosnia & Herzegovina') selected @endif value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
             <option @if(old('citizenship') == 'Botswana') selected @endif value="Botswana">Botswana</option>
             <option @if(old('citizenship') == 'Brazil') selected @endif value="Brazil">Brazil</option>
             <option @if(old('citizenship') == 'British Indian Ocean Ter') selected @endif value="British Indian Ocean Ter">British Indian Ocean Ter</option>
             <option @if(old('citizenship') == 'Brunei') selected @endif value="Brunei">Brunei</option>
             <option @if(old('citizenship') == 'Bulgaria') selected @endif value="Bulgaria">Bulgaria</option>
             <option @if(old('citizenship') == 'Burkina Faso') selected @endif value="Burkina Faso">Burkina Faso</option>
             <option @if(old('citizenship') == 'Burundi') selected @endif value="Burundi">Burundi</option>
             <option @if(old('citizenship') == 'Cambodia') selected @endif value="Cambodia">Cambodia</option>
             <option @if(old('citizenship') == 'Cameroon') selected @endif value="Cameroon">Cameroon</option>
             <option @if(old('citizenship') == 'Canada') selected @endif value="Canada">Canada</option>
             <option @if(old('citizenship') == 'Canary Islands') selected @endif value="Canary Islands">Canary Islands</option>
             <option @if(old('citizenship') == 'Cape Verde') selected @endif value="Cape Verde">Cape Verde</option>
             <option @if(old('citizenship') == 'Cayman Islands') selected @endif value="Cayman Islands">Cayman Islands</option>
             <option @if(old('citizenship') == 'Central African Republic') selected @endif value="Central African Republic">Central African Republic</option>
             <option @if(old('citizenship') == 'Chad') selected @endif value="Chad">Chad</option>
             <option @if(old('citizenship') == 'Channel Islands') selected @endif value="Channel Islands">Channel Islands</option>
             <option @if(old('citizenship') == 'Chile') selected @endif value="Chile">Chile</option>
             <option @if(old('citizenship') == 'China') selected @endif value="China">China</option>
             <option @if(old('citizenship') == 'Christmas Island') selected @endif value="Christmas Island">Christmas Island</option>
             <option @if(old('citizenship') == 'Cocos Island') selected @endif value="Cocos Island">Cocos Island</option>
             <option @if(old('citizenship') == 'Colombia') selected @endif value="Colombia">Colombia</option>
             <option @if(old('citizenship') == 'Comoros') selected @endif value="Comoros">Comoros</option>
             <option @if(old('citizenship') == 'Congo') selected @endif value="Congo">Congo</option>
             <option @if(old('citizenship') == 'Cook Islands') selected @endif value="Cook Islands">Cook Islands</option>
             <option @if(old('citizenship') == 'Costa Rica') selected @endif value="Costa Rica">Costa Rica</option>
             <option @if(old('citizenship') == 'Cote DIvoire') selected @endif value="Cote DIvoire">Cote DIvoire</option>
             <option @if(old('citizenship') == 'Croatia') selected @endif value="Croatia">Croatia</option>
             <option @if(old('citizenship') == 'Cuba') selected @endif value="Cuba">Cuba</option>
             <option @if(old('citizenship') == 'Curaco') selected @endif value="Curaco">Curacao</option>
             <option @if(old('citizenship') == 'Cyprus') selected @endif value="Cyprus">Cyprus</option>
             <option @if(old('citizenship') == 'Czech Republic') selected @endif value="Czech Republic">Czech Republic</option>
             <option @if(old('citizenship') == 'Denmark') selected @endif value="Denmark">Denmark</option>
             <option @if(old('citizenship') == 'Djibouti') selected @endif value="Djibouti">Djibouti</option>
             <option @if(old('citizenship') == 'Dominica') selected @endif value="Dominica">Dominica</option>
             <option @if(old('citizenship') == 'Dominican Republic') selected @endif value="Dominican Republic">Dominican Republic</option>
             <option @if(old('citizenship') == 'East Timor') selected @endif value="East Timor">East Timor</option>
             <option @if(old('citizenship') == 'Ecuador') selected @endif value="Ecuador">Ecuador</option>
             <option @if(old('citizenship') == 'Egypt') selected @endif value="Egypt">Egypt</option>
             <option @if(old('citizenship') == 'El Salvador') selected @endif value="El Salvador">El Salvador</option>
             <option @if(old('citizenship') == 'Equatorial Guinea') selected @endif value="Equatorial Guinea">Equatorial Guinea</option>
             <option @if(old('citizenship') == 'Eritrea') selected @endif value="Eritrea">Eritrea</option>
             <option @if(old('citizenship') == 'Estonia') selected @endif value="Estonia">Estonia</option>
             <option @if(old('citizenship') == 'Ethiopia') selected @endif value="Ethiopia">Ethiopia</option>
             <option @if(old('citizenship') == 'Falkland Islands') selected @endif value="Falkland Islands">Falkland Islands</option>
             <option @if(old('citizenship') == 'Faroe Islands') selected @endif value="Faroe Islands">Faroe Islands</option>
             <option @if(old('citizenship') == 'Fiji') selected @endif value="Fiji">Fiji</option>
             <option @if(old('citizenship') == 'Finland') selected @endif value="Finland">Finland</option>
             <option @if(old('citizenship') == 'France') selected @endif value="France">France</option>
             <option @if(old('citizenship') == 'French Guiana') selected @endif value="French Guiana">French Guiana</option>
             <option @if(old('citizenship') == 'French Polynesia') selected @endif value="French Polynesia">French Polynesia</option>
             <option @if(old('citizenship') == 'French Southern Ter') selected @endif value="French Southern Ter">French Southern Ter</option>
             <option @if(old('citizenship') == 'Gabon') selected @endif value="Gabon">Gabon</option>
             <option @if(old('citizenship') == 'Gambia') selected @endif value="Gambia">Gambia</option>
             <option @if(old('citizenship') == 'Georgia') selected @endif value="Georgia">Georgia</option>
             <option @if(old('citizenship') == 'Germany') selected @endif value="Germany">Germany</option>
             <option @if(old('citizenship') == 'Ghana') selected @endif value="Ghana">Ghana</option>
             <option @if(old('citizenship') == 'Gibraltar') selected @endif value="Gibraltar">Gibraltar</option>
             <option @if(old('citizenship') == 'Great Britain') selected @endif value="Great Britain">Great Britain</option>
             <option @if(old('citizenship') == 'Greece') selected @endif value="Greece">Greece</option>
             <option @if(old('citizenship') == 'Greenland') selected @endif value="Greenland">Greenland</option>
             <option @if(old('citizenship') == 'Grenada') selected @endif value="Grenada">Grenada</option>
             <option @if(old('citizenship') == 'Guadeloupe') selected @endif value="Guadeloupe">Guadeloupe</option>
             <option @if(old('citizenship') == 'Guam') selected @endif value="Guam">Guam</option>
             <option @if(old('citizenship') == 'Guatemala') selected @endif value="Guatemala">Guatemala</option>
             <option @if(old('citizenship') == 'Guinea') selected @endif value="Guinea">Guinea</option>
             <option @if(old('citizenship') == 'Guyana') selected @endif value="Guyana">Guyana</option>
             <option @if(old('citizenship') == 'Haiti') selected @endif value="Haiti">Haiti</option>
             <option @if(old('citizenship') == 'Hawaii') selected @endif value="Hawaii">Hawaii</option>
             <option @if(old('citizenship') == 'Honduras') selected @endif value="Honduras">Honduras</option>
             <option @if(old('citizenship') == 'Hong Kong') selected @endif value="Hong Kong">Hong Kong</option>
             <option @if(old('citizenship') == 'Hungary') selected @endif value="Hungary">Hungary</option>
             <option @if(old('citizenship') == 'Iceland') selected @endif value="Iceland">Iceland</option>
             <option @if(old('citizenship') == 'Indonesia') selected @endif value="Indonesia">Indonesia</option>
             <option @if(old('citizenship') == 'India') selected @endif value="India">India</option>
             <option @if(old('citizenship') == 'Iran') selected @endif value="Iran">Iran</option>
             <option @if(old('citizenship') == 'Iraq') selected @endif value="Iraq">Iraq</option>
             <option @if(old('citizenship') == 'Ireland') selected @endif value="Ireland">Ireland</option>
             <option @if(old('citizenship') == 'Isle of Man') selected @endif value="Isle of Man">Isle of Man</option>
             <option @if(old('citizenship') == 'Israel') selected @endif value="Israel">Israel</option>
             <option @if(old('citizenship') == 'Italy') selected @endif value="Italy">Italy</option>
             <option @if(old('citizenship') == 'Jamaica') selected @endif value="Jamaica">Jamaica</option>
             <option @if(old('citizenship') == 'Japan') selected @endif value="Japan">Japan</option>
             <option @if(old('citizenship') == 'Jordan') selected @endif value="Jordan">Jordan</option>
             <option @if(old('citizenship') == 'Kazakhstan') selected @endif value="Kazakhstan">Kazakhstan</option>
             <option @if(old('citizenship') == 'Kenya') selected @endif value="Kenya">Kenya</option>
             <option @if(old('citizenship') == 'Kiribati') selected @endif value="Kiribati">Kiribati</option>
             <option @if(old('citizenship') == 'Korea North') selected @endif value="Korea North">Korea North</option>
             <option @if(old('citizenship') == 'Korea South') selected @endif value="Korea South">Korea South</option>
             <option @if(old('citizenship') == 'Kuwait') selected @endif value="Kuwait">Kuwait</option>
             <option @if(old('citizenship') == 'Kyrgyzstan') selected @endif value="Kyrgyzstan">Kyrgyzstan</option>
             <option @if(old('citizenship') == 'Laos') selected @endif value="Laos">Laos</option>
             <option @if(old('citizenship') == 'Latvia') selected @endif value="Latvia">Latvia</option>
             <option @if(old('citizenship') == 'Lebanon') selected @endif value="Lebanon">Lebanon</option>
             <option @if(old('citizenship') == 'Lesotho') selected @endif value="Lesotho">Lesotho</option>
             <option @if(old('citizenship') == 'Liberia') selected @endif value="Liberia">Liberia</option>
             <option @if(old('citizenship') == 'Libya') selected @endif value="Libya">Libya</option>
             <option @if(old('citizenship') == 'Liechtenstein') selected @endif value="Liechtenstein">Liechtenstein</option>
             <option @if(old('citizenship') == 'Lithuania') selected @endif value="Lithuania">Lithuania</option>
             <option @if(old('citizenship') == 'Luxembourg') selected @endif value="Luxembourg">Luxembourg</option>
             <option @if(old('citizenship') == 'Macau') selected @endif value="Macau">Macau</option>
             <option @if(old('citizenship') == 'Macedonia') selected @endif value="Macedonia">Macedonia</option>
             <option @if(old('citizenship') == 'Madagascar') selected @endif value="Madagascar">Madagascar</option>
             <option @if(old('citizenship') == 'Malaysia') selected @endif value="Malaysia">Malaysia</option>
             <option @if(old('citizenship') == 'Malawi') selected @endif value="Malawi">Malawi</option>
             <option @if(old('citizenship') == 'Maldives') selected @endif value="Maldives">Maldives</option>
             <option @if(old('citizenship') == 'Mali') selected @endif value="Mali">Mali</option>
             <option @if(old('citizenship') == 'Malta') selected @endif value="Malta">Malta</option>
             <option @if(old('citizenship') == 'Marshall Islands') selected @endif value="Marshall Islands">Marshall Islands</option>
             <option @if(old('citizenship') == 'Martinique') selected @endif value="Martinique">Martinique</option>
             <option @if(old('citizenship') == 'Mauritania') selected @endif value="Mauritania">Mauritania</option>
             <option @if(old('citizenship') == 'Mauritius') selected @endif value="Mauritius">Mauritius</option>
             <option @if(old('citizenship') == 'Mayotte') selected @endif value="Mayotte">Mayotte</option>
             <option @if(old('citizenship') == 'Mexico') selected @endif value="Mexico">Mexico</option>
             <option @if(old('citizenship') == 'Midway Islands') selected @endif value="Midway Islands">Midway Islands</option>
             <option @if(old('citizenship') == 'Moldova') selected @endif value="Moldova">Moldova</option>
             <option @if(old('citizenship') == 'Monaco') selected @endif value="Monaco">Monaco</option>
             <option @if(old('citizenship') == 'Mongolia') selected @endif value="Mongolia">Mongolia</option>
             <option @if(old('citizenship') == 'Montserrat') selected @endif value="Montserrat">Montserrat</option>
             <option @if(old('citizenship') == 'Morocco') selected @endif value="Morocco">Morocco</option>
             <option @if(old('citizenship') == 'Mozambique') selected @endif value="Mozambique">Mozambique</option>
             <option @if(old('citizenship') == 'Myanmar') selected @endif value="Myanmar">Myanmar</option>
             <option @if(old('citizenship') == 'Namibia') selected @endif value="Namibia">Namibia</option>
             <option @if(old('citizenship') == 'Nauru') selected @endif value="Nauru">Nauru</option>
             <option @if(old('citizenship') == 'Nepal') selected @endif value="Nepal">Nepal</option>
             <option @if(old('citizenship') == 'Netherland Antilles') selected @endif value="Netherland Antilles">Netherland Antilles</option>
             <option @if(old('citizenship') == 'Netherlands') selected @endif value="Netherlands">Netherlands (Holland, Europe)</option>
             <option @if(old('citizenship') == 'Nevis') selected @endif value="Nevis">Nevis</option>
             <option @if(old('citizenship') == 'New Caledonia') selected @endif value="New Caledonia">New Caledonia</option>
             <option @if(old('citizenship') == 'New Zealand') selected @endif value="New Zealand">New Zealand</option>
             <option @if(old('citizenship') == 'Nicaragua') selected @endif value="Nicaragua">Nicaragua</option>
             <option @if(old('citizenship') == 'Niger') selected @endif value="Niger">Niger</option>
             <option @if(old('citizenship') == 'Nigeria') selected @endif value="Nigeria">Nigeria</option>
             <option @if(old('citizenship') == 'Niue') selected @endif value="Niue">Niue</option>
             <option @if(old('citizenship') == 'Norfolk Island') selected @endif value="Norfolk Island">Norfolk Island</option>
             <option @if(old('citizenship') == 'Norway') selected @endif value="Norway">Norway</option>
             <option @if(old('citizenship') == 'Oman') selected @endif value="Oman">Oman</option>
             <option @if(old('citizenship') == 'Pakistan') selected @endif value="Pakistan">Pakistan</option>
             <option @if(old('citizenship') == 'Palau Island') selected @endif value="Palau Island">Palau Island</option>
             <option @if(old('citizenship') == 'Palestine') selected @endif value="Palestine">Palestine</option>
             <option @if(old('citizenship') == 'Panama') selected @endif value="Panama">Panama</option>
             <option @if(old('citizenship') == 'Papua New Guinea') selected @endif value="Papua New Guinea">Papua New Guinea</option>
             <option @if(old('citizenship') == 'Paraguay') selected @endif value="Paraguay">Paraguay</option>
             <option @if(old('citizenship') == 'Peru') selected @endif value="Peru">Peru</option>
             <option @if(old('citizenship') == 'Phillipines') selected @endif value="Phillipines">Philippines</option>
             <option @if(old('citizenship') == 'Pitcairn Island') selected @endif value="Pitcairn Island">Pitcairn Island</option>
             <option @if(old('citizenship') == 'Poland') selected @endif value="Poland">Poland</option>
             <option @if(old('citizenship') == 'Portugal') selected @endif value="Portugal">Portugal</option>
             <option @if(old('citizenship') == 'Puerto Rico') selected @endif value="Puerto Rico">Puerto Rico</option>
             <option @if(old('citizenship') == 'Qatar') selected @endif value="Qatar">Qatar</option>
             <option @if(old('citizenship') == 'Republic of Montenegro') selected @endif value="Republic of Montenegro">Republic of Montenegro</option>
             <option @if(old('citizenship') == 'Republic of Serbia') selected @endif value="Republic of Serbia">Republic of Serbia</option>
             <option @if(old('citizenship') == 'Reunion') selected @endif value="Reunion">Reunion</option>
             <option @if(old('citizenship') == 'Romania') selected @endif value="Romania">Romania</option>
             <option @if(old('citizenship') == 'Russia') selected @endif value="Russia">Russia</option>
             <option @if(old('citizenship') == 'Rwanda') selected @endif value="Rwanda">Rwanda</option>
             <option @if(old('citizenship') == 'St Barthelemy') selected @endif value="St Barthelemy">St Barthelemy</option>
             <option @if(old('citizenship') == 'St Eustatius') selected @endif value="St Eustatius">St Eustatius</option>
             <option @if(old('citizenship') == 'St Helena') selected @endif value="St Helena">St Helena</option>
             <option @if(old('citizenship') == 'St Kitts-Nevis') selected @endif value="St Kitts-Nevis">St Kitts-Nevis</option>
             <option @if(old('citizenship') == 'St Lucia') selected @endif value="St Lucia">St Lucia</option>
             <option @if(old('citizenship') == 'St Maarten') selected @endif value="St Maarten">St Maarten</option>
             <option @if(old('citizenship') == 'St Pierre & Miquelon') selected @endif value="St Pierre & Miquelon">St Pierre & Miquelon</option>
             <option @if(old('citizenship') == 'St Vincent & Grenadines') selected @endif value="St Vincent & Grenadines">St Vincent & Grenadines</option>
             <option @if(old('citizenship') == 'Saipan') selected @endif value="Saipan">Saipan</option>
             <option @if(old('citizenship') == 'Samoa') selected @endif value="Samoa">Samoa</option>
             <option @if(old('citizenship') == 'Samoa American') selected @endif value="Samoa American">Samoa American</option>
             <option @if(old('citizenship') == 'San Marino') selected @endif value="San Marino">San Marino</option>
             <option @if(old('citizenship') == 'Sao Tome & Principe') selected @endif value="Sao Tome & Principe">Sao Tome & Principe</option>
             <option @if(old('citizenship') == 'Saudi Arabia') selected @endif value="Saudi Arabia">Saudi Arabia</option>
             <option @if(old('citizenship') == 'Senegal') selected @endif value="Senegal">Senegal</option>
             <option @if(old('citizenship') == 'Seychelles') selected @endif value="Seychelles">Seychelles</option>
             <option @if(old('citizenship') == 'Sierra Leone') selected @endif value="Sierra Leone">Sierra Leone</option>
             <option @if(old('citizenship') == 'Singapore') selected @endif value="Singapore">Singapore</option>
             <option @if(old('citizenship') == 'Slovakia') selected @endif value="Slovakia">Slovakia</option>
             <option @if(old('citizenship') == 'Slovenia') selected @endif value="Slovenia">Slovenia</option>
             <option @if(old('citizenship') == 'Solomon Islands') selected @endif value="Solomon Islands">Solomon Islands</option>
             <option @if(old('citizenship') == 'Somalia') selected @endif value="Somalia">Somalia</option>
             <option @if(old('citizenship') == 'South Africa') selected @endif value="South Africa">South Africa</option>
             <option @if(old('citizenship') == 'Spain') selected @endif value="Spain">Spain</option>
             <option @if(old('citizenship') == 'Sri Lanka') selected @endif value="Sri Lanka">Sri Lanka</option>
             <option @if(old('citizenship') == 'Sudan') selected @endif value="Sudan">Sudan</option>
             <option @if(old('citizenship') == 'Suriname') selected @endif value="Suriname">Suriname</option>
             <option @if(old('citizenship') == 'Swaziland') selected @endif value="Swaziland">Swaziland</option>
             <option @if(old('citizenship') == 'Sweden') selected @endif value="Sweden">Sweden</option>
             <option @if(old('citizenship') == 'Switzerland') selected @endif value="Switzerland">Switzerland</option>
             <option @if(old('citizenship') == 'Syria') selected @endif value="Syria">Syria</option>
             <option @if(old('citizenship') == 'Tahiti') selected @endif value="Tahiti">Tahiti</option>
             <option @if(old('citizenship') == 'Taiwan') selected @endif value="Taiwan">Taiwan</option>
             <option @if(old('citizenship') == 'Tajikistan') selected @endif value="Tajikistan">Tajikistan</option>
             <option @if(old('citizenship') == 'Tanzania') selected @endif value="Tanzania">Tanzania</option>
             <option @if(old('citizenship') == 'Thailand') selected @endif value="Thailand">Thailand</option>
             <option @if(old('citizenship') == 'Togo') selected @endif value="Togo">Togo</option>
             <option @if(old('citizenship') == 'Tokelau') selected @endif value="Tokelau">Tokelau</option>
             <option @if(old('citizenship') == 'Tonga') selected @endif value="Tonga">Tonga</option>
             <option @if(old('citizenship') == 'Trinidad & Tobago') selected @endif value="Trinidad & Tobago">Trinidad & Tobago</option>
             <option @if(old('citizenship') == 'Tunisia') selected @endif value="Tunisia">Tunisia</option>
             <option @if(old('citizenship') == 'Turkey') selected @endif value="Turkey">Turkey</option>
             <option @if(old('citizenship') == 'Turkmenistan') selected @endif value="Turkmenistan">Turkmenistan</option>
             <option @if(old('citizenship') == 'Turks & Caicos Is') selected @endif value="Turks & Caicos Is">Turks & Caicos Is</option>
             <option @if(old('citizenship') == 'Tuvalu') selected @endif value="Tuvalu">Tuvalu</option>
             <option @if(old('citizenship') == 'Uganda') selected @endif value="Uganda">Uganda</option>
             <option @if(old('citizenship') == 'United Kingdom') selected @endif value="United Kingdom">United Kingdom</option>
             <option @if(old('citizenship') == 'Ukraine') selected @endif value="Ukraine">Ukraine</option>
             <option @if(old('citizenship') == 'United Arab Erimates') selected @endif value="United Arab Erimates">United Arab Emirates</option>
             <option @if(old('citizenship') == 'United States of America') selected @endif value="United States of America">United States of America</option>
             <option @if(old('citizenship') == 'Uraguay') selected @endif value="Uraguay">Uruguay</option>
             <option @if(old('citizenship') == 'Uzbekistan') selected @endif value="Uzbekistan">Uzbekistan</option>
             <option @if(old('citizenship') == 'Vanuatu') selected @endif value="Vanuatu">Vanuatu</option>
             <option @if(old('citizenship') == 'Vatican') selected @endif value="Vatican City State">Vatican City State</option>
             <option @if(old('citizenship') == 'Venezuela') selected @endif value="Venezuela">Venezuela</option>
             <option @if(old('citizenship') == 'Vietnam') selected @endif value="Vietnam">Vietnam</option>
             <option @if(old('citizenship') == 'Virgin Islands (Brit)') selected @endif value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
             <option @if(old('citizenship') == 'Virgin Islands (USA)') selected @endif value="Virgin Islands (USA)">Virgin Islands (USA)</option>
             <option @if(old('citizenship') == 'Wake Island') selected @endif value="Wake Island">Wake Island</option>
             <option @if(old('citizenship') == 'Wallis & Futana Is') selected @endif value="Wallis & Futana Is">Wallis & Futana Is</option>
             <option @if(old('citizenship') == 'Yemen') selected @endif value="Yemen">Yemen</option>
             <option @if(old('citizenship') == 'Zaire') selected @endif value="Zaire">Zaire</option>
             <option @if(old('citizenship') == 'Zambia') selected @endif value="Zambia">Zambia</option>
          </select> 
           
         @error('citizenship') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        
      </div>
      <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-5 pr-5" >
        <div class="md:w-4/20 px-3 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="grid-tittle">Sex</label>
          <select type="text" name="sex" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
            <option value="">Select</option>
            <option @if(old('sex') == 'Male') selected @endif value="Male">Male</option>
            <option @if(old('sex') == 'Female') selected @endif value="Female">Female</option>
           </select> 
           
         @error('sex') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="grid-cohort_id">Session</label>
          <select class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" name="cohort_id">
            <option value="">Select</option>
            @foreach($cohorts as $cohort)
            @if (old('cohort_id') == $cohort->id)
            <option value="{{ $cohort->id }}" selected>{{ $cohort->name }}</option>
            @else
            <option value="{{$cohort->id}}">{{$cohort->name}}</option>
            @endif
            @endforeach
          </select>
          
        </div> 
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2" for="grid-entry_type_id">Type of Entry</label>
          <select class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" name="entry_type_id">
            <option value="">Select</option>
            @foreach($entry_types as $entry_type)
            @if (old('entry_type_id') == $entry_type->id)
            <option value="{{ $entry_type->id }}" selected>{{ $entry_type->type }}</option>
            @else
            <option value="{{$entry_type->id}}">{{$entry_type->type}}</option>
            @endif
            @endforeach
          </select>
          
        </div> 
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-study_program">Study Programme</label>
          <select type="text" name="study_program_id" value="{{ old('study_program') }}" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white"> 
          <option value="">Select</option>           
            @foreach($study_programs as $study_program)
            @if (old('study_program_id') == $study_program->id)
            <option value= "{{$study_program->id}}" selected>{{$study_program->type}}</option>
            @else
            <option value= "{{$study_program->id}}">{{$study_program->type}}</option>
            @endif
            @endforeach
          </select> 
           
         @error('study_program') <span class="error text-red-800">{{ $message }}</span> @enderror 
        </div>
        
  <!-- Insert selected programmes (max = 4) in to the hidden select input using alpinejs.-->
        <div x-data="{ items: [], adding: false }" x-init="$refs.ok.disabled = true">
          <table class="table-fixed mt-8">
            <thead>
              <tr class="bg-blue-800 text-white" >
                <th class="border px-4 py-2">Selceted Programmes</th>
                <th class="border px-4 py-2">Action</th>
              </tr>
            </thead>
            <tbody>
              <template x-for="item in items" :key="item">
                <tr>
                  <td class="border px-4 py-2">
                      <p x-text="item"></p>
                  </td>    
                  <td class="border px-4 py-2">
                  <button class="bg-transparent hover:bg-red-800 text-red-800 font-semibold hover:text-white py-1 px-3 border border-red-500 hover:border-transparent rounded " type="button" @click="items = items.filter(i => i !== item)"><x-zondicon-close class="h-6 w-6"/></button></td>
                </tr>
              </template>
            </tbody>    
          </table>
          <select name="programmes[]" multiple hidden>
            <template x-for="item in items" :key="item">
              <option x-bind:value="item" x-text="item" selected></option>
            </template>            
          </select>
          <div class="flex content-end flex-wrap mt-8">
            <div class="md:w-1/3"></div>
            <button class="bg-transparent ml-10 hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-1 px-3 border border-blue-500 hover:border-transparent rounded self-ends" type="button" @click="adding = true">Choose Programmes</button>
          </div>
          <div x-show.transition="adding" @click.away="adding = false">
            <label for="programme">Programme:</label>
            <select id="programme" x-model="programme" @change="$refs.ok.disabled = programme.length === 0; $refs.ok.disabled = items.length === 4" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
              <option value="">Select</option>
              @foreach($programmes as $programme)
              <option value="{{$programme->name}}">{{$programme->name}}</option>
              @endforeach
            </select>
            <button class="bg-transparent hover:bg-yellow-600 text-yellow-600 font-semibold hover:text-white py-1 px-3 border border-yellow-500 hover:border-transparent rounded " type="button" @click="adding = false">CANCEL</button>
            <button class="bg-transparent hover:bg-green-800 text-green-800 font-semibold hover:text-white py-1 px-3 border border-green-500 hover:border-transparent rounded " type="button" @click="adding = false; items.push(programme); programme = ''; $refs.ok.disabled = true" x-ref="ok">OK</button>
          </div>
        </div>        
        
        <div x-data="{ open: false }">
          <div class="md:flex mb-6 mt-8">
            <label class="inline-flex font-bold uppercase tracking-wide">
              <span class="text-sm  text-blue-800">
                I am A Permanent Resident of Zimbabwe
              </span> 
              <input type="hidden" value="0" name="perm_res_zim">             
              <input class="leading-tigh form-checkbox h-4 w-4 text-green-500 ml-8" type="checkbox"  value="1" name="perm_res_zim" checked @change="open = !open">
            </label>
          </div>                   
          <div x-show="open">
            <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
              <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-permit">Permit</label>
              <input type="text" name="permit" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">  
            </div>                   
          
            <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
              <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-zim_res_period">Period of residence in Zimbabwe</label>
              <input type="text" name="zim_res_period" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 ml-20 leading-tight focus:outline-none focus:bg-white">  
            </div> 
          </div>
        </div>

        <div x-data="{ open: false }">
          <div class="md:flex mb-2">
            <label class="inline-flex font-bold uppercase tracking-wide">
              <span class="text-sm  text-blue-800">
                Any Disabilities
              </span> 
              <input type="hidden" value="0" name="has_dis">             
              <input class="leading-tigh form-checkbox h-4 w-4 text-green-500 ml-8" type="checkbox"  value="1" name="has_dis" @change="open = !open">
            </label>
          </div>  
          
          <div x-show="open">
            <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
              <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-disability_id">Disability</label>
              <select class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" name="disability_id">
                <option value="">Select</option>
                @foreach($disabilities as $disability)
                @if (old('disability_id') == $disability->id)
                <option value= "{{$disability->id}}" selected="">{{$disability->description}}</option>
                @else
                <option value= "{{$disability->id}}">{{$disability->description}}</option>
                @endif
                  @endforeach
              </select>
              
            </div> 
          </div>
        </div> 
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-2" for="grid-birth_certificate_fl">Birth Certificate</label>
          <input type="file" name="birth_certificate_fl" value="{{ old('birth_certificate_fl') }}" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">  
        </div>                     

        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-id_card_fl">National Identity Card</label>
          <input type="file" name="id_card_fl" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" value="{{ old('id_card_fl') }}">  
        </div>                                           

        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-marriage_certificate_fl">Marriage Certificate</label>
          <input type="file" name="marriage_certificate_fl" value="{{ old('marriage_certificate_fl') }}" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">  
        </div>                                                    

        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-cv_fl">Curriculum Vitae</label>
          <input type="file" name="cv_fl" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white" value="{{ old('cv_fl') }}">  
        </div>
        
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-referee_1">1st Reference Name</label>
          <input type="text" value="{{ old('referee_1') }}" name="referee_1" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">  
        </div>     
          
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-ref1_address">1st Reference Address</label>
          <textarea type="text" name="ref1_address" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">{{ old('ref1_address') }}</textarea>
        </div>  
          
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-referee_2">2nd Reference Name</label>
          <input type="text" value="{{ old('referee_2') }}" name="referee_2" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">  
        </div>     
          
        <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
          <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8" for="grid-ref2_address">2nd Reference Address</label>
          <textarea type="text" name="ref2_address" class="appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">{{ old('ref2_address') }}</textarea>
        </div>
      </div>
    </div>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  </div>
</form>   
   
@endsection