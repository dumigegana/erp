<div class="min-w-full">  
  <div class="py-2 flex flex-row mb-4 min-w-full">
    <div class="w-1/6">
          @if(auth()->user()->hasPermission('applicants.admin'))
      <div x-data="{ 'showModal': false }" @eModal.window="showModal = true" @keydown.escape="showModal = false"  x-cloak>
        <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-0 hover:border-transparent rounded "><a href="#" @click.prevent="showModal = true" wire:click="create"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
          <!-- overlay -->
        <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                <!--Dialog-->
          <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg relative text-left " x-show="showModal" @click.away="showModal = false" >
  
            <button class="absolute top-0 right-0 mr-2 text-red-500" type="button" @click="showModal = false">✖</button> 
        {{-- @include('livewire.erp.applicants.create') --}}
          </div>
        </div>
      </div>
      @else    
      <button class="bg-transparent text-gray-400 font-semibold border border-gray-400 py-2 px-2 rounded "><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
       @endif 
    </div>
    
    <div class="relative w-5/6 flex flex-row-reverse ">
      <div class="relative text-gray-800 mr-4 order-4">
        <select wire:model="orderBy" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-1 rounded leading-tight pr-5 pl-2 focus:outline-none focus:bg-white focus:border-yellow-500">
            <option value="fullname">Name</option>
            <option value="student_id">Student ID</option>
            <option value="previous_surname">Maiden name</option>
            <option value="national_id">National ID</option>
            <option value="cell_number">Cell phone</option>
            <option value="home_phone">Home phone</option>
            <option value="birth_cert_no">Birth Certificate #</option>
            <option value="prospective_sponsor">Sponsor</option>
            <option value="email">Email Address</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
            <x-zondicon-cheveron-down class="h-4 w-4"/>
          </div>        
        </div> 
        <div class="relative text-gray-800 mr-4 order-3">
          <select wire:model="orderAsc" class="bg-white border border-blue-100 text-blue-800 py-1 pr-4 pl-2 rounded leading-tight focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none">
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 p-1 text-grey-darker leading-tight mt-1 ml-3">
            <x-zondicon-cheveron-down class="h-4 w-4"/>
          </div>        
        </div>
        <div class="relative text-gray-800 mr-4 order-2">
          <select wire:model="perPage" class="bg-white border border-blue-100 text-blue-800 py-1 pl-2 pr-2 rounded leading-5 focus:outline-none focus:bg-white focus:border-yellow-500 appearance-none ">
            <option>10</option>
            <option>25</option>
            <option>50</option>
            <option>75</option>
            <option>100</option>
            <option>200</option>
          </select> 
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center text-grey-darker">
            <x-zondicon-cheveron-down class="h-4 w-4"/>
          </div>           
        </div> 
        <div class="text-gray-800 relative order-1">
          <input type="search" wire:model.debounce.300ms="search" placeholder="Search" class="bg-white p-1 rounded-full text-sm focus:outline-none">
          <button class="absolute inset-y-0 right-0 mx-2">
            <x-zondicon-search class="h-4 w-4"/>
          </button>
        </div>
      </div>
    </div>


  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-sidebar text-white">
        <tr>
          <th class="w-1/3 text-left py-1 px-4 uppercase font-semibold text-sm">Name</th>
          <th class="w-1/3 text-left py-1 px-4 uppercase font-semibold text-sm">Email</th>
          <th class="text-left py-1 px-4 uppercase font-semibold text-sm">Options</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">                          
       @foreach($applicants as $applicant)
        @if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
          <td class="w-1/3 text-left py-1 px-4">{{ $applicant->full_name }} </td>
          <td class="w-1/3 text-left py-1 px-4">{{ $applicant->email }}</td>
          <!-- <td>{{-- $applicant->prefered_prog --}}</td> -->
          <td> @if(auth()->user()->hasPermission('admissions.admin')) 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_academic', ['id' => $applicant->id]) }}"><x-zondicon-list-add class="h-4 w-4"/></a></button> @endif
            @if($applicant->check->status == "Student")
            <button class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_show', ['id' => $applicant->id]) }}"><x-zondicon-thermometer class="h-4 w-4"/></a></button> 
            @if(auth()->user()->hasPermission('admissions.admin'))
            <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_edit', ['id' => $applicant->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>@endif @endif
            <!-- <button class="bg-transparent hover:bg-purple-500 text-purple-700 font-semibold hover:text-white py-2 px-4 border border-purple-500 hover:border-transparent rounded"><a href="{{-- route('ERP::applicants_permissions', ['id' => $applicant->id]) --}}"><x-zondicon-adjust class="h-4 w-4"/></a></button> -->
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>
    {!! $applicants->links('vendor.pagination.tailwind_live') !!} 
  </div> 
</div> 