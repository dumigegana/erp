<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Acceptance Letter</title>
	<link rel="stylesheet" type="text/css" href="app.css">
</head>
<body class="m-16 font-smll">
<div class="relative flex flex-row">
   
    <div class="relative top-0 mt-8 ">
      <address>
      	<p class="text-black font-bold mt-8">STUDENT NUMBER: G0211728M </p>
	    Mr Talent Ncube  <br>
		Mufakose, Munondost Harare<br>
	  <address>
    </div>   
    <div class="text-gray-600 absolute right-0 top-0 mr-8 mt-8">
       <!-- <img class="h-28 w-24" src="{{URL::asset('/img/logo.png')}}" alt="GSU Logo"> -->
<img class="h-28 w-24" src="./img/logo.png" alt="Logo">	  
    </div>
</div> 
  <div >
<p class="text-black font-normal mb-4">DEAR MR NCUBE </p>
<p class="text-black underline mb-4">RE: ADMISSION INTO THE BACHELOR OF SCIENCE HONOURS DEGREE IN MINING ENGINEERING (CONVENTIONAL) PROGRAMME: 2021/2022 ACADEMIC YEAR</p>

<p class="text-black font-normal mb-4">I am pleased to inform you that Gwanda State University is offering you a place as stated above. Visit www.gsu.zw view our fees structer and available payment methods.</p>
<p  class="text-black font-normal mb-4"> To ensure that a place is reserved for you, you must pay a minimum of ZWL 6 075 non-refundable deposit by 31 August 2021, failing which you will forfeit your place and it will be offered to someone else on the waiting list, If you accept the offer, the deposit will be credited towards the prescribed total fees.</p>

<p class="text-black font-normal mb-4">This offer is subject to: </p>
<ul>
<li> Verification of your academic qualifications (bring originals and certified copies at Registration)</li> 
<li>Verification of your particulars (bring originals and certified copies of birth certificates and national ID),</li>
<li>That you are not registered with any other university</li>
<li>Any other requirements that the University may deem applicable. The above information is also available on the Gwanda State University website: www.gsu.ac.zw</li>
</ul>

<p class="text-black font-normal mb-4">You will not be permitted to attend class or have access to the university facilities unless you have completed the registration procedures. If you are declining this offer please indicate on the acceptance form. Registration and Orientation will run on 27 September 2021 to 28 September 2021.</p>
<p class="text-black font-normal mb-4">Lectures will begin on 29 September 2021. </p>

<p class="text-black font-normal mb-4">We would like to advise that this offer is made without prejudice to the rights which the University may have to withdraw or cancel it in the event of you or the University being unable to meet the conditions of the offer.</p>
<p class="text-black font-normal mb-4">Congratulations on your admission to Gwanda State University.</p>

<p class="text-black font-normal mb-4">MR S NDLOVU</p>
<p class="text-black font-normal mb-4">DEPUTY REGISTRAR</p>
  </div>

  
      <script type="text/javascript" src="alpine.js"></script>          
</body>
</html>