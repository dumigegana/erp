<div class="w-full rounded overflow-hidden  mr-5 my-5 ">
  @if ($addSubjects == false)
  <form wire:submit.prevent="step4" enctype="multipart/form-data">
    <div class="w-full rounded overflow-hidden mr-5 my-5 px-5 pb-2">
      @if (session()->has('message'))
      <div class="w-full">
      <center><p class="text-green-700 font-bold ">{{ session('message') }}</p></center>
      </div>
      @endif

          @if ($errors->any())
         <ul>
           @foreach($errors->all() as $err)
           <li>{{ $err }}</li>
           @endforeach
         </ul>
         @endif 
      
      <h2 class="text-xl font-bold text-blue-800 my-5">GCE Certificates and results</h2>
      <h3 class="text-sm font-bold text-blue-800 my-5">Upload one certificate at a time, Starting with O'level certificate/s.</h3>
      <div class="flex flex-wrap mb-4">
        <div class="w-full md:w-1/2 px-3 order-1">      
          <label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-8" for="certificate">Upload Certificate</label>
          <input class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="file" value="{{ old('file_loc') }}" wire:model.lazy="file_loc" name="file_loc"/>
          @error('file_loc') <span class="error text-red-800">{{ $message }}</span> @enderror
        </div>
        <div class="w-full md:w-1/2 px-3 order-2">
          <label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-8" for="exam_body">Examination Body</label>
          <input type="text" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" name="exam_body" value="{{ old('exam_body') }}" wire:model.lazy="data.exam_body" placeholder="e.g. ZIMSEC, Cambridge ...">
          @error('exam_body') <span class="error text-red-800">{{ $message }}</span> @enderror
        </div>            
      </div>
      <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0 order-1">
        	<label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-8" for="level">Level</label>
        		@error('data.level') <span class="error">{{ $message }}</span> @enderror
    	    <select wire:model.lazy="data.level" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option selected>-- select --</option>    	     	
            <option @if(old('data.level') == 'O') selected @endif value="O">O'Level</option>
            <option @if(old('data.level') == 'A') selected @endif value="A">A'Level</option>
            <option @if(old('data.level') == 'M') selected @endif value="M">Matric</option>
    	    </select>
    	  </div>
        <div class="w-full md:w-1/3 px-3 order-2">
    	    <label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-8" for="year">Year awarded</label>
    	    @error('data.year') <span class="error">{{ $message }}</span> @enderror
    	    <select wire:model.lazy="data.year" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option selected>-- select --</option>            
            <option @if(old('data.year') == '1950') selected @endif value="1950">1950</option>
            <option @if(old('data.year') == '1951') selected @endif value="1951">1951</option>
            <option @if(old('data.year') == '1952') selected @endif value="1952">1952</option>
            <option @if(old('data.year') == '1953') selected @endif value="1953">1953</option>
            <option @if(old('data.year') == '1954') selected @endif value="1954">1954</option>
            <option @if(old('data.year') == '1955') selected @endif value="1955">1955</option>
            <option @if(old('data.year') == '1956') selected @endif value="1956">1956</option>
            <option @if(old('data.year') == '1957') selected @endif value="1957">1957</option>
            <option @if(old('data.year') == '1958') selected @endif value="1958">1958</option>
            <option @if(old('data.year') == '1959') selected @endif value="1959">1959</option>
            <option @if(old('data.year') == '1960') selected @endif value="1960">1960</option>
            <option @if(old('data.year') == '1961') selected @endif value="1961">1961</option>
            <option @if(old('data.year') == '1962') selected @endif value="1962">1962</option>
            <option @if(old('data.year') == '1963') selected @endif value="1963">1963</option>
            <option @if(old('data.year') == '1964') selected @endif value="1964">1964</option>
            <option @if(old('data.year') == '1965') selected @endif value="1965">1965</option>
            <option @if(old('data.year') == '1966') selected @endif value="1966">1966</option>
            <option @if(old('data.year') == '1967') selected @endif value="1967">1967</option>
            <option @if(old('data.year') == '1968') selected @endif value="1968">1968</option>
            <option @if(old('data.year') == '1969') selected @endif value="1969">1969</option>
            <option @if(old('data.year') == '1970') selected @endif value="1970">1970</option>
            <option @if(old('data.year') == '1971') selected @endif value="1971">1971</option>
            <option @if(old('data.year') == '1972') selected @endif value="1972">1972</option>
            <option @if(old('data.year') == '1973') selected @endif value="1973">1973</option>
            <option @if(old('data.year') == '1974') selected @endif value="1974">1974</option>
            <option @if(old('data.year') == '1975') selected @endif value="1975">1975</option>
            <option @if(old('data.year') == '1976') selected @endif value="1976">1976</option>
            <option @if(old('data.year') == '1977') selected @endif value="1977">1977</option>
            <option @if(old('data.year') == '1978') selected @endif value="1978">1978</option>
            <option @if(old('data.year') == '1979') selected @endif value="1979">1979</option>
            <option @if(old('data.year') == '1980') selected @endif value="1980">1980</option>
            <option @if(old('data.year') == '1981') selected @endif value="1981">1981</option>
            <option @if(old('data.year') == '1982') selected @endif value="1982">1982</option>
            <option @if(old('data.year') == '1983') selected @endif value="1983">1983</option>
            <option @if(old('data.year') == '1984') selected @endif value="1984">1984</option>
            <option @if(old('data.year') == '1985') selected @endif value="1985">1985</option>
            <option @if(old('data.year') == '1986') selected @endif value="1986">1986</option>
            <option @if(old('data.year') == '1987') selected @endif value="1987">1987</option>
            <option @if(old('data.year') == '1988') selected @endif value="1988">1988</option>
            <option @if(old('data.year') == '1989') selected @endif value="1989">1989</option>
            <option @if(old('data.year') == '1990') selected @endif value="1990">1990</option>
            <option @if(old('data.year') == '1991') selected @endif value="1991">1991</option>
            <option @if(old('data.year') == '1992') selected @endif value="1992">1992</option>
            <option @if(old('data.year') == '1993') selected @endif value="1993">1993</option>
            <option @if(old('data.year') == '1994') selected @endif value="1994">1994</option>
            <option @if(old('data.year') == '1995') selected @endif value="1995">1995</option>
            <option @if(old('data.year') == '1996') selected @endif value="1996">1996</option>
            <option @if(old('data.year') == '1997') selected @endif value="1997">1997</option>
            <option @if(old('data.year') == '1998') selected @endif value="1998">1998</option>
            <option @if(old('data.year') == '1999') selected @endif value="1999">1999</option>
            <option @if(old('data.year') == '2000') selected @endif value="2000">2000</option>
            <option @if(old('data.year') == '2001') selected @endif value="2001">2001</option>
            <option @if(old('data.year') == '2002') selected @endif value="2002">2002</option>
            <option @if(old('data.year') == '2003') selected @endif value="2003">2003</option>
            <option @if(old('data.year') == '2004') selected @endif value="2004">2004</option>
            <option @if(old('data.year') == '2005') selected @endif value="2005">2005</option>
            <option @if(old('data.year') == '2006') selected @endif value="2006">2006</option>
            <option @if(old('data.year') == '2007') selected @endif value="2007">2007</option>
            <option @if(old('data.year') == '2008') selected @endif value="2008">2008</option>
            <option @if(old('data.year') == '2009') selected @endif value="2009">2009</option>
            <option @if(old('data.year') == '2010') selected @endif value="2010">2010</option>
            <option @if(old('data.year') == '2011') selected @endif value="2011">2011</option>
            <option @if(old('data.year') == '2012') selected @endif value="2012">2012</option>
            <option @if(old('data.year') == '2013') selected @endif value="2013">2013</option>
            <option @if(old('data.year') == '2014') selected @endif value="2014">2014</option>
            <option @if(old('data.year') == '2015') selected @endif value="2015">2015</option>
            <option @if(old('data.year') == '2016') selected @endif value="2016">2016</option>
            <option @if(old('data.year') == '2017') selected @endif value="2017">2017</option>
            <option @if(old('data.year') == '2018') selected @endif value="2018">2018</option>
            <option @if(old('data.year') == '2019') selected @endif value="2019">2019</option>
            <option @if(old('data.year') == '2020') selected @endif value="2020">2020</option>
            <option @if(old('data.year') == '2021') selected @endif value="2021">2021</option>
          </select>
        </div>		    
        <div class="w-full md:w-1/3 px-3 order-3"> 
    	    <label class="block uppercase tracking-wide text-blue-800 text-sm mb-2 mt-8" for="month">Month</label>
    		    @error('data.month') <span class="error">{{ $message }}</span> @enderror
    	    <select wire:model.lazy="data.month" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option value="" selected >Select...</option>
            <option @if(old('data.month') == '01') selected @endif value="01">Jan</option>
            <option @if(old('data.month') == '02') selected @endif value="02">Feb</option>
            <option @if(old('data.month') == '03') selected @endif value="03">Mar</option>
            <option @if(old('data.month') == '04') selected @endif value="04">Apr</option>
            <option @if(old('data.month') == '05') selected @endif value="05">May</option>
            <option @if(old('data.month') == '06') selected @endif value="06">Jun</option>
            <option @if(old('data.month') == '07') selected @endif value="07">Jul</option>
            <option @if(old('data.month') == '08') selected @endif value="08">Aug</option>
            <option @if(old('data.month') == '09') selected @endif value="09">Sept</option>
            <option @if(old('data.month') == '10') selected @endif value="10">Oct</option>
            <option @if(old('data.month') == '11') selected @endif value="11">Nov</option>
            <option @if(old('data.month') == '12') selected @endif value="12">Dec</option>
          </select>         
        </div>
      </div>        
      <center>
    		<button class="w-2/5 mt-4 p-4 text-blue-500 font-bold bg-white border-4 rounded-lg border-blue-500 cursor-pointer transition-all duration-700 hover:bg-blue-500 hover:text-white hover:border-transparent" type="submit">Submit</button>
    	</center>
    </div>  
  </form>
  @elseif ($file_loc)
  <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
    <div class="w-full h-full">
    <img src="{{ $file_loc->temporaryUrl() }}" alt="test image" />
    </div>
  </div>
  <div class="w-full rounded overflow-hidden shadow-lg">
    <h2 class="text-xl font-bold text-blue-800 ml-10 mr-20 my-10">List in chronological order, all subjects and results ( including failures ) in the above certificate. </h2> 
        
  <form >
    <div class="flex flex-wrap mx-20 mb-2">
      <div class="md:w-4/6 px-1 mb-2 md:mb-0">
        <label class="block uppercase tracking-wide text-gray-700 text-sm font-bold mb-2" for="grid-subject-id">
        Subject
        </label>
        <select class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-1 px-2 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"  wire:model.lazy="subject_id.0">
          <option value="" selected class="text-red-800">Select Subject ...</option>
          @foreach ($subjects as $sub)
          <option value="{{ $sub->id }}"> {{ $sub->name }}</option>
          @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
          </svg>
        </div>
        @error('subject_id.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
      </div>
      <div class="md:w-1/6 px-3">
        <label class="block uppercase tracking-wide text-gray-700 text-sm font-bold mb-2" for="grid-grade-id">
        Grade
        </label>
        <select class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-1 px-1 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" wire:model.lazy="grade_id.0">
          <option value="" selected class="text-red-800"> Select ...</option>
          @foreach ($grades as $grad)
          <option value="{{ $grad->id }}"> {{ $grad->grade }}</option>
          @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
          </svg>
        </div>
          @error('grade_id.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
      </div>
      <div class="md:w-1/6 px-3">
        <label class="block uppercase tracking-wide text-gray-700 text-sm font-bold mb-2" for="grid-add">
        Action
        </label>
        <button class="bg-green-500 hover:bg-green-700 text-white py-1 px-4 rounded" wire:click.prevent="add({{$i}})"><x-zondicon-add-solid class="h-6 w-6"/></button>
      </div>
    </div>

     @foreach($inputs as $key => $value)
    <div class="flex flex-wrap mx-20 mx-3 mb-2">
      <div class="md:w-4/6 px-1 mb-2 md:mb-0">
        <select class="appearance-none block w-full bg-blue-100 text-blue-800 border border-gray-200 rounded py-1 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"  wire:model.lazy="subject_id.{{ $value }}">
          <option value="" selected class="text-red-500"> Select Subject ...</option>
          @foreach ($subjects as $sub)
          <option value="{{ $sub->id }}"> {{ $sub->name }}</option>
          @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
          </svg>
        </div>
        @error('subject_id.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
      </div>
      <div class="md:w-1/6 px-3">
        <select class="appearance-none block w-full bg-blue-100 text-blue-800 border border-gray-200 rounded  py-1 px-1 mb-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" wire:model.lazy="grade_id.{{ $value }}">
          <option value="" selected class="text-red-500"> Select ...</option>
          @foreach ($grades as $grad)
          <option value="{{ $grad->id }}"> {{ $grad->grade }}</option>
          @endforeach
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
          </svg>
        </div>
          @error('grade_id.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
      </div>
      <div class="md:w-1/6 px-3">
        <button class="bg-red-500 hover:bg-red-700 text-white py-1 px-4 rounded" wire:click.prevent="remove({{$key}})"><x-zondicon-trash class="h-4 w-4"/></button>
      </div>
    </div>
    @endforeach
    <center><button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" wire:submit.prevent="step4_2" type="submit">Save</button></center>
    <br>
  </form>
    @endif 
  <div x-data="{ open: false }"> 
    <div class="w-full">
      <p class="text-sm font-bold text-blue-800 mx-10 mt-5">
        If you have submitted all your O' Level and A' level Certificates, and you have entered all your subjects and grades,  <button class=" p-1 bg-white border-2 text-red-300 rounded-lg border-red-300 cursor-pointer transition-all duration-700 hover:bg-red-800 hover:text-white hover:border-transparent" @click="open = !open">Click</button> to move to the next step.
      </p>
    </div>
    <div class="w-full rounded overflow-hidden shadow-xl mr-5 px-5 pb-2">       
      <div x-show="open">
        <p class="text-sm font-bold text-red-800 mx-10 my-10">
         Wanning, this action can not be reversed!! Please confirm your action by clicking the Confirm button.
        </p><center><button class=" p-1 bg-white border-2 rounded-lg border-blue-300 cursor-pointer transition-all duration-700 hover:bg-blue-800 hover:text-white hover:border-transparent" @click="open = !open" wire:click.prevent="finStep4" >Confirm</button></center> 
      </div> 
    </div>
  </div> 
</div>  



  

