<form wire:submit.prevent="step1">
	<fieldset class="px-5">
    <h2 class="text-xl font-bold text-blue-800 my-5">Terms and Conditions</h2>
    <h3 class="text-sm font-bold text-blue-800 my-5">First read the NOTES below, then complete the application forms</h3>
    <div class="w-full rounded overflow-hidden mr-5 mb-4 px-10 pb-2">
    	<ol class="list-decimal">
    		<li>
    			All applicants must complete all sections of the application form carefully and legibly. If the University discovers that any information submitted by the applicant is false, the University will reject that application and may refer the matter for legal action.
				</li>
				<li class="mt-3">
					All applicants must endorse at the bottom of this page that they have understood the notes given below and that they agree to their application being considered under the conditions outlined below.
				</li>
				<li class="mt-3">
					 Applicants should submit this form to the Senior Assistant Registrar, Admissions and Student Records Office, P.O. Box 30 Filabusi, Zimbabwe.
				</li>
				<li class="text-red-600">
					 Closing date for receipt of applications for Normal Entry is 9 March. The closing date for receipt of applications for Special Entry is 28 February. Late applications for Normal Entry only will be considered up to 30 March.
				</li>
				<li class="mt-3">
					 All applicants MUST submit with this form, photocopies (not originals) of all qualifications/certificates referred to in the application, including birth certificates and national identity card. The copies of the certificates must be certified by a Commissioner of Oaths or Head/Principal of the institution at which the examinations were taken.
				</li>
				<li class="mt-3"><br>
					<ol class="list-roman ml-6">
					 	<li class="mt-3">
					 		Applicants must give careful thought to their choice of degree programme in relation to the entry requirements for that programme and their own subject passes.
						</li>
						<li class="text-red-600 mt-3">
							It is important for applicants to understand that admission will be made as far as possible, in accordance with the degree preference made by the applicant. Therefore the first preference should be what the candidate genuinely wants to pursue. Applicants will be considered for their stated alternatives only if their first choice is unsuccessful and only if places on the alternative degree programme are available. In the event of a candidate not being selected for any of their preferences, an offer may be made which is not in accordance with his/her preference, should the candidate’s points be above the cut-off points for that programme.
						</li>
						<li class="mt-3">
							Applicants who are in doubt regarding the selection of preference should seek advice from the Admissions and Student Records Office before completing their forms (at the Campus, Tel. 263-84 2824720, 2824714 – 2824729). 
						</li>
						<li class="mt-3">
							Details of the courses on offer, entry requirements and guidelines on the cut-off points have been outlined in section 4 of this application form.
						</li>
						<li class="mt-3">
							All Special Entry applications will be limited to their first preference only.
						</li>	
					</ol>
				</li>
				<li class="mt-3">
					 FINANCE
				Applicants must ensure that they have the necessary finance to pay on registration. Students may apply for a loan through a Government Student Loan Facility. It is the students’ responsibility to secure sponsorship, if this is needed.
				</li>
				<li class="my-3">
					 Decisions on applications for admission to the University will NOT be made until the results of all qualifying examinations are known.
				Most offers will be issued in May/June.
				</li>
    	</ol>
    </div>
    <div x-data="{ open: false }">          
      <div class="flex flex-wrap -mx-3 ml-4 mt-16">
        <div class="w-full">
          <label class="inline-flex font-bold uppercase tracking-wide">
            <span class="text-sm  text-blue-800">
             Click to accept 
            </span> 
            <!-- <input type="hidden" value="0" wire:model.lazy="data.terms_agree">              -->
            <input class="leading-tigh form-checkbox h-4 w-4 text-green-500 ml-8 border-blue-500" type="checkbox"  value="1" wire:model.lazy="data.terms_agree" @change="open = true">
          </label>
        </div>
        <div class="w-full rounded overflow-hidden shadow-xl mr-5 my-10 px-5 pb-2">       
          <div x-show="open">
            <div class="flex flex-wrap mx-3 mb-6">
              <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-first-name">
                  First Name
                </label>
                <input value="{{ old('first_name') }}" wire:model.lazy="data.first_name" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text">
                @error('data.first_name') <span class="error">{{ $message }}</span> @enderror
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-middle-name">
                  Middle Name/s <a href="#" class="tooltip">
                  ?
                   <span class='tooltip-text bg-blue-200 p-3 -mt-1 lg:-mt-4 rounded'>As they appear in your birth certificate.If nun leave blank.</span>
                  </a> 
                </label>
                <input value="{{ old('middle_names') }}" wire:model.lazy="data.middle_names"  class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text">
                  @error('data.middle_names') <span class="error">{{ $message }}</span> @enderror
              </div>       
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-surname">
                  Surname
                </label>
                <input value="{{ old('surname') }}" wire:model.lazy="data.surname" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text"> @error('data.surname') <span class="error">{{ $message }}</span> @enderror
              </div> 
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-maiden-name">
                  Maiden Name
                </label>
                <input value="{{ old('previous_surnname') }}" wire:model.lazy="data.previous_surnname" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text">
              </div>       
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-email">
                  Email address
                </label>
                <input value="{{ old('email') }}" wire:model.lazy="data.email" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="email" >
               @error('data.email') <span class="error">{{ $message }}</span> @enderror
              </div>
              <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-email_confirmation">
                  Confirm Email Address
                </label>
                <input value="{{ old('email_confirmation') }}" wire:model.lazy="data.email_confirmation" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="email">
              </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-2">
              <div class="w-full md:w-2/6 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="cell_number">
                  Cell Number
                </label>
                <input value="{{ old('cell_number') }}" wire:model.lazy="data.cell_number" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text" placeholder="+263712345621"> @error('data.cell_number') <span class="error">{{ $message }}</span> @enderror
              </div>
              <div class="w-full md:w-1/6 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-sex">
                  sex
                </label>
                <div class="relative">
                  <select wire:model.lazy="data.sex" class="block appearance-none w-full bg-blue-100 border border-blue-100 text-blue-800 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-500">
                    <option value="" selected>Select</option>
                  <option @if(old('sex') == 'Male') selected @endif value="Male">Male</option>
                  <option @if(old('sex') == 'Female') selected @endif value="Female">Female</option>
                  </select>
                  <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>
                   @error('data.sex') <span class="error">{{ $message }}</span> @enderror
                </div>
              </div>
              <div class="w-full md:w-1/6 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-marital_status">
                  marital status
                </label>
                <div class="relative">
                  <select wire:model.lazy="data.marital_status" class="block appearance-none w-full bg-blue-100 border border-blue-100 text-blue-800 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-blue-500">
                    <option value="" selected>Select</option>
                  <option @if(old('marital_status') == 'Married') selected @endif value="Married">Married</option>
                  <option @if(old('marital_status') == 'Single') selected @endif value="Single">Single</option>
                  <option @if(old('marital_status') == 'Devoced') selected @endif value="Devoced">Devoced</option>
                  <option @if(old('marital_status') == 'Widowed') selected @endif value="Widowed">Widowed</option>
                  </select> 
                  <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                  </div>
            @error('data.marital_status') <span class="error">{{ $message }}</span> @enderror
                </div>
              </div>
              <div class="w-full md:w-2/6 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-blue-800 text-xs font-bold mb-2" for="grid-national-id-Number">
                  national id Number
                </label>
                <input value="{{ old('data.national_id') }}" wire:model.lazy="data.national_id" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text">   @error('data.national_id') <span class="error">{{ $message }}</span> @enderror
              </div>
            </div>
            <center>
            <button class="w-2/5 mt-16 p-4 bg-white border-4 rounded-lg border-blue-300 cursor-pointer transition-all duration-700 hover:bg-blue-800 hover:text-white hover:border-transparent" type="submit">Submit</button></center>
          </div> 
        </div>
      </div> 
    </div>
  </fieldset>  
</form>



  

