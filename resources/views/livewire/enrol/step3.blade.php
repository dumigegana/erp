<form wire:submit.prevent="step3" enctype="multipart/form-data">
	<fieldset >
  <div class="w-full rounded overflow-hidden mr-5 my-5 px-5 pb-2">
  <h2 class="text-xl font-bold text-blue-500 my-5">Application details</h2>
  @if (session()->has('message'))
  <div class="w-full">
    <center><p class="text-green-700 font-bold ">{{ session('message') }}</p></center>
  </div>
    @endif
  <div class="flex flex-wrap  mb-4">
    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-6 order-1">
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold  mb-2 mt-8" for="entry_type">Entry Type</label>
    		@error('data.entry_type_id') <span class="error">{{ $message }}</span> @enderror
	    <select wire:model.lazy="data.entry_type_id" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
	     	<option value="" selected >Select...</option>
	     	@foreach($entry_types as $e_type)
        @if (old('entry_type_id') == $e_type->id)
        <option value="{{ $e_type->id }}">{{ $e_type->type }}</option>
        @else
        <option value="{{$e_type->id}}">{{$e_type->type}}</option>
        @endif
        @endforeach
	    </select>
	  </div>		
	  <div class="w-full md:w-1/3 px-3 order-2"> 
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold  mb-2 mt-8" for="session">Applying For:</label>
    		@error('data.cohort_id') <span class="error">{{ $message }}</span> @enderror
	    <select wire:model.lazy="data.cohort_id" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>        
	     	@foreach($cohorts as $cohort)
        <option value="{{$cohort->id}}">{{$cohort->name}}</option>
        @endforeach
	    </select>
	  </div>				
	  <div class="w-full md:w-1/3 px-3 order-3"> 
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold  mb-2 mt-8" for="study_programme">Study Session</label>
    		@error('data.study_program_id') <span class="error">{{ $message }}</span> @enderror
	    <select wire:model.lazy="data.study_program_id" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
        <option selected>-- select --</option>	     	
	     	@foreach($study_programs as $stud_pro)
        @if (old('study_program_id') == $stud_pro->id)
        <option value="{{ $stud_pro->id }}" selected>{{ $stud_pro->type }}</option>
        @else
        <option value="{{$stud_pro->id}}">{{$stud_pro->type}}</option>
        @endif
        @endforeach
	    </select>
	  </div>			           
  </div>
<!-- Select Programmes -->
  <div class="flex flex-wrap mx-20 ">
    <div class="md:w-7/16 px-3 mb-2 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 font-bold mb-2">
      Programmes
      </label>
      <select class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"  wire:model.lazy="programme_id.0">
        <option value="" selected class="text-red-800"> Select Programme ...</option>
        @foreach ($progs as $prog)
        <option value="{{ $prog->id }}"> {{ $prog->name }}</option>
        @endforeach
      </select>
      @error('programme_id.0')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
    </div>
    <div class="md:w-3/16 px-3">
      <label class="block uppercase tracking-wide text-blue-800 font-bold mb-2">
      Action
      </label>
      <button class="bg-green-500 hover:bg-green-700 text-white py-1 px-4 rounded" wire:click.prevent="add({{$i}})"><x-zondicon-add-solid class="h-6 w-6"/></button>
    </div>
  </div>

   @foreach($inputs as $key => $value)
   @if($key < 3)
  <div class="flex flex-wrap mx-20 mx-3">
    <div class="md:w-7/16 px-3 mb-2 md:mb-1">
      <select class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-1 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"  wire:model.lazy="programme_id.{{ $value }}">
        <option value="" selected class="text-red-500"> Select Programme ...</option>
        @foreach ($progs as $prog)
        <option value="{{ $prog->id }}"> {{ $prog->name }}</option>
        @endforeach
      </select>
      @error('programme_id.'.$value)<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
    </div>              
    <div class="md:w-3/16 px-3">
      <button class="bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded" wire:click.prevent="remove({{$key}})"><x-zondicon-trash class="h-4 w-4"/></button>
    </div>
  </div>
  @endif
            @endforeach  
	<!-- end -->
	  

  <h3 class="text-xl font-bold text-blue-800 mt-10">Upload your certificates</h3>
  <div class="flex flex-wrap  mb-4">		
	  <div class="w-full md:w-1/2 px-3 order-1"> 
	  	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="birth_certificate">Birth Certificate</label>
    		@error('data.ref1_address') <span class="error">{{ $message }}</span> @enderror
	    <input wire:model.lazy="data.birth_certificate_fl" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" value="{{ old('birth_certificate_fl') }}" type="file">
	  </div>				
	  <div class="w-full md:w-1/2 px-3 order-2"> 
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="national_id">National ID</label>
    		@error('data.id_card_fl') <span class="error">{{ $message }}</span> @enderror
	    <input wire:model.lazy="data.id_card_fl" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" value="{{ old('id_card_fl') }}" type="file">
	  </div>	
	</div>
	<div class="flex flex-wrap  mb-4">
	  <div class="w-full md:w-1/2 px-3 order-1"> 
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold  mb-2 mt-8" for="full_name">Marriage Certificate</label>
    		@error('data.marriage_certificate_fl') <span class="error">{{ $message }}</span> @enderror
	    <input wire:model.lazy="data.marriage_certificate_fl" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="file" value="{{ old('marriage_certificate_fl') }}">
	  </div>
    @if($data['entry_type_id'] != 1)				
	  <div class="w-full md:w-1/2 px-3 order-2"> 
    	<label class="block uppercase tracking-wide text-blue-500 text-sm font-bold  mb-2 mt-8" for="curriculum_vitae">Curriculum Vitae</label>
    		@error('data.cv_fl') <span class="error">{{ $message }}</span> @enderror
	    <input wire:model.lazy="data.cv_fl" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="file" value="{{ old('cv_fl') }}">
	  </div>	
    @endif             
  </div>  
	<center>
		<button class="w-2/5 mt-4 p-4 text-blue-500 font-bold bg-white border-4 rounded-lg border-blue-500 cursor-pointer transition-all duration-700 hover:bg-blue-500 hover:text-white hover:border-transparent" type="Submit">Submit</button>
	</center>
  </fieldset>
</form>




  

