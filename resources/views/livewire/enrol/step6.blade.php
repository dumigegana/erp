<form wire:submit.prevent="step6">
	<fieldset >
		<div class="w-full rounded overflow-hiddens pb-2">
	    <h2 class="text-xl font-bold text-blue-500 my-5">Work Experience</h2>
      @if (session()->has('message'))
  <div class="w-full">
    <center><p class="text-green-700 font-bold ">{{ session('message') }}</p></center>
  </div>
    @endif
      <div class="flex mb-4 mr-3" >
        <div class="md:w-1/5 px-3 mb-2 md:mb-0 order-1">          
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="year">From Year</label>
          @error('data.year_f') <span class="error">{{ $message }}</span> @enderror
          <select wire:model.lazy="data.year_f" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option selected>-- select --</option>            
            <option @if(old('data.year_f') == '1950') selected @endif value="1950">1950</option>
            <option @if(old('data.year_f') == '1951') selected @endif value="1951">1951</option>
            <option @if(old('data.year_f') == '1952') selected @endif value="1952">1952</option>
            <option @if(old('data.year_f') == '1953') selected @endif value="1953">1953</option>
            <option @if(old('data.year_f') == '1954') selected @endif value="1954">1954</option>
            <option @if(old('data.year_f') == '1955') selected @endif value="1955">1955</option>
            <option @if(old('data.year_f') == '1956') selected @endif value="1956">1956</option>
            <option @if(old('data.year_f') == '1957') selected @endif value="1957">1957</option>
            <option @if(old('data.year_f') == '1958') selected @endif value="1958">1958</option>
            <option @if(old('data.year_f') == '1959') selected @endif value="1959">1959</option>
            <option @if(old('data.year_f') == '1960') selected @endif value="1960">1960</option>
            <option @if(old('data.year_f') == '1961') selected @endif value="1961">1961</option>
            <option @if(old('data.year_f') == '1962') selected @endif value="1962">1962</option>
            <option @if(old('data.year_f') == '1963') selected @endif value="1963">1963</option>
            <option @if(old('data.year_f') == '1964') selected @endif value="1964">1964</option>
            <option @if(old('data.year_f') == '1965') selected @endif value="1965">1965</option>
            <option @if(old('data.year_f') == '1966') selected @endif value="1966">1966</option>
            <option @if(old('data.year_f') == '1967') selected @endif value="1967">1967</option>
            <option @if(old('data.year_f') == '1968') selected @endif value="1968">1968</option>
            <option @if(old('data.year_f') == '1969') selected @endif value="1969">1969</option>
            <option @if(old('data.year_f') == '1970') selected @endif value="1970">1970</option>
            <option @if(old('data.year_f') == '1971') selected @endif value="1971">1971</option>
            <option @if(old('data.year_f') == '1972') selected @endif value="1972">1972</option>
            <option @if(old('data.year_f') == '1973') selected @endif value="1973">1973</option>
            <option @if(old('data.year_f') == '1974') selected @endif value="1974">1974</option>
            <option @if(old('data.year_f') == '1975') selected @endif value="1975">1975</option>
            <option @if(old('data.year_f') == '1976') selected @endif value="1976">1976</option>
            <option @if(old('data.year_f') == '1977') selected @endif value="1977">1977</option>
            <option @if(old('data.year_f') == '1978') selected @endif value="1978">1978</option>
            <option @if(old('data.year_f') == '1979') selected @endif value="1979">1979</option>
            <option @if(old('data.year_f') == '1980') selected @endif value="1980">1980</option>
            <option @if(old('data.year_f') == '1981') selected @endif value="1981">1981</option>
            <option @if(old('data.year_f') == '1982') selected @endif value="1982">1982</option>
            <option @if(old('data.year_f') == '1983') selected @endif value="1983">1983</option>
            <option @if(old('data.year_f') == '1984') selected @endif value="1984">1984</option>
            <option @if(old('data.year_f') == '1985') selected @endif value="1985">1985</option>
            <option @if(old('data.year_f') == '1986') selected @endif value="1986">1986</option>
            <option @if(old('data.year_f') == '1987') selected @endif value="1987">1987</option>
            <option @if(old('data.year_f') == '1988') selected @endif value="1988">1988</option>
            <option @if(old('data.year_f') == '1989') selected @endif value="1989">1989</option>
            <option @if(old('data.year_f') == '1990') selected @endif value="1990">1990</option>
            <option @if(old('data.year_f') == '1991') selected @endif value="1991">1991</option>
            <option @if(old('data.year_f') == '1992') selected @endif value="1992">1992</option>
            <option @if(old('data.year_f') == '1993') selected @endif value="1993">1993</option>
            <option @if(old('data.year_f') == '1994') selected @endif value="1994">1994</option>
            <option @if(old('data.year_f') == '1995') selected @endif value="1995">1995</option>
            <option @if(old('data.year_f') == '1996') selected @endif value="1996">1996</option>
            <option @if(old('data.year_f') == '1997') selected @endif value="1997">1997</option>
            <option @if(old('data.year_f') == '1998') selected @endif value="1998">1998</option>
            <option @if(old('data.year_f') == '1999') selected @endif value="1999">1999</option>
            <option @if(old('data.year_f') == '2000') selected @endif value="2000">2000</option>
            <option @if(old('data.year_f') == '2001') selected @endif value="2001">2001</option>
            <option @if(old('data.year_f') == '2002') selected @endif value="2002">2002</option>
            <option @if(old('data.year_f') == '2003') selected @endif value="2003">2003</option>
            <option @if(old('data.year_f') == '2004') selected @endif value="2004">2004</option>
            <option @if(old('data.year_f') == '2005') selected @endif value="2005">2005</option>
            <option @if(old('data.year_f') == '2006') selected @endif value="2006">2006</option>
            <option @if(old('data.year_f') == '2007') selected @endif value="2007">2007</option>
            <option @if(old('data.year_f') == '2008') selected @endif value="2008">2008</option>
            <option @if(old('data.year_f') == '2009') selected @endif value="2009">2009</option>
            <option @if(old('data.year_f') == '2010') selected @endif value="2010">2010</option>
            <option @if(old('data.year_f') == '2011') selected @endif value="2011">2011</option>
            <option @if(old('data.year_f') == '2012') selected @endif value="2012">2012</option>
            <option @if(old('data.year_f') == '2013') selected @endif value="2013">2013</option>
            <option @if(old('data.year_f') == '2014') selected @endif value="2014">2014</option>
            <option @if(old('data.year_f') == '2015') selected @endif value="2015">2015</option>
            <option @if(old('data.year_f') == '2016') selected @endif value="2016">2016</option>
            <option @if(old('data.year_f') == '2017') selected @endif value="2017">2017</option>
            <option @if(old('data.year_f') == '2018') selected @endif value="2018">2018</option>
            <option @if(old('data.year_f') == '2019') selected @endif value="2019">2019</option>
            <option @if(old('data.year_f') == '2020') selected @endif value="2020">2020</option>
            <option @if(old('data.year_f') == '2021') selected @endif value="2021">2021</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
            </svg>
          </div>
        </div>        
        <div class="w-full md:w-1/5 px-3 order-2"> 
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="month">Month</label>
            @error('data.month_f') <span class="error">{{ $message }}</span> @enderror
          <select wire:model.lazy="data.month_f" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option value="" selected >Select...</option>
            <option @if(old('data.month_f') == '01') selected @endif value="01">Jan</option>
            <option @if(old('data.month_f') == '02') selected @endif value="02">Feb</option>
            <option @if(old('data.month_f') == '03') selected @endif value="03">Mar</option>
            <option @if(old('data.month_f') == '04') selected @endif value="04">Apr</option>
            <option @if(old('data.month_f') == '05') selected @endif value="05">May</option>
            <option @if(old('data.month_f') == '06') selected @endif value="06">Jun</option>
            <option @if(old('data.month_f') == '07') selected @endif value="07">Jul</option>
            <option @if(old('data.month_f') == '08') selected @endif value="08">Aug</option>
            <option @if(old('data.month_f') == '09') selected @endif value="09">Sept</option>
            <option @if(old('data.month_f') == '10') selected @endif value="10">Oct</option>
            <option @if(old('data.month_f') == '11') selected @endif value="11">Nov</option>
            <option @if(old('data.month_f') == '12') selected @endif value="12">Dec</option>
          </select>      
            
        </div>
        <div class="md:w-1/6 px-3 order-3">
        </div>              
        <div class="md:w-1/5 px-3 order-4">
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="year">To Year</label>
          @error('data.year_t') <span class="error">{{ $message }}</span> @enderror
          <select wire:model.lazy="data.year_t" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option selected>-- select --</option>            
            <option @if(old('data.year_t') == '1950') selected @endif value="1950">1950</option>
            <option @if(old('data.year_t') == '1951') selected @endif value="1951">1951</option>
            <option @if(old('data.year_t') == '1952') selected @endif value="1952">1952</option>
            <option @if(old('data.year_t') == '1953') selected @endif value="1953">1953</option>
            <option @if(old('data.year_t') == '1954') selected @endif value="1954">1954</option>
            <option @if(old('data.year_t') == '1955') selected @endif value="1955">1955</option>
            <option @if(old('data.year_t') == '1956') selected @endif value="1956">1956</option>
            <option @if(old('data.year_t') == '1957') selected @endif value="1957">1957</option>
            <option @if(old('data.year_t') == '1958') selected @endif value="1958">1958</option>
            <option @if(old('data.year_t') == '1959') selected @endif value="1959">1959</option>
            <option @if(old('data.year_t') == '1960') selected @endif value="1960">1960</option>
            <option @if(old('data.year_t') == '1961') selected @endif value="1961">1961</option>
            <option @if(old('data.year_t') == '1962') selected @endif value="1962">1962</option>
            <option @if(old('data.year_t') == '1963') selected @endif value="1963">1963</option>
            <option @if(old('data.year_t') == '1964') selected @endif value="1964">1964</option>
            <option @if(old('data.year_t') == '1965') selected @endif value="1965">1965</option>
            <option @if(old('data.year_t') == '1966') selected @endif value="1966">1966</option>
            <option @if(old('data.year_t') == '1967') selected @endif value="1967">1967</option>
            <option @if(old('data.year_t') == '1968') selected @endif value="1968">1968</option>
            <option @if(old('data.year_t') == '1969') selected @endif value="1969">1969</option>
            <option @if(old('data.year_t') == '1970') selected @endif value="1970">1970</option>
            <option @if(old('data.year_t') == '1971') selected @endif value="1971">1971</option>
            <option @if(old('data.year_t') == '1972') selected @endif value="1972">1972</option>
            <option @if(old('data.year_t') == '1973') selected @endif value="1973">1973</option>
            <option @if(old('data.year_t') == '1974') selected @endif value="1974">1974</option>
            <option @if(old('data.year_t') == '1975') selected @endif value="1975">1975</option>
            <option @if(old('data.year_t') == '1976') selected @endif value="1976">1976</option>
            <option @if(old('data.year_t') == '1977') selected @endif value="1977">1977</option>
            <option @if(old('data.year_t') == '1978') selected @endif value="1978">1978</option>
            <option @if(old('data.year_t') == '1979') selected @endif value="1979">1979</option>
            <option @if(old('data.year_t') == '1980') selected @endif value="1980">1980</option>
            <option @if(old('data.year_t') == '1981') selected @endif value="1981">1981</option>
            <option @if(old('data.year_t') == '1982') selected @endif value="1982">1982</option>
            <option @if(old('data.year_t') == '1983') selected @endif value="1983">1983</option>
            <option @if(old('data.year_t') == '1984') selected @endif value="1984">1984</option>
            <option @if(old('data.year_t') == '1985') selected @endif value="1985">1985</option>
            <option @if(old('data.year_t') == '1986') selected @endif value="1986">1986</option>
            <option @if(old('data.year_t') == '1987') selected @endif value="1987">1987</option>
            <option @if(old('data.year_t') == '1988') selected @endif value="1988">1988</option>
            <option @if(old('data.year_t') == '1989') selected @endif value="1989">1989</option>
            <option @if(old('data.year_t') == '1990') selected @endif value="1990">1990</option>
            <option @if(old('data.year_t') == '1991') selected @endif value="1991">1991</option>
            <option @if(old('data.year_t') == '1992') selected @endif value="1992">1992</option>
            <option @if(old('data.year_t') == '1993') selected @endif value="1993">1993</option>
            <option @if(old('data.year_t') == '1994') selected @endif value="1994">1994</option>
            <option @if(old('data.year_t') == '1995') selected @endif value="1995">1995</option>
            <option @if(old('data.year_t') == '1996') selected @endif value="1996">1996</option>
            <option @if(old('data.year_t') == '1997') selected @endif value="1997">1997</option>
            <option @if(old('data.year_t') == '1998') selected @endif value="1998">1998</option>
            <option @if(old('data.year_t') == '1999') selected @endif value="1999">1999</option>
            <option @if(old('data.year_t') == '2000') selected @endif value="2000">2000</option>
            <option @if(old('data.year_t') == '2001') selected @endif value="2001">2001</option>
            <option @if(old('data.year_t') == '2002') selected @endif value="2002">2002</option>
            <option @if(old('data.year_t') == '2003') selected @endif value="2003">2003</option>
            <option @if(old('data.year_t') == '2004') selected @endif value="2004">2004</option>
            <option @if(old('data.year_t') == '2005') selected @endif value="2005">2005</option>
            <option @if(old('data.year_t') == '2006') selected @endif value="2006">2006</option>
            <option @if(old('data.year_t') == '2007') selected @endif value="2007">2007</option>
            <option @if(old('data.year_t') == '2008') selected @endif value="2008">2008</option>
            <option @if(old('data.year_t') == '2009') selected @endif value="2009">2009</option>
            <option @if(old('data.year_t') == '2010') selected @endif value="2010">2010</option>
            <option @if(old('data.year_t') == '2011') selected @endif value="2011">2011</option>
            <option @if(old('data.year_t') == '2012') selected @endif value="2012">2012</option>
            <option @if(old('data.year_t') == '2013') selected @endif value="2013">2013</option>
            <option @if(old('data.year_t') == '2014') selected @endif value="2014">2014</option>
            <option @if(old('data.year_t') == '2015') selected @endif value="2015">2015</option>
            <option @if(old('data.year_t') == '2016') selected @endif value="2016">2016</option>
            <option @if(old('data.year_t') == '2017') selected @endif value="2017">2017</option>
            <option @if(old('data.year_t') == '2018') selected @endif value="2018">2018</option>
            <option @if(old('data.year_t') == '2019') selected @endif value="2019">2019</option>
            <option @if(old('data.year_t') == '2020') selected @endif value="2020">2020</option>
            <option @if(old('data.year_t') == '2021') selected @endif value="2021">2021</option>
          </select>
          <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
            </svg>
          </div>
        </div>        
        <div class="w-full md:w-1/5 px-3 order-5"> 
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="month">Month</label>
            @error('data.month_t') <span class="error">{{ $message }}</span> @enderror
          <select wire:model.lazy="data.month_t" class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300">
            <option value="" selected >Select...</option>
            <option @if(old('data.month_t') == '01') selected @endif value="01">Jan</option>
            <option @if(old('data.month_t') == '02') selected @endif value="02">Feb</option>
            <option @if(old('data.month_t') == '03') selected @endif value="03">Mar</option>
            <option @if(old('data.month_t') == '04') selected @endif value="04">Apr</option>
            <option @if(old('data.month_t') == '05') selected @endif value="05">May</option>
            <option @if(old('data.month_t') == '06') selected @endif value="06">Jun</option>
            <option @if(old('data.month_t') == '07') selected @endif value="07">Jul</option>
            <option @if(old('data.month_t') == '08') selected @endif value="08">Aug</option>
            <option @if(old('data.month_t') == '09') selected @endif value="09">Sept</option>
            <option @if(old('data.month_t') == '10') selected @endif value="10">Oct</option>
            <option @if(old('data.month_t') == '11') selected @endif value="11">Nov</option>
            <option @if(old('data.month_t') == '12') selected @endif value="12">Dec</option>
          </select>
                  
        </div>
      </div>
      <div class="flex flex-wrap ml-3 mr-10 mb-6 -4">
        <div class="md:w-1/2 px-3">
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="grid-occupation">
          Occupation:
          </label>
          <input class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300"  wire:model.lazy="data.occupation">
          @error('data.occupation')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
        </div>
        <div class="md:w-1/2 px-3">
          <label class="block uppercase tracking-wide text-blue-500 text-sm font-bold mb-2 mt-8" for="grid-name-of-employer" for="grid-name-of-employer">
          Name of Employer
          </label>
          <input class="appearance-none block w-full bg-blue-100 text-blue-500 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" wire:model.lazy="data.name_of_employer">
        @error('data.name_of_employer')<p class="text-red-500 text-xs italic">{{ $message }}</p>@enderror
        </div>
      </div>
      @if($step == 8) 
      <h3 class="text-xl font-bold text-blue-800 mt-10">References </h3>
  <div class="flex flex-wrap  mb-4">    
    <div class="w-full md:w-1/3 px-3 order-1"> 
      <label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-6" for="full_name">referee 1</label>
        @error('data.referee_1') <span class="error">{{ $message }}</span> @enderror
      <input wire:model.lazy="data.referee_1" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text" value="{{ old('referee_1') }}" placeholder="Full name">
    </div>        
    <div class="w-full md:w-2/3 px-3 order-2"> 
      <label class="block uppercase tracking-wide text-blue-800 text-sm mb-2 mt-6" for="address">Address</label>
        @error('data.ref1_address') <span class="error">{{ $message }}</span> @enderror
      <textarea wire:model.lazy="data.ref1_address" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" value="{{ old('ref1_address') }}"></textarea>
    </div>                 
  </div>  
  <div class="flex flex-wrap  mb-4">    
    <div class="w-full md:w-1/3 px-3 order-1"> 
      <label class="block uppercase tracking-wide text-blue-800 text-sm  mb-2 mt-8" for="full_name">referee 2</label>
        @error('data.referee_2') <span class="error">{{ $message }}</span> @enderror
      <input wire:model.lazy="data.referee_2" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" type="text" value="{{ old('referee_2') }}" placeholder="Full name">
    </div>        
    <div class="w-full md:w-2/3 px-3 order-2"> 
      <label class="block uppercase tracking-wide text-blue-800 text-sm mb-2 mt-8" for="address">Address</label>
        @error('data.ref1_address') <span class="error">{{ $message }}</span> @enderror
      <textarea wire:model.lazy="data.ref2_address" class="appearance-none block w-full bg-blue-100 text-blue-800 border border-blue-100 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-blue-300" value="{{ old('ref2_address') }}"></textarea>
    </div>                 
  </div>
      @endif
	    <center>
				<button class="w-2/5 mt-4 p-4 text-blue-500 font-bold bg-white border-4 rounded-lg border-blue-500 cursor-pointer transition-all duration-700 hover:bg-blue-500 hover:text-white hover:border-transparent" type="Submit">Submit</button>
			</center>
		</div>
  </fieldset>
</form>
<div x-data="{ open: false }">
  <div class="w-full rounded overflow-hidden shadow-xl mr-5 px-5 pb-2">  
    <div class="w-full">
      <p class="text-sm font-bold text-blue-500 mx-10 mt-5">
        If you have submitted all your details <button class=" p-1 bg-white border-2 text-red-300 rounded-lg border-red-300 cursor-pointer transition-all duration-700 hover:bg-red-800 hover:text-white hover:border-transparent" @click="open = !open">Click</button> complete.
      </p>
    </div>      
    <div x-show="open">
      <p class="text-sm font-bold text-red-800 mx-10 my-10">
       Wanning, this action can not be reversed!! Please confirm your action by clicking this button.
      </p><center><button class=" p-1 bg-white border-2 rounded-lg border-blue-300 cursor-pointer transition-all duration-700 hover:bg-blue-800 hover:text-white hover:border-transparent" wire:click.prevent="finish" >Click</button></center> 
    </div> 
  </div>
</div> 


  

