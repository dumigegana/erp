@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::cohorts') }}" class="text-xl text-blue-700 mx-4 underline">Cohorts</a>
  <a class="text-xl ml-4">Create Cohort</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'Cohorts')
@section('content')
<form  method="POST" >
	<div class="w-72 px-20 lg:px-56">
    @csrf
    @include('forms/master')
    <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  </div>
</form>  
   
@endsection 