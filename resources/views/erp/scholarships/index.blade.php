@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Scholarships List</b>
@endsection
@section('content')
  @livewire('erp.scholarships')
@endsection