@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Scholarships List</b>
@endsection
@section('content')
  @livewire('erp.scholar_students', ['key'=>"{{ now() }}", 'scholarship_id' => $scholarship_id ])
@endsection