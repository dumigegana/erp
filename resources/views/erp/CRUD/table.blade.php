@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::CRUD') }}" class="text-uderline text-blue-300 text-xl">ERP Tables</a>
  <a class="text-xl">ERP Table Tittles</a>
@endsection
@section('title', 'Table')
@section('content')
<div class="bg-white overflow-auto">
<?php require(ERP::dataPath() . '/Edit/DevGet.php'); $allow_edit = $allow; ?>
<?php require(ERP::dataPath() . '/Create/DevGet.php'); ?>
  <table class="min-w-full bg-white">
    <thead class="bg-gray-800 text-white">
      <tr>
        @foreach($columns as $column)
        <th class="text-left py-2 px-1 uppercase font-semibold text-sm">{{$column}}</th>
        @endforeach
        <th class="text-left py-2 px-1 uppercase font-semibold text-sm">Edit</th>
        <th class="text-left py-2 px-1 uppercase font-semibold text-sm">Delete</th>
      </tr>  
    </thead>
    <tbody>
  
          <?php
              require(ERP::dataPath() . '/DevData.php');
              $hide = [];
              if(array_key_exists($name, $data)){
                  if(array_key_exists('hide_display', $data[$name])){
                      $hide = $data[$name]['hide_display'];
                  }
              }
          ?>
          @foreach($rows as $row)
      @if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
        @foreach($columns as $column)
        <td class="text-left">@if(in_array($column,$hide))<i>HIDDEN</i>@else @if($row->$column == "")<i>EMPTY</i>@else {{ $row->$column }} @endif @endif</td>
                  @endforeach
                  @if($allow_edit and \Schema::hasColumn($name, 'id'))
        <td class="text-left">
          <button class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white p-1 border border-blue-500 hover:border-transparent rounded">
            <a href="{{ route('ERP::CRUD_edit', ['table' => $name, 'id' => $row->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a>
          </button>
        </td>
          @else
        <td class="text-left">
          <button class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white p-1 border border-blue-500 hover:border-transparent rounded">
            <a><x-zondicon-edit-pencil class="h-4 w-4"/></a>
          </button>
        </td>
    @endif
    <?php
        # Check if you're allowed to delete rows
        require(ERP::dataPath() . '/DevData.php');
        $del = true;
        if(array_key_exists($name, $data)){
            if(array_key_exists('delete', $data[$name])) {
                if(!$data[$name]['delete']){
                    $del = false;
                }
            }
        }
    ?>
    @if($del)
        <td class="text-left">
          <button class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white p-1 border border-blue-500 hover:border-transparent rounded">
            <a href="{{ route('ERP::CRUD_delete', ['table' => $name, 'id' => $row->id]) }}"><x-zondicon-trash class="h-4 w-4"/></a>
          </button>
        </td>
    @else
        <td class="text-left">
          <button class="bg-transparent hover:bg-blue-500 text-blue-700 hover:text-white p-1 border border-blue-500 hover:border-transparent rounded">
            <a ><x-zondicon-trash class="h-4 w-4"/></a></button>
        </td>
          @endif
      </tr>
  @endforeach
	  </tbody>
	</table>
  {!! $rows->links('vendor.pagination.tailwind_live') !!} 
</div>
@endsection
