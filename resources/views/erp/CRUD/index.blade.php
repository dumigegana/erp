@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Database Tables</b>
@endsection
@section('title', "System Admin Section")
@section('content')
<div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
      <tr>
        <th class="w-1/4 text-left py-2 px-1 uppercase font-semibold text-sm">Table</th>
        <th class="w-1/4 text-left py-2 px-1 uppercase font-semibold text-sm">Columns</th>
        <th class="w-1/4 text-left py-2 px-1 uppercase font-semibold text-sm">Rows</th> 
        <th class="w-1/4 text-left py-2 px-1 uppercase font-semibold text-sm">Options</th>
      </tr>
    </thead>
    <tbody class="text-gray-700">
       @foreach($tables as $table)
      @if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
        <td class="w-1/4 text-left p-1">{{ $table }}</td>
        <td class="w-1/4 text-left p-1">{{ count(\Schema::getColumnListing($table)) }}</td>
        <td class="w-1/4 text-left p-1">{{ count(\DB::table($table)->get()) }}</td>
        <td class="w-1/4 text-left p-1"><button class="bg-transparent hover:bg-blue-500 text-xl text-blue-700 hover:text-white p-0 border border-blue-500 hover:border-transparent rounded">
            <a href="{{ route('ERP::CRUD_table', ['table' => $table]) }}"><x-zondicon-edit-pencil class="h-4 w-6"/></a></button>
        </td>
      </tr>
				@endforeach
      </tbody>
  	</table>
  </div>
        <br>
        
</div>
@endsection
