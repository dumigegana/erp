@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::CRUD') }}" class="text-xl text-blue-700 mx-4 underline">ERP Tables</a>
<a href="{{ route('ERP::CRUD_table', ['table' => $name]) }}" class="text-xl text-blue-700 mx-4 underline">{{$name}}</a>
  <a class="text-xl text-yellow ">Create Record</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'Create ' . $name)
@section('content')

<form  method="POST" >
<div class="w-72 px-20 lg:px-56">
            @csrf
            @include('forms/master')
    
         <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
        </div>   
        </form> 
   
@endsection
