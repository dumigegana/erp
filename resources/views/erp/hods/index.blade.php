@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Department heads List</b>

@endsection
@section('content')
@livewire('erp.hods')
@endsection 