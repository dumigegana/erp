@extends('layouts.admin')
@section('breadcrumb')
  <a href="{{ route('ERP::roles') }}" class="text-xl text-blue-700 mx-4 underline">Roles List</a>
  <a class="text-xl ml-4">Edit Role Permissions</a>
@endsection
@section('title', 'Edit Role Permissions')
@section('subtitle', 'Edit ')
@section('content')
<form  method="POST" >
   @csrf
    @include('forms/permissions')
    <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  
</form>
   
@endsection

