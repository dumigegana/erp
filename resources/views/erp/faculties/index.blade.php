@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Faculties List</b>
@endsection
@section('content')
  @livewire('erp.faculties')
@endsection