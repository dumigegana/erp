@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Semester Fees Settings per program</b>
@endsection
@section('content')
@livewire('erp.semfees')
@endsection 