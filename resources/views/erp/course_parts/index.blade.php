@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Program Courses List</b>

@endsection
@section('content')
@livewire('erp.course-parts')
@endsection 