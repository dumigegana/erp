@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::users') }}" class="text-xl text-blue-700 mx-4 underline">Users</a>
  <a class="text-xl ml-4">Create user</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'User')
@section('content')
<form  method="POST" >
	<div class="w-72 px-20 lg:px-56">     
        @csrf
        @include('forms/master')
<br>
     <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
      </div>
    </div>    
   
@endsection