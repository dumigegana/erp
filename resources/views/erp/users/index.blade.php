@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Users List</b>
@endsection
@section('title', "System User")
@section('icon', "user-secret")
@section('subtitle',"Users" )
@section('content')
@livewire('erp.users')
@endsection