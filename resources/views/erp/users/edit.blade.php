@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::users') }}" class="text-uderline text-blue-300 text-xl">Users</a>
  <a class="text-xl ml-4">Edit User Details</a>

@endsection 
@section('title', 'Edit User Details')
@section('icon', "edit")
@section('subtitle', 'Edit ' . $row->full_name)
@section('content')
<form class="bg-white overflow-auto m-7 p-10" method="POST" >
	<div class="w-72 px-20 lg:px-56">     
            @csrf
            @include('forms/edit')
    <br>       
         <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
    </div>
        </div>    
   
@endsection
