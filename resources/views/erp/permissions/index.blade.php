@extends('layouts.admin')
@section('breadcrumb')
		<b class="text-xl text-yellow">Permissions List</b>
@endsection
@section('title', "System Permissions")
@section('subtitle',"Erp Permissions" ) 
@section('content')
<div class="">
  
	<div class="py-4 relative flex flex-row">
	@if(auth()->user()->hasPermission('permissions.admin')) 
		<button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-2 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::permissions_create') }}"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
			 @endif 
		<!-- <div class="text-gray-600 absolute right-0 top-0 mt-3">
			<input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
			<button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
				<x-zondicon-search class="h-6 w-6"/>
			</button>
		</div> -->
	</div>
	<div class="bg-white overflow-auto">
		<table class="min-w-full bg-white">
			<thead class="bg-gray-800 text-white">
				<tr>
					<th class="w-9/10 text-left py-1 px-4 uppercase font-semibold text-sm">Slug</th>
					<th class="text-left py-1 px-4 uppercase font-semibold text-sm">Options</th>
				</tr>
			</thead>
			<tbody class="text-gray-700">  
				 @foreach($permissions as $permission)
				@if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
					<td class="w-9/10 text-left py-1 px-4">{{ $permission->slug }}</td>
					<td> 
						<button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-1 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::permissions_edit', ['id' => $permission->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
						<button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white p-1 border border-red-500 hover:border-transparent rounded"><a href="{{ route('ERP::permissions_delete', ['id' => $permission->id]) }}"><x-zondicon-trash class="h-4 w-4"/></a></button></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
 
				{!! $permissions->links() !!}
</div>      
@endsection
