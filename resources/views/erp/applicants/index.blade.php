@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Applicants List</b>
@endsection
@section('content')
  @livewire('erp.applicants')
@endsection