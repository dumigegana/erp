@extends('layouts.admin')
@section('breadcrumb')
  <a href="{{ route('ERP::applicants') }}" class="text-xl text-blue-700 mx-4 underline"><i class="fa fa-user-secret"></i>Applicants</a>
  <a class="w3-bar-item">Details</a>

@endsection
@section('title', $applicant->full_name)
@section('icon', "user")
@section('subtitle', $applicant->email )
@section('content')
@livewire('erp.show-applicant', ['applicant' => $applicant, 'programmes' => $programmes])

@endsection


       