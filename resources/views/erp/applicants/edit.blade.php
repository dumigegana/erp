@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::applicants') }}" class="text-xl text-blue-700 mx-4 underline"><i class="fa fa-user-secret"></i>Applicants</a>
  <a class="w3-bar-item">Edit Applicant</a>

@endsection 
@section('title', 'Edit Applicant')
@section('icon', "edit")
@section('subtitle', 'Edit ')
@section('content')

<form  method="POST" >
            @csrf
            @include('forms/edit')
    <br>
        
        
            <button type="submit" class="w3-button theme">Submit</button>
        </div>    
   
@endsection
