@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::applicants') }}" class="text-xl text-blue-700 mx-4 underline"><i class="fa fa-user-secret"></i>Applicants</a>
  <a class="w3-bar-item">Create Applicant</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'Applicants')
@section('content')
@livewire('erp.applicat-results', ['row' => $row])
@endsection
@section('livescipt')
@endsection