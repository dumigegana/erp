@extends('layouts.admin')
@section('breadcrumb')
  <a href="{{ route('ERP::applicants') }}" class="text-xl text-blue-700 mx-4 underline"><i class="fa fa-user-secret"></i>Applicants</a>
  <a class="w3-bar-item">Details</a>

@endsection
@section('title', $applicant->full_name)
@section('icon', "user-secret")
@section('subtitle', $applicant->email )
@section('content')


<div class="w3-container">
  <div class="w3-card-4">
    <div class="w3-row" style="padding: 40px;">
      <div class="w3-card w3-half" style="padding-left: 10px; padding-right: 15px;">
        <table class="w3-table">    
          <tr>
            <td><h5>Tittle:</h5></td>
            <td>{{ $applicant->tittle }}</td>          
          </tr>
          @if($applicant->previous_surname)  
          <tr>
            <td><h5>Previous Surname :</h5></td>
            <td>{{ $applicant->previous_surname }}</td>          
          </tr>
          @endif
          <tr>
            <td><h5>Email :</h5></td>
            <td>{{ $applicant->email }}</td>          
          </tr>
          <tr>
            <td><h5>Address :</h5></td>
            <td>{{ $applicant->home_address }}</td>          
          </tr>
          <tr>
            <td><h5>Sex :</h5></td>
            <td>{{ $applicant->sex }}</td>          
          </tr>
          <tr>
            <td><h5>National Id :</h5></td>
            <td>{{ $applicant->national_id }}</td>          
          </tr>
          <tr>
            <td><h5>Marital Status :</h5></td>
            <td>{{ $applicant->marital_status }}</td>          
          </tr> 
        </table>
      </div>  
      <div class="w3-card w3-half" style="padding-left:30px;">
        <table class="w3-table">    
          @if($applicant->permit)  
          <tr>
            <td><h5>Permit :</h5></td>
            <td>{{ $applicant->permit }}</td>          
          </tr>
          @endif          
          @if($applicant->disability_details)  
          <tr>
            <td><h5>Disability :</h5></td>
            <td>{{ $applicant->disability_details }}</td>          
          </tr>
          @endif
          <tr>
            <td><h5>Cell number :</h5></td>
            <td>{{ $applicant->cell_number }}</td>          
          </tr>
          <tr>
            <td><h5>Date of birth :</h5></td>
            <td>{{ $applicant->date_of_birth }}</td>          
          </tr>
          <tr>
            <td><h5>Place of birth :</h5></td>
            <td>{{ $applicant->place_of_birth }}</td>          
          </tr>
          <tr>
            <td><h5>Prospective Sponsor :</h5></td>
            <td>{{ $applicant->prospective_sponsor }}</td>          
          </tr>
          <tr>
            <td><h5>Nationality :</h5></td>
            <td>{{ $applicant->nationality }}</td>          
          </tr>
          <tr>
            <td><h5>Citizenship :</h5></td>
            <td>{{ $applicant->citizenship }}</td>          
          </tr>
        </table>
        </div>
    </div> 
  </div>
</div>       
@endsection


 <div class="flex mb-4" style="padding: 40px;">
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8" >
            <table class="table-fixed">           
              <tbody>
                <tr>
                  <td><h5>Tittle:</h5></td>
                  <td>{{ $applicant->tittle }}</td>          
                </tr>
                @if($applicant->previous_surname)  
                <tr>
                  <td><h5>Previous Surname :</h5></td>
                  <td>{{ $applicant->previous_surname }}</td>          
                </tr>
                @endif
                <tr>
                  <td><h5>Email :</h5></td>
                  <td>{{ $applicant->email }}</td>          
                </tr>
                <tr>
                  <td><h5>Address :</h5></td>
                  <td>{{ $applicant->home_address }}</td>          
                </tr>
                <tr>
                  <td><h5>Sex :</h5></td>
                  <td>{{ $applicant->sex }}</td>          
                </tr>
                <tr>
                  <td><h5>National Id :</h5></td>
                  <td>{{ $applicant->national_id }}</td>          
                </tr>
                <tr>
                  <td><h5>Marital Status :</h5></td>
                  <td>{{ $applicant->marital_status }}</td>          
                </tr>
              </tbody> 
            </table>
          </div>
             
          <div class="w-full sm:w-1/2 rounded overflow-hidden shadow-lg  pl-20 pr-20 p-8" >
            <table class="table-fixed">           
              <tbody>
                @if($applicant->permit)  
                <tr>
                  <td><h5>Permit :</h5></td>
                  <td>{{ $applicant->permit }}</td>          
                </tr>
                @endif          
                @if($applicant->disability_details)  
                <tr>
                  <td><h5>Disability :</h5></td>
                  <td>{{ $applicant->disability_details }}</td>          
                </tr>
                @endif
                <tr>
                  <td><h5>Cell number :</h5></td>
                  <td>{{ $applicant->cell_number }}</td>          
                </tr>
                <tr>
                  <td><h5>Date of birth :</h5></td>
                  <td>{{ $applicant->date_of_birth }}</td>          
                </tr>
                <tr>
                  <td><h5>Place of birth :</h5></td>
                  <td>{{ $applicant->place_of_birth }}</td>          
                </tr>
                <tr>
                  <td><h5>Prospective Sponsor :</h5></td>
                  <td>{{ $applicant->prospective_sponsor }}</td>          
                </tr>
                <tr>
                  <td><h5>Nationality :</h5></td>
                  <td>{{ $applicant->nationality }}</td>          
                </tr>
                <tr>
                  <td><h5>Citizenship :</h5></td>
                  <td>{{ $applicant->citizenship }}</td>          
                </tr>                
              </tbody> 
            </table>
          </div> 
        </div>
        <div> 
          <center>
            <button wire:click="birth" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Birth Certificate</button>
            <button wire:click="nId" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">National ID</button>
            <button wire:click="mariage" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Mariage Certificate</button>
          </center>
        </div> 
        <div class="px-4">
         @if($birth  == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->birth_certificate) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
         @if($nId  == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->id_card) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
         @if($mariage == true)
          <div style="height:300px;width:100%;overflow-y:scroll; overflow-x: scroll;">      
            <div class="w-full h-full">
              <img src="{{ asset('storage/'.$applicant->marriage_certificate) }}" alt="test image"/>
            </div>
          </div><br>
          @endif
        </div>