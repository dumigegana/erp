@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Subjects List</b>
@endsection
@section('content')
	@livewire('erp.subjects')
@endsection