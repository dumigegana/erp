@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::subjects') }}" class="text-xl text-blue-700 mx-4 underline">Subjects</a>
  <a class="text-xl ml-4">Edit Subject</a>

@endsection 
@section('title', 'Edit Subject')
@section('icon', "edit")
@section('subtitle', 'Edit ')
@section('content')
<form  method="POST" >
	<div class="w-72 px-20 lg:px-56">
    @csrf
    @include('forms/edit')
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Level</label>
        <select  name="type" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
           <option value="O"  @if(old('type', $row->type) == "O") selected @endif >Ordinary Level</option>
           <option {{ old('type', $row->type) == 'A' ? 'selected' : '' }} value="A">Advanced Level</option>
           <option value="OA"  {{ old('type', $row->type) == 'OA' ? 'selected' : '' }} >Both O & A'Level</option>
        </select>
      {{--  @if($error)
       
            <p class="text-red-500 text-sm italic">
                {{ $error }}
            </p>
        @endif --}}
        </div> 
    <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  </div>
</form>
   
@endsection
