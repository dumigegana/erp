@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::subjects') }}" class="text-xl text-blue-700 mx-4 underline">Subjects</a>
  <a class="text-xl ml-4">Create Subject</a>
@endsection
@section('title', 'Create')
@section('icon', "plus")
@section('subtitle', 'Subjects')
@section('content')
<form  method="POST" >
	<div class="w-72 px-20 lg:px-56">
    @csrf
    @include('forms/master')
    <div class="md:w-4/20 px-3 mb-2 mt-8 md:mb-0">
      <label class="block uppercase tracking-wide text-blue-800 text-sm font-bold mb-2 mt-8">Level</label>
        <select  name="type" id="type" class=" appearance-none block w-full text-blue-700 border border-blue-200 rounded py-1 px-6 mb-3 leading-tight focus:outline-none focus:bg-white">
          
           <option value="O">Ordinary Level</option>
           <option value="A">Advanced Level</option>
           <option value="OA">Both O & A'Level</option>
        </select>
        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
              <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
              </svg>
            </div>
        {{-- @if($error)
       
            <p class="text-red-500 text-sm italic">
                {{ $error }}
            </p>
        @endif --}}
        </div> 
    <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  </div>
</form>  
   
@endsection 