@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Paynow Payments</b>
@endsection
@section('content')
@livewire('erp.payments')
@endsection 