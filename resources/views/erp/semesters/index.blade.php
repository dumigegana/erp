@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Semesters List</b>

@endsection
@section('title', "System Semester")
@section('icon', "user-secret")
@section('subtitle',"Semesters" )
@section('content')
<div class="">
  
  <div class="py-4 relative flex flex-row mb-10">
          @if(auth()->user()->hasPermission('admin.access')) 
    <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::semesters_create') }}"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
    @else    
    <button class="bg-transparent text-gray-400 font-semibold py-2 px-4 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
       @endif 
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button>
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">From Reg Date</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">To Reg Date</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Status</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Options</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($semesters as $semester)
        <tr class="theme-text">
          <td class="text-left p-1">{{ $semester->name }} </td>
          <td class="text-left p-1">{{ $semester->reg_from }} </td>
          <td class="text-left p-1">{{ $semester->reg_to }} </td>
          <td class="text-left p-1">@if ($semester->active == true) Active @else dff @endif</td>
          <td class="text-left p-1"> 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-1 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::semesters_edit', ['id' => $semester->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>    
  </div>
 {!! $semesters->links('vendor.pagination.tailwind') !!}
       
</div>      
@endsection