@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Courses List</b>
 
@endsection
@section('title', "Courses")
@section('icon', "user-secret")
@section('subtitle',"Courses" )
@section('content')
<div class="">
  
  <div class="py-4 relative flex flex-row">
            
    <button class="bg-transparent text-gray-400 font-semibold py-2 px-2 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
      
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button> 
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Semester</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Course</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Lecturer</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Students</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Status</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Action</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($semCoses as $semCos)
        @if($loop->iteration % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
          <td class="text-left py-1 px-2">{{ $semCos->semester->name }} </td>
          <td class="text-left py-1 px-2">
            <div x-data="{ tooltip: false }" class="inline-flex">
              <div x-on:mouseover="tooltip = true" x-on:mouseleave="tooltip = false" class="p-1 hover:bg-indigo-500 hover:text-white cursor-pointer shadow">
            {{ $semCos->course_part->course->code }}
              </div>
              <div class="relative" x-cloak x-show.transition.origin.top="tooltip">
                <div class="absolute top-0 z-10 w-64 p-2 -mt-1 text-sm leading-tight theme-text transform -translate-x-1/2 -translate-y-full bg-yellow-400 rounded-lg shadow-lg">
                 {{ $semCos->course_part->programme->department->name }}
                </div>
                <svg class="absolute z-10 w-10 h-6 text-yellow-400 transform -translate-x-12 -translate-y-3 fill-current stroke-current" width="8" height="8">
                  <rect x="12" y="-10" width="10" height="8" transform="rotate(45)" />
                </svg>
              </div>
            </div>:{{ $semCos->course_part->course->name }} </td>
          <td class="text-left py-1 px-2">{{ $semCos->staff->user->fullname }} </td>
          <td class="text-left py-1 px-2">{{ $semCos->registrations->count() }} </td>
          <td class="text-left py-1 px-2 text-green-500">
            @if($state >= $semCos->state) Submitted @endif 
          </td>
          <td class="text-left py-1 px-2"> 
            <div x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
              <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-2 border border-red-500 hover:border-transparent rounded"><a href="#" @click="showModal = true"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
                <!-- overlay -->
              <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                      <!--Dialog-->
                <div class="bg-white w-full rounded shadow-lg py-4 text-left px-6" x-show="showModal" @click.away="showModal = false" >
                <button class="relative top-0 right-0 pr-3 mb-0" type="button" @click="showModal = false">✖</button>             
              @livewire('erp.marks.c-list', ['semCos_id' =>encrypt($semCos->id), 'state' => $state])
                </div>
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>       
</div>      
@endsection