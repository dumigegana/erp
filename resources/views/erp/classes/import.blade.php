<form action="{{ route('ERP::import') }}" method="POST" enctype="multipart/form-data" class="flex flex-row ml-2">                @csrf
	<div class="">
	  <label class="flex flex-column items-center px-2 py-2 text-purple-500 rounded shadow-lg tracking-wide uppercase border border-purple-500 cursor-pointer hover:bg-purple-500 hover:text-white">
	    <x-zondicon-upload class="h-4 w-4"/>
	    <!-- <span class="ml-2 text-base leading-normal">Select a file</span> -->
	    <input type='file' class="hidden" />
	  </label>
	</div>
<button type="submit" class="px-2 py-2 text-purple-500 rounded shadow-lg tracking-wide uppercase border border-purple-500 cursor-pointer hover:bg-purple-500 hover:text-white">Submit</button>
</form>