@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Faculty Deans List</b>

@endsection
@section('content')
@livewire('erp.deans')
@endsection 