@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::programmes') }}" class="text-xl text-blue-700 mx-4 underline">Programmes</a>
  <a class="text-xl ml-4">Edit Programme</a>

@endsection 
@section('title', 'Edit Programme')
@section('icon', "edit")
@section('subtitle', 'Edit ')
@section('content')
<form  method="POST" >
	<div class="w-72 px-20 lg:px-56">
    @csrf
    @include('forms/edit')
    <br>
    <center><button type="submit" class="bg-transparent hover:bg-blue-800 text-blue-800 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded my-8">Submit</button></center>
  </div>
</form>
   
@endsection
