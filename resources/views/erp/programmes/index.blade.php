@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Programmes List</b>

@endsection
@section('title', "System Programme")
@section('icon', "user-secret")
@section('subtitle',"Programmes" )
@section('content')
<div class="">
  
  <div class="py-4 relative flex flex-row">
          @if(auth()->user()->hasPermission('programmes.admin')) 
    <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::programmes_create') }}"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
    @else    
    <button class="bg-transparent text-gray-400 font-semibold py-2 px-4 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
       @endif 
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button>
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Code</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Department</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Options</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($programmes as $programme)
        <tr class="theme-text">
          <td class="text-left p-1">{{ $programme->code }} </td>
          <td class="text-left p-1">{{ $programme->name }} </td>
          <td class="text-left p-1">{{ $programme->department->name }} </td>
          <td class="text-left p-1"> 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-1 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::programmes_edit', ['id' => $programme->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
 
       
</div>      
@endsection