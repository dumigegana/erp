@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Semester Courses and Lectures</b>
@endsection
@section('content')
@livewire('erp.sem-courses')
@endsection 