@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Student Registration List</b>

@endsection
@section('title', "Registration")
@section('content')
<div class="">
  
  <div class="py-4 relative flex flex-row mb-10">
          @if(auth()->user()->hasPermission('registration.access')) 
    <button class="bg-transparent  text-blue-700 font-semibold hover:text-white py-2 px-4 border hover:border-transparent rounded"><a href="#"></a></button>
    @else    
    <button class="bg-transparent text-gray-400 font-semibold py-2 px-4 border border-gray-400  rounded"><a href="#"></a></button>
       @endif 
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button>
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Student</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Semester</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Part</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Program</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Course</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Options</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($registrations as $registration)
        <tr class="theme-text">
          <td class="text-left py-3 px-4">{{ $registration->student->gsu_id }} </td>
          <td class="text-left py-3 px-4">{{ $registration->sem_course->semester->name }} </td>
          <td class="text-left py-3 px-4">{{ $registration->part->part }}:{{ $registration->part->semester }}</td>
          <td class="text-left py-3 px-4">{{ $registration->student->programme->code }} </td>
          <td class="text-left py-3 px-4">{{ $registration->sem_course->course_part->course->code }}</td>
          <td class="text-left py-3 px-4"> 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::sem_courses_edit', ['id' => $registration->id]) }}"><x-zondicon-edit-pencil class="h-6 w-6"/></a></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
 
       
</div>      
@endsection