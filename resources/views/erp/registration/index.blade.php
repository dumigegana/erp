@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Registrations List</b>
@endsection
@section('content')
@livewire('erp.registrations')
@endsection