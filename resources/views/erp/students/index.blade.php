@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Students List</b>
@endsection
@section('content')
  @livewire('erp.students')
@endsection