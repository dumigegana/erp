@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-grey-700">Student List</b>

@endsection
@section('title', 'GSU Students')
@section('subtitle', 'Erp Students')

@section('content')
<div class="py-4 relative flex flex-row">
  @if(auth()->user()->hasPermission('admin.access')) 
  <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{-- route('ERP::students_create') --}}"><x-zondicon-user-add class="h-6 w-6"/></a></button>
     @endif 
  <div class="text-gray-600 absolute right-0 top-0 mt-3 mr-4">
    <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
    <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
      <x-zondicon-search class="h-6 w-6"/>
    </button>
  </div>
</div>
<div class="bg-white overflow-auto">
  <table class="min-w-full bg-white">
    <thead class="bg-gray-800 text-white">
      <tr>
        <th class="text-left py-3 px-4 uppercase font-semibold text-sm">ID</th>
        <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
        <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Email</th>
        <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Status</th>
        <!-- <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Email</th> -->
        <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Options</th>
      </tr>
    </thead>
    <tbody class="text-gray-700">                          
     @foreach($students as $student)
      <tr>
        <td class="text-left py-3 px-4">{{ $student->gsu_id }} </td>
        <td class="text-left py-3 px-4">{{ $student->user->full_name }} </td>
        <td class="text-left py-3 px-4">{{ $student->user->email }}</td>
        <!-- <td>  
          {{-- <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::students_edit', ['id' => $student->id]) }}"><x-zondicon-edit-pencil class="h-6 w-6"/></a></button>  
          <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::students_delete', ['id' => $student->id]) }}"><x-zondicon-trash class="h-6 w-6"/></a></button> --}}  
      </tr>-->              
        </td>
      @endforeach
    </tbody>
  </table>
  {!! $students->links() !!}
</div>    
@endsection
