@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Entry Types </b>
@endsection
@section('content')
@livewire('erp.entry_types')
@endsection