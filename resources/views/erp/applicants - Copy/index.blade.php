@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Applicants List</b>
@endsection
@section('title', "Applicants")
@section('icon', "user-secret")
@section('subtitle',"Applicants" )
@section('content')
<div class="">
 
  <div class="py-4 relative flex flex-row">
  @if(auth()->user()->hasPermission('admin.access')) 
    <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_create') }}"><x-zondicon-user-add class="h-6 w-6"/></a></button>
       @endif 
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button>
    </div>
  </div>
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm">Name</th>
          <th class="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm">Email</th>
          <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Options</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">                          
       @foreach($applicants as $applicant)
        <tr>
          <td class="w-1/3 text-left py-3 px-4">{{ $applicant->full_name }} </td>
          <td class="w-1/3 text-left py-3 px-4">{{ $applicant->email }}</td>
          <!-- <td>{{-- $applicant->prefered_prog --}}</td> -->
          <td>  
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_accademic', ['id' => $applicant->id]) }}"><x-zondicon-list-add class="h-6 w-6"/></a></button>
            <button class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_show', ['id' => $applicant->id]) }}"><x-zondicon-thermometer class="h-6 w-6"/></a></button>
            <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"><a href="{{ route('ERP::applicants_edit', ['id' => $applicant->id]) }}"><x-zondicon-edit-pencil class="h-6 w-6"/></a></button>
            <button class="bg-transparent hover:bg-purple-500 text-purple-700 font-semibold hover:text-white py-2 px-4 border border-purple-500 hover:border-transparent rounded"><a href="{{-- route('ERP::applicants_permissions', ['id' => $applicant->id]) --}}"><x-zondicon-adjust class="h-6 w-6"/></a></button>
          </td>
        </tr>
      @endforeach
    </tbody>
    </table>
  </div>
</div>      
@endsection