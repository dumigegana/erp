@extends('layouts.admin')
@section('breadcrumb')
        <b class="text-xl text-yellow">Confirm Action</b>
@endsection
@section('title', "Are You Sure!!")
@section('icon', "warning")
@section('subtitle', "Confirm Action")
@section('content')
<div class="w-full overflow-x-hidden border-t flex flex-col">
    <div class="py-4 px-64">
            <h2 class="text-xl font-bold text-red-600 my-5">Are You Sure!!</h2>
            <p class="text-blue-500">This page is a confirmation page to avoid things you might not wanted to do, or just to let you know this action can not be undone and so it requires a bit more than a single click to go ahead with it.</p>
            <br>
            <div class="py-4 relative flex flex-row">
                <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0 order-1">
                    <button class=" mt-4 p-2 text-blue-500 font-bold bg-white border-4 rounded-lg border-blue-500 cursor-pointer transition-all duration-700 hover:bg-blue-500 hover:text-white hover:border-transparent"><a href="{{ URL::previous() }}">Back</a></button>                    
                </div>
                <div class="w-full md:w-2/4 px-3 order-2">
                </div>
                <div class="w-full md:w-1/4 px-3 order-3">
                    <form method="POST">
                        @csrf
                        <button class=" mt-4 p-2 text-red-500 font-bold bg-white border-4 rounded-lg border-red-500 cursor-pointer transition-all duration-700 hover:bg-red-500 hover:text-white hover:border-transparent" type="submit">Continue</button>
                    </form>
                </div>
            </div>
            <div class="ui bottom attached progress" style="display: none;" data-value="1" data-total="75" id="security_progress">
                <div class="bar"></div>
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('js')
<script>
$('#security_progress').fadeIn(750)
var interval = setInterval(function(){
    var success = $('#security_progress').progress('is success');
    if(success) {
        $('#security_progress').fadeOut(750)
        setTimeout(function(){
            $('#security_continue').removeClass('disabled');
            $('#security_continue').removeClass('loading');
            clearInterval(interval);
        }, 250);
    } else {
        $('#security_progress').progress('increment');
    }
}, 50);
</script>
@endsection
