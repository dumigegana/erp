@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Departments List</b>
@endsection
@section('content')
  @livewire('erp.departments')
@endsection