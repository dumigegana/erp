@extends('layouts.admin')
@section('breadcrumb')
    <b class="text-xl text-yellow">Disabilities List</b>
@endsection
@section('content')
@livewire('erp.disabilities')
@endsection