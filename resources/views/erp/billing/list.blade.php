@extends('layouts.admin')
@section('breadcrumb')
<a href="{{ route('ERP::s_class') }}" class="text-xl text-blue-700 mx-4 underline">Courses</a>
  <a class="text-xl ml-2">{{$semCos->semester->name}}</a>
  <a class="text-xl ml-2">{{$semCos->course_part->course->name}}</a>
  <!-- <a class="text-xl ml-2">{{$semCos->staff->user->fullname}}</a> -->
@endsection
@section('title', "Class List")
@section('icon', "user-secret")
@section('subtitle',"Courses" )
@section('content')
<div class="">
  
  <div class="py-4 flex flex-row">
    <div class="static left-0 top-0 mt-3 flex flex-row ">
          @if(auth()->user()->hasPermission('academic.access')) 
      <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold mr-2 hover:text-white py-2 px-2 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::export', ['id' =>encrypt($semCos->id)]) }}"><x-zondicon-download class="h-4 w-4"/></a></button>
      <div x-data="{ 'showModal': false }" @keydown.escape="showModal = false" x-cloak>
        <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-2 border border-red-500 hover:border-transparent rounded"><a href="#" @click="showModal = true"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
          <!-- overlay -->
        <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showModal" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showModal }">
                <!--Dialog-->
          <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6" x-show="showModal" @click.away="showModal = false" >
          <button class="relative top-0 right-0 pr-3 mb-0" type="button" @click="showModal = false">✖</button>             
        @livewire('erp.marks.add', ['semCos' => $semCos])
          </div>
        </div>
      </div>

      <div x-data="{ 'showImport': false }" @keydown.escape="showImport = false" x-cloak>
        <button class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-2 border border-red-500 hover:border-transparent rounded"><a href="#" @click="showImport = true"><x-zondicon-upload class="h-4 w-4"/></a></button>
          <!-- overlay -->
        <div class="overflow-auto" style="background-color: rgba(0,0,0,0.5)" x-show="showImport" :class="{ 'absolute inset-0 z-10 flex items-center justify-center': showImport}">
                <!--Dialog-->
          <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg py-4 text-left px-6" x-show="showImport" @click.away="showImport = false" >
          <button class="relative top-0 right-0 pr-3 mb-0 text-red-500" type="button" @click="showImport = false">✖</button>             
         <form action="{{ route('ERP::import') }}" method="POST" enctype="multipart/form-data" class="ml-2 p-8">                @csrf
          <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-grey-darker font-bold mb-1" for="grid-password">
                    Weight
                </label>
                <select class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" name="weight_id">
                        <option class="font-bold" disabled>Course work : Exam</option>
                        @foreach($weights as $weight)
                            <option value="{{$weight->id}}">Course work {{ $weight->course_w * 100}} : Exam {{$weight->exam * 100}}</option>
                        @endforeach
                    </select>
                  <p class="text-grey-dark text-xs italic"></p>
              </div>
          </div>
            <div class="items-center">
              <label class="flex flex-column items-center px-2 py-2 text-purple-500 rounded shadow-lg tracking-wide uppercase border border-purple-500 cursor-pointer hover:bg-purple-500 hover:text-white" x-data="{ files: null }">
                <x-zondicon-upload class="h-4 w-4"/>
                <span class="ml-2 text-base leading-normal"  x-text="files ? files.map(file => file.name).join(', ') : 'Select single file...'"></span>
                <input type='file' name="file" class="hidden" x-on:change="files = Object.values($event.target.files)"/>
              </label>
            </div>
            <button type="submit" class="p-2 bottom-0 right-0 my-6 text-red-500 rounded shadow-lg tracking-wide uppercase border border-red-500 cursor-pointer hover:bg-red-500 hover:text-white">Submit</button>
          </form>
          </div>
        </div>
      </div>   
      
      @else    
      <button class="bg-transparent text-gray-400 font-semibold py-2 px-2 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-4 w-4"/></a></button>
       @endif 
    </div>
    <div class="text-gray-600 absolute right-0 mt-3 mr-6">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-4 w-4"/>
      </button>
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">ID</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Full Name</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Course work</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Exam</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Overall</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Grade</th>
          <th class="text-left py-1 px-2 uppercase font-semibold text-sm">Action</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($semCos->registrations as $reg)
        <tr class="theme-text">
          <td class="text-left py-1 px-4">{{ $reg->student->gsu_id }} </td>
          <td class="text-left py-1 px-4">{{ $reg->student->user->fullname }} </td>
          @if($reg->result)
          <td class="text-left py-1 px-4">{{ $reg->result->course_w }} </td>
          <td class="text-left py-1 px-4">{{ $reg->result->exam }} </td>
          <td class="text-left py-1 px-4">{{ $reg->result->overall }} </td>
          <td class="text-left py-1 px-4">{{ $reg->result->grade }} </td>
          @else
          <td class="text-left py-1 px-4">Pending </td>
          <td class="text-left py-1 px-4">Pending </td>
          <td class="text-left py-1 px-4">Pending</td>
          <td class="text-left py-1 px-4">Pending </td>
          @endif
          <td class="text-left py-1 px-4"> 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-1 px-2 border border-blue-500 hover:border-transparent rounded"><a href="{{-- route('ERP::class_list', ['id' =>encrypt($semCos->id)]) --}}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
 
       
</div>      
@endsection