@extends('layouts.admin')
@section('breadcrumb')

    <b class="text-xl text-yellow">Semester Billing List</b>

@endsection
@section('content')
<div class="">
  
  <div class="py-4 relative flex flex-row">
            
    <button class="bg-transparent text-gray-400 font-semibold py-2 px-4 border border-gray-400  rounded"><a href="#"><x-zondicon-add-outline class="h-6 w-6"/></a></button>
      
    <div class="text-gray-600 absolute right-0 top-0 mt-3">
      <input type="search" name="serch" placeholder="Search" class="bg-white h-10 px-5 pr-10 rounded-full text-sm focus:outline-none">
      <button type="submit" class="absolute right-0 top-0 mt-3 mr-4">
        <x-zondicon-search class="h-6 w-6"/>
      </button> 
    </div>
  </div> 
  <div class="bg-white overflow-auto">
    <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
        <tr> 
          <th class="text-left py-2 px-4 uppercase font-semibold text-sm">Semester</th>
          <th class="text-left py-2 px-4 uppercase font-semibold text-sm">Status</th>
          <th class="text-left py-2 px-4 uppercase font-semibold text-sm">Students</th>
          <th class="text-left py-2 px-4 uppercase font-semibold text-sm">Action</th>
        </tr>
      </thead>
      <tbody class="text-gray-700">
         @foreach($semesters as $semester)
         @if($semester->id % 2 == 1)@php $bg =''; @endphp @else @php $bg ='bg-purple-100'; @endphp @endif
        <tr class="theme-text {{$bg}}">
          <td class="text-left py-2 px-4">{{ $semester->name }} </td>

          <td class="text-left py-2 px-4">
            @if($semester->billed )<x-zondicon-checkmark class="h-5 w-5 text-green-500"/> 
            @else<x-zondicon-close class="h-5 w-5 text-red-500 "/> @endif
          </td>
          <td class="text-left py-2 px-4">{{-- $semester->sem_courses->registrations->distinct()->count(student_id) --}} </td>
          <td class="text-left py-2 px-4"> 
            <button class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white p-1 border border-blue-500 hover:border-transparent rounded"><a href="{{ route('ERP::bill_sem', ['id' =>$semester->id]) }}"><x-zondicon-edit-pencil class="h-4 w-4"/></a></button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>         
</div>      
@endsection