<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where we register Staff portal web routes for the ERP. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group.
|
*/
Auth::routes([
  'register' => false, // Register Routes...
  'reset' => false, // Reset Password Routes...
  'verify' => false, // Email Verification Routes...
]);

Route::group(['middleware' =>'guest'],  function () {
    Route::get('/', 'Auth\WelcomeController@welcome')->name('welcome');
    Route::get('wrong', 'Auth\WelcomeController@wrong')->name('wrong');
    Route::get('login', 'Auth\WelcomeController@welcome')->name('login');
    Route::post('/reset/{id}', 'Auth\RegisterController@reset')->name('reset');
    Route::post('/rec_signup', 'Auth\RegisterController@rec_signup')->name('rec_signup');
    Route::get('/signUp/{id}', 'Auth\RegisterController@confirm')->name('signUp');
    Route::post('/signUp/{id}', 'Auth\RegisterController@create');
});
Route::group(['middleware' => ['auth', 'erp.base', 'erp.auth'], 'prefix' => 'admin',  'as' => 'ERP::'], function () { 
# Users Routes
    Route::get('/', 'DashboardController@dash')->name('dash');
    Route::get('/users', 'UsersController@index')->name('users');
    Route::post('/users/roles', 'UsersController@setRoles')->name('user_roles');

    # Roles Routes --No Wire--
    Route::get('/roles', 'RolesController@index')->name('roles');
    Route::get('/roles/create', 'RolesController@create')->name('roles_create');
    Route::post('/roles/create', 'RolesController@store');
    Route::get('/roles/{id}', 'RolesController@show')->name('roles_show');
    Route::get('/roles/{id}/edit', 'RolesController@edit')->name('roles_edit');
    Route::post('/roles/{id}/edit', 'RolesController@update');
    Route::get('/roles/{id}/permissions', 'RolesController@editPermissions')->name('roles_permissions');
    Route::post('/roles/{id}/permissions', 'RolesController@setPermissions');
    Route::get('/roles/{id}/delete', 'SecurityController@confirm')->name('roles_delete');
    Route::post('/roles/{id}/delete', 'RolesController@destroy'); 

    # Permissions Routes --No Wire--
    Route::get('/permissions', 'PermissionsController@index')->name('permissions');
    Route::get('/permissions/create', 'PermissionsController@create')->name('permissions_create');
    Route::post('/permissions/create', 'PermissionsController@store');
    Route::get('/permissions/{id}/edit', 'PermissionsController@edit')->name('permissions_edit');
    Route::post('/permissions/{id}/edit', 'PermissionsController@update');
    Route::get('/permissions/{id}/delete', 'SecurityController@confirm')->name('permissions_delete');
    Route::post('/permissions/{id}/delete', 'PermissionsController@destroy');

    # Wire Routes 
    Route::get('/students', 'StudentsController@index')->name('students');
    Route::get('/faculties', 'FacultiesController@index')->name('faculties');
    Route::get('/cohorts', 'CohortsController@index')->name('cohorts');
    Route::get('/entry_types', 'Entry_typesController@index')->name('entry_types');
    Route::get('/course_parts', 'Course_PartsController@index')->name('course_parts');
    Route::get('/departments', 'DepartmentsController@index')->name('departments');
    Route::get('/subjects', 'SubjectsController@index')->name('subjects');
    Route::get('/courses', 'CoursesController@index')->name('courses');
    Route::get('/disabilities', 'DisabilitiesController@index')->name('disabilities');
    Route::get('/payments', 'PaymentsController@index')->name('payments');
    Route::get('/sem_fees', 'SemfeesController@index')->name('sem_fees');

    # Scholarships Routes
    Route::get('/scholarships', 'ScholarshipsController@index')->name('scholarships');
    Route::get('/scholarships/{id}', 'ScholarshipsController@students')->name('scholar_stud');

     # Programmes Routes --No Wire--
    Route::get('/programmes', 'ProgrammesController@index')->name('programmes');
    Route::get('/programmes/create', 'ProgrammesController@create')->name('programmes_create');
    Route::post('/programmes/create', 'ProgrammesController@store');
    Route::get('/programmes/{id}', 'ProgrammesController@show')->name('programmes_show');
    Route::get('/programmes/{id}/edit', 'ProgrammesController@edit')->name('programmes_edit');
    Route::post('/programmes/{id}/edit', 'ProgrammesController@update');
    Route::get('/programmes/{id}/delete', 'SecurityController@confirm')->name('programmes_delete');
    Route::post('/programmes/{id}/delete', 'ProgrammesController@destroy'); 
     
    # Sem_Courses Routes   --No Wire--
    Route::get('/sem_courses', 'Sem_CoursesController@index')->name('sem_courses');
    Route::get('/sem_courses/create', 'Sem_CoursesController@create')->name('sem_courses_create');
    Route::post('/sem_courses/create', 'Sem_CoursesController@store');
    Route::get('/sem_courses/{id}', 'Sem_CoursesController@show')->name('sem_courses_show');
    Route::get('/sem_courses/{id}/edit', 'Sem_CoursesController@edit')->name('sem_courses_edit');
    Route::post('/sem_courses/{id}/edit', 'Sem_CoursesController@update');
    Route::get('/sem_courses/{id}/delete', 'SecurityController@confirm')->name('sem_courses_delete');
    Route::post('/sem_courses/{id}/delete', 'Sem_CoursesController@destroy');

    # Semesters Routes  --No Wire--
    Route::get('/semesters', 'SemestersController@index')->name('semesters');
    Route::get('/semesters/create', 'SemestersController@create')->name('semesters_create');
    Route::post('/semesters/create', 'SemestersController@store');
    Route::get('/semesters/{id}', 'SemestersController@show')->name('semesters_show');
    Route::get('/semesters/{id}/edit', 'SemestersController@edit')->name('semesters_edit');
    Route::post('/semesters/{id}/edit', 'SemestersController@update');
    Route::get('/semesters/{id}/delete', 'SecurityController@confirm')->name('semesters_delete');
    Route::post('/semesters/{id}/delete', 'SemestersController@destroy');

    # Parts Routes  --No Wire--
    Route::get('/parts', 'PartsController@index')->name('parts');
    Route::get('/parts/create', 'PartsController@create')->name('parts_create');
    Route::post('/parts/create', 'PartsController@store');
    Route::get('/parts/{id}', 'PartsController@show')->name('parts_show');
    Route::get('/parts/{id}/edit', 'PartsController@edit')->name('parts_edit');
    Route::post('/parts/{id}/edit', 'PartsController@update');
    Route::get('/parts/{id}/delete', 'SecurityController@confirm')->name('parts_delete');
    Route::post('/parts/{id}/delete', 'PartsController@destroy');

    # Applicants Routes
    Route::get('/applicants', 'ApplicantsController@index')->name('applicants');
    // Route::get('/applicants/create', 'ApplicantsController@create')->name('applicants_create');
    // Route::post('/applicants/create', 'ApplicantsController@store');
    Route::get('/applicants/{id}/show', 'ApplicantsController@show')->name('applicants_show');
    Route::get('/applicants/{id}/academic', 'ApplicantsController@academic')->name('applicants_academic');
    Route::get('/applicants/{id}/edit', 'ApplicantsController@edit')->name('applicants_edit');    
    // Route::get('/applicants/{id}/delete', 'SecurityController@confirm')->name('applicants_delete');
    // Route::post('/applicants/{id}/delete', 'ApplicantsController@destroy'); 

    # Registrations Routes
    Route::get('/registration', 'RegistrationController@index')->name('reg');
    Route::get('/registration/register', 'RegistrationController@register')->name('register');
    Route::post('/registration/register', 'RegistrationController@store');
    Route::get('/registration/{id}/show', 'RegistrationController@show')->name('reg_show');
    Route::get('/registration/{id}/edit', 'RegistrationController@edit')->name('reg_edit');
    Route::post('/registration/{id}/edit', 'RegistrationController@update');    
    Route::get('/registration/{id}/delete', 'SecurityController@confirm')->name('reg_delete');
    Route::post('/registrations/{id}/delete', 'RegistrationController@destroy'); 

    # Semester classes Routes
    Route::get('/s_class', 'S_classController@index')->name('s_class');
    Route::get('/s_class/ch', 'S_classController@hod')->name('s_class_hod');
    Route::get('/s_class/dn', 'S_classController@dean')->name('s_class_dn');
    Route::get('/s_class/rg', 'S_classController@registry')->name('s_class_reg');
    Route::get('export/{id}', 'S_classController@export')->name('export');
    Route::post('import', 'S_classController@import')->name('import');    
    Route::get('/s_class/{id}/delete', 'SecurityController@confirm')->name('reg_delete');
    Route::post('/s_class/{id}/delete', 'S_classController@destroy');     
    Route::get('/s_class/{state}/{name}/submit', 'SecurityController@confirm')->name('lect_submit');
    Route::post('/s_class/{state}/{name}/submit', 'S_classController@approve_results'); 

     #Billing 
    Route::get('/semester/billing', 'BillingController@index')->name('billing');
    Route::get('/semester/{id}/billing', 'SecurityController@confirm')->name('bill_sem');
    Route::get('/semester/{id}/un_billing', 'SecurityController@confirm')->name('un_bill_sem');
    Route::post('/semester/{id}/billing', 'un_billing@bill'); 
    Route::post('/semester/{id}/un_billing', 'un_billing@un_bill'); 

    # Database CRUD routes
    Route::get('/CRUD', 'CRUDController@index')->name('CRUD');
    Route::get('/CRUD/{table}', 'CRUDController@table')->name('CRUD_table');
    Route::get('/CRUD/{table}/create', 'CRUDController@create')->name('CRUD_create');
    Route::post('/CRUD/{table}/create', 'CRUDController@createRow');
    Route::get('/CRUD/{table}/{id}', 'CRUDController@row')->name('CRUD_edit');
    Route::post('/CRUD/{table}/{id}', 'CRUDController@saveRow');
    Route::get('/CRUD/{table}/{id}/delete', 'SecurityController@confirm')->name('CRUD_delete');
    Route::post('/CRUD/{table}/{id}/delete', 'CRUDController@deleteRow');

    Route::get('/deans', 'DeansController@index')->name('deans');
    Route::get('/hods', 'HodsController@index')->name('hods');
});
Route::get('/home', 'HomeController@index')->name('home');
