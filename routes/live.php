<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where the Student portal web routes are registered for this ERP. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. 
|
*/
Route::group(['prefix' => 'portal',  'as' => 'LIVE::'], function () { 
    Route::get('/', function () {
        return view('home');
    })->name('guest')->middleware('guest');

    Route::get('/apply', 'EnrolController@index')->name('apply');
    Route::get('/apply/{code}', 'EnrolController@code')->name('code');
    Route::get('/apply/{id}/data', 'EnrolController@data')->name('data');
    Route::get('/pdf', 'EnrolController@createPDF')->name('pdf');
});

Route::group(['middleware' => ['auth', 'live.base', 'live.auth'], 'prefix' => 'portal',  'as' => 'LIVE::'], function () { 

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/check', 'HomeController@checksheet')->name('check');
    Route::get('/results', 'HomeController@results')->name('results');
    // Route::get('/switch/{overall}', 'HomeController@switch')->name('switch');
    Route::get('/register/{state}', 'RegistrationController@register')->name('register');
    Route::get('/paynow', 'PaynowController@intiate')->name('paynow');
    Route::post('/paynow', 'PaynowController@init_response');
    Route::get('/respons', 'PaynowController@respons')->name('res');
});