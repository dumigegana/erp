<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Study_program extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';   
    use SoftDeletes;

  protected $fillable = [
        'type', 'description', 'enable',  
    ]; 

  public function applicants()
   {
   	 return $this->hasMany('App/Applicant');
   }

  public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('description','Like', '%'.$search.'%')->orwhere('type','Like', '%'.$search.'%');
    }
   
}
