<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
   protected $connection = 'mysql';
   protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'department_id', 'code', 'name',   
    ];

     public function bulletins()
    {
        return $this->belongsToMany('App\Bulletin');
    }  

    public function department()
    {
        return $this->belongsTo('App\Department');
    } 

    public function oldresults()
    {
        return $this->hasMany('App\Oldresult');
    }
     
     public function course_parts()
    {
        return $this->hasMany('App\CoursePart');
    }
     public function courses()
    {
        return $this->hasMany('App\Course');
    }

     public function sem_courses()
    {
        return $this->hasManyThrough('App\SemCourse', 'App\CoursePart');
    }

    public function hasBulletin($id)
    {
        foreach($this->bulletins as $bulletin) {
            if($bulletin->id == $id) {
                return true;
            } else{        
            return false;
        }
       }
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('code','Like', '%'. $search.'%')->orwherehas('department', function($p) use ($search) {$p->where('name',  'like', '%' . $search . '%');});
    } 

}
