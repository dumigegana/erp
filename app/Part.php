<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Part extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
     protected $connection = 'mysql';
    use SoftDeletes;

    protected $fillable = [
        'part', 'semester',  
    ]; 

     public function course_parts()
    {
        return $this->hasMany('App\CoursePart');
    } 

     public function registrations()
    {
        return $this->hasMany('App\Registration');
    } 

    public function oldresults()
    {
        return $this->hasMany('App\Oldresult');
    }
 

    /**
    * A accessor to generate full part name 
    *
    */
    public function getFullnameAttribute()
    {
      return $this->part . ":" . $this->semester;
    }
}
