<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class SemCourse extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    use SoftDeletes; 
    // use HasFactory;

    protected $fillable = [
        'course_part_id', 'semester_id', 'staff_id', 'lect_submit', 'dept_aproved', 'faclt_aproved', 'acad_aproved', 'state', 'weight_id'
    ]; 
    protected $date = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function semester()
    {
        return $this->belongsTo('App\Semester');
    } 

     public function weight()
    {
        return $this->belongsTo('App\Weight');
    } 

    public function staff()
    {
        return $this->belongsTo('App\Staff');
    }

    public function course_part()
    {
        return $this->belongsTo('App\CoursePart');
    }     

     public function registrations()
    {
        return $this->hasMany('App\Registration');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->wherehas('part', function($o) use ($search) {$o->where('semester',  'like', '%' . $search . '%');})
            ->orwherehas('part', function($p) use ($search) {$p->where('part',  'like', '%' . $search . '%');})                                        
            ->orwherehas('course', function($q) use ($search) {$q->where('name', 'like', '%' . $search . '%');})
            ->orwherehas('course', function($r) use ($search) {$r->where('code', 'like', '%' . $search . '%');})
            ->orwherehas('programme', function($s) use ($search) {$s->where('code', 'like', '%' . $search . '%');})
            ->orwherehas('programme', function($t) use ($search) {$t->where('name', 'like', '%' . $search . '%');});
    }

}
