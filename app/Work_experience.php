<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Work_experience extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
	public $timestamps = false;
	protected $dates = ['from_date', 'to_date'];
    use SoftDeletes;

	protected $fillable = [
        'applicant_id', 'name_of_employer', 'occupation', 'from_date', 'to_date', 
    ];     
   
     public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    } 

    public function oldresults()
    {
        return $this->hasMany('App\Oldresult');
    }
 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name_of_employer','Like', '%'.$search.'%')->orwhere('occupation','Like', '%'.$search.'%');
    }
}
