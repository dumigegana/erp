<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission_Role;
use ERP;
Use Alert;
use App\Permission;

class PermissionsController extends Controller
{
    public function index()
    {
        ERP::permissionToAccess('users.admin');

    	# Get all the permissions
    	$permissions = Permission::paginate(20);

    	# Return the view
    	return view('erp/permissions/index', ['permissions' => $permissions]);
    }

    public function create()
    {
        ERP::permissionToAccess('users.admin');

        # Check permissions
        ERP::permissionToAccess('users.admin');


        $data_index = 'permissions';
        require('Data/Create/Get.php');

    	# Return the creation view
    	return view('erp/permissions/create', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('users.admin');

        # Check permissions
        ERP::permissionToAccess('users.admin');

		# Create the permission
		$row = ERP::newPermission();
        $data_index = 'permissions';
		require('Data/Create/Save.php');

		# return a redirect
		return redirect()->route('ERP::permissions')->with('success', 'Permission created');
    }

    public function edit($id)
    {
        ERP::permissionToAccess('users.admin');

        # Check permissions
        ERP::permissionToAccess('users.admin');

    	# Get the permission
    	$row = ERP::permission('id', $id);

        $data_index = 'permissions';
		require('Data/Edit/Get.php');


    	# Return the view
    	return view('erp/permissions/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('users.admin');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Get the permission
    	$row = ERP::permission('id', $id);

        $data_index = 'permissions';
		require('Data/Edit/Save.php');

		# return a redirect
		return redirect()->route('ERP::permissions')->with('success', 'Changes saved');
    }

    public function destroy($id)
    {
        ERP::permissionToAccess('users.admin');
        
        # Check permissions
        ERP::permissionToAccess('users.admin');

    	# Get the permission
    	$perm = ERP::permission('id', $id);

        # Check if it's su
        if($perm->su) {
            abort(403, trans('error_security_reasons'));
        }

    	# Delete relationships
    	$rels = Permission_Role::where('permission_id', $perm->id)->get();
    	foreach($rels as $rel) {
    		$rel->delete();
    	}

    	# Delete Permission
    	$perm->delete();

    	# Return a redirect
    	return redirect()->route('ERP::permissions')->with('success', 'Deleted');
    }

}
