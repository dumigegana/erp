<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Department;
use Auth;
use ERP;
Use Alert;

class DepartmentsController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('departments.access');

        # Get all the departments
        $departments = Department::all();

        
        # Return the view
        return view('erp/departments/index', ['departments' => $departments]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('departments.access');

        # Get the department
        $department = Department::find($id);

        # Return the view
        return view('admin/departments/show', ['department' => $department]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('departments.access');

        # Check permissions
        ERP::permissionToAccess('departments.admin');

        # Find the department
        $row = Department::find($id);
        
        # Get all the data
        $data_index = 'departments';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/departments/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('departments.access');

        # Check permissions
        ERP::permissionToAccess('departments.admin');

        # Find the row
        $row = Department::find($id);
       
        # Save the data
        $data_index = 'departments';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::departments')->with('success', 'Department Changed');
    }

    public function create()
    {
        ERP::permissionToAccess('departments.access');

        # Check permissions
        ERP::permissionToAccess('departments.admin');

        # Get all the data
        $data_index = 'departments';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/departments/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('departments.access');

        # Check permissions
        ERP::permissionToAccess('departments.admin');

        # create new department
        $row = new Department;

        # Save the data
        $data_index = 'departments';
        require('Data/Create/Save.php');

        # Set the permissions
        $this->setPermissions($row->id, $request);

        # Return the admin to the departments page with a success message
        return redirect()->route('ERP::departments')->with('success', 'Department created');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('departments.access');
        
        # Check permissions
        ERP::permissionToAccess('departments.admin');

        # Select Department
        $department = ERP::department('id', $id);

        
        # Delete Department
        $department->delete();

        # Redirect the admin
        return redirect()->route('ERP::departments')->with('success', 'Department deleted');
    }
}
