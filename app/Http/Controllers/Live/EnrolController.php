<?php

namespace App\Http\Controllers\Live;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Applicant_code;
use App\Applicant;

class EnrolController extends Controller
{
    public function index()
    {
    	return view('live.apply');
    }

    public function data($id)
    {
        $applicant = Applicant::findOrFail($id);

        return view('Live.apply_code', ['applicant' => $applicant]);
    }

     // Generate PDF
    public function createPDF() {
        $course_parts = CoursePart::all();
        $inputForm = 0;
        $pdf = PDF::loadView('livewire.erp.course_parts.acceptance');
                            // dd($pdf);
      return $pdf->download('pdf_file.pdf');
    }

    public function code($code)
    { 
    	$appl_code = Applicant_code::where('code', $code)->first();
      if(!$appl_code) {
        abort(401, "Sorry, this link expired. Click below to reset it.");
      }
      
        $applicant =  Applicant::findOrFail($appl_code->applicant_id);
        return view('live.apply_code', ['applicant' => $applicant]);
    }
}
