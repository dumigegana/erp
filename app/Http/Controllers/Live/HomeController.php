<?php

namespace App\Http\Controllers\Live;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Registration;
use App\CoursePart;
use App\Semester;
use App\Programme;
use App\Part;
use App\Oldresult; 
use Illuminate\Support\Arr;


class HomeController extends Controller
{
    public function index()
    {
      $thisStud = Auth::user()->student;
        $cRegs =Registration::where('student_id', $thisStud->id)->where('part_id', $thisStud->part_id)->get();
        //dd($cRegs);
    
    	return view('live.home', ['cRegs' => $cRegs]);
    }

    public function results()
    {
      $thisStud = Auth::user()->student;
        $semesters = Semester::with(['sem_courses.registrations']) 
                            ->orwherehas('sem_courses', function($v) use ($thisStud) {
                                $v->where('lect_submit', true)
                                ->where('dept_aproved', true)
                                ->where('faclt_aproved', true)
                                ->wherehas('registrations',function($query) use ($thisStud) {
                                    $query->where('student_id', $thisStud->id);
                                });
                            })->get();
          if ($thisStud->imprt) {
            $parts = Part::with(['oldresults' => function ($query) use ($thisStud) {
                              $query->where('student_id', $thisStud->id);
                          }])
                          ->wherehas('oldresults', function($v) use ($thisStud) {
                                $v->where('student_id', $thisStud->id);
                            })->orderBy('id', 'desc')->get();
            
            return view('live.results', ['semesters' => $semesters, 'parts' => $parts]);
          }
          else {        
    
        return view('live.results', ['semesters' => $semesters]);
      }
    }  

    public function checksheet()
    {
      $stud = Auth::user()->student;
        $course_ps =CoursePart::where('programme_id', $stud->programme_id)->orderby('part_id', 'asc')->get();

        $result_keys = Oldresult::where('student_id', $stud->id)->get()
                            ->filter(function ($result) {
                                return $result->overall >= 50;
                            })->pluck('course_id');

       $fail_keys = Oldresult::where('student_id', $stud->id)->get()
                            ->filter(function ($result) {
                                return $result->overall < 50;
                            })->pluck('course_id');
        $repeats = $fail_keys->diff($result_keys)->all();
    //     $mkeys =$course_ps->modelKeys();

    //     $prpend =Arr::collapse([$repeats, $mkeys]);  
          
        $bulletin= $stud->applicant->cohort->bulletin_id;
        return view('live.checksheet', ['course_ps' => $course_ps, 'repeats'  => $repeats, 'result_keys' =>$result_keys, 'bulletin' => $bulletin]);
    }

    public function data($id) 
    {
        $applicant = Applicant::findOrFail($id);

        return view('Live.apply_code', ['applicant' => $applicant]);
    }

    public function code($code)
    { 
    	$appl_code = Applicant_code::where('code', $code)->first();
      if(!$appl_code) {
        abort(401, "Sorry, this link expired. Click below to reset it.");
      }
      
        $applicant =  Applicant::findOrFail($appl_code->applicant_id);
        return view('live.apply_code', ['applicant' => $applicant]);
    }    

}
