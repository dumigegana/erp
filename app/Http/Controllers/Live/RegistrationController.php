<?php

namespace App\Http\Controllers\Live;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class RegistrationController extends Controller
{
    public function register($stat)
    {
    	return view('live.registration.register', ['stat' => $stat]);
    }
}
    
