<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Student;
use App\Semester;
use App\SemCourse;
use App\Part;
//use DB;

class RegistrationController extends Controller
{
    public function index()
    {
    	return view('erp/registration/index');
    }

    public function register()
    {
    	ERP::permissionToAccess('registration.access');
    	$parts = Part::All();    	
    	$semesters = Semester::where('active',1)->get();    	

    	return view('erp/registration/register', [
	    		       'parts' => $parts,
	    		       'semesters' => $semesters
	    		    ]);
    }

    public function store(Request $request)
    {
    	ERP::permissionToAccess('registration.admin');
        
        $this->validate($request, [
            'semester_id' => 'required|integer',
            'gsu_id' => 'required', 
        ]);

        $sid  = $request['semester_id'];
    	$stud = Student::where('gsu_id', $request['gsu_id'])->first();
    	// dd(DB::connection('mysql')->connection('mysql')->getQueryLog());
    	if ($stud) {    		
        	$prId = $stud->programme_id;
        	$stId = $stud->id;
            $pid  = $stud->part->id + 1;
            
	    	$rep_P = Registration::where('part_id', $pid)
	    	                     ->where('student_id', $stId)->first();

	    	$rep_S = SemCourse::wherehas('registrations',function($q) use ($stId) {
				         $q->where('student_id', '=', $stId);
				    })->get(); 

	    	if ($rep_P ) {
				return redirect()->route('ERP::reg')->with('success', 'Can not Register Part Twice');
			}
			elseif ($rep_S) {
				$semCorses = SemCourse::where('semester_id', $sid) 
                                    ->wherehas('course_part', function($q) use ($pid, $prId) {
                                    $q->where('part_id', $pid)
                                      ->where('programme_id', $prId); })->get();

                $semcourses = $semCorses->except($rep_S->modelKeys());
                courses($semcourses, $pid, $stId);
			}
			
	    	$semcourses = SemCourse::where('semester_id', $sid) 
	    	                        ->wherehas('course_part', function($q) use ($pid, $prId) {
	    	                        $q->where('part_id', $pid)
	    	                          ->where('programme_id', $prId); })->get();
            courses($semcourses, $pid, $stId);  
        }               	  
		
    }

     public function courses($semcourses, $pid, $stId)
     {
         $num = $semcourses->count();
               
        if($num < 1) {      
            return redirect()->route('ERP::reg')->with('warning', 'Failed, Configure Semester Courses for this Class.'); 
        }

        foreach ($semcourses as $s_cos) {
            Registration::create([
                'student_id' => $stId,
                'part_id' => $pid,
                'sem_course_id' => $s_cos->id,
            ]);

        }    
        Student::where('gsu_id', $request['gsu_id'])->update(['part_id' => $pid]);

        return redirect()->route('ERP::reg')->with('success', 'Student registered');        
    }

    public function show($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return view('erp/registration/show', ['row' => $row]);
    }

    public function edit($id, Request $request)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail($id);
         # Get all the data
        $data_index = 'registrations';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/registration/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }

    public function finance($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }

    public function approve($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }
}
