<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_User; 
use App\User;
use App\Permission;
use App\Permission_Role;
// use Schema;
use Auth;
use ERP;
Use Alert;

class RolesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('roles.access');

    	# Get all the roles
    	$roles = Role::all();

    	
    	# Return the view
    	return view('erp/roles/index', ['roles' => $roles]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('roles.access');

    	# Get the role
    	$role = Role::find($id);

    	# Return the view
    	return view('admin/roles/show', ['role' => $role]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('roles.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

        # Find the role
        $row = Role::find($id);
        
        # Get all the data
        $data_index = 'roles';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/roles/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('roles.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

        # Find the row
        $row = Role::find($id);

        if(!$row->allow_editing and !ERP::loggedInuser()->su) {
            abort(403, trans('error_editing_disabled'));
        }

        # Save the data
        $data_index = 'roles';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::roles')->with('success', 'Changes saved');
    }

    public function create()
    {
        ERP::permissionToAccess('roles.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

        # Get all the data
        $data_index = 'roles';
    	require('Data/Create/Get.php');

        # All permissions
        $permissions = ERP::permissions();

        # Return the view
        return view('erp/roles/create', [
            'permissions'   =>  $permissions,
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'permissions'   =>  $permissions,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('roles.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

    	# create new role
        $row = new Role;

        # Save the data
        $data_index = 'roles';
        require('Data/Create/Save.php');

        # Set the permissions
        $this->setPermissions($row->id, $request);

        # Return the admin to the roles page with a success message
        return redirect()->route('ERP::roles')->with('success', 'Role created');
    }

    public function editPermissions($id)
    {
        ERP::permissionToAccess('roles.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

    	# Find the role
    	$role = ERP::role('id', $id);

        // if(!$role->allow_editing and !ERP::loggedInuser()->su) {
        //     abort(403, trans('error_editing_disabled'));
        // }

    	# All permissions
    	$permissions = ERP::permissions();


    	# Return the view
    	return view('erp/roles/permissions', [
            'role' => $role,
            'permissions' => $permissions,
        ]);
    }

  public function setPermissions($id, Request $request)
  {
    ERP::permissionToAccess('roles.access');

    # Check permissions
    ERP::permissionToAccess('roles.admin');

  	# Find the role
  	$role = ERP::role('id', $id);

        // if(!$role->allow_editing and !ERP::loggedInuser()->su) {
        //     abort(403, trans('error_editing_disabled'));
        // }

  	# All permissions
  	$permissions = ERP::permissions();

  	# Edit the permission
  	foreach($permissions as $perm){

            // # Check for su
            // $modify = true;
            // if($role->su) {
            //     if($perm->su) {
            //         $modify = false;
            //     }
            // }

            // if(!$perm->assignable and !ERP::loggedInUser()->su) {
            //     $modify = false;
            // }

    
      if($request->input($perm->id)) {
        # The admin selected that permission

        # Check if the relation existed
        if($this->checkPerm($perm->id, $role->id)) {
            # The role had already that permission
        } else {
            # The role did not have that permission, so we need to add it
            $this->addPerm($perm->id, $role->id);
        }
      } else {
        # The admin did not select that permission

        # Check if the relation existed
        if($this->checkPerm($perm->id, $role->id)) {
            # The role had this permission, so we need to delete it
            $this->deletePerm($perm->id, $role->id);
        } else {
            # The role did not exist and nothing need to be done
        }
    }

	}

    	# Return a redirect
    	return redirect()->route('ERP::roles')->with('success', 'Changes saved');
    }

    public function checkPerm($perm_id, $role_id)
    {
        ERP::permissionToAccess('roles.access');

    	# This function returns true if the specified permission is found in the specified role and false if not

    	if(Permission_Role::wherePermission_idAndRole_id($perm_id, $role_id)->first()) {
    		return true;
    	} else {
    		return false; 
    	}
    }

    public function deletePerm($perm_id, $role_id)
    {
        ERP::permissionToAccess('roles.access');

    	$rel = Permission_Role::wherePermission_idAndRole_id($perm_id, $role_id)->first();
    	if($rel) {
    		$rel->delete();
    	}
    }

    public function addPerm($perm_id, $role_id)
    {
        ERP::permissionToAccess('roles.access');

    	$rel = Permission_Role::wherePermission_idAndRole_id($perm_id, $role_id)->first();
    	if(!$rel) {
    		$rel = new Permission_Role;
    		$rel->permission_id = $perm_id;
    		$rel->role_id = $role_id;
    		$rel->save();
    	}
    }

    public function destroy($id)
    {
        ERP::permissionToAccess('roles.access');
        
        # Check permissions
        ERP::permissionToAccess('roles.admin');

    	# Select Role
    	$role = ERP::role('id', $id);

        if(!$role->allow_editing and !ERP::loggedInuser()->su) {
            abort(403, trans('error_editing_disabled'));
        }

        # Check if it's su
        if($role->su) {
            return abort(403, trans('error_security_reasons'));
        }

        # Check if it's the default role
        if($role->id == ERP::defaultRole()->id) {
            abort(403, trans('error_security_reasons_default_role'));
        }

    	# Delete all relationships

    	# Permission Relation
    	$rels = Permission_Role::where('role_id', $id)->get();
    	foreach($rels as $rel) {
    		$rel->delete();
    	}
    	# Users Relation
    	$rels = Role_User::where('role_id', $id)->get();
    	foreach($rels as $rel) {
    		$rel->delete();
    	}

    	# Delete Role
    	$role->delete();

    	# Redirect the admin
    	return redirect()->route('ERP::roles')->with('success', 'Role Deleted');
    }
}
