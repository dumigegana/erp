<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SecurityController extends Controller
{
    public function confirm()
    {
    	return view('erp/security/confirm');
    }

    

    public function remove()
    {
    	return view('erp/security/remove');
    }
}
