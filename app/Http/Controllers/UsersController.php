<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Role_User;
use ERP;
Use Alert;
use Gate;
use App\User;
use App\Student;
use App\Staff;
use App\Visitor;

class UsersController extends Controller
{
     /* 
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. 
    |
    */
    public function search(Request $request){
     ERP::permissionToAccess('users.access');
    $read = "";
    $post = $request['keywords'];
    if(empty($post)) {
         $users = User::paginate(100);
     }
    else{
        $users = User::where('full_name','like', '%' . $post . '%')
                       ->orwhere('username','like', $post . '%')
                       ->orwhere('email','like', $post . '%')->get();
    }
    $active_users = ERP::users('active', true);

       

        # Get all roles
    $roles = ERP::roles();
    $read = view('erp/users/search', [ 
            'users'         =>  $users,
            'roles'         =>  $roles, 
            'active_users'  =>  $active_users,
        ]);
  return $read;

 }


    public function index()
    {
        ERP::permissionToAccess('users.access');

    	# Get all users
    	$users = User::paginate(100);
    	# Get the active users
    	$active_users = ERP::users('active', true);


    	# Get all roles
    	$roles = ERP::roles();

    	# Return the view
    	return view('erp/users/index', [ 
    		'users' 		=> 	$users,
    		'roles'			=>	$roles, 
    		'active_users'	=>	$active_users,
		]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('users.access');

    	# Find the user
    	$user = ERP::user('id', $id);

    	# Return the view
    	return view('erp/users/show', ['user' => $user]);
    }

    public function create()
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

       
        # Get all the data
        $data_index = 'users';
        require('Data/Create/Get.php');

        # Return the view
        return view('erp/users/create', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('admin.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # create the user
        $row = ERP::newUser();

        
        # Save the data
        $data_index = 'users';
        require('Data/Create/Save.php');

                
        # Activate the user if set
        if($request->input('active')){
            $row->active = true;
        }               
        

        # Save the user
        $row->save();
        # Return the admin to the users page with a success message
        return redirect()->route('ERP::users')->with('success', 'User created');
    }

    public function edit($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find the user
        $row = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($row);

        # Get all the data
        $data_index = 'users';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/users/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find the user
        $row = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($row);

        # Save the data
        $data_index = 'users';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::users')->with('success', 'User edited');
    }

    public function editRoles($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('roles.admin');

    	# Find the user
    	$user = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($user);

    	# Get all roles
    	$roles = ERP::roles()->where('name', '!=', 'Student');

    	# Return the view
    	return view('erp/users/roles', ['user' => $user, 'roles' => $roles]);
    }


    
    public function setRoles(Request $request)
    {
        ERP::permissionToAccess('users.access');

        // # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find the user
        $user = ERP::user('id', $request['id']);

        # Check if admin access
       // ERP::mustNotBeStudent($user);

        # Get all roles
        $roles = ERP::roles();

        # Change user's roles
        foreach($roles as $role) {
          if($request->input($role->id)){
            # The admin selected that role

            # Check if the user was already in that role
            if($this->checkRole($user->id, $role->id)) {
                # The user is already in that role, so no change is made
            } else {
                # Add the user to the selected role
                $this->addRel($user->id, $role->id);
            }
          } else {
            # The admin did not select that role

            # Check if the user was in that role
            if($this->checkRole($user->id, $role->id)) {
                # The user is in that role, so as the admin did not select it, we need to delete the relationship
               $this->deleteRel($user->id, $role->id);
            } else {
                # The user is not in that role and the admin did not select it
            }
          }            
        }
        # Return Redirect
        return redirect()->route('ERP::users')->with('success', 'Roles updated!');
    }

    public function checkRole($user_id, $role_id)
    {
        ERP::permissionToAccess('users.access');

        # This function returns true if the specified user is found in the specified role and false if not

        if(Role_User::whereUser_idAndRole_id($user_id, $role_id)->first()) {
            return true;
        } else {
            return false;
        }

    }

    public function deleteRel($user_id, $role_id)
    {
        ERP::permissionToAccess('users.admin');

      $rel = Role_User::whereUser_idAndRole_id($user_id, $role_id)->first();
      if($rel) {
           $rel->delete();
      }
    }

    public function addRel($user_id, $role_id)
    {
        // ERP::permissionToAccess('users.admin');

      $rel = Role_User::whereUser_idAndRole_id($user_id, $role_id)->first();
      if(!$rel) {
        $rel = new Role_User;
        $rel->user_id = $user_id;
        $rel->role_id = $role_id;
        $rel->save();
      }
    }

    

    public function destroy($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find The User
        $user = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($user);

        # Check if it's su
        if($user->su) {
            // abort(403, trans('error_security_reasons'));
        }

    	# Check before deleting
    	if($id == ERP::loggedInUser()->id) {
            // abort(403, trans('error_user_delete_yourself'));
    	} else {

    		
             $stud = Student::where('user_id', $user->id)->first();
            if($stud){
                $stud ->delete();
            }

             $staff = Staff::where('user_id', $user->id)->first();
            if($staff){
                $staff ->delete();
            }

             $visit = Visitor::where('user_id', $user->id)->first();
            if($visit){
                $visit ->delete();
            }

    		# Delete User
    		$user->delete();

    		# Return the admin with a success message
            return redirect()->route('ERP::users')->with('success', 'User deleted');
    	}
    }

   
}
