<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cohort;
use Auth;
use ERP;
Use Alert;

class CohortsController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('admissions.access');

        # Get all the cohorts
        $cohorts = Cohort::all();

        
        # Return the view
        return view('erp/cohorts/index', ['cohorts' => $cohorts]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Get the cohort
        $cohort = Cohort::find($id);

        # Return the view
        return view('admin/cohorts/show', ['cohort' => $cohort]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the cohort
        $row = Cohort::find($id);
        
        # Get all the data
        $data_index = 'cohorts';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/cohorts/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the row
        $row = Cohort::find($id);
       
        # Save the data
        $data_index = 'cohorts';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::cohorts')->with('success', 'Changes saved');
    }

    public function create()
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Get all the data
        $data_index = 'cohorts';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/cohorts/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # create new cohort
        $row = new Cohort;

        # Save the data
        $data_index = 'cohorts';
        require('Data/Create/Save.php');

        # Return the admin to the cohorts page with a success message
        return redirect()->route('ERP::cohorts')->with('success', 'Data Saved');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('admissions.access');
        
        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Select Cohort
        $cohort = ERP::cohort('id', $id);

        
        # Delete Cohort
        $cohort->delete();

        # Redirect the admin
        return redirect()->route('ERP::cohorts')->with('success', 'Deleted');
    }
}
