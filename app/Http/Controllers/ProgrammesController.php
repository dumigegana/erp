<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Programme;
use Auth;
use ERP;
Use Alert;

class ProgrammesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('programmes.access');

        # Get all the programmes
        $programmes = Programme::all();

        
        # Return the view
        return view('erp/programmes/index', ['programmes' => $programmes]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('programmes.access');

        # Get the programme
        $programme = Programme::find($id);

        # Return the view
        return view('admin/programmes/show', ['programme' => $programme]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('programmes.access');

        # Check permissions
        ERP::permissionToAccess('programmes.admin');

        # Find the programme
        $row = Programme::find($id);
        
        # Get all the data
        $data_index = 'programmes';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/programmes/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('programmes.access');

        # Check permissions
        ERP::permissionToAccess('programmes.admin');

        # Find the row
        $row = Programme::find($id);
       
        # Save the data
        $data_index = 'programmes';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::programmes')->with('success', 'Changes Saved');
    }

    public function create()
    {
        ERP::permissionToAccess('programmes.access');

        # Check permissions
        ERP::permissionToAccess('programmes.admin');

        # Get all the data
        $data_index = 'programmes';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/programmes/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('programmes.access');

        # Check permissions
        ERP::permissionToAccess('programmes.admin');

        # create new programme
        $row = new Programme;

        # Save the data
        $data_index = 'programmes';
        require('Data/Create/Save.php');

        # Return the admin to the programmes page with a success message
        return redirect()->route('ERP::programmes')->with('success', 'Program Created');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('programmes.access');
        
        # Check permissions
        ERP::permissionToAccess('programmes.admin');

        # Select Programme
        $programme = ERP::programme('id', $id);

        
        # Delete Programme
        $programme->delete();

        # Redirect the admin
        return redirect()->route('ERP::programmes')->with('success', 'Deleted');
    }
}
