<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Applicant;
use App\Study_program;
use App\Programme;
use App\Cohort;
use App\Check;
use ERP;
Use Alert;

class DashboardController extends Controller
{
    #
      public function dash()
    {
        // ERP::permissionToAccess('dashboard.access');        

        # Return the view
        return view('dashboard');
    }

    
}
