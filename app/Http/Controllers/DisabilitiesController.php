<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Disability;
use Auth;
use ERP;
Use Alert;

class DisabilitiesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('admissions.access');

        # Get all the disabilities
        $disabilities = Disability::all();

        
        # Return the view
        return view('erp/disabilities/index', ['disabilities' => $disabilities]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Get the disability
        $disability = Disability::find($id);

        # Return the view
        return view('admin/disabilities/show', ['disability' => $disability]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the disability
        $row = Disability::find($id);
        
        # Get all the data
        $data_index = 'disabilities';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/disabilities/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the row
        $row = Disability::find($id);
       
        # Save the data
        $data_index = 'disabilities';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::disabilities')->with('success', 'Disability Changed');
    }

    public function create()
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Get all the data
        $data_index = 'disabilities';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/disabilities/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # create new disability
        $row = new Disability;

        # Save the data
        $data_index = 'disabilities';
        require('Data/Create/Save.php');
        
        # Return the admin to the disabilities page with a success message
        return redirect()->route('ERP::disabilities')->with('success', 'Disability saved');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('admissions.access');
        
        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Select Disability
        $disability = ERP::disability('id', $id);

        
        # Delete Disability
        $disability->delete();

        # Redirect the admin
        return redirect()->route('ERP::disabilities')->with('success', 'Deleted');
    }
}
