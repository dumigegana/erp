<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Dean;
use ERP;

class DeansController extends Controller
{
    public function index()
    {
        ERP::permissionToAccess('hr.admin');

        # Get all the deans
        $deans = Dean::all();
        
        # Return the view
        return view('erp/deans/index', ['deans' => $deans]);
    }
}
