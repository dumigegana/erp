<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sem_fee;
use App\Semester;
use App\CoursePart;
use App\Staff;
use Auth;
use ERP;
Use Alert;

class SemfeesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('registration.access');

        
        # Return the view
        return view('erp/sem_fees/index');
    }    
}
