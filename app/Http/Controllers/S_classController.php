<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Student;
use App\Semester;
use App\SemCourse;
use App\Hod;
use App\Dean;
use App\Course;
use App\Weight;
use App\Part;
Use Auth;
use Carbon\Carbon;
use App\Exports\RegistrationsExportMapping;
use App\Imports\ResultsImport;
use Excel;
use Maatwebsite\Excel\Validators\ValidationException;
//use DB;

class S_classController extends Controller 
{  
    public function index()
    {        
        $staff_id = Auth::user()->staff->id;
    	 ERP::permissionToAccess('academic.access');
    	
        $semCoses = SemCourse::with('registrations.result')->has('registrations')
                            ->where('staff_id', $staff_id)
                            ->paginate(50);
        $state = 8;

    	return view('erp/classes/index', ['semCoses' => $semCoses, 'state' => $state ]);
    }
    
    public function hod()
    {
         ERP::permissionToAccess('academic.hod');           
        $staff_id = Auth::user()->staff->id;
        $hod = Hod::where('staff_id', $staff_id)
                    ->where('active', true)->first();
        if($hod) 
        {           
            $semCoses = SemCourse::with('registrations.result')->has('registrations')
                                ->where('lect_submit', true)
                                ->wherehas('course_part', function($q) use ($hod) {
                                    $q->wherehas('programme', function ($x) use ($hod) {
                                        $x->where('department_id', $hod->department_id);
                                    });
                                })->paginate(50);

           $state = 7;
        return view('erp/classes/index', ['semCoses' => $semCoses, 'state' => $state ]);
        } else {
            abort(401, "You are not authorised to access this area");
        }
    }

    public function dean()  
    {
         ERP::permissionToAccess('academic.dean');
        $staff_id = Auth::user()->staff->id;
        $dean = Dean::where('staff_id', $staff_id)
                    ->where('active', true)->first();

        if($dean) 
        {           
            $semCoses = SemCourse::with('registrations.result')->has('registrations')                ->where('lect_submit', true)
                                ->where('dept_aproved', true) 
                                ->wherehas('course_part', function($q) use ($dean) {
                                    $q->wherehas('programme', function ($x) use ($dean) {
                                        $x->wherehas('department',function ($z) use ($dean) {
                                            $z->where('faculty_id', $dean->faculty_id);
                                        });
                                    });
                                })->paginate(50);

           $state = 6;

        return view('erp/classes/index', ['semCoses' => $semCoses, 'state' => $state ]);
        } else {
            abort(401, "You are not authorised to access this area");
        }        

    }

    public function registry()
    {
        ERP::permissionToAccess('academic.reg');
        $semCoses = SemCourse::with('registrations.result')->has('registrations')        
                            ->where('lect_submit', true)
                            ->where('dept_aproved', true)
                            ->where('faclt_aproved', true)
                            ->paginate(50);

       $state = 5;

        return view('erp/classes/index', ['semCoses' => $semCoses, 'state' => $state ]);
    }

    public function approve_results($state, $name)
    {
        $stat = decrypt($state);
        $id = decrypt($name) / $stat;
        $msg = "Somthing is wrong";
        $rt = 'ERP::dash';

        $semC = SemCourse::find($id);

        if($stat == 8) //Lecturer
        { 
            $semC->update(['lect_submit' => true, 'state' => $stat]);
            $msg = "Grades submited Succesifully";
            $rt = 'ERP::s_class';
        }
        
        elseif($stat == 7)  //Department Chair
        {
           $semC->update(['dept_aproved' => true, 'state' => $stat]);
            $msg = "Grades submited Succesifully";
            $rt = 'ERP::s_class_hod';  
        }
        
        elseif($stat == 6) //Faculty Chair
        {
             $semC->update(['faclt_aproved' => true, 'state' => $stat]);
            $msg = "Grades submited Succesifully";
            $rt = 'ERP::s_class_dn';
        }
        
        elseif($stat == 5 ) //Academic Board Chair  
        {
            $senC->update(['acad_aproved' => true, 'state' => $stat]);
            $msg = "Grades submited Succesifully";
            $rt = 'ERP::s_class_reg'; 
        }        

        # Redirect the admin
        return redirect()->route($rt)->with('success', $msg);
    }

    // Get Class list
    public function class_l($id, $state )
    {
    	ERP::permissionToAccess('academic.access');
        
        $semCos = SemCourse::with('registrations.result')->find(decrypt($id)); 

        $weights = Weight::all(); 
        $name = decrypt($id);
        return view('erp/classes/list', ['semCos' => $semCos, 'weights' => $weights, 'name' => $name, 'state' => $state]);
    }

    // Download Class list to excel file
    public function export($sid)
    {  
        $now = Carbon::now();
        $id = decrypt($sid);
        $semCos = SemCourse::find($id);
        $name = $semCos->course_part->course->code ."_". $now->year ."_". $now->month.".xlsx";
        return Excel::download(new RegistrationsExportMapping($id), $name);
    }

    // Upload Results from excel file
    public function import(Request $request)
    { 
    	ERP::permissionToAccess('academic.admin');
        $validated = $request->validate([
            'file' => 'required|max:1000|mimes:xlsx,xls,csv',
            'weight_id' => 'required'
        ]);
          
	   $data = [
            'weight_id' => $request['weight_id'], 
            // other data here
        ];
      
       $import = new ResultsImport($data);
        $import->import($request->file('file'));
         Excel::import(new ResultsImport($data), $request->file('file'));

        if($import->failures()->isNotEmpty()) {
            dd($import->failures());
            return back()->withFailure($import->failures());
        }
       return redirect()->back()->with(['success'=>'Marks  uploaded successfully']);
    }


    public function update($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }

    public function finance($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }

    public function approve($id)
    {
    	ERP::permissionToAccess('registration.admin');
    	$row = Registration::findOrFail('id',$id);

    	return redirect()->route('ERP::reg')->with('success', 'Registration Changed');
    }
}
