<?php

namespace App\Http\Controllers;

use Request;
use Auth;
use Storage;
use Schema;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Permission; 
use App\Student;
use App\Staff;
use App\Visitor;
use App\Semester;
use App\Maintenance; 
use App\Department;;
use App\Course;
use URL;


/** 
 * The ERP class
 */

class ERP extends Controller
{
    public static function newUser()
    {
        return new User;
    }

    public static function users($type = null, $data = null)
    {
        if($type and $data) {
            return User::where($type, $data)->get();
        }
        return User::paginate(50);
    }
    public static function students($type = null, $data = null)
    {
        if($type and $data) {
            return Student::where($type, $data)->get();
        }
        return Student::paginate(50);
    }
   
    public static function courses($type = null, $data = null)
    {
        if($type and $data) {
            return Course::where($type, $data)->get();
        }
        return Course::all();
    }
    
    public static function semesters($type = null, $data = null)
    {
        if($type and $data) {
            return Semester::where($type, $data)->get();
        }
        return Semester::all();
    }

    public static function course($type, $data)
    {
        if($type == 'id') {
            return Course::findOrFailOrFail($data);
        }
        return Course::where($type, $data)->first();
    }

    public static function roles($type = null, $data = null)
    {
        if($type and $data) {
            return Role::where($type, $data)->get();
        }
        return Role::where('name', '!=', 'Student')->get();
    }

    public static function permissions($type = null, $data = null)
    {
        if($type and $data) {
            return Permission::where($type, $data)->get();
        }
        return Permission::all();
    }
    

    public static function user($type, $data)
    {
        if($type == 'id') {
            return User::findOrFail($data);
        }
        return User::where($type, $data)->first();
    }

    public static function role($type, $data)
    {
        if($type == 'id') {
            return Role::findOrFail($data);
        }
        return Role::where($type, $data)->first();
    }

    public static function permission($type, $data)
    {
        if($type == 'id') {
            return Permission::findOrFail($data);
        }
        return Permission::where($type, $data)->first();
    }

    public static function student($type, $data)
    {
        if($type == 'id') {
            return Student::findOrFail($data);
        }
        return Student::where($type, $data)->first();
    }

    

    public static function depatment($type, $data)
    {
        if($type == 'id') {
            return Department::findOrFail($data);
        }
        return Department::where($type, $data)->first();
    }

    public static function permissionToAccess($slug)
    {
        if(!Auth::user()->hasPermission($slug)) {
            abort(401, "You don't have permissions to access this area");
        }
    }
    public static function loggedInUser() {
        return Auth::user();
    }

    public static function scanFiles($directory){
        return scandir($directory);
    }

    public static function files()
    {
        $files = Storage::files();
        $ignore = ['.gitignore'];
        $final_files = [];
        foreach($files as $file) {
            $add = true;
            foreach($ignore as $ign){
                if($ign == $file) {
                    $add = false;
                }
            }
            if($add) {
                array_push($final_files, $file);
            }
        }
        $files = $final_files;

        return $files;
    }

    public static function isDocument($file_name)
    {
        if(ERP::document('name', $file_name)) {
            return true;
        } else {
            return false;
        }
    }

    
    public static function imageFormats()
    {
        return ['png', 'jpg', 'jpeg', 'gif', 'bmp'];
    }

    public static function checkInstalled()
    {
        if(env('ERP_INSTALLED', false)){
            return true;
        }
        return false;
    }

   
    
    public static function allowEditingField()
    {
        return 'allow_editing';
    }

   
    public static function checkValueInRelation($data, $value, $value_index)
    {
        foreach($data as $dta) {
            if($dta->$value_index == $value) {
                return true;
            }
        }
        return false;
    }

    public static function rettype($mixed, $type = NULL) {
        $type === NULL || settype($mixed, $type);
        return $mixed;
    }

   
    
    public static function permissionName($slug)
    {
        $perm_file = 'permissions';
        $trans = trans($perm_file.'.'.$slug);
        if($perm_file.'.'.$slug == $trans) {
            return $slug;
        } else {
            return $trans;
        }
    }

    public static function permissionDescription($slug)
    {
        $perm_file = 'permissions';
        $trans = trans($perm_file.'.'.$slug.'_desc');
        $slug = $slug . '_desc';
        if($perm_file.'.'.$slug == $trans) {
            return "No description";
        } else {
            return $trans;
        }
    }

    ### Grouped by Model

    #Staff Model
    public static function staffs($type = null, $data = null)
    {
        if($type and $data) {
            return Staff::where($type, $data)->get();
        }
        return Staff::paginate(50);
    }
    
    public static function staff($type, $data)
    {
        if($type == 'id') {
            return Staff::findOrFail($data);
        }
        return Staff::where($type, $data)->first();
    }

    public static function dataPath()
    {
        return app_path() . '/Http/Controllers/Data';
    }

    public static function apiData()
    {
        require('Data/API.php');
        if($api){
            return $api;
        }
    }

    public static function mustBeStaff($user)
    {
        if(!ERP::isStaff($user) and !ERP::isAdmin($user)) {
            abort(403, trans('Only staff allowed'));
        }
    }
    public static function mustBeStudent($user)
        {
            if(!ERP::isStudent($user) and !ERP::isAdmin($user)) {
                abort(403, trans('Only Students allowed '));
            }
        }

 
    public static function mustNotBeStudent($user)
    {
        if(ERP::isStudent($user) and ERP::loggedInUser()->su) {
            abort(403, trans('Action not allowed on Student accounts. '));
        }
    }

    public static function mustNotBeStaff($user)
    {
        if(ERP::isStaff($user) and ERP::loggedInUser()->su) {
            abort(403, trans('Action not allowed on Staff accounts. '));
        }
    }

    public static function mustNotBeAdmin($user)
    {
        if(ERP::isAdmin($user)) {
            abort(403, trans('Action can\'t be performed on Admin user'));
        }
    }

    public static function isStaff($user)
    {
        foreach($user->roles as $role) {
            if($role->name == 'Staff') {
                return true;
            }
        }
    return false;
    }

    public static function isAdmin($user)
    {
        foreach($user->roles as $role) {
            if($role->name == 'Admin') {
                return true;
            }
        }
    return false;
    }

    public static function isStudent($user)
    {
        foreach($user->roles as $role) {
            if($role->name == 'Student') {
                return true;
            }
        }
        return false;
    }

    // public static function isStaff($user)
    // {
    //     return $user->isStaff();
    // }

    // public static function isStudent($user)
    // {
    //     return $user->isStudent();
    // }

}
