<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Part;
use Auth;
use ERP;
Use Alert;

class PartsController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('registration.access');

        # Get all the parts
        $parts = Part::paginate(20);

        
        # Return the view
        return view('erp/parts/index', ['parts' => $parts]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('registration.access');

        # Get the part
        $part = Part::find($id);

        # Return the view
        return view('admin/parts/show', ['part' => $part]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the part
        $row = Part::find($id);
        
        # Get all the data
        $data_index = 'parts';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/parts/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the row
        $row = Part::find($id);
       
        # Save the data
        $data_index = 'parts';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::parts')->with('success','Updated Successfully');
    }

    public function create()
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Get all the data
        $data_index = 'parts';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/parts/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # create new part
        $row = new Part;

        # Save the data
        $data_index = 'parts';
        require('Data/Create/Save.php');

        # Return the admin to the parts page with a success message
        return redirect()->route('ERP::parts')->with('success Success', 'Created');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('registration.access');
        
        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Select Part
        $part = ERP::part('id', $id);

        
        # Delete Part
        $part->delete();

        # Redirect the admin
        return redirect()->route('ERP::parts')->with('success', 'Deleted Successfully');
    }
}
