<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SemCourse;
use App\Semester;
use App\CoursePart;
use App\Staff;
use Auth;
use ERP;
Use Alert;

class Sem_CoursesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('registration.access');

        
        # Return the view
        return view('erp/sem_courses/index');
    }

    public function show($id)
    {
        ERP::permissionToAccess('registration.access');

        # Get the sem_course
        $sem_course = SemCourse::find($id);

        # Return the view
        return view('admin/sem_courses/show', ['sem_course' => $sem_course]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the sem_course
        $row = SemCourse::find($id);
        $staffs = Staff::all();
        $course_parts = CoursePart::all();
        # Get all the data
        $data_index = 'sem_courses';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/sem_courses/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'staffs'    =>  $staffs,
            'course_parts'        =>  $course_parts,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the row
        $row = SemCourse::find($id);
       
        # Save the data        
        $row->update([
            'semester_id'=>$request['semester_id'], 
            'staff_id' =>$request['staff_id'], 
            'course_part_id' =>$request['course_part_id']
        ]);   

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::sem_courses')->with('success', 'Changes saved');
    }

    public function create()
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');


        $staffs = Staff::all();
        $course_parts = CoursePart::all();
        // dd($course_parts);

        # Get all the data
        $data_index = 'sem_courses';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/sem_courses/create', [
            'fields'        =>  $fields,
            'staffs'        =>  $staffs,
            'course_parts'        =>  $course_parts,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

         #Validate posted data
        $this->validate($request, [
            'course_part_id' => 'required|integer',
            'semester_id' => 'required|integer',
            'staff_id' => 'required|integer',
        ]);    
        
        #Store the New Sem_Course
        $applicant = SemCourse::create($request->all());

        # Return the admin to the sem_courses page with a success message
        return redirect()->route('ERP::sem_courses')->with('success', 'Data saved');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('registration.access');
        
        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Select Sem_Course
        $sem_course = ERP::sem_course('id', $id);

        
        # Delete Sem_Course
        $sem_course->delete();

        # Redirect the admin
        return redirect()->route('ERP::sem_courses')->with('success', 'Deleted');
    }
}
