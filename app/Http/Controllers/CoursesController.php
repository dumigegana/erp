<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use Auth;
use ERP;
Use Alert;

class CoursesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('academic.access');

        # Get all the courses
        $courses = Course::all();

        
        # Return the view
        return view('erp/courses/index', ['courses' => $courses]);
    }

    public function show($id) 
    {
        ERP::permissionToAccess('academic.access');

        # Get the course
        $course = Course::find($id);

        # Return the view
        return view('admin/courses/show', ['course' => $course]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('academic.access');

        # Check permissions
        ERP::permissionToAccess('academic.admin');

        # Find the course
        $row = Course::find($id);
        
        # Get all the data
        $data_index = 'courses';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/courses/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('academic.access');

        # Check permissions
        ERP::permissionToAccess('academic.admin');

        # Find the row
        $row = Course::find($id);
       
        # Save the data
        $data_index = 'courses';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::courses')->with('success', trans('msg_course_edited'));
    }

    public function create()
    {
        ERP::permissionToAccess('academic.access');

        # Check permissions
        ERP::permissionToAccess('academic.admin');

        # Get all the data
        $data_index = 'courses';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/courses/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('academic.access');

        # Check permissions
        ERP::permissionToAccess('academic.admin');

        # create new course
        $row = new Course;

        # Save the data
        $data_index = 'courses';
        require('Data/Create/Save.php');

        # Return the admin to the courses page with a success message
        return redirect()->route('ERP::courses')->with('success', trans('msg_course_created'));
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('academic.access');
        
        # Check permissions
        ERP::permissionToAccess('academic.admin');

        # Select Course
        $course = ERP::course('id', $id);

        
        # Delete Course
        $course->delete();

        # Redirect the admin
        return redirect()->route('ERP::courses')->with('success', trans('msg_course_deleted'));
    }
}
