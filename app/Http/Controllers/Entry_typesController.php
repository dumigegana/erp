<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entry_type;
use Auth;
use ERP;
Use Alert;

class Entry_typesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('admissions.access');

        # Get all the entry_types
        $entry_types = Entry_type::all();

        
        # Return the view
        return view('erp/entry_types/index', ['entry_types' => $entry_types]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Get the entry_type
        $entry_type = Entry_type::find($id);

        # Return the view
        return view('admin/entry_types/show', ['entry_type' => $entry_type]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the entry_type
        $row = Entry_type::find($id);
        
        # Get all the data
        $data_index = 'entry_types';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/entry_types/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Find the row
        $row = Entry_type::find($id);
       
        # Save the data
        $data_index = 'entry_types';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::entry_types')->with('success', 'Changes saves');
    }

    public function create()
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Get all the data
        $data_index = 'entry_types';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/entry_types/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('admissions.access');

        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # create new entry_type
        $row = new Entry_type;

        # Save the data
        $data_index = 'entry_types';
        require('Data/Create/Save.php');

        # Return the admin to the entry_types page with a success message
        return redirect()->route('ERP::entry_types')->with('success','Data saved');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('admissions.access');
        
        # Check permissions
        ERP::permissionToAccess('admissions.admin');

        # Select Entry_type
        $entry_type = ERP::entry_type('id', $id);

        
        # Delete Entry_type
        $entry_type->delete();

        # Redirect the admin
        return redirect()->route('ERP::entry_types')->with('success', 'Deleted!');
    }
}
