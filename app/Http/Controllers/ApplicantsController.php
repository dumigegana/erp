<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Applicant;
use App\Entry_type;
use App\Study_program;
use App\Programme; 
use App\Applicant_Programme;
use App\Disability;
use App\Cohort;
use App\Check;
Use Alert;

class ApplicantsController extends Controller
{
    #
      public function index()
    {
        ERP::permissionToAccess('admissions.access');
        return view('erp/applicants/index');
    }

    public function show($id)
    {
         ERP::permissionToAccess('admissions.access');

        # Get the applicant
        $applicant = Applicant::findOrFail($id);

        # Get all the Programmes
        $programmes = Programme::all();
        

        # Return the view
        return view('erp/applicants/show', ['applicant' => $applicant, 'programmes' => $programmes]);
    }

    public function edit($id) 
    {
         ERP::permissionToAccess('admissions.access');

        # Check permissions
         ERP::permissionToAccess('admissions.admin');
        $entry_types = Entry_type::all();
        # Find the applicant
        $row = Applicant::find($id);

        // if(!$row->allow_editing and !ERP::loggedInuser()->su) {
        //     abort(403, trans('error_editing_disabled'));
        // }

        # Get all the data
        $data_index = 'applicants';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/applicants/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
            'entry_types' =>  $entry_types,
        ]);
    }

    public function update($id, Request $request)
    {
         ERP::permissionToAccess('admissions.access');

        # Check permissions
         ERP::permissionToAccess('admissions.admin');

        # Find the row
        $row = Applicant::find($id);

        if(!$row->allow_editing and !ERP::loggedInuser()->su) {
            abort(403, trans('error_editing_disabled'));
        }

        # Save the data
        $data_index = 'applicants';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::applicants')->with('success', 'Changes saved');
    }

    public function academic($id)
    {
         ERP::permissionToAccess('admissions.access');

        # Check permissions
         ERP::permissionToAccess('admissions.admin');
        # Find the applicant
        $row = Applicant::findOrFail($id);
        
        # Return the view
        return view('erp/applicants/accademic', [
            'row'       =>  $row
        ]);
    }

    public function create()
    {
         ERP::permissionToAccess('admissions.access');

        # Check permissions
         ERP::permissionToAccess('admissions.admin');

         #Relationships
         $entry_types = Entry_type::all();
         $study_programs = Study_program::all();
         $disabilities = Disability::all();         
         $programmes = Programme::all();
         $cohorts = Cohort::where('active', true)->get();

        # Get all the data
        $data_index = 'applicants';
        require('Data/Create/Get.php');

        
        # Return the view
        return view('erp/applicants/create', [
            'programmes'   =>  $programmes,
            'cohorts'   =>  $cohorts,
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
            'entry_types' =>  $entry_types,
            'study_programs' =>  $study_programs,
            'disabilities' =>  $disabilities,
        ]);
    }

    public function store(Request $request)
    {
         ERP::permissionToAccess('admissions.access');

        # Check permissions
         ERP::permissionToAccess('admissions.admin');

         $this->validate($request, [
            'entry_type_id' => 'required|integer',
            'cohort_id' => 'required|integer',
            'study_program_id' => 'required|integer',
            'referee_1' => 'nullable', 
            'ref1_address' =>'nullable', 
            'referee_2' =>'nullable', 
            'ref2_address' =>'nullable', 
            'place_of_birth' =>'required',
            'cv_fl' =>'nullable|max:2048',
            'id_card_fl' =>'required|max:2048',
            'birth_certificate_fl' =>'required|max:2048',
            'programmes'  => 'required|array|min:4|',
            'first_name'   => 'required',
            'surname'      => 'required', 
            'email'        => 'required|unique:applicants|email',
            'sex'        => 'required',
            'home_address' => 'required',  
            'national_id'  => 'required', 
            'tittle'       => 'required', 
            'marital_status' => 'required', 
            'nationality'  => 'required', 
            'citizenship'  => 'required', 
            'perm_res_zim' => 'required',
            'cell_number'  => 'required',
            'home_phone'   => 'required', 
            'date_of_birth'=> 'required', 
            'birth_cert_no'=> 'required', 
            'place_of_birth'=>'required', 
            'prospective_sponsor' => 'required',
        ]);

        $id_card = $request->file('id_card_fl')->store('id');
        $birth_certificate = $request->file('birth_certificate_fl')->store('birth');

        $request->request->add(['id_card'  =>  $id_card]);        
        $request->request->add(['birth_certificate'  =>  $birth_certificate]);
        if($request['marriage_certificate_fl']) 
        {

        $marriage_certificate = $request->file('marriage_certificate_fl')->store('marriage');
         $request->request->add(['marriage_certificate'  =>  $marriage_certificate]);
        }

        if($request['cv_fl']) 
        {

        $cv = $request->file('cv_fl')->store('cv');
         $request->request->add(['cv'  =>  $cv]);
        }        
        
        #Store the New Applicant
        $applicant = Applicant::create($request->all());
        foreach( $request->programmes as $prog){
            $program = Programme::where('name', $prog)->first();
            $row = new Applicant_Programme;
            $row->applicant_id = $applicant->id; 
            $row->programme_id = $program->id;
            $row->save();


        }
        $check = new Check;
        $check->applicant_id = $applicant->id;
        $check->step = 3;
        $check->save();

        # Return the admin to the applicants page with a success message
        return redirect()->route('ERP::applicants')->with('success', 'Data stored');
    }
    // public function destroy($id)
    // {
    //      ERP::permissionToAccess('admissions.access');
        
    //     # Check permissions
    //      ERP::permissionToAccess('admissions.admin');

    //     # Select Applicant
    //     $applicant = ERP::applicant('id', $id);


    //     # Delete Applicant
    //     $applicant->delete();

    //     # Redirect the admin
    //     return redirect()->route('ERP::applicants')->with('success', 'Deleted'));
    // }
}
