<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Faculty;
use Auth;
use ERP;
Use Alert;

class FacultiesController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('faculties.access');

        # Get all the faculties 
        $faculties  = Faculty::all();
        
        # Return the view
        return view('erp/faculties/index', ['faculties' => $faculties ]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('faculties.access');

        # Get the faculty
        $faculty = Faculty::find($id);

        # Return the view
        return view('admin/faculties /show', ['faculty' => $faculty]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('faculties.access');

        # Check permissions
        ERP::permissionToAccess('faculties.admin');

        # Find the faculty
        $row = Faculty::find($id);
        
        # Get all the data
        $data_index = 'faculties ';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/faculties /edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('faculties.access');

        # Check permissions
        ERP::permissionToAccess('faculties.admin');

        # Find the row
        $row = Faculty::find($id);
       
        # Save the data
        $data_index = 'faculties ';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::faculties ')->with('success', 'Faculty Edited');
    }

    public function create()
    {
        ERP::permissionToAccess('faculties.access');

        # Check permissions
        ERP::permissionToAccess('faculties.admin');

        # Get all the data
        $data_index = 'faculties ';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/faculties /create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'permissions'   =>  $permissions,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('faculties.access');

        # Check permissions
        ERP::permissionToAccess('faculties.admin');

        # create new faculty
        $row = new Faculty;

        # Save the data
        $data_index = 'faculties ';
        require('Data/Create/Save.php');

        # Set the permissions
        $this->setPermissions($row->id, $request);

        # Return the admin to the faculties  page with a success message
        return redirect()->route('ERP::faculties ')->with('success', 'Faculty Created');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('faculties.access');
        
        # Check permissions
        ERP::permissionToAccess('faculties.admin');

        # Select Faculty
        $faculty = ERP::faculty('id', $id);

        
        # Delete Faculty
        $faculty->delete();

        # Redirect the admin
        return redirect()->route('ERP::faculties ')->with('success', 'Deleted');
    }
}
