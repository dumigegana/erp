<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class SubjectsController extends Controller
{  
    public function index()
    {
        ERP::permissionToAccess('admissions.access');
        
        # Return the view
        return view('erp/subjects/index');
    }
   
}
