<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Role_User;
use ERP;
Use Alert;
use Auth;
use Gate;
use App\User;
use App\Student;
use App\Staff;
use App\Visitor;

class StudentsController extends Controller
{
     /* 
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. 
    |
    */
    public function search(Request $request){
     ERP::permissionToAccess('admissions.access');
    $read = "";
    $post = $request['keywords'];
    if(empty($post)) {
         $students = Student::paginate(100);
     }
    else{
        $students = Student::where('gsu_id','like', '%' . $post . '%')
                       ->orwhere('username','like', $post . '%')
                       ->orwhere('email','like', $post . '%')->get();
    }
    $active_students = ERP::students('active', true);

       

        # Get all users
    $users = ERP::users();
    $read = view('erp/users/search', [ 
            'users'         =>  $users,
        ]);
  return $read;

 }


    public function index()
    {
        ERP::permissionToAccess('admissions.access');

    	# Get all students
    	$students = Student::paginate(100);

    	# Return the view
    	return view('erp/students/index');
    }

    public function show($id)
    {
        ERP::permissionToAccess('users.access');

    	# Find the user
    	$user = ERP::user('id', $id);

    	# Return the view
    	return view('erp/users/show', ['user' => $user]);
    }

    public function create()
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

       
        # Get all the data
        $data_index = 'users';
        require('Data/Create/Get.php');

        # Return the view
        return view('erp/users/create', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('admin.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # create the user
        $row = ERP::newUser();

        
        # Save the data
        $data_index = 'users';
        require('Data/Create/Save.php');

                
        # Activate the user if set
        if($request->input('active')){
            $row->active = true;
        }               
        

        # Save the user
        $row->save();
        # Return the admin to the users page with a success message
        return redirect()->route('ERP::users')->with('success', 'User created');
    }

    public function edit($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find the user
        $row = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($row);

        # Get all the data
        $data_index = 'users';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/users/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find the user
        $row = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($row);

        # Save the data
        $data_index = 'users';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::users')->with('success', 'Student records changed');
    }

    public function editRoles($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.roles');

    	# Find the user
    	$user = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($user);

    	# Get all roles
    	$roles = ERP::roles()->where('name', '!=', 'Student');

    	# Return the view
    	return view('erp/users/roles', ['user' => $user, 'roles' => $roles]);
    }


    public function checkRole($user_id, $role_id)
    {
        ERP::permissionToAccess('users.access');

    	# This function returns true if the specified user is found in the specified role and false if not
        $user = User::find($user_id);

    	if($user->role_id == $role_id) {
    		return true;
    	} else {
    		return false;
    	}

    }

    

    public function destroy($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find The User
        $user = ERP::user('id', $id);

        # Check if admin access
        // ERP::mustNotBeAdmin($user);

        # Check if it's su
        if($user->su) {
            abort(403, trans('erp.error_security_reasons'));
        }

    	# Check before deleting
    	if($id == ERP::loggedInUser()->id) {
            abort(403, trans('erp.error_user_delete_yourself'));
    	} else {

    		
             $stud = Student::where('user_id', $user->id)->first();
            if($stud){
                $stud ->delete();
            }

             $staff = Staff::where('user_id', $user->id)->first();
            if($staff){
                $staff ->delete();
            }

             $visit = Visitor::where('user_id', $user->id)->first();
            if($visit){
                $visit ->delete();
            }

    		# Delete User
    		$user->delete();

    		# Return the admin with a success message
            return redirect()->route('ERP::users')->with('success', 'Deleted');
    	}
    }

   
}
