
<?php
/*
+---------------------------------------------------------------------------+
| ERP Developer Data Configuration  ADVANCED SETTINGS								|
+---------------------------------------------------------------------------+
|                                                               			|
| * Available settings:                  									|
|							                                                |
| hide_display: Hides the field when the table is displayed                 |
| table: The table name 												    +-------------+
| hidden: Columns that will not be displayed in the edit form, and they won't be updated +----------------------------+
| empty: Columns that will not have their current value when editing them (eg: password field is hidden in the model) |
| confirmed: fields that will need to be confirmed twice                                                              +-+
| encrypted: Fields that will be encrypted using: Crypt::encrypt(); when they are saved and decrypted when editing them +---------------------------+
| hashed: Fields that will be hashed when they are saved in the database, will be empty on editing, and if saved as empty they will not be modified |
| masked: Fields that will be displayed as a type='password', so their content when beeing modified won't be visible +------------------------------+
| default_random: Fields that if no data is set, they will be randomly generated (10 characters) +-------------------+
| su_hidden: Columns that will be added to the hidden array if the user is su +------------------+
| code: Fields that can be edited using a code editor                       +-+
| wysiwyg: Fields that can be edited using a wysiwyg editor                 |
| validator: validator settings when executing: $this->validate();          |
| allow: allows to create/edit the row                                      |
| delete: allows to delete the row                                          |
|																			|
| Note: The first index indicates the table name    						|
|																			|
+---------------------------------------------------------------------------+
|																			|
| This file allows you to setup all the information                         |
| to be able to manage your app without problems            				|
|																			|
+---------------------------------------------------------------------------+
*/
use Illuminate\Validation\Rule;

if(!isset($row)){
    # the row will be the user logged in if no row is set
    $row = Auth::user();
}

$data = [

 
    'users' =>  [

        'hide_display'  =>  ['password'],
        'delete'    =>  true,
        'create'    =>  [
            'allowed'           =>  true,
            'hidden'            =>  ['id', 'su', 'active', 'banned', 'remember_token', 'created_at', 'updated_at', 'has_room'], 
            'default_random'    =>  ['password'],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
				'full_name'              => 'sometimes|required|max:255',
                'username'              => 'required|max:255|unique:users',
                'password'          => 'confirmed|min:8|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'username', 'remember_token', 'created_at', 'updated_at', 'has_room', ],
            'su_hidden'         =>  ['full_name', 'active', 'banned', 'password'],
            'empty'             =>  ['password'],
            'default_random'    =>  [],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'full_name'         => 'sometimes|required|max:255',
                'password'          => 'sometimes|required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/|confirmed',
            ],
        ],
    ],
    'students' =>  [

        'table'     =>  'students',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'gender', 'degree_level'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => ['required', 'max:255', Rule::unique('students')->ignore($row->id) ],
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at', 'gender', 'degree_level','updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => 'sometimes|required|max:255|unique:students',
            ],
        ],
    ],
];
