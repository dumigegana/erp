<?php


/*
+---------------------------------------------------------------------------+
| ERP Data Configuration												|
+---------------------------------------------------------------------------+
|                                                               			|
| * Available settings:                   									|
|																			|
| table: The table name 												    +-------------+
| hidden: Columns that will not be displayed in the edit form, and they won't be updated +----------------------------+
| empty: Columns that will not have their current value when editing them (eg: password field is hidden in the model) |
| confirmed: fields that will need to be confirmed twice                                                              +-+
| encrypted: Fields that will be encrypted using: Crypt::encrypt(); when they are saved and decrypted when editing them +---------------------------+
| hashed: Fields that will be hashed when they are saved in the database, will be empty on editing, and if saved as empty they will not be modified |
| masked: Fields that will be displayed as a type='password', so their content when beeing modified won't be visible +------------------------------+
| default_random: Fields that if no data is set, they will be randomly generated (10 characters) +-------------------+
| su_hidden: Columns that will be added to the hidden array if the user is su +------------------+
| code: Fields that can be edited using a code editor                       +-+
| wysiwyg: Fields that can be edited using a wysiwyg editor                 |
| validator: validator settings when executing: $this->validate();          |
| relations: a relationship between a column and a table, or a dropdown     |
|																			|
| Note: Do not change the first index               						|
|																			|
+---------------------------------------------------------------------------+
|																			|
| This file allows you to setup all the information                         |
| to be able to manage your app without problems            				|
|																			|
+---------------------------------------------------------------------------+
*/
use Illuminate\Validation\Rule;

if(!isset($row)){
    # the row will be the user logged in if no row is set
    $row = Auth::user();
}

$data = [
     
    'users' =>  [

        'table'     =>  'users',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'active',  'profile_picture', 'remember_token', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  ['password'],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'role_id'       =>  [
                        'data'  =>  ERP::roles(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],    
            'validator'         =>  [
                'first_name'         => 'required|max:255',
                'surname'         => 'required|max:255',
				'username'          => 'required|max:255|unique:users',
                'password'          => 'required|confirmed',                
                'cell_number'         => 'required|max:14',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'username', 'remember_token', 'created_at', 'updated_at', 'password', 'deleted_at'],
            'su_hidden'         =>  ['fname', 'lname', 'email', 'active', 'banned', 'password'],
            'empty'             =>  ['password'],
            'default_random'    =>  [],
            'confirmed'         =>  ['password'],
            'encrypted'         =>  [],
            'hashed'            =>  ['password'],
            'masked'            =>  ['password'],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'role_id'       =>  [
                        'data'  =>  ERP::roles(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],    
            'validator'         =>  [
				'full_name'         => 'sometimes|required|max:255',
                'password'          => 'sometimes|required|min:8|regex:/^(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*\d).+$/|confirmed',
            ],
        ],
    ],
     'applicants' =>  [

        'table'     =>  'applicants',
        'create'    =>  [
            'hidden'            =>  ['id',  'cohort_id', 'sex', 'nationality', 'citizenship', 'disability_id', 'student_id', 'gsu_id', 'terms_agree', 'programme_id', 'entry_type_id', 'study_program_id', 'admissions_comment', 'fdean_outcome', 'fdean_comment', 'hod_outcome', 'hod_comment', 'bursar_outcome', 'bursar_comment', 'created_at', 'outcome_at', 'has_dis', 'referee_1', 'admissions_outcome', 'ref1_address', 'referee_2', 'ref2_address', 'birth_certificate', 'tittle', 'marital_status', 'birth_cert_no', 'id_card', 'perm_res_zim', 'permit', 'zim_res_period', 'disability', 'disability_details', 'marriage_certificate', 'cv', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'first_name'   => 'required',
                'surname'      => 'required', 
                'email'        => 'equired|unique|email',
                'home_address' => 'required',  
                'national_id'  => 'reuired', 
                'tittle'       => 'required', 
                'marital_status' => 'required', 
                'nationality'  => 'required', 
                'citizenship'  => 'required', 
                'perm_res_zim' => 'required',
                'cell_number'  => 'required',
                'home_phone'   => 'required', 
                'date_of_birth'=> 'required', 
                'birth_cert_no'=> 'required', 
                'place_of_birth'=>'required', 
                'prospective_sponsor' => 'required',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id',  'user_id', 'student_id', 'gsu_id', 'terms_agree', 'programme_id', 'entry_type_id', 'admissions_comment', 'fdean_outcome', 'fdean_comment', 'hod_outcome', 'hod_comment', 'bursar_outcome', 'bursar_comment', 'created_at', 'outcome_at', 'referee_1', 'admissions_outcome', 'ref1_address', 'referee_2', 'ref2_address', 'perm_res_zim', 'permit', 'zim_res_period', 'disability', 'disability_details', 'birth_certificate', 'id_card', 'marriage_certificate', 'cv', 'updated_at', 'deleted_at'],
            'su_hidden'         =>  ['name'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                
            ],
            'relations'         =>  [
                'department_id'       =>  [
                        'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
                
                'entry_type_id'       =>  [
                        'data'  =>  App\Entry_type::all(),
                        'value' =>  'id',
                        'show'  =>  'type', 
                         ],
            'validator'         =>  [
                'gsu_id'         => 'required|max:255|unique:students',
                
            ],
        ],
    ],


    'students' =>  [

        'table'     =>  'students',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', 
                         ],
                'department_id'       =>  [
                       // 'data'  =>  ERP::department(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => 'required|max:255|unique:students',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name'],
                         ],
                'department_id'       =>  [
                       // 'data'  =>  ERP::departments(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => ['required', 'max:255', Rule::unique('students')->ignore($row->id) ],
                
            ],
            
        ],
    ],

   
    'staffs' =>  [

        'table'     =>  'staffs',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'gender', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],             
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  'full_name', 
                         ],
                'department_id'       =>  [
                        //'data'  =>  ERP::departments(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => 'required|max:255|unique:staffs',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'user_id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'user_id'       =>  [
                        'data'  =>  ERP::users(),
                        'value' =>  'id',
                        'show'  =>  ['full_name'],
                         ],
                'department_id'       =>  [
                       'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'programme', ],
                        ],
            'validator'         =>  [
                'gsu_id'         => ['required', 'max:255', Rule::unique('staffs')->ignore($row->id) ],
                
            ],
            
        ],
    ],
    
    'subjects' =>  [

        'table'     =>  'subjects',
        'create'    =>  [
            'hidden'            =>  ['id', 'type', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'type', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
            
        ], 
    ],

 'parts' =>  [

        'table'     =>  'parts',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                'part'         => 'required|integer|between:1,4',
                'semester'         => 'required|integer|between:1,2',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                'part'         => ['required|integer|between:1,4', Rule::unique('parts')->ignore($row->id) ],
                'semester'         => ['required|integer|between:1,2', Rule::unique('parts')->ignore($row->id) ],
            ],
            
        ], 
    ],


     'cohorts' =>  [

        'table'     =>  'cohorts',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
            
        ],
    ], 

     'entry_types' =>  [

        'table'     =>  'entry_types',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
            
        ],
    ],

     'disabilities' =>  [

        'table'     =>  'disabilities',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [],
            'validator'         =>  [
                
            ],
            
        ],
    ],

     'programmes' =>  [

        'table'     =>  'programmes',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'department_id'       =>  [
                       'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'department_id'       =>  [
                       'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                
            ],
            
        ],
    ],


     'course_parts' =>  [

        'table'     =>  'course_parts',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'part_id', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'course_id'       =>  [
                       'data'  =>  App\Course::all(),
                        'value' =>  'id',
                        'show'  =>  'code', 
                        ],
                'programme_id'       =>  [
                       'data'  =>  App\Programme::all(),
                        'value' =>  'id',
                        'show'  =>  'code'
                       ],
                    ],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at', 'part_id','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'course_id'       =>  [
                       'data'  =>  App\Course::all(),
                        'value' =>  'id',
                        'show'  =>  'name', 
                        ],
                'programme_id'       =>  [
                       'data'  =>  App\Programme::all(),
                        'value' =>  'id',
                        'show'  =>  'code',
                        ],
                  ],
            'validator'         =>  [
                
            ],
            
        ],
    ],


     'sem_courses' =>  [

        'table'     =>  'sem_courses',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'staff_id', 'course_part_id','updated_at', 'deleted_at','ap_level', 'dept_chair', 'faclt_chair', 'acad_chair', 'dept_aproved', 'faclt_aproved', 'acad_aproved'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'semester_id'       =>  [
                       'data'  =>  App\Semester::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at', 'staff_id', 'course_part_id','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'semester_id'       =>  [
                       'data'  =>  App\Semester::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                    
            ],
            
        ],
    ],

     'departments' =>  [

        'table'     =>  'departments',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'faculty_id'       =>     [
                       'data'  =>  App\Faculty::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'          => 'required|max:255',
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'faculty_id'       =>  [
                       'data'  =>  App\Faculty::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                'name'         => ['required|max:255', Rule::unique('semesters')->ignore($row->id) ],                
            ],
            
        ],
    ],

     'faculties' =>  [

        'table'     =>  'faculties',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],            
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'dean_id'       =>  [
                       'data'  =>  App\Dean::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                
            ],
        ],
        'edit'      =>  [
             'hidden'            =>  ['id', 'created_at','updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'dean_id'       =>  [
                       'data'  =>  App\Dean::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                        ],
            'validator'         =>  [
                
            ],
            
        ],
    ],

    'roles' =>  [

        'table'     =>  'roles',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => 'required|unique:roles',
                
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at', 'deleted_at'],
            'su_hidden'         =>  ['name'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                
            ],
        ],
    ],


    'permissions'   => [
        'table'     =>  'permissions',
        'create'    =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'slug'  => 'required|max:255|unique:permissions',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'su', 'created_at', 'updated_at'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'slug'  => ['required', 'max:255', Rule::unique('permissions')->ignore($row->id) ],
            ],
        ],
    ],
    'semesters'   => [
        'table'     =>  'semesters',
        'create'    =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => 'required|max:255|unique:semesters',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'validator'         =>  [
                'name'  => ['required', 'max:255', Rule::unique('semesters')->ignore($row->id) ],
            ],
        ],
    ],

    'courses' =>  [

        'table'     =>  'courses',
        'create'    =>  [
            'hidden'            =>  ['id',  'created_at', 'updated_at', 'deleted_at'],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'department_id'       =>  [
                        'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],                
            'validator'         =>  [
                'name'  => 'required|unique:courses',
                'code'  => 'required|unique:courses',
            ],
        ],
        'edit'      =>  [
            'hidden'            =>  ['id', 'created_at', 'updated_at', 'deleted_at'],
            'su_hidden'         =>  [],
            'empty'             =>  [],
            'default_random'    =>  [],
            'confirmed'         =>  [],
            'encrypted'         =>  [],
            'hashed'            =>  [],
            'masked'            =>  [],
            'code'              =>  [],
            'wysiwyg'           =>  [],
            'relations'         =>  [
                'department_id'       =>  [
                        'data'  =>  App\Department::all(),
                        'value' =>  'id',
                        'show'  =>  'name', ],
                ],    
            'validator'         =>  [
                'name' => 'sometimes|required|unique:courses,name,'.$row->id,
                'code' => 'sometimes|required|unique:courses,code,'.$row->id,
            ],
        ],
    ],

];
