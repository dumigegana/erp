<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Semester;
use ERP;

class BillingController extends Controller
{

    public function index()
    {
        #ERP::permissionToAccess('bursars.access');

        # Get all the cohorts
        $semesters = Semester::with('sem_courses.registrations')->get();
        
        # Return the view
        return view('erp/billing/index', ['semesters' => $semesters]);
    }
    
   
    public function bill($id)
   {
        ERP::permissionToAccess('bursars.access');

        # Check permissions
        ERP::permissionToAccess('bursars.admin');

        # Select Semester
        $semester = Semester::find('id', $id)->update(['billed' => true]); 

        # Return the admin to the cohorts page with a success message
        return redirect()->route('ERP::billing')->with('success', 'Billed');
    }

    
    public function un_bill($id)
    {
        ERP::permissionToAccess('bursars.access');
        
        # Check permissions
        ERP::permissionToAccess('bursars.admin');

        # Select Semester
        $semester = Semester::find('id', $id)->update(['billed' => false]); 

        # Redirect the admin
        return redirect()->route('ERP::billing')->with('success', 'Changed');
    }
}
