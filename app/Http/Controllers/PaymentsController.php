<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Payment;
Use Alert;

class PaymentsController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('bursary.access');

        # Get all the faculties 
        $payments  = Payment::all();
        
        # Return the view
        return view('erp/payments/index', ['payments' => $payments ]);
    }

    
}
