<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hod;
use ERP;

class HodsController extends Controller
{
    public function index()
    {
        ERP::permissionToAccess('hr.admin');

        # Get all the hods
        $hods = Hod::all();
        
        # Return the view
        return view('erp/hods/index', ['hods' => $hods]);
    }
}
