<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Part;
use App\CoursePart;
use Auth;
use ERP;
use PDF;
Use Alert;

class Course_PartsController extends Controller
{

    public function index()
    {
        ERP::permissionToAccess('check.sheet.access');

        # Get all the course_parts
        $course_parts = CoursePart::all();

        
        # Return the view
        return view('erp/course_parts/index', ['course_parts' => $course_parts]);
    } 

    public function show($id)
    {
        ERP::permissionToAccess('check.sheet.access');

        # Get the course_part
        $course_part = CoursePart::find($id);

        # Return the view
        return view('admin/course_parts/show', ['course_part' => $course_part]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('check.sheet.access');

        # Check permissions
        ERP::permissionToAccess('check.sheet.admin');

        # Find the course_part
        $row = CoursePart::find($id);
        $parts =Part::all();
        # Get all the data
        $data_index = 'course_parts';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/course_parts/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'parts'    =>  $parts,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    {
        ERP::permissionToAccess('check.sheet.access');

        # Check permissions
        ERP::permissionToAccess('check.sheet.admin');

        # Find the row
        $row = CoursePart::findOrFail($id);

        $row->update([
            'part_id'=>$request['part_id'], 
            'course_id' =>$request['course_id'], 
            'programme_id' =>$request['programme_id']
        ]);    
        

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::course_parts')->with('success', 'Changes saved');
    }

    public function create()
    {
        ERP::permissionToAccess('check.sheet.access');

        # Check permissions
        ERP::permissionToAccess('check.sheet.admin');
        $parts =Part::all();

        # Get all the data
        $data_index = 'course_parts';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/course_parts/create', [
            'fields'        =>  $fields,
            'parts'        =>  $parts,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('check.sheet.access');

        # Check permissions
        ERP::permissionToAccess('check.sheet.admin');
        
        #Validate posted data
        $this->validate($request, [
            'course_id' => 'required|integer',
            'part_id' => 'required|integer',
            'programme_id' => 'required|integer',
        ]);    
        
        #Store the New Course_Part
        $applicant = CoursePart::create($request->all());

        # Return the admin to the course_parts page with a success message
        return redirect()->route('ERP::course_parts')->with('success', 'Data saved');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('check.sheet.access');
        
        # Check permissions
        ERP::permissionToAccess('check.sheet.admin');

        # Select Course_Part
        $course_part = ERP::course_part('id', $id);

        
        # Delete Course_Part
        $course_part->delete();

        # Redirect the admin
        return redirect()->route('ERP::course_parts')->with('success', 'Deleted');
    }
}
