<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Welcome page Controller
    |--------------------------------------------------------------------------
    |
    |     
    */

    public function welcome()
    { 
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
        return view('welcome');
    }

    public function wrong()
    {
       return view('home')->with('error','Email-Address And Password Are Wrong.');
    }
}
