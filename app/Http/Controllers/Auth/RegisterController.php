<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use App\User;
use App\Staff;
use App\Code;
use App\Department;
use Hash;
use App\Jobs\SendEmailSignUpJob;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users(Staff) as well as their
    | validation and creation. It also handles password resets.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Save the user name and the random string in Code model and send the 
     * code the signUp form link through email.
     *
     * 
     */

    public function rec_signup(Request $request)
    {
        $this->validate($request, [
            'username' => ['required', 'string', 'max:255', 'unique:users', 'regex:/^([^0-9]*)+([a-zA-Z]+\.+[a-zA-Z]+$)/'],
        ]);
         $code =Str::random(40);

        Code::where('created_at','<=', Carbon::now()->subHours(3)->toDateTimeString())
              ->delete();

        if ($request['type'] > 1) {
            if (User::where('username', $request['username'])->exists()) {
                if ($request['type'] > 2) {
                     $details=[
                    "email"=> $request['username'] . '@students.gsu.ac.zw',
                    "code"=>$code,
                    "url"=>'live_reset/'. $code,
                    "body"=>'Reset your password.',
                ];
               
                } else {
                     $details=[
                    "email"=> $request['username'] . '@gsu.ac.zw',
                    "code"=>$code,
                    "url"=>'signUp/'. $code,
                    "body"=>'Reset your password.',
                ];
                }

                return $this->sendMail($request, $details); 
            }
            return redirect('/live')->with('warning', 'Account not existing.');

        }
        Code::where('uname', $request['username'])->delete();

        if (User::where('username', $request['username'])->exists()) {
            return redirect('/')->with('warning', 'Account exist.');
        }
       
        $details=[
                "email"=> $request['username'] . '@gsu.ac.zw',
                    "code"=>$code,
                    "url"=>'signUp/'. $code,
                    "body"=>'create your account.',
                ];
         return $this->sendMail($request, $details); 
    }

    protected function sendMail($request, $details) {
        $code = new Code;
        $code->uname = $request['username'];
        $code->code = $details['code'];
        $code->type = $request['type'];
        $code->save();

         // $details=[
         //        "email"=>$code->uname . '@gsu.ac.zw',
         //            "code"=>$code->code,
         //       ];

        dispatch(new SendEmailSignUpJob($details));         

        return redirect('/')->with('success', 'Thank you. To create your account, confirm your username in the email sent to your GSU email account and fill in the signup form');

    }

    public function reset($id, Request $request)
    {
        $this->validate($request, [
            'password' =>'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/', 
            //regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/
        ]);

        Code::where('created_at','<=', Carbon::now()->subHours(3)->toDateTimeString())
              ->delete();  

        if (Code::where('code', $request['code'])->exists()) {
         $code = Code::where('code', $request['code'])->first();
          User::where('username', $code->uname)
              ->update(['password' => bcrypt($request['password'])]);
                if ($code->type > 2){
                    return redirect('/live')->with('success', 'Password reseted');
                }
                return redirect('/')->with('success', 'Password reseted');
            }
        return redirect('/live')->with('warning', 'This Link has expired.');      
    }

    public function confirm($id)
    { 
        Code::Where('created_at','<=', Carbon::now()->subHours(3)->toDateTimeString())
              ->delete();
        $code = Code::where('code', $id)->first();
        if($code) {
            $type =$code->type;
            if ($type > 1) { 
                if (User::where('username', $code->uname)->exists()){
                    return view('auth/resert', ['id'=> $id]);
                }
                return redirect('/')->with('warning', 'Account not existing' );
            }
          $departments = Department::all(); 
            return view('auth/register', [
                'id'=> $id,
                 'departments' => $departments
             ]);
           
        }
       return redirect('/')->with('warning', 'Link not existing.'); 
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $request
     * @return \App\User
     */
    protected function create($id, Request $request)
    {
        Code::where('uname', $request['username'])
              ->orWhere('created_at','<=', Carbon::now()->subHours(3)->toDateTimeString())
              ->delete();

        $this->validate($request, [
            'first_name' =>'required|string|max:255',
            'gsu_number' =>'nullable|string|email|max:255|unique:users',
            'title' =>'required|string|max:255',
            'gsu_number' =>'required|',
            'position' =>'required|string|',
            'password' =>'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/', 
            //regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/
            'cell_number' =>'required|string|',
            'profile_picture' =>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'sex' =>'required',
            'title' =>'required',
            'department_id' =>'required',
        ]);


        // $user = new User;
        // $user->first_name = $request['first_name'];
        // $user->middle_names = $request['middle_names'];
        // $user->surname = $request['surname'];
        // $user->cell_number = $request['cell_number'];
        // $user->password = Hash::make($request['password']);
        // $user->save
        $code = Code::where('code', $id)->first();
        
        if ($code){
            if (User::where('username', $code->uname)->exists()) {
                return redirect('/')->with('warning', 'Account exist.');
            }
            $department = Department::findOrFail($request['department_id']);

            if($department->faculty_id > 1) {
                $type = 1;
                $role = [2,3];
            }
            else{
                $type = 0;
                $role = 2;
            }

            $user = User::create([
                'role_id' => $role,
                'username' => $code->uname,
                'first_name' => $request['first_name'],
                'middle_names' => $request['middle_names'],
                'surname' => $request['surname'],
                'cell_number' => $request['cell_number'],
                'password' => Hash::make($request['password']),
                ]); 
                $user->roles()->attach($role);           

           $staff = Staff::create([
                'user_id' => $user->id,
                'gsu_number' => $request['gsu_number'],
                'title' => $request['title'],
                'sex' => $request['sex'],
                'department_id' => $request['department_id'],
                'position' => $request['position'],
                'cell_number' => $request['cell_number'],
                'teach' => $type,
            ]);
            Code::where('code', $id)->delete();
            return redirect('/')->with('success', 'Account Created.');
        }
        return redirect('/')->with('warning', 'Link expired.'); 
    }
}
