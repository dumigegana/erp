<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use ERP;
use Auth;
use Illuminate\Support\Facades\Schema;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectTo = url()->previous();
    }

    public function username()
    {
        return 'username';
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function login(Request $request)

    {  
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        //$credentials = $request->only('username', 'password');
        // if (Auth::attempt(['username' => $request['username'], 'password' => $request['password'], 'active' => 1])) {
    // Authentication was successful...
//}
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }

        if (Auth::attempt(['username' => $request['username'], 'password' => $request['password'], 'active' => 1])) {
            if (ERP::isStaff(auth()->user())) {
                return redirect()->route('ERP::dash');

            }else{
                // try {
                //     Schema::connection('sqlsrv')->hasTable('CCClient');
                //  } catch (\Illuminate\Database\QueryException $e) {
                //    $request->session()->put('sage', true);                    
                //  }
                return redirect()->route('LIVE::home');
            }

        }else{

            // return redirect('/')->with('error', 'Incorrect login Credentials');
             return back()->withErrors([
            'username' => 'The provided credentials do not match our records.',
        ]);

        }

          

    }
    
    
}
