<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Scholarship;
use Auth;
use ERP;
Use Alert;

class ScholarshipsController extends Controller
{
 
    public function index()
    {
        ERP::permissionToAccess('admissions.access');

        # Get all the Scholarships 
        $scholarships = Scholarship::all();
        
        # Return the view
        return view('erp/scholarships/index', ['scholarships' => $scholarships ]);
    }

    public function students($scholarship_id)
    {
        ERP::permissionToAccess('admissions.access');
        
         # Return the view
        return view('erp/scholarships/stud', ['scholarship_id' => $scholarship_id ]);
    }

}
