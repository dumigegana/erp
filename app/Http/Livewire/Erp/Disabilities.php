<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use App\Disability;
use ERP;

class Disabilities extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'code';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $disability_id = 0;
    public $disability =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the disabilities
        $disabilities = Disability::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.disabilities.index', ['disabilities' => $disabilities]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('disabilities.admin');
        $disability = $this->disability;
        $this->validate([
            'data.code' => 'required|integer |max:255',
            'data.description' => 'required|string'
        ],
          [
            'code.required' => 'The code is required',
            'code.max' => 'Disability code too long, max is 255 characters'
          ]);

        if ($this->disability_id > 0 ) {
         $this->validate([
            'data.code' => Rule::unique('disabilities', 'code')->ignore($disability->id),
        ],
          [
            'code.unique' => 'Code already existing',
          ]);
         $mess ='Disability updated successfully';
     }else {
        $mess ='New disability submitted successfully';
         $this->validate([
            'data.code' => 'unique:disabilities,code'
        ],
          [
            'code.unique' => 'Code already existing',
          ]);
     }
        $disability = Disability::updateOrCreate(['id' => $this->disability_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->disability = Disability::find($id);
        $this->disability_id = $id;
        $this->data['code'] = $this->disability->code;
        $this->data['description'] = $this->disability->description;
        // $this->dispatchBrowserEvent('eModal');        
    }

     public function delete($id)
    {
        $this->disability_id = $id;
        $this->disability = Disability::find($id);
        // dd($this->disability);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->disability->code, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->disability->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Disability::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Disability deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->disability =  []; 
    $this->disability_id = 0;
    }
}
