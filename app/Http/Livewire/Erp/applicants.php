<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Applicant;
use ERP; 

class Applicants extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $applicant_id =0;
    public $applicant =[];


    public function render()
    {       
        # Get all the applicants
        $applicants = Applicant::search($this->search)
                            ->where('imprt', false)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.applicants.index', ['applicants' => $applicants]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('applicants.admin');
        $applicant = $this->applicant;
        $this->validate([
            'data.name' => 'required |max:255',
            'data.type' => 'required'
        ],
          [
            'name.required' => 'The name of the Applicant is required',
            'name.max' => 'Applicant name too long, max is 255 characters',
            'type.required' => 'Level is required',
          ]);

        if ($this->applicant_id > 0 ) {
         $this->validate([
            'data.name' => Rule::unique('applicants', 'name')->ignore($applicant->id),
        ],
          [
            'name.unique' => 'Can\'t enter applicant twice',
          ]);
         $mess ='Applicant updated successfully';
     }else {
        $mess ='New applicant submitted successfully';
         $this->validate([
            'data.name' => 'unique:applicants,name'
        ],
          [
            'name.unique' => 'Can\'t enter applicant twice',
          ]);
     }
        $applicant = Applicant::updateOrCreate(['id' => $this->applicant_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->applicant = Applicant::findOrFail($id);
        $this->applicant_id = $id;
        $this->data['name'] = $this->applicant->name;
        $this->data['type'] = $this->applicant->type;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->applicant_id = $id;
        $this->applicant = Applicant::find($id);
        // dd($this->applicant);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->applicant->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->applicant->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Applicant::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Applicant deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->applicant =  []; 
    $this->applicant_id = 0;
    }
}
