<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Dean;
use App\Staff;
use App\Faculty;
use ERP;

class Deans extends Component
{
   use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'faculties.code';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $dean_id = 0, $inputForm = 0;
    public $dean =[], $staffs =[], $faculties =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        ERP::permissionToAccess('hr.admin');
        # Get all the deans
        $deans = Dean::search($this->search)
                        ->select('deans.*')
                        ->join('faculties', 'deans.faculty_id', '=', 'faculties.id')
                        //->join('staffs', 'deans.staff_id', '=', 'staffs.id')
                        ->orderBy($this->orderBy, $this->orderAsc)
                        ->paginate($this->perPage);
        return view('livewire.erp.deans.index', ['deans' => $deans]);
    }

    public function create()
    {   
        $this->resetInputFields();        
        $this->faculties = Faculty::all();
        $this->staffs = Staff::all();     
        $this->inputForm = 1;
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('hr.admin');
        $dean = $this->dean;
        $this->validate([
            'data.staff_id' => 'required |integer',
            'data.from_date' => 'required |date',
            'data.faculty_id' => 'required |integer'
        ],
          [
            'from_date.required' => 'The Start date is required',
            'staff_id.required' => 'The Staff is required',
            'faculty_id.required' => 'The Faculty is required'
          ]);

        if ($this->dean_id > 0 ) {
         $mess ='Dean updated successfully';
     }else {
        $mess ='New dean submitted successfully';
     }
        $dean = Dean::updateOrCreate(['id' => $this->dean_id], $this->data); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    { 
        ERP::permissionToAccess('hr.admin');
        $this->faculties = Faculty::all();
        $this->staffs = Staff::all();
        $this->dean = Dean::findOrFail($id);
        $this->dean_id = $id;
        $this->data['staff_id'] = $this->dean->staff_id;
        $this->data['from_date'] = $this->dean->from_date;      
        $this->data['to_date'] = $this->dean->to_date;
        $this->data['faculty_id'] = $this->dean->faculty_id;
        $this->inputForm = 1;        
    }

     public function delete($id)
    {   
         ERP::permissionToAccess('hr.admin');
        $this->dean_id = $id;
        $this->dean = Dean::find($id);
        // dd($this->dean);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete ', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->dean->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('hr.admin');
        Dean::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Dean deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->dean =  []; 
    $this->dean_id = 0;    
    $this->staffs =  [];
    $this->faculties =  [];
    $this->inputForm = 0;
    }
}      