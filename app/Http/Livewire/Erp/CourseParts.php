<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\CoursePart;
use App\Part;
use App\Course;
use App\Programme;
use Auth;
use PDF;
use ERP;

class CourseParts extends Component
{
   use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $course_part_id = 0, $inputForm = 0;
    public $course_part =[], $parts =[], $courses =[], $programmes =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        ERP::permissionToAccess('registration.admin');
        # Get all the course_parts
        $course_parts = CoursePart::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage);
        return view('livewire.erp.course_parts.index', ['course_parts' => $course_parts]);
    }

    public function create()
    {   
        ERP::permissionToAccess('registration.admin');
        $this->resetInputFields();        
        $this->courses = Course::all();
        $this->parts = Part::all();
        $this->programmes = Programme::all();        
        $this->inputForm = 1;
    }

    public function check()
    {
         $this->validate([
            'data.part_id' => 'required |integer',
            'data.programme_id' => 'required |integer',
            'data.course_id' => 'required |integer'
        ],
          [
            'programme_id.required' => 'The Program is required',
            'part_id.required' => 'The Part is required',
            'course_id.required' => 'The Course is required'
          ]);


         if($check = CoursePart::where('part_id', $this->data['part_id'])
                                    ->where('programme_id', $this->data['programme_id'])
                                    ->where('course_id', $this->data['course_id'])
                                    ->exists())  
        {
            $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   =>  'Exists already', 
            'timeout' => 10000
            ]);
        }
        else{
        $this->store();
        }                                          
    }
    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('registration.admin');
        $course_part = $this->course_part;
        
        if ($this->course_part_id > 0 ) {
         $mess ='Program Course updated successfully';
     }else {
        $mess ='New Program Course submitted successfully';
     }
        $course_part = CoursePart::updateOrCreate(['id' => $this->course_part_id], $this->data); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
         ERP::permissionToAccess('registration.admin');
        $this->courses = Course::all();
        $this->parts = Part::all();
        $this->programmes = Programme::all();
        $this->course_part = CoursePart::findOrFail($id);
        $this->course_part_id = $id;
        $this->data['part_id'] = $this->course_part->part_id;
        $this->data['programme_id'] = $this->course_part->programme_id;
        $this->data['course_id'] = $this->course_part->course_id;
        $this->inputForm = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {   
         ERP::permissionToAccess('registration.admin');
        $this->course_part_id = $id;
        $this->course_part = CoursePart::find($id);
        // dd($this->course_part);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete ', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->course_part->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('registration.admin');
        CoursePart::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'CoursePart deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->course_part =  []; 
    $this->course_part_id = 0;    
    $this->parts =  [];
    $this->courses =  [];
    $this->programmes =  [];
    $this->inputForm = 0;
    }
}
