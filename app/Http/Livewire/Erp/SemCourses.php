<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\CoursePart;
use App\SemCourse;
use App\Semester;
use App\Part;
use App\Course;
use App\Staff;
use App\Programme;
use App\Jobs\LecturerCoursesJob;
use Auth;
use ERP;

class SemCourses extends Component
{
   use WithPagination;
    public $perPage = 10;
    public $search = '', $cosSearch = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $inputForm=0, $isOpen = 0;
    public $sem_course_id=0,$course_part_id=0, $part_id=0, $staff_id=0, $programme_id=0, $semester_id=0;
    public $sem_course, $course_parts =[], $parts =[], $courses =[], $staffs =[], $semesters =[], $programmes =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        ERP::permissionToAccess('registration.admin');
        # Get all the sem_courses    
        $sem_courses = SemCourse::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage);
        return view('livewire.erp.sem_courses.index', ['sem_courses' => $sem_courses]);
    }

    public function create()
    {   
        ERP::permissionToAccess('registration.admin');
        $this->resetInputFields(); 
        $this->semesters = Semester::all();
        $this->parts = Part::all();
        $this->staffs = Staff::all();        
        $this->programmes = Programme::all();        
        $this->inputForm = 1;
        if ($this->programme_id > 0 && $this->part_id >0) 
        {
            $this->course_parts = CoursePart::where('programme_id', $this->programme_id )
                                         ->where('part_id', $this->part_id)->get();
        }
        else
        {

        $this->course_parts = CoursePart::all(); 
        }
    }

    public function note()
    {   
        ERP::permissionToAccess('academic.admin');
        $this->resetInputFields(); 
        $this->semesters = Semester::all();
        $this->programmes = Programme::all(); 
    }

    public function updateC()
    {
       $this->course_parts = CoursePart::where('programme_id', $this->programme_id )
                                         ->where('part_id', $this->part_id)->get();
    }

    public function check()
    {
         $this->validate([
            'course_part_id' => 'required |integer',
            'semester_id' => 'required |integer',
            'staff_id' => 'required |integer'
        ],
          [
            'course_part_id.required' => 'The Course field is required',
            'semester_id.required' => 'The Semester field is required',
            'staff_id.required' => 'The Lecturer field is required'
          ]);

         if($check = SemCourse::where('course_part_id', $this->course_part_id)
                                    ->where('semester_id', $this->semester_id)
                                    // ->where('staff_id', $this->staff_id)
                                    ->exists())  
        {
            $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   =>  'Exists already', 
            'timeout' => 10000
            ]);
        }
        else{
        $this->store();
        }                                          
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('registration.admin');
        $sem_course = $this->sem_course;
       
        if ($this->sem_course_id > 0 ) {
         $mess ='Semester Course updated successfully';
     }else {
        $mess ='Semester Course submitted successfully';
     }
        $sem_course = SemCourse::updateOrCreate(['id' => $this->sem_course_id],
                                                [
                                                 'course_part_id' => $this->course_part_id,
                                                 'semester_id' => $this->semester_id,
                                                 'staff_id' => $this->staff_id,
                                             ]); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
         ERP::permissionToAccess('registration.admin');
        $this->resetInputFields(); 
        $this->semesters = Semester::all();
        $this->staffs = Staff::all();
        $this->sem_course = SemCourse::findOrFail($id);
        $this->sem_course_id = $id;
        $this->semester_id = $this->sem_course->semester_id;
        $this->staff_id = $this->sem_course->staff_id;
        $this->course_part_id = $this->sem_course->course_part_id;
        $this->inputForm = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {   
         ERP::permissionToAccess('registration.admin');
        $this->sem_course_id = $id;
        $this->sem_course = SemCourse::find($id);
        // dd($this->sem_course);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete ', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->sem_course->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('registration.admin');
        SemCourse::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'SemCourse deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    public function notify() 
    { 
        $sem = Semester::find($this->semester_id);
        $prog = Programme::find($this->programme_id);
        $staffs = Staff::whereHas('sem_courses', function($q) use ($sem, $prog) {
                    $q->where('semester_id', $sem->id)
                    ->whereHas('course_part', function($c) use ($prog) {
                        $c->where('programme_id', $prog->id);
                    });
                })->get();
        foreach ($staffs as $lecturer) {      
            $details=[
                "emailTo"=> Auth::user()->username . '@gsu.ac.zw',
                "emailFro"=> $lecturer->user->username . '@gsu.ac.zw',
                "semester"=>$sem->name,
                "program"=>$prog->fullname,
            ];

            $sem_courses = SemCourse::where('semester_id', $sem->id)
                         ->where('staff_id', $lecturer->id )
                        ->whereHas('course_part', function($c) use ($prog_id) {
                            $c->where('programme_id', $prog_id);
                        })->get();
            $this->sendMail($details, $sem_courses); 
        }
         $this->resetInputFields();  
    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->sem_course =  []; 
    $this->sem_course_id = 0;    
    $this->semester_id = 0;    
    $this->programme_id = 0;    
    $this->parts =  [];
    $this->courses =  [];
    $this->programmes =  [];
    $this->inputForm = 0;
    }
}
