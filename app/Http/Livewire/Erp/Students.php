<?php
namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use App\Role_Student;
use ERP;
use App\Student;
use App\User;
use App\Applicant;

class Students extends Component
{    
  use WithPagination;
  public $perPage = 10;
  public $search = '';
  public $orderBy = 'id';
  public $orderAsc = 'asc';
  public $isOpen = 0, $roleEdit = 0;
  public $data = [], $student_id = 0;
  public $student, $roles;

  protected $listeners = ['destroy', 'reset' => 'create'];
   /* 
  |--------------------------------------------------------------------------
  | Students Component
  |--------------------------------------------------------------------------
  |
  | This controller handles the CRUD functions of the Student model 
  |  
  |
  */

  public function render()
  {       
    # Get all the students
    $students = Student::search($this->search)
                        ->orderBy($this->orderBy, $this->orderAsc)
                        ->paginate($this->perPage);
    return view('livewire.erp.students.index', ['students' => $students]);
  }

  public function store()
  {
    # Check permissions
    ERP::permissionToAccess('students.admin');
    $student = $this->student;

    $this->validate([
      'data.studentname' => 'required |max:255',
      'data.first_name' => 'nullable|max:255',
      'data.middle_names' => 'nullable|max:255',
      'data.surname' => 'required|max:255',
      'data.cell_number' => 'required |max:255',
      'data.profile_picture' => 'nullable',
      'data.su' => 'nullable|boolean',
      'data.active' => 'nullable|boolean'
    ]);

    if ($this->student_id > 0 ) {
        $this->validate([
            'data.gsu_id' => Rule::unique('students', 'gsu_id')->ignore($student->id),
        ]);
        $mess ='Student updated successfully';     
    }

    $student = Student::find($this->student_id)->update($this->data); 
                 
    
    $this->resetInputFields();
    $this->emit('swal:modal', [
        'type'    => 'success',
        'icon'    => 'success',
        'title'   => $mess, 
        'timeout' => 10000
    ]);
      
  }

  public function edit($id)
  {
    $this->student = Student::findOrFail($id);
    $this->student_id = $id;
    $this->data['user_id'] = $this->student->user_id;
    $this->data['cohort_id'] = $this->student->cohort_id;
    $this->data['gsu_id'] = $this->student->gsu_id;
    $this->data['programme_id'] = $this->student->programme_id;
      
  }

   public function delete($id)
  {
    $this->student_id = $id;
    $this->student = Student::find($id);

    $this->emit('swal:confirm', [
      'type'    => 'warning',
      'icon'    => 'warning',
      'title'   => 'This action can not be reversed! ', 
      'text'   => 'Are sure you want to delete '. $this->student->name, 
      'timeout' => 10000,
      'confirmText' => 'Yes', 
      'method' => 'destroy',
      'params' => $this->student->id,
      'callback' => 'reset'
    ]);
  }

   public function destroy($id)
  {
    ERP::permissionToAccess('students.access');

    # Check permissions
    ERP::permissionToAccess('students.admin');

    # Find The Student' user account
    $this->student = Student::findOrFail($id);    

     $user = User::findOrFail($this->student->user_id);
    if($user){
      $user ->delete();
    }
    
    if($stud){
      $stud ->delete();
    }

    Student::destroy($id);
    $this->emit('swal:modal', [
      'type'    => 'success',
      'icon'    => 'success',
      'title'   => 'Student deleted', 
      'timeout' => 10000
    ]);
    $this->resetInputFields();

  }
    
 private function resetInputFields() {
  $this->data =  []; 
  $this->student =  []; 
  $this->student_id = 0;
  $this->roleEdit = 0;
  }
    
}
