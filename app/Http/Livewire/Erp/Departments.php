<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Department;
use App\Faculty;
use App\Staff;
use App\Hod;
use ERP;

class Departments extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $isOpen = 0, $isEdit = 0;
    public $data = [], $department_id =0, $hod_id = 0, $old_hod = 0;
    public $department =[], $hods = [], $faculty_id = 0;
    public $staffs = [], $departs = [];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the departments
        $faculties = Faculty::all();
        $departments = Department::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.departments.index', ['departments' => $departments, 'faculties' => $faculties]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }


    public function createH()
    { 
        $this->resetInputFields();  
        $this->departs = Department::all();
        $this->staffs = Staff::all(); //where('teach', true)->get();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('hr.admin');
        $department = $this->department;
        $this->validate([
            'data.name' => 'required|max:255',
        ],
          [
            'name.required' => 'The name  is required',
            'name.max' => 'name too long, max is 255 characters',
          ]);

        if ($this->department_id > 0 ) {
            $this->data['hod_id'] = $this->hod_id;

        //  $this->validate([
        //     'data.name' => 'sometimes|required|unique:departments', 
        //     'data.hod_id' => 'required',
        // ],
        //   [
        //     'name.unique' => 'This department name already exit',
        //     'hod_id.required' => 'Hod is required',
        //   ]);
         $mess ='Updated successfully';
         if($this->old_hod > 0) {
          Hod::find($this->old_hod)->update(['active' => false]);
        }
         Hod::find($this->hod_id)->update(['active' => true]);
     }else {
        $mess ='Submitted successfully';
         $this->validate([
            'data.name' => 'unique:departments,name',
        ],
          [
            'name.unique' => 'This departments name already exit',
          ]);
     }
      $departments = Department::updateOrCreate(['id' => $this->department_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    
    public function storeH()
    { 
            # Check permissions
        ERP::permissionToAccess('hr.admin');
        // $hod = $this->hod;
        $this->validate([
            'data.staff_id' => 'required |integer',
            'data.from_date' => 'required |date',
            'data.department_id' => 'required |integer'
        ],
          [
            'from_date.required' => 'The Start date is required',
            'staff_id.required' => 'The Staff is required',
            'department_id.required' => 'The Department is required'
          ]);

        $mess ='New HOD submitted successfully';
     
        Hod::Create($this->data); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }


    public function edit($id)
    {
        $this->department = Department::findOrFail($id);
        $this->faculty_id = $this->department->faculty_id;
        $this->hods = Hod::where('department_id', $this->department->id)->get();
        $this->department_id = $id;
        $this->data['name'] = $this->department->name; 
        $this->old_hod = $this->department->hod_id; 
        $this->data['department_id'] = $this->department->id;
        $this->isEdit = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }
     public function delete($id)
    {
        $this->departments_id = $id;
        $this->departments = Department::find($id);
        // dd($this->departments);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->departments->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->departments->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Department::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Department deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
  
   private function resetInputFields() {
      $this->data =  []; 
      $this->department =  []; 
      $this->hods =  []; 
      $this->department_id = 0;
      $this->hod_id = 0;
      $this->old_hod = 0;
      $this->isEdit = 0;
    }
}
