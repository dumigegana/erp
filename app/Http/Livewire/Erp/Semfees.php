<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Semfee;
use App\Semester;   
use App\Programme;
use ERP;
use Carbon\Carbon;

class Semfees extends Component
{
   use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $inputForm=0, $isOpen = 0;
    public $sem_fee_id=0, $programme_id=0, $semester_id=0, $fee, $registration, $results, $progs=[];
    public $sem_fee, $semesters =[], $programmes =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        ERP::permissionToAccess('bursary.admin');
        # Get all the sem_fees    
        $sem_fees = Semfee::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage);
        return view('livewire.erp.sem_fees.index', ['sem_fees' => $sem_fees]);
    }

    public function create()
    {   
        ERP::permissionToAccess('bursary.admin');
        $this->resetInputFields(); 
        $this->semesters = Semester::whereDate('reg_to', '>', Carbon::today()->toDateString())->get();
        $this->programmes = Programme::all();  
        $this->registration = 60;      
        $this->results = 100;      
        $this->inputForm = 1;
    }

    public function updateC()
    {
       $this->sem_fees = Semfee::where('programme_id', $this->programme_id )
                                         ->where('semesters_edit', $this->semester_id)->get();
    }

    public function check()
    {
         $this->validate([
            'programme_id' => 'required |integer',
            'semester_id' => 'required |integer',
          ]);

         if($sem_fees = Semfee::where('programme_id', $this->programme_id)
                                    ->where('semester_id', $this->semester_id)
                                    // ->where('staff_id', $this->staff_id)
                                    ->exists())  
        {
            $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   =>  'Exists already', 
            'timeout' => 10000
            ]);
        }
        else{
        $this->store();
        }                                          
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('bursary.admin');
        $mess ='Updated successfully';
        foreach($this->progs as $prog) 
        {
         $sem_fee = Semfee::updateOrCreate([
                                                     'programme_id' => $prog,
                                                     'semester_id' => $this->semester_id,
                                                 ],
                                                 [
                                                    'fee' => $this->fee,
                                                    'registration' => $this->registration,
                                                    'results' => $this->results,
                                                 ]
                                             ); 

        }
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
         ERP::permissionToAccess('bursary.admin');
        $this->resetInputFields(); 
        $this->semesters = Semester::all();
        $this->sem_fee_id = $id;
        $this->sem_fee = Semfee::findOrFail($id);
        $this->semester_id = $this->sem_fee->semester_id;
        $this->programme_id = $this->sem_fee->programme_id;
        $this->inputForm = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {   
         ERP::permissionToAccess('bursary.admin');
        $this->sem_fee_id = $id;
        $this->sem_fee = Semfee::find($id);
        // dd($this->sem_fee);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete ', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->sem_fee->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('bursary.admin');
        Semfee::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Sem_fee deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
       
   private function resetInputFields() {
    $this->data =  []; 
    $this->sem_fee =  []; 
    $this->sem_fee_id = 0;    
    $this->semester_id = 0;    
    $this->programme_id = 0;  
    $this->programmes =  [];
    $this->inputForm = 0;
    }
}
