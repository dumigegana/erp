<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Subject;
use ERP;

class Subjects extends Component
{
	use WithPagination;
	public $perPage = 10;
	public $search = '';
	public $orderBy = 'name';
	public $orderAsc = 'asc';
	public $isOpen = 0;
	public $data = [], $subject_id =0;
	public $subject =[];

	protected $listeners = ['destroy', 'reset' => 'create'];

	public function render()
    {    	
    	# Get all the subjects
        $subjects = Subject::search($this->search)
        					->orderBy($this->orderBy, $this->orderAsc)
        					->paginate($this->perPage); 
        return view('livewire.erp.subjects.index', ['subjects' => $subjects]);
    }

    public function create()
    {	
    	$this->resetInputFields();
    }

    public function store()
    {
    		# Check permissions
        ERP::permissionToAccess('admissions.admin');
        $subject = $this->subject;
        $this->validate([
        	'data.name' => 'required |max:255',
	        'data.type' => 'required'
      	],
	      [
	        'name.required' => 'The name of the Subject is required',
	        'name.max' => 'Subject name too long, max is 255 characters',
	        'type.required' => 'Level is required',
	      ]);

        if ($this->subject_id > 0 ) {
         $this->validate([
        	'data.name' => Rule::unique('subjects', 'name')->ignore($subject->id),
      	],
	      [
	        'name.unique' => 'Can\'t enter subject twice',
	      ]);
         $mess ='Subject updated successfully';
     }else {
     	$mess ='New subject submitted successfully';
         $this->validate([
        	'data.name' => 'unique:subjects,name'
      	],
	      [
	        'name.unique' => 'Can\'t enter subject twice',
	      ]);
     }
   		$subject = Subject::updateOrCreate(['id' => $this->subject_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
    	$this->subject = Subject::findOrFail($id);
        $this->subject_id = $id;
        $this->data['name'] = $this->subject->name;
        $this->data['type'] = $this->subject->type;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->subject_id = $id;
        $this->subject = Subject::find($id);
        // dd($this->subject);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->subject->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->subject->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Subject::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Subject deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->subject =  []; 
    $this->subject_id = 0;
	}
}
