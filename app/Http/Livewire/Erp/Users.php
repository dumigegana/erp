<?php
namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use App\Role_User;
use ERP;
use App\User;
use App\Student;
use App\Staff;
use App\Role;

class Users extends Component
{    
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $user_id = 0;
    public $user, $roles;

    protected $listeners = ['destroy', 'reset' => 'create'];
     /* 
    |--------------------------------------------------------------------------
    | Users Component
    |--------------------------------------------------------------------------
    |
    | This controller handles the CRUD functions of the User model 
    |  
    |
    */

    public function mount()
    {
        $this->roles =  Role::where('name', '!=', 'Student')->get();
    }   

    public function render()
    {       
        # Get all the users
        $users = User::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage);
        
        return view('livewire.erp.users.index', ['users' => $users]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('users.admin');
        $user = $this->user;

        $this->validate([
            'data.username' => 'required |max:255',
            'data.first_name' => 'nullable|max:255',
            'data.middle_names' => 'nullable|max:255',
            'data.surname' => 'required|max:255',
            'data.cell_number' => 'required |max:255',
            'data.profile_picture' => 'nullable',
            'data.su' => 'nullable|boolean',
            'data.active' => 'nullable|boolean'
        ]);

        if ($this->user_id > 0 ) {
         $this->validate([
            'data.username' => Rule::unique('users', 'username')->ignore($user->id),
        ]);
         $mess ='User updated successfully';
     }else {
        $mess ='New user submitted successfully';
         $this->validate([
            'data.username' => 'unique:users,username'
        ]);
     }

        $user = User::updateOrCreate(['id' => $this->user_id], $this->data); 
         if(Arr::exists($this->data, 'active')) {
            $user->update(['active'=> true]);
        } else{
            $user->update(['active'=> false]);
        } 

        if(Arr::exists($this->data, 'su')){
            $user->update(['su'=> true]);
        } else{
            $user->update(['su'=> false]);
        }              
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->user = User::findOrFail($id);
        $this->user_id = $id;
        $this->data['username'] = $this->user->username;
        $this->data['first_name'] = $this->user->first_name;
        $this->data['middle_names'] = $this->user->middle_names;
        $this->data['surname'] = $this->user->surname;
        $this->data['cell_number'] = $this->user->cell_number;
        $this->data['profile_picture'] = $this->user->profile_picture;
        
    }

     public function delete($id)
    {
        $this->user_id = $id;
        $this->user = User::find($id);

        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->user->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->user->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

        # Find The User
        $this->user = User::findOrFail($id);

        # Check if admin access
         ERP::mustNotBeAdmin($this->user);

        # Check if it's su
        if($this->user->su) {
            abort(403, trans('Action can\'t be performed on Super user'));
        }

        # Check before deleting
        if($id == ERP::loggedInUser()->id) {
            abort(403, trans('You can\'t delete your account'));
        } else {

            
             $stud = Student::where('user_id', $this->user->id)->first();
            if($stud){
                $stud ->delete();
            }

             $staff = Staff::where('user_id', $this->user->id)->first();
            if($staff){
                $staff ->delete();
            }
        }

        User::destroy($id);
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'User deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->user =  []; 
    $this->user_id = 0;
    }

  public function editRoles($id)
    {
      ERP::permissionToAccess('users.admin');

      # Check permissions
      ERP::permissionToAccess('roles.access');

      # Find the user
      $this->user = User::findOrFail($id);

      # Check if admin access
      ERP::mustNotBeStudent($this->user);
      // $this->roles = Role::where('name', '!=', 'Student')->get();

      //  return view('livewire.erp.users.roles');
    }


    public function setRoles($id, Request $request)
    {
        ERP::permissionToAccess('users.access');

        # Check permissions
        ERP::permissionToAccess('users.admin');

    # Find the user
      $user = ERP::user('id', $id);

        # Check if admin access
        OHMS::mustNotBeStudent($user);

      # Get all roles
      $roles = OHMS::roles();

      # Change user's roles
      foreach($roles as $role) {
        if($request->input($role->id)){
          # The admin selected that role

          # Check if the user was already in that role
          if($this->checkRole($user->id, $role->id)) {
              # The user is already in that role, so no change is made
          } else {
              # Add the user to the selected role
            $this->addRel($user->id, $role->id);
          }
        } else {
          # The admin did not select that role

          # Check if the user was in that role
          if($this->checkRole($user->id, $role->id)) {
              # The user is in that role, so as the admin did not select it, we need to delete the relationship
            $this->deleteRel($user->id, $role->id);
          } else {
              # The user is not in that role and the admin did not select it
          }
        }
            
      }
    }

    public function checkRole($user_id, $role_id)
    {
        ERP::permissionToAccess('users.access');

      # This function returns true if the specified user is found in the specified role and false if not

      if(Role_User::whereUser_idAndRole_id($user_id, $role_id)->first()) {
        return true;
      } else {
        return false;
      }

    }
    public function deleteRel($role_id)
    {
      ERP::permissionToAccess('users.admin');
      $this->user->roles()->detach($role_id);      
    }

    public function addRel($role_id)
    {
      ERP::permissionToAccess('users.access');
      $this->user->roles()->attach($role_id);

      // $rel = Role_User::whereUser_idAndRole_id($this->user->id, $role_id)->first();
      // if(!$rel) {
      //     $rel = new Role_User;
      //     $rel->user_id = $this->user_id;
      //     $rel->role_id = $role_id;
      //     $rel->save();
      // }
    }

}
