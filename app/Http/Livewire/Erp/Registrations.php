<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Registration;
use ERP;

class Registrations extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'desc';
    public $isOpen = 0;
    public $data = [], $registration_id =0;
    public $registration =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the registrations
        $registrations = Registration::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.registrations.index', ['registrations' => $registrations]);
    }
    
    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('registrations.admin');
        $registration = $this->registration;
        $this->validate([
            'data.name' => 'required |max:255',
            'data.type' => 'required'
        ],
          [
            'name.required' => 'The name of the Registration is required',
            'name.max' => 'Registration name too long, max is 255 characters',
            'type.required' => 'Level is required',
          ]);

        if ($this->registration_id > 0 ) {
         $this->validate([
            'data.name' => Rule::unique('registrations', 'name')->ignore($registration->id),
        ],
          [
            'name.unique' => 'Can\'t enter registration twice',
          ]);
         $mess ='Registration updated successfully';
     }else {
        $mess ='New registration submitted successfully';
         $this->validate([
            'data.name' => 'unique:registrations,name'
        ],
          [
            'name.unique' => 'Can\'t enter registration twice',
          ]);
     }
        $registration = Registration::updateOrCreate(['id' => $this->registration_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->registration = Registration::findOrFail($id);
        $this->registration_id = $id;
        $this->data['name'] = $this->registration->name;
        $this->data['type'] = $this->registration->type;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->registration_id = $id;
        $this->registration = Registration::find($id);
        // dd($this->registration);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->registration->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->registration->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Registration::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Registration deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->registration =  []; 
    $this->registration_id = 0;
    }
}
