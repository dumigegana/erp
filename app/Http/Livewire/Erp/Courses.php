<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Course;
use App\Department;
use ERP;

class Courses extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'name';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $course_id =0;
    public $course =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {   
        ERP::permissionToAccess('academic.access');    
        # Get all the courses
        $departments = Department::all();
        $courses = Course::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.courses.index', ['courses' => $courses, 'departments' => $departments]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('academic.admin');
        $course = $this->course;
        $this->validate([
            'data.name' => 'required|max:255',
            'data.code' => 'required|max:8',
            'data.department_id' => 'required|integer'
        ],
          [
            'name.required' => 'The name of the Course is required',
            'name.max' => 'Course name too long, max is 255 characters',
            'code.max' => 'Course code too long, max is 5 characters',
            'code.required' => 'Course code is required',
            'department_id.required' => 'Select the department is required',
          ]);

        if ($this->course_id > 0 ) {
         $this->validate([
            'data.name' => Rule::unique('courses', 'name')->ignore($course->id),
            'data.code' => Rule::unique('courses', 'code')->ignore($course->id),
        ],
          [
            'name.unique' => 'This course name already exit',
            'code.unique' => 'This course code already exit',
          ]);
         $mess ='Course updated successfully';
     }else {
        $mess ='New course submitted successfully';
         $this->validate([
            'data.name' => 'unique:courses,name',
            'data.code' => 'unique:courses,code'
        ],
          [
            'name.unique' => 'This course name already exit',
            'code.unique' => 'This course code already exit',
          ]);
     }
        $course = Course::updateOrCreate(['id' => $this->course_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        ERP::permissionToAccess('academic.admin');
        $this->course = Course::findOrFail($id);
        $this->course_id = $id;
        $this->data['name'] = $this->course->name;
        $this->data['code'] = $this->course->code;
        $this->data['department_id'] = $this->course->department_id;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        ERP::permissionToAccess('academic.admin');
        $this->course_id = $id;
        $this->course = Course::find($id);
        // dd($this->course);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->course->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->course->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('academic.admin');
        Course::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Course deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->course =  []; 
    $this->course_id = 0;
    }
}
