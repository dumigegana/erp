<?php

namespace App\Http\Livewire\Erp;

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Payment;

class Payments extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'id';
    public $orderAsc = 'asc';
    public $data = [], $payment_id =0;


   public function render()
    {       
        # Get all the payments
        $payments = Payment::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.payments.index', ['payments' => $payments]);
    }

}