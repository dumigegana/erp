<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Hod;
use App\Staff;
use App\Department;
use ERP;

class Hods extends Component
{
   use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'departments.name';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $hod_id = 0, $inputForm = 0;
    public $hod =[], $staffs =[], $departments =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        ERP::permissionToAccess('hr.admin');
        # Get all the hods
        $hods = Hod::search($this->search)
                        ->select('hods.*')
                        ->join('departments', 'hods.department_id', '=', 'departments.id')
                        //->join('staffs', 'hods.staff_id', '=', 'staffs.id')
                        ->orderBy($this->orderBy, $this->orderAsc)
                        ->paginate($this->perPage);
        return view('livewire.erp.hods.index', ['hods' => $hods]);
    }

    public function create()
    {   
        $this->resetInputFields();        
        $this->departments = Department::all();
        $this->staffs = Staff::all();     
        $this->inputForm = 1;
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('hr.admin');
        $hod = $this->hod;
        $this->validate([
            'data.staff_id' => 'required |integer',
            'data.from_date' => 'required |date',
            'data.department_id' => 'required |integer'
        ],
          [
            'from_date.required' => 'The Start date is required',
            'staff_id.required' => 'The Staff is required',
            'department_id.required' => 'The Department is required'
          ]);

        if ($this->hod_id > 0 ) {
         $mess ='Hod updated successfully';
     }else {
        $mess ='New hod submitted successfully';
     }
        $hod = Hod::updateOrCreate(['id' => $this->hod_id], $this->data); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    { 
        ERP::permissionToAccess('hr.admin');
        $this->departments = Department::all();
        $this->staffs = Staff::all();
        $this->hod = Hod::findOrFail($id);
        $this->hod_id = $id;
        $this->data['staff_id'] = $this->hod->staff_id;
        $this->data['from_date'] = $this->hod->from_date;      
        $this->data['to_date'] = $this->hod->to_date;
        $this->data['department_id'] = $this->hod->department_id;
        $this->inputForm = 1;        
    }

     public function delete($id)
    {   
         ERP::permissionToAccess('hr.admin');
        $this->hod_id = $id;
        $this->hod = Hod::find($id);
        // dd($this->hod);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete ', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->hod->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        ERP::permissionToAccess('hr.admin');
        Hod::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Hod deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->hod =  []; 
    $this->hod_id = 0;    
    $this->staffs =  [];
    $this->departments =  [];
    $this->inputForm = 0;
    }
}      