<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use App\Check;
use App\Applicant;
use App\User;
use App\Student;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ShowApplicant extends Component
{
	public $applicant, $programmes;
	public $complete, $programme, $general, $academic, $reffs, $assess, $status;
	public $outcomes, $admissions_outcome, $admissions_comment, $fdean_outcome;
    public $fdean_comment, $hod_outcome, $hod_comment, $bursar_outcome, $bursar_comment;
	public $isBirth, $isCv, $isId, $isMariage;

	public function mount()
	{
	    $this->fill([
	    	'isBirth' => false,
            'isCv' => false,
	    	'isId' => false,
	    	'isMariage' => false,
	    ]);
	}

	public function resetFilters()
	{
	  $this->reset(['isBirth', 'isCv', 'isId', 'isMariage']);
	}

    public function render()
    {
        return view('livewire.erp.show-applicant');
    }

    public function saveGen()
    {
      $validatedDate = $this->validate([
            'general' => 'required',
        ]);
 
        if ($this->applicant->check) {
            $check = Check::find($this->applicant->check->id);
            $check->update([
                'personal' => $this->general,
            ]);
            $this->reset('general');
        }
    }
    
    public function saveAcademic()
    {
      $validatedDate = $this->validate([
            'academic' => 'required',
        ]);
 
        if ($this->applicant->check) {
            $check = Check::find($this->applicant->check->id);
            $check->update([
                'academic' => $this->academic,
            ]);
            $this->reset('academic');
        }
    }    
    
    public function saveReffs()
    {
      $validatedDate = $this->validate([
            'reffs' => 'required',
        ]);
 
        if ($this->applicant->check) {
            $check = Check::find($this->applicant->check->id);
            $check->update([
                'files' => $this->reffs,
            ]);
            $this->reset('reffs');
        }
    }
        
    public function saveAssess()
    {
      $validatedDate = $this->validate([
            'assess' => 'required',
        ]);
 
        if ($this->applicant->check) {
            $check = Check::find($this->applicant->check->id);
            $check->update([
                'outcomes' => $this->assess,
            ]);
            $this->reset('assess');
        }
    }
    
    public function saveAdmissions()
    {
      $validatedDate = $this->validate([
            'admissions_outcome' => 'required',
        ]);
 
      $applie = Applicant::find($this->applicant->id);
      $applie->update([
        'admissions_outcome' => $this->admissions_outcome,
        'admissions_comment' => $this->admissions_comment,
            ]);
        $this->reset(['admissions_outcome', 'admissions_comment']);        
    }

    public function saveBussary()
    {
      $validatedDate = $this->validate([
            'bursar_outcome' => 'required',
        ]);
 
      $applie = Applicant::find($this->applicant->id);
      $applie->update([
        'bursar_outcome' => $this->bursar_outcome,
        'bursar_comment' => $this->bursar_comment,
            ]);
      $this->reset(['bursar_outcome', 'bursar_comment']);
    }

    public function saveHod()
    {
      $validatedDate = $this->validate([
            'hod_outcome' => 'required',
        ]);
 
      $applie = Applicant::find($this->applicant->id);
      $applie->update([
        'hod_outcome' => $this->hod_outcome,
        'hod_comment' => $this->hod_comment,
            ]);
      $this->reset(['hod_outcome', 'hod_comment']);
    }

    public function saveDean()
    {
      $validatedDate = $this->validate([
            'fdean_outcome' => 'required',
        ]);
 
      $applie = Applicant::find($this->applicant->id);
      $applie->update([
        'fdean_outcome' => $this->fdean_outcome,
        'fdean_comment' => $this->fdean_comment,
            ]);
      $this->reset(['fdean_outcome', 'fdean_comment']);
    }

    public function saveOutcome()
    {
      $validatedDate = $this->validate([
            'status' => 'required',
        ]);
      $check = Check::find($this->applicant->check->id);
      //$applie = Applicant::find($this->applicant->id);
      $this->applicant->update([
        'outcome_at' => Carbon::now(),        
        'student-id' => $this->stud_id($this->applicant->id),
            ]);
      $role_id = 2;

      $user = User::create([
      'first_name' => $this->applicant->first_name,
      'middle_names' => $this->applicant->middle_names,
      'surname' => $this->applicant->surname,
      'username' => $this->stud_id($this->applicant->id),
      'password' => "987Gsu432!",
      'cell_number' => $this->applicant->cell_number,
      'role_id' => $role_id,
    ]);
      if($this->programme == "") {
        $prog_id = $this->applicant->prefered_prog_id;
      } else {
        $prog_id = $this->programmes;
      }

      $student = Student::create([
      'user_id' => $user->id,
      'programme_id' => $prog_id,
      'cohort_id' => $this->applicant->cohort_id,
      'gsu_id' => $this->stud_id($this->applicant->id),
      'sex' => $this->applicant->sex,
      'cell_number' => $this->applicant->cell_number,
      'disability' => $this->applicant->disability,
    ]);
      $this->reset(['status', 'programme']);
    }

    public function stud_id($id)
    {
      $yy = Carbon::now()->format('y');
      $len = Str::length($id);
      if ($len < 4){
        if ($len == 2) {
          $num = '00'.$id;
        }  
        elseif ($len == 3) {
          $num = '0'.$id;
        }  
        elseif ($len == 1) {
          $num = '000'.$id;
        }  
      } elseif(4 < $len) {
        $num = Str::substr($id, $len - 4, 4);
      } else{
        $num = $id;
      }     

      return 'G0'.$yy.$num. Str::upper(Str::random(1));
    }

    public function birth()
     {
     	if ($this->isBirth == false) {
     		$this->isBirth  = true;
     	} else{
     		$this->reset('isBirth');
     	}
     }

   public function nId()
   {
   	if ($this->isId == false) { 
   		$this->isId  = true;
   	} else {
   		$this->reset('isId');
   	}
   }
   public function mariage()
   {
   	if ($this->isMariage == false) {
   		$this->isMariage  = true;
   	} else {
   		$this->reset('isMariage');
   	}
   }

   public function cv()
   {
      if ($this->isCv == false) {
          $this->isCv  = true;
      } else {
          $this->reset('isCv');
      }
   }
}