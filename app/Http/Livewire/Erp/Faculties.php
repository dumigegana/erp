<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Faculty;
use App\Dean;
use App\Staff;
use ERP;

class Faculties extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'name';
    public $orderAsc = 'asc';
    public $isOpen = 0, $isEdit = 0;
    public $data = [], $faculty_id =0, $dean_id = 0, $old_dean;
    public $faculty =[], $deans = [];
    public $staffs = [], $faclts = [];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the faculties
        $faculties = Faculty::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.faculties.index', ['faculties' => $faculties]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function createD()
    { 
        $this->resetInputFields();  
        $this->faclts = Faculty::all();
        $this->staffs = Staff::all(); //where('teach', true)->get();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('faculties.admin');
        $faculty = $this->faculty;
        $this->validate([
            'data.name' => 'required|max:255',
            'data.code' => 'required|max:5',
        ],
          [
            'name.required' => 'The name  is required',
            'name.max' => 'name too long, max is 255 characters',
            'code.required' => 'The code  is required',
            'code.max' => 'code too long, max is 5 characters',
          ]);

        if ($this->faculty_id > 0 ) {
            $this->data['dean_id'] = $this->dean_id;

        //  $this->validate([
        //     'data.name' => Rule::unique('faculties', 'name')->ignore($faculty->id),
        //     'data.code' => Rule::unique('faculties', 'code')->ignore($faculty->id),
        //     'data.dean_id' => 'required',
        // ],
        //   [
        //     'name.unique' => 'This faculty name already exit',
        //     'code.unique' => 'This faculty code already exit',
        //     'dean_id.required' => 'Dean is required',
        //   ]);
         $mess ='Updated successfully';
         if ($this->old_dean > 0) {
         Dean::find($this->old_dean)->update(['active' => false]);
        }
         Dean::find($this->dean_id)->update(['active' => true]);
     }else {
        $mess ='Submitted successfully';
         $this->validate([
            'data.name' => 'unique:faculties,name',
            'data.code' => 'unique:faculties,code',
        ],
          [
            'name.unique' => 'This faculties name already exit',
            'code.unique' => 'This faculties code already exit',
          ]);
     }
        $faculties = Faculty::updateOrCreate(['id' => $this->faculty_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function storeD()
    { 
            # Check permissions
        ERP::permissionToAccess('deans.admin');
        // $dean = $this->dean;
        $this->validate([
            'data.staff_id' => 'required |integer',
            'data.from_date' => 'required |date',
            'data.faculty_id' => 'required |integer'
        ],
          [
            'from_date.required' => 'The Start date is required',
            'staff_id.required' => 'The Staff is required',
            'faculty_id.required' => 'The Faculty is required'
          ]);

        $mess ='New dean submitted successfully';
     
        Dean::Create($this->data); 
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }


    public function edit($id)
    {
        $this->faculty = Faculty::findOrFail($id);
        $this->deans = Dean::where('faculty_id', $this->faculty->id)->get();
        $this->faculty_id = $id;
        $this->data['name'] = $this->faculty->name; 
        $this->data['code'] = $this->faculty->code; 
        $this->old_dean = $this->faculty->dean_id; 
        $this->data['faculty_id'] = $this->faculty->id;
        $this->isEdit = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->faculty_id = $id;
        $this->faculty = Faculty::find($id);
        // dd($this->faculty);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->faculty->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->faculty->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Faculty::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Faculty deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
        $this->data =  []; 
        $this->faculty =  []; 
        $this->deans =  []; 
        $this->faculty_id = 0;
        $this->dean_id = 0;
        $this->old_dean = 0;
        $this->isEdit = 0;
    }
}
