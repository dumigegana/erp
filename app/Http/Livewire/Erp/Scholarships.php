<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Scholarship;
use ERP;

class Scholarships extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'name';
    public $orderAsc = 'asc';
    public $isOpen = 0, $isEdit = 0;
    public $data = [], $scholarship_id =0;
    public $scholarship =[];
    public $schl = [];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {      
        # Get all the scholarships
        $scholarships = Scholarship::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.scholarships.index', ['scholarships' => $scholarships]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    {
            # Check permissions
        ERP::permissionToAccess('admissions.admin');
        $this->validate([
            'data.name' => 'required:unique',
            'data.description' => 'required',
        ]);
        if($this->scholarship_id > 0){
            $mess ='Submitted successfully';
        }
        else{
            $mess ='Updated successfully';            
        }
       $scholarships = Scholarship::updateOrCreate(['id' => $this->scholarship_id], $this->data);
        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->scholarship = Scholarship::findOrFail($id);
        $this->scholarship_id = $id;
        $this->data['name'] = $this->scholarship->name; 
        $this->data['description'] = $this->scholarship->description;
        $this->data['scholarship_id'] = $this->scholarship->id;
        $this->isEdit = 1;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->scholarship_id = $id;
        $this->scholarship = Scholarship::find($id);
        // dd($this->scholarship);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->scholarship->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->scholarship->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Scholarship::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Scholarship deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }

    public function studs($id)
    {
        $url = '/admin/scholarships/' . $id;
        redirect()->to($url, ['id' => $id]);
    }
    
   private function resetInputFields() {
        $this->data =  []; 
        $this->scholarship =  []; 
        $this->scholarship_id = 0;
        $this->isEdit = 0;
    }
}
