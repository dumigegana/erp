<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Student;
use App\Scholarship;
use ERP;

class Scholar_students extends Component
{
    use WithPagination;
    public $perPageS = 10;
    public $searchStud = '', $stud_id;
    public $orderByS = 'id';
    public $orderAscS = 'asc';
    public $isOpen = 0, $isEdit = 0;
    public $data = [], $scholarship, $scholarship_id;
    public $studs = [];

    protected $listeners = ['add', 'rel_destroy', 'reset' => 'create'];

    public function create() 
    {
        $this->resetInputFields();
    } 

    public function render()
    {
        # Get all students under the selected scholarships
        $students = Student::searchStud($this->scholarship_id, $this->searchStud)
                            ->orderBy($this->orderByS, $this->orderAscS)
                            ->paginate($this->perPageS); 
        return view('livewire.erp.scholarships.studs', ['students' => $students]);
    }
     public function mount()
    {
        $studts = Student::has('scholarship', '<', 1)->get();
        $scholarp = Scholarship::find($this->scholarship_id);
        $this->studs = $studts;
        $this->scholarship = $scholarp;
    }

    
    public function confirm()
    {
            # Check permissions
        ERP::permissionToAccess('admissions.admin');
        $this->validate([
            'stud_id' => 'required:integer',
        ]);
        $thisStudent = Student::find($this->stud_id);

        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'Are sure you want to Add ' . $thisStudent->gsu_id . ' ' . $thisStudent->user->fullname . ' to the ' . $this->scholarship->name . ' Scholarship',
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'add',
            'params' => $thisStudent->id,
            'callback' => 'reset'
        ]);
        
    } 

    public function add($id)
    { 
          # Check permissions
        ERP::permissionToAccess('admissions.admin');
        
        Student::where('id', $id)->update(['scholarship_id' => $this->scholarship->id]);
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Updated Successfully', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();
    }
    
     public function remove($id)
    {
         # Check permissions
        ERP::permissionToAccess('admissions.admin');
        $thisStudent = Student::find($id);        
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to remove ' . $thisStudent->user->fullname . ' from the ' . $this->scholarship->name . ' Scholarship', 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'rel_destroy',
            'params' => $thisStudent->id,
            'callback' => 'reset'
        ]);
    }

     public function rel_destroy($id)
    {
         # Check permissions
        ERP::permissionToAccess('admissions.admin');
        Student::where('id', $id)->update(['scholarship_id' => 0]);
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Updated successfully!', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
        $this->data =  [];
        $this->isEdit = 0;
        $this->stud_id = 0;
    }
}
