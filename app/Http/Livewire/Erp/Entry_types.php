<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use App\Entry_type;
use ERP;

class Entry_types extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'type';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $entry_type_id = 0;
    public $entry_type =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the entry_types
        $entry_types = Entry_type::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.entry_types.index', ['entry_types' => $entry_types]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('entry_types.admin');
        $entry_type = $this->entry_type;
        $this->validate([
            'data.type' => 'required |max:255',
            'data.description' => 'required |max:255',
            'data.enable' => 'nullable|boolean'
        ],
          [
            'type.required' => 'The type is required',
            'type.max' => 'Field too long, max is 255 characters'
          ]);

        if ($this->entry_type_id > 0 ) {
         $this->validate([
            'data.type' => Rule::unique('entry_types', 'type')->ignore($entry_type->id),
        ],
          [
            'type.unique' => 'Can\'t enter entry_type twice',
          ]);
         $mess ='Entry_type updated successfully';
     }else {
        $mess ='New entry_type submitted successfully';
         $this->validate([
            'data.type' => 'unique:entry_types,type'
        ],
          [
            'type.unique' => 'Can\'t enter entry_type twice',
          ]);
     }
        $entry_type = Entry_type::updateOrCreate(['id' => $this->entry_type_id], $this->data);  
        if(Arr::exists($this->data, 'enable')) {      
             if($this->data['enable'] == true) {
                $entry_type->update(['enable'=> true]);
            } 
        }else{
                $entry_type->update(['enable'=> false]);
            } 

        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->entry_type = Entry_type::findOrFail($id);
        $this->entry_type_id = $id;
        $this->data['type'] = $this->entry_type->type;
        $this->data['description'] = $this->entry_type->description;
        $this->data['enable'] = $this->entry_type->enable;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->entry_type_id = $id;
        $this->entry_type = Entry_type::find($id);
        // dd($this->entry_type);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->entry_type->type, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->entry_type->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Entry_type::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Entry_type deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->entry_type =  []; 
    $this->entry_type_id = 0;
    }
}
