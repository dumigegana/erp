<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Livewire\WithPagination;
use App\Semester;
use ERP;

class Semesters extends Component
{
    use WithPagination;

    public function index()
    {
        ERP::permissionToAccess('registration.access');

        # Get all the semesters
        $semesters = Semester::paginate(20);

        
        # Return the view
        return view('erp/semesters/index', ['semesters' => $semesters]);
    }

    public function show($id)
    {
        ERP::permissionToAccess('registration.access');

        # Get the semester
        $semester = Semester::find($id);

        # Return the view
        return view('admin/semesters/show', ['semester' => $semester]);
    }

    public function edit($id)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the semester
        $row = Semester::find($id);
        
        # Get all the data
        $data_index = 'semesters';
        require('Data/Edit/Get.php');

        # Return the view
        return view('erp/semesters/edit', [
            'row'       =>  $row,
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'empty'     =>  $empty,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ]);
    }

    public function update($id, Request $request)
    { 
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Find the row
        $row = Semester::find($id);
       
        # Save the data
        $data_index = 'semesters';
        require('Data/Edit/Save.php');

        # Return the admin to the users page with a success message
        return redirect()->route('ERP::semesters')->with('success', 'Semester changed');
    }

    public function create()
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Get all the data
        $data_index = 'semesters';
        require('Data/Create/Get.php');


        # Return the view
        return view('erp/semesters/create', [
            'fields'        =>  $fields,
            'confirmed'     =>  $confirmed,
            'encrypted'     =>  $encrypted,
            'hashed'        =>  $hashed,
            'masked'        =>  $masked,
            'table'         =>  $table,
            'code'          =>  $code,
            'wysiwyg'       =>  $wysiwyg,
            'relations'     =>  $relations,
        ]);
    }

    public function store(Request $request)
    {
        ERP::permissionToAccess('registration.access');

        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # create new semester
        $row = new Semester;

        # Save the data
        $data_index = 'semesters';
        require('Data/Create/Save.php');

        # Return the admin to the semesters page with a success message
        return redirect()->route('ERP::semesters')->with('success', 'Semester Created');
    }

    
    public function destroy($id)
    {
        ERP::permissionToAccess('registration.access');
        
        # Check permissions
        ERP::permissionToAccess('registration.admin');

        # Select Semester
        $semester = ERP::semester('id', $id);

        
        # Delete Semester
        $semester->delete();

        # Redirect the admin
        return redirect()->route('ERP::semesters')->with('success', 'Semester deleted');
    }
}