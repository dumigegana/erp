<?php

namespace App\Http\Livewire\Erp\Marks;

use Livewire\Component;
use App\Regitration;
use App\Result;

class Create extends Component
{
    public $reg_id, $state;
    public $mark; // Receives Post Request values in an array 
 
    public function mount($reg_id, $state)
    {
        $this->reg_id = $reg_id;
        $this->state = $state;
    }

    public function save()
    {
        $this->validate([
            'mark' => 'required|between:0,100|regex:/^\d+(\.\d{1,2})?$/'
        ]);
        $result = new Result;
        $result->registration_id = $this->reg_id;    
        $result->{$this->state} = $this->mark; 
        $result->save(); 
        // $this->resetInputs(); 
    }


    public function render()
    {
        return view('livewire.erp.marks.create');
    }
}
 