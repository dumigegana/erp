<?php

namespace App\Http\Livewire\Erp\Marks;

use Livewire\Component;
use App\SemCourse;
use App\Hod;
use App\Dean;
use App\Course;
use App\Weight;
use App\Part;
Use Auth;
use ERP;
use Carbon\Carbon;
use App\Exports\RegistrationsExportMapping;
use App\Imports\ResultsImport;
use Excel;
use Maatwebsite\Excel\Validators\ValidationException;

class CList extends Component
{
    public $semCos, $state, $semCos_id;
    public $data, $weights;

    public function submit() 
    {
        $this->validate([ 
          'data.field' =>'required', 
          'data.mark' =>'required', 
          'data.reg_id' =>'required', 
          'data.weight_id' =>'required', 
        ]);

        $res = Result::updateOrCreate(
            ['registration_id' => $this->data['reg_id']],
            [$this->data['field'] => $this->data['mark']],            
        );  
        $semCourse = Registration::findOrFail($this->data['reg_id'])->sem_course();
        $semCourse->update(['weight_id' => $this->data['weight_id']]);
        $this->resetInputFields(); 
        session()->flash('message', 'Mark submitted successfully.');
    }

    public function save() 
    {
        $this->validate([ 
          'data.course_w' =>'required', 
          'data.exam' =>'required', 
          'data.reg_id' =>'required',  
        ]);

        $res = Result::updateOrCreate(
            ['registration_id' => $this->data['reg_id']],
            [$this->data['field'] => $this->data['mark']],
            ['weight_id' => $this->data['weight_id']],
        );  
        $this->resetInputFields(); 
        session()->flash('message', 'Mark submitted successfully.');
    }
    #Reset input field values
  private function resetInputFields(){
    $this->data['reg_id'] =  ''; 
    $this->data['mark'] = ''; 
  }

    public function render()
    { 
        ERP::permissionToAccess('academic.access');     
        return view('livewire.erp.marks.c-list');
    }

    public function mount()
    {   
        
        $this->semCos = SemCourse::with('registrations.result')->find(decrypt($this->semCos_id));
        $this->weights = Weight::all();         
    }

    // Download Class list to excel file
    public function export($sid)
    {  
        $now = Carbon::now();
        $id = decrypt($sid);
        $semCos = SemCourse::find($id);
        $name = $semCos->course_part->course->code ."_". $now->year ."_". $now->month.".xlsx";
        return Excel::download(new RegistrationsExportMapping($id), $name);
    }

    // Upload Results from excel file
    public function import(Request $request)
    { 
        ERP::permissionToAccess('academic.admin');
        $validated = $request->validate([
            'file' => 'required|max:1000|mimes:xlsx,xls,csv',
            'weight_id' => 'required'
        ]);
          
       $data = [
            'weight_id' => $request['weight_id'], 
            // other data here
        ];
      
       $import = new ResultsImport($data);
        $import->import($request->file('file'));
         Excel::import(new ResultsImport($data), $request->file('file'));

        if($import->failures()->isNotEmpty()) {
            dd($import->failures());
            return back()->withFailure($import->failures());
        }
       return redirect()->back()->with(['success'=>'Marks  uploaded successfully']);
    }
}
