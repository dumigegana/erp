<?php

namespace App\Http\Livewire\Erp\Marks;

use Livewire\Component;
use App\SemCourse;
use App\Weight;
use App\Result;

class Add extends Component
{
	public $weight_id;
	public $weights, $sem_cos;

	public function submit() 
	{
		$this->validate([ 
	      'weight_id' =>'required', 
	    ]);

		   $this->sem_cos->update(['weight_id' => $this->weight_id]);
		  
		$this->resetInputFields();
	}
	#Reset input field values
  private function resetInputFields(){
    $this->weight_id =  ''; 
  }

  public function render()
  { 
  	return view('livewire.erp.marks.add');
  }

  public function mount($semCos_id) 
  {
  	$this->weights = Weight::all();
  	$this->sem_cos = SemCourse::findOrFail($semCos_id);

  }
}
