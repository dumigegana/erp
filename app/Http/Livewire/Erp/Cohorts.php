<?php

namespace App\Http\Livewire\Erp;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\WithPagination;
use App\Cohort;
use ERP;

class Cohorts extends Component
{
    use WithPagination;
    public $perPage = 10;
    public $search = '';
    public $orderBy = 'name';
    public $orderAsc = 'asc';
    public $isOpen = 0;
    public $data = [], $cohort_id = 0;
    public $cohort =[];

    protected $listeners = ['destroy', 'reset' => 'create'];

    public function render()
    {       
        # Get all the cohorts
        $cohorts = Cohort::search($this->search)
                            ->orderBy($this->orderBy, $this->orderAsc)
                            ->paginate($this->perPage); 
        return view('livewire.erp.cohorts.index', ['cohorts' => $cohorts]);
    }

    public function create()
    {   
        $this->resetInputFields();
    }

    public function store()
    { 
            # Check permissions
        ERP::permissionToAccess('cohorts.admin');
        $cohort = $this->cohort;
        $this->validate([
            'data.name' => 'required |max:255',
            'data.active' => 'nullable|boolean'
        ],
          [
            'name.required' => 'The name of the Cohort is required',
            'name.max' => 'Cohort name too long, max is 255 characters'
          ]);

        if ($this->cohort_id > 0 ) {
         $this->validate([
            'data.name' => Rule::unique('cohorts', 'name')->ignore($cohort->id),
        ],
          [
            'name.unique' => 'Can\'t enter cohort twice',
          ]);
         $mess ='Cohort updated successfully';
     }else {
        $mess ='New cohort submitted successfully';
         $this->validate([
            'data.name' => 'unique:cohorts,name'
        ],
          [
            'name.unique' => 'Can\'t enter cohort twice',
          ]);
     }
        $cohort = Cohort::updateOrCreate(['id' => $this->cohort_id], $this->data);  
        if(Arr::exists($this->data, 'active')) {      
             if($this->data['active'] == true) {
                $cohort->update(['active'=> true]);
            } 
        }else{
                $cohort->update(['active'=> false]);
            } 

        
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => $mess, 
            'timeout' => 10000
        ]);
        
    }

    public function edit($id)
    {
        $this->cohort = Cohort::findOrFail($id);
        $this->cohort_id = $id;
        $this->data['name'] = $this->cohort->name;
        $this->data['active'] = $this->cohort->active;
        // $this->dispatchBrowserEvent('eModal');
        
    }

     public function delete($id)
    {
        $this->cohort_id = $id;
        $this->cohort = Cohort::find($id);
        // dd($this->cohort);
        $this->emit('swal:confirm', [
            'type'    => 'warning',
            'icon'    => 'warning',
            'title'   => 'This action can not be reversed! ', 
            'text'   => 'Are sure you want to delete '. $this->cohort->name, 
            'timeout' => 10000,
            'confirmText' => 'Yes', 
            'method' => 'destroy',
            'params' => $this->cohort->id,
            'callback' => 'reset'
        ]);
    }

     public function destroy($id)
    {
        Cohort::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Cohort deleted', 
            'timeout' => 10000
        ]);
        $this->resetInputFields();

    }
    
   private function resetInputFields() {
    $this->data =  []; 
    $this->cohort =  []; 
    $this->cohort_id = 0;
    }
}
