<?php

namespace App\Http\Livewire\Erp;
# Imports
use Livewire\WithFileUploads;
use Livewire\Component;
use App\School_certificate;
use App\School_result;
use App\Subject;
use App\Grade;
use App\Tertiary_info;
use App\Tertiary_subject;
use App\Work_experience;
use App\Check;

class ApplicatResults extends Component
{
	use WithFileUploads; #from imports

  # Variables from Blade
  public $row, $subjects, $grades; 
  # GCE variables
  public $level, $awaded_date, $exam_body, $file_loc, $subject_id, $from_date, $to_date, $grade_id; 
  # Work Experience variebles
  public $occupation, $name_of_employer; # also uses  $to_date & $grade_id 
  # Tertiary Infor variables
  public $certificat_loc, $transcript_loc, $institution, $period, $awaded, $programme, $subject, $class;
  # states
  public $addSubjects = false;
  public $inputs = [];
  public $i = 1;
  Public $cert_id;

# Store O & A'level infomation, exclusive of subjects and grades 
  public function save()
  {
    $this->validate([
      'file_loc' => 'required|image|max:2000',
      'exam_body' => 'required|max:255', 
      'level' =>'required', 
      'awaded_date' =>'required', 
    ]);

    $filename = $this->file_loc->store('school');

    $school_certificate = School_certificate::create([
      'applicant_id' => $this->row->id,
      'exam_body' => $this->exam_body,
      'awaded_date' => $this->awaded_date . "-01",
      'level' => $this->level,
      'file_loc' => $filename,
    ]);
    $this->cert_id = $school_certificate->id;
    $this->addSubjects = true;

    if ($this->level != 'M')  {
	    $this->subjects = Subject::where('type', $this->level)
	    												 ->orWhere('type', 'OA')->get();
	    $this->grades = Grade::where('type', $this->level)->get();
     } else {
	    $this->subjects = Subject::all();
	    $this->grades = Grade::all();
	  }
  }

# Store O & A'level infomation inclusive of subjects
 public function saveTertiary()
  {
    $this->validate([
      'certificat_loc' => 'required|image|max:2000',
      'transcript_loc' => 'sometimes|image|max:2000',
      'institution' => 'required|max:255', 
      'period' =>'required', 
      'awaded' =>'required', 
      'programme' =>'required', 
      'class' =>'required',
      'subject.0' => 'required',
      'subject.*' => 'required', 
    ]);

    $filename = $this->certificat_loc->store('tertiary');
    if($this->transcript_loc) {
      $filename1 = $this->transcript_loc->store('tertiary');
    }
    else {
      $filename1 = '';
    }

    $tertiary_info = Tertiary_info::create([
      'applicant_id' => $this->row->id,
      'institution' => $this->institution,
      'awaded' => $this->awaded. "-01",
      'period' => $this->period,
      'programme' => $this->programme,
      'class' => $this->class,
      'certificat_loc' => $filename,
      'transcript_loc' => $filename1,
    ]);
 
    foreach ($this->subject as $key => $value) {
      Tertiary_subject::create([
        'tertiary_info_id' => $tertiary_info->id, 
        'subject' => $this->subject[$key],
      ]);
    }
    $this->row->check->update([
        'step' => 5,   
            ]); 
 
    $this->inputs = [];

    $this->resetInputFields();

    session()->flash('message', 'Tertiary information submitted Successfully.');
    return redirect()->route('ERP::applicants_academic', ['id' => $this->row->id]);

  }

# For dynamic fields, add field value to the $input array variable
  public function add($i)
  {
    $i = $i + 1;
    $this->i = $i;
    array_push($this->inputs ,$i);
  }

# For dynamic fields, remove field value to the $input array variable
  public function remove($i)
  {
    unset($this->inputs[$i]);
  }

# Reset all input field values
	private function resetInputFields(){
    $this->subject_id = '';
    $this->grade_id = '';
    $this->file_loc =  '';
    $this->exam_body =  ''; 
    $this->level = ''; 
    $this->awaded_date = '';
    $this->cert_id = '';
    $this->occupation = '';
    $this->from_date = '';
    $this->to_date = ''; 
    $this->name_of_employer = ''; 
    $this->addSujects = false;
  }

# Store O & A'level subjects and grades
  public function storeSubjects()
  {
    $validatedDate = $this->validate([
      'subject_id.0' => 'required',
      'grade_id.0' => 'required',
      'subject_id.*' => 'required',
      'grade_id.*' => 'required',
      ],
      [
      'subject_id.0.required' => 'The Subject field is required',
      'grade_id.0.required' => 'The Grade field is required',
      'subject_id.*.required' => 'The Subject field is required',
      'grade_id.*.required' => 'The Grade field is required',
      ]
        );
 
    foreach ($this->subject_id as $key => $value) {
      School_result::create([
      	'school_certificate_id' => $this->cert_id, 
      	'subject_id' => $this->subject_id[$key], 
      	'grade_id' => $this->grade_id[$key]
      ]);
    }
    if($this->row->entry_type->id == 1) {
    $step = 6;
    $complete = true;  
  }else{
    $step = 4;    
    $complete = false;
  }

    $this->row->check->update([
        'step' => $step, 
        'complete' => $complete,    
            ]); 
 
    $this->inputs = [];

    $this->resetInputFields();

    session()->flash('message', 'Subjects and grades submitted Successfully. You may add another Certificate');
    return redirect()->route('ERP::applicants_academic', ['id' => $this->row->id]);
  }
# Store O & A'level Work Experience
  public function saveWork()
  {
    $validatedDate = $this->validate([
        'occupation.0' => 'required',
        'from_date.0' => 'required',
        'to_date.0' => 'required',
        'name_of_employer.0' => 'required',
        'from_date.*' => 'required',
        'to_date.*' => 'required',
        'occupation.*' => 'required',
        'name_of_employer.*' => 'required',
    ],
    [
        'occupation.0.required' => 'The Occupation field is required',
        'from_date.0.required' => 'The Starting month & year field is required',
        'to_date.0.required' => 'The Ending month & year field is required',
        'name_of_employer.0.required' => 'The name of employer field is required',
        'occupation.*.required' => 'The Occupation field is required',
        'from_date.*.required' => 'The Starting month & year field is required',
        'to_date.*.required' => 'The Ending month & year field is required',
        'name_of_employer.*.required' => 'The name of employer field is required',
    ]
        );
 
        foreach ($this->occupation as $key => $value) {
          // dd($this->from_date[$key] ."    " . $this->to_date[$key]);
            Work_experience::create([
            	'applicant_id' => $this->row->id, 
            	'occupation' => $this->occupation[$key], 
            	'from_date' => $this->from_date[$key],
            	'to_date' => $this->to_date[$key],
             'name_of_employer' => $this->name_of_employer[$key],
            ]);
        }
        $this->row->check->update([
        'step' => 6,   
        'complete' => true,   
            ]); 
        $this->inputs = [];
 
        $this->resetInputFields();
 
        session()->flash('message', 'Employment history submitted Successfully.');
        return redirect()->route('ERP::applicants_academic', ['id' => $this->row->id]);
  }
# Displays the SPA view for uploading Applicants' academic and experience profile.
  public function render()
  {
    return view('livewire.erp.applicat-results');
  }
}
