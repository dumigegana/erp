<?php

namespace App\Http\Livewire\People;

use Livewire\Component;

class Students extends Component
{
    public function render()
    {
        return view('livewire.people.students');
    }
}
