<?php

namespace App\Http\Livewire\People;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\User;
Use App\Http\Controllers\ERP;

class Users extends Component
{

  use WithFileUploads;
  public $data, $name, $phone, $selected_id, $profile_img;
  public $updateMode = false;
   public $createMode = false;

  public function render()
  {
      $this->data = User::all();
      # Get all the data
        $data_index = 'users';
        require(app_path() . '/Http/Controllers/Data/Create/Get.php');

	
      return view('livewire.people.users', [
            'fields'    =>  $fields,
            'confirmed' =>  $confirmed,
            'encrypted' =>  $encrypted,
            'hashed'    =>  $hashed,
            'masked'    =>  $masked,
            'table'     =>  $table,
            'code'      =>  $code,
            'wysiwyg'   =>  $wysiwyg,
            'relations' =>  $relations,
        ])->layout('layouts.est');
  }

  private function resetInput()
  {
      $this->name = null;
      $this->phone = null;
      $this->profile_img = "";
  }

  public function store()
  {
      $this->validate([
          'name' => 'required|min:4',
          'phone' => 'required',
          'profile_img' => 'image|max:1024',
      ]);
     $filename = $this->profile_img->store('photos');
      User::create([
          'name' => $this->name,
          'phone' => $this->phone,
          'profile_img' => $filename
      ]);

      $this->resetInput();
      return redirect()->to('/users');
  }

  public function edit($id)
  {
      $record = User::findOrFail($id);

      $this->selected_id = $id;
      $this->name = $record->name;
      $this->phone = $record->phone;
      $this->profile_img = $record->profile_img;

      $this->updateMode = true;


  }

  public function update()
  {
      $this->validate([
          'selected_id' => 'required|numeric',
          'name' => 'required|min:5',
          'phone' => 'required',
          'profile_img' => 'image|max:1024',
      ]);

      if ($this->selected_id) {
          $record = User::find($this->selected_id);
          $record->update([
              'name' => $this->name,
              'phone' => $this->phone
          ]);

          $this->resetInput();
          $this->updateMode = false;
      }

  }

  public function destroy($id)
  {
      if ($id) {
          $record = User::where('id', $id);
          $record->delete();
      }
  }

}

