<?php

namespace App\Http\Livewire\Apply;

use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use App\Applicant;
use App\Mail\ApplicantCode;
use App\Jobs\ApplicantMailJob;
use App\Applicant_code;
use App\Entry_type;
use App\Study_program;
use App\School_result;
use App\Programme;
use App\Applicant_Programme;
// use App\Tertiary_info;
// use App\Tertiary_subject;
// use App\School_certificate;
use App\Disability;
use App\Cohort;
// use App\Subject;
// use App\Grade;
use App\Check;
// use App\Work_experience;
use Carbon\Carbon;

//Use App\Http\Controllers\ERP;
use Illuminate\Support\Facades\Mail;

class Enrol extends Component
{
	use WithFileUploads;

	public $step;
	public $applicant;
	public $disabilities, $progs;
	public $programmes;
	public $entry_types;
	public $cohorts;
	public $study_programs;
  # states
  public $addSubjects = false;
  public $inputs = [];
  public $i = 1;
  Public $cert_id, $email, $programme_id, $file_loc;
  // public $subjects, $grades, $grade_id, $subject, $subject_id;
	public $data = [		
		 'entry_type_id' => 0,	
     'perm_res_zim'   => 1, 
		 'has_dis'   => 0,		
     'marriage_certificate_fl'   => "",    
	]; 
	
 	public function increment()
    {
       $this->step++;             
    }

  public function other()
    {
      $this->validate([
        'data.email' => 'required|email',
      ]);
     $app = Applicant::where('email', $this->data['email'])->first();
     if($app) {   
       $appCode = Str::random(40);   
       $code = Applicant_code::updateOrCreate(
              ['applicant_id' =>  $app->id,
               'code' =>  $appCode        
              ]
            );

     Mail::to($this->data['email'])->queue(new ApplicantCode($code));
   
      $this->step = 0;

       $this->emit('swal:modal', [
            'type'  => 'success',
            'icon'  => 'success',
            'title' => 'Success!!',
            'text'  => 'An email has been sent to '. $this->data['email'] .' Kindly open the email and confirm your identity as instructed',
        ]);
        } 
        else{
        $this->emit('swal:modal', [
            'type'  => 'error',
            'icon'  => 'error',
            'title' => 'Error!!',
            'text'  => 'The email address '. $this->data['email'] .' was not found. Make sure you enter the correct email address, else restart the application process.',
        ]);          
        }
    } 

    public function step1()
    {
    	$this->validate([
    		'data.first_name' => 'required',
    		'data.surname' => 'required',
    		'data.email' => 'required|email|confirmed',
    		'data.sex' => 'required',
    		'data.national_id' => 'required',
    		'data.marital_status' => 'required',
        'data.cell_number' => 'required|phone:US,BE,ZW,ZA,ZB,',
        'data.terms_agree' => 'required',
    	]);

    	
    	$app = Applicant::updateOrCreate(['email' => $this->data['email']], $this->data);
      // dd($app->id);
    	$check = Check::updateOrCreate(['applicant_id' => $app->id], ['step' => 2]);
		  $to_email = $this->data['email'];

		 $appCode = Str::random(40);
		 $code = Applicant_code::updateOrCreate(['applicant_id' => $app->id],['code'=>$appCode]);

     Mail::to($this->data['email'])->queue(new ApplicantCode($code));
     // dispatch(new ApplicantMailJob($to_email, $code));
    	 $this->step = 0;

       $this->emit('swal:modal', [
            'type'  => 'success',
            'icon'  => 'success',
            'title' => 'Success!!',
            'text'  => 'An email has been sent to '. $this->data['email'] .' Kindly open the email and confirm your identity as instructed',
        ]);

    	// session()->flash('message',  'An email has been sent to '. $this->data['email'] .' Kindly open the email and confirm your identity as instructed');
    }

    public function step2()
    {
      $min_date = Carbon::now()->subYears(14);
    	$this->validate([
    		'data.tittle' => 'required',
    		'data.date_of_birth' => 'required|date|before:'.$min_date .'|after:1900-01-01',
    		'data.home_address' => 'required',
    		'data.birth_cert_no' => 'required',
    		'data.place_of_birth' => 'required',
        'data.home_phone' => 'required|phone:US,BE,ZW',
    		'data.prospective_sponsor' => 'required',
    		'data.nationality' => 'required', 
    		'data.citizenship' => 'required',
    	]);
    // Update permit details if applicant is non Zim Resident.
      if ($this->data['perm_res_zim'] == 0) {
        $this->validate([
        'data.permit' => 'required',
        'data.zim_res_period'=> 'required',
        ]);
        $this->applicant->update([
        'permit'             => $this->data['permit'],
        'zim_res_period'     => $this->data['zim_res_period'],
        ]);
        }
    // Update Disability details if applicant has Disabilities.
      if ($this->data['has_dis'] == 1) {
        $this->validate([
        'data.disability_id' => 'required',
        ]);
        $this->applicant->update([
        'disability_id'      => $this->data['disability_id'],
        ]);
      }

    	 // dd($this->data['first_name']);
    	$this->applicant->update([
        'tittle'              => $this->data['tittle'],
        'date_of_birth'       => $this->data['date_of_birth'],
        'home_address'        => $this->data['home_address'],
        'birth_cert_no'       => $this->data['birth_cert_no'],
        'place_of_birth'      => $this->data['place_of_birth'],
        'home_phone'          => $this->data['home_phone'],
        'prospective_sponsor' => $this->data['prospective_sponsor'],
        'nationality'         => $this->data['nationality'],
        'citizenship'         => $this->data['citizenship'],
        'perm_res_zim'        => $this->data['perm_res_zim'],
        'has_dis'          => $this->data['has_dis'],
				]);
    	$check = Check::where('applicant_id', $this->applicant->id)
    									->update(['step' => 3]);
      
    	 $this->step = 3;

       $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Submitted Succesifily', 
            'timeout' => 10000
        ]);
    }

		public function step3()
    { 
      if ($this->step == 3) {
    	$this->validate([
    		'data.entry_type_id' => 'required',
    		'data.cohort_id' => 'required',
    		'data.study_program_id' => 'required',
        'data.id_card_fl' =>'required|max:2048',
        'data.birth_certificate_fl' =>'required|max:2048',
        'data.marriage_certificate_fl' =>'max:2048',
        'programme_id.0' => 'required',
        'programme_id.*' => 'required',
    	],
      [
        'entry_type_id.required' => 'The Entry type is required',
        'cohort_id.required' => 'Apply for is required',
        'id_card_fl.required' => 'National Id is required',
        'study_program_id.required' => 'Study Program is required',
        'birth_certificate_fl.required' => 'Birth certificate is required',
        'birth_certificate_fl.max' => 'File too big. Maximum is 2Mb',
        'id_card_fl.max' => 'File too big. Maximum is 2Mb',
        'marriage_certificate_fl.max' => 'File too big. Maximum is 2Mb',
        'programme_id.0.required' => 'The Programme is required',
        'programme_id.*.required' => 'The Programme is required',
      ]);
      if ($this->data['entry_type_id'] != 1){
        $this->validate([
            'data.cv_fl' =>'required|max:2048',
          ],
          [
            'cv_fl.required' => 'CV is required',
            'cv_fl.max' => 'File too big. Maximum is 2Mb',
        ]);
        $cv = $this->data['cv_fl']->store('cv');
      }else {$cv = '';}
        $id_card = $this->data['id_card_fl']->store('id');
        $birth_certificate = $this->data['birth_certificate_fl']->store('birth');
      
        if($this->data['marriage_certificate_fl']) 
        {
         $marriage_certificate = $this->data['marriage_certificate_fl']->store('marriage');
          $this->applicant->update([
            'marriage_certificate'      => $marriage_certificate,
          ]);
        }        
        

    	 // dd($this->data['first_name']);
    	  $this->applicant->update([
          'entry_type_id'              => $this->data['entry_type_id'],
          'cohort_id'       => $this->data['cohort_id'],
          'study_program_id'        => $this->data['study_program_id'],
          'birth_certificate'         => $birth_certificate,
          'id_card'        => $id_card,
          'cv'              => $cv,
				]);
 
      foreach ($this->programme_id as $key => $value) {
        Applicant_Programme::create([
          'applicant_id' => $this->applicant->id, 
          'programme_id' => $this->programme_id[$key], 
        ]);
      }
       
    	$check = Check::where('applicant_id', $this->applicant->id)
    									->update(['step' => 4]);
      
    	 $this->step = 4;
       return redirect()->route('LIVE::data', ['id' => $this->applicant->id]);
     } else {
      $this->emit('swal:modal', [
            'type'    => 'error',
            'icon'    => 'error',
            'title'   => 'Unknown error, please start afresh.', 
            'timeout' => 10000
        ]);
     }

    }


# For dynamic fields, add field value to the $input array variable
  public function add($i)
  {
    $i = $i + 1;
    $this->i = $i;
    array_push($this->inputs ,$i);
  }

# For dynamic fields, remove field value to the $input array variable
  public function remove($i)
  {
    unset($this->inputs[$i]);
  }

    public function render()
    {
    	$this->entry_types = Entry_type::all();
	    $this->study_programs = Study_program::all();
	    $this->disabilities = Disability::all();         
	    $this->progs = Programme::all();
	    $this->cohorts = Cohort::where('active', true)->get();
        return view('livewire.apply.enrol');
    }
}
