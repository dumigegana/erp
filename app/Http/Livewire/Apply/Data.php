<?php

namespace App\Http\Livewire\Apply;

use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use App\Applicant;
use App\Jobs\SendEmail;
use App\Applicant_code;
use App\School_result;
// use App\Programme;
use App\Tertiary_info;
use App\Tertiary_subject;
use App\School_certificate;
use App\Subject;
use App\Grade;
use App\Check;
use App\Work_experience;
use Carbon\Carbon;
// use Mail;  

class Data extends Component
{
	use WithFileUploads;

	public $step;
	public $applicant;
  # states
  public $addSubjects = false;
  public $inputs = [];
  public $i = 1;
 	public $cert_id, $email, $programme_id, $file_loc, $subjects, $grades, $grade_id, $subject, $subject_id;
	public $data = []; 
	
 	public function increment()
    {
       $this->step++;             
    }

     # Store O & A'level infomation, exclusive of subjects and grades 
  public function step4()
  {
    $this->validate([
      'file_loc' => 'required|image|max:2000',
      'data.exam_body' => 'required|max:255', 
      'data.level' =>'required', 
      'data.year' =>'required', 
      'data.month' =>'required', 
    ]);
    

    $filename = $this->file_loc->store('school');

    $school_certificate = School_certificate::create([
      'applicant_id' => $this->applicant->id,
      'exam_body' => $this->data['exam_body'],
      'awaded_date' => $this->data['year'] . "-". $this->data['month'] . "-01",
      'level' => $this->data['level'],
      'file_loc' => $filename,
    ]);
    $this->cert_id = $school_certificate->id;
    $this->addSubjects = true;

    if ($this->data['level'] != 'M')  {
      $this->subjects = Subject::where('type', $this->data['level'])
                               ->orWhere('type', 'OA')->get();
      $this->grades = Grade::where('type', $this->data['level'])->get();
     } else {
      $this->subjects = Subject::all();
      $this->grades = Grade::all();
    }
  }


# Store O & A'level subjects and grades
  public function step4_2()
  {
    $validatedDate = $this->validate([
      'subject_id.0' => 'required',
      'grade_id.0' => 'required',
      'subject_id.*' => 'required',
      'grade_id.*' => 'required',
      ],
      [
      'subject_id.0.required' => 'The Subject field is required',
      'grade_id.0.required' => 'The Grade field is required',
      'subject_id.*.required' => 'The Subject field is required',
      'grade_id.*.required' => 'The Grade field is required',
      ]
        );
 
    foreach ($this->subject_id as $key => $value) {
      School_result::create([
        'school_certificate_id' => $this->cert_id, 
        'subject_id' => $this->subject_id[$key], 
        'grade_id' => $this->grade_id[$key]
      ]);
    }   
 
    $this->inputs = [];

    $this->resetInputFields();
    $this->step = 4;

     $this->emit('swal:modal', [
            'type'  => 'success',
            'title' => 'Success!!',
            'text'  => 'Subjects and grades submitted Successfully. You may add another Certificate and subjects or move to next level.',
        ]);

    
  }
  
  public function finStep4()
  {  
    if($this->applicant->entry_type->id == 1) {
      $this->step = 7;
    }else{
      $this->increment();
    }

    $check = Check::where('applicant_id', $this->applicant->id)
                  ->update(['step' => $this->step]); 
  }

# Store Tertiary infomation inclusive of subjects
  public function step5()
  { 
    $this->validate([
      'data.certificat_loc' => 'required|image|max:2000',
      'data.transcript_loc' => 'sometimes|image|max:2000',
      'data.institution' => 'required|max:255', 
      'data.period' =>'required',
      'data.year' =>'required',
      'data.month' =>'required',
      'data.programme' =>'required', 
      'data.class' =>'required',
      'subject.0' => 'required',
      'subject.*' => 'required', 
    ]);

    $filename = $this->data['certificat_loc']->store('tertiary');
    if($this->data['transcript_loc']) {
      $filename1 = $this->data['transcript_loc']->store('tertiary');
    }
    else {
      $filename1 = '';
    }

    $tertiary_info = Tertiary_info::create([
      'applicant_id' => $this->applicant->id,
      'institution' => $this->data['institution'],
      'awaded' => $this->data['year'] . "-". $this->data['month'] . "-01",
      'period' => $this->data['period'],
      'programme' => $this->data['programme'],
      'class' => $this->data['class'],
      'certificat_loc' => $filename,
      'transcript_loc' => $filename1,
    ]);
 
    foreach ($this->subject as $key => $value) {
      Tertiary_subject::create([
        'tertiary_info_id' => $tertiary_info->id, 
        'subject' => $this->subject[$key],
      ]);
    }
 
    $this->inputs = [];
    $this->resetInputFields();

     $this->emit('swal:modal', [
            'type'  => 'success',
            'title' => 'Success!!',
            'text'  => 'Tertiary Certificate and details submitted Successfully. You may add another Certificate and subjects or move to next level.',
        ]);

  }


  # Store Work Experience
  public function step6()
  {
    $validatedDate = $this->validate([
        'data.occupation' => 'required',
        'data.year_f' => 'required',
        'data.year_t' => 'required',
        'data.name_of_employer' => 'required',
        'data.month_f' => 'required',
        'data.month_t' => 'required',
      ],
      [
        'data.occupation.required' => 'The Occupation field is required',
        'data.year_f.required' => 'The Starting year field is required',
        'data.year_t.required' => 'The Ending  year field is required',
        'data.name_of_employer.required' => 'The name of employer field is required',
        'data.month_f.required' => 'The Starting month field is required',
        'data.month_t.required' => 'The Ending  month field is required',
      ]
    );

    if($this->step== 8) {
      $validatedDate = $this->validate([      
        'data.referee_1' => 'required',
        'data.referee_2' => 'required',
        'data.ref1_address' => 'required',
        'data.ref2_address' => 'required',
      ],
      [
        'data.referee_1.required' => 'The Reference is required',
        'data.referee_2.required' => 'The Reference year field is required',
        'data.ref1_address.required' => 'The Reference address is required',
        'data.ref2_address.required' => 'The Reference address is required',
      ]
      );
      $this->applicant->update([
        'referee_1'      => $this->data['referee_1'],
        'referee_2'          => $this->data['referee_2'],
        'ref1_address' => $this->data['ref1_address'],
        'ref2_address'         => $this->data['ref2_address'],
      ]); 
    }

    Work_experience::create([
      'applicant_id' => $this->applicant->id, 
      'occupation' => $this->data['occupation'], 
      'from_date' => $this->data['year_f'] ."-". $this->data['month_f'] . '-01',
      'to_date' => $this->data['year_t'] ."-". $this->data['month_t'] . '-01',
      'name_of_employer' => $this->data['name_of_employer'],
    ]);
   
    
    $this->inputs = [];

    $this->resetInputFields();
    if ($this->step == 8) {
      $this->finish();
    }else{
      $this->step = 8;
     $this->emit('swal:modal', [
            'type'  => 'success',
            'title' => 'Success!!',
            'text'  => 'Employment history submitted Successfully.',
        ]);
   }
  }

  public function finish()
  {    
    $this->step = 7;

    Check::where('applicant_id', $this->applicant->id)
         ->update([
          'step' => $this->step, 
          'complete' => true,
        ]);

        $this->emit('swal:modal', [
            'type'  => 'success',
            'title' => 'Success!!',
            'text'  => 'You have successfully submitted your application. Expect to hear from  us soon through your email: '. $this->applicant->email .'.',
        ]);    
  }


# For dynamic fields, add field value to the $input array variable
  public function add($i)
  {
    $i = $i + 1;
    $this->i = $i;
    array_push($this->inputs ,$i);
  }

# For dynamic fields, remove field value to the $input array variable
  public function remove($i)
  {
    unset($this->inputs[$i]);
  }

# Reset all input field values
  private function resetInputFields(){
    $this->subject_id = '';
    $this->subject = '';
    $this->grade_id = '';
    $this->file_loc =  '';
    $this->data['exam_body'] =  ''; 
    $this->data['level'] = ''; 
    $this->data['year'] = '';
    $this->data['month'] = '';
    $this->data['certificat_loc'] = '';
    $this->data['transcript_loc'] = '';
    $this->data['institution'] = '';
    $this->data['period'] = '';
    $this->data['class'] = '';
    $this->data['occupation'] = '';
    $this->data['year_t'] = '';
    $this->data['name_of_employer'] = '';
    $this->data['year_f'] = '';
    $this->data['month_f'] = '';
    $this->data['month_t'] = '';
    $this->cert_id = '';
    $this->occupation = '';
    $this->from_date = '';
    $this->to_date = ''; 
    $this->name_of_employer = ''; 
    $this->addSujects = false;
  }
    public function render()
    {
    	
        return view('livewire.apply.data');
    }
}
