<?php

namespace App\Http\Livewire\Student\Registration;
use Livewire\Component;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use App\Semester;
use App\SemCourse;
use App\Student;
use App\Oldresult;
use App\Fail;
use App\Registration;
use App\Result;
use App\Semfee;
use App\Sage\CCClient;
use Auth;
use Carbon\Carbon;
use DB;

class Register extends Component
{
	public $semesters, $semcourses, $regCorses, $prpend	;
	public $semester_id = 0;
	public $stat, $bal;

	protected $listeners = ['updateSem' => 'getCourses'];

	 public function getCourses()
    { 
    	$student =  Auth::user()->student;
    	$sem_fee = Semfee::where('programme_id', $student->programme_id)
    						->where('semester_id', $this->semester_id)
    						->first();

        if($sem_fee) {
	    	$req = $sem_fee->fee * $sem_fee->registration / 100;
	    	$this->bal =  CCClient::where('Account', strtoupper(Auth::user()->student->gsu_id))->first();    	
	    	$bal_C = $this->bal->DCBalance * -1;
	        if($bal_C < $req && $student->scholarship_id < 1) {
	        	$diff = $req - $bal_C; 
	    		$this->emit('swal:modal', [
		            'type'  => 'error',
		            'title' => 'Minimum Balance',
		            'text'  => 'To be able to register, you need to pay atleast ZW$'. $diff .'. Contact payment@gsu.ac.zw or Click on Pay Fees to pay fees online.',
		        ]);				
			} else { 
				$this->regCourses($student);
			    $this->semcourses = $this->regCorses->except($this->prpend);
			    $this->courses();
			    $this->bal =  CCClient::where('Account', strtoupper(Auth::user()->student->gsu_id))->first();
			} 
		}else{
				$this->emit('swal:modal', [
		            'type'  => 'error',
		            'title' => 'Fee Stracture',
		            'text'  => 'Contact payment@gsu.ac.zw to be assisted.',
			        ]);	
		}
		
    }

    public function regCourses($student)
    {
    	$prId = $student->programme_id;
		$stId = $student->id;
		$pid = $student->part_id + 1;
	 	$fails= [];	

		$fails = Fail::wherehas('fail_result', function($q) use ($stId) {
					$q->wherehas('registration',function($q) use ($stId) {
					         $q->where('student_id', '=', $stId);
					    	});	
						})->where('repeated', false)->pluck('course_id');
	// if ($thisStud->imprt){
		$result_keys = Oldresult::where('student_id', $stId)->get()
	                        ->filter(function ($result) {
	                            return $result->overall >= 50;
	                        })->pluck('course_id');

	   $fail_keys = Oldresult::where('student_id', $stId)->get()
	                        ->filter(function ($result) {
	                            return $result->overall < 50;
	                        })->pluck('course_id');

	    $fail_kys = $fail_keys->diff($result_keys)->all();
	    $repeats = Arr::collapse([ $fail_kys, $fails]);

	    $carry = SemCourse::where('semester_id', $this->semester_id) 
		    	                        ->wherehas('course_part', function($q) use ($pid, $prId, $repeats) {
		    	                        $q->wherein('course_id', $repeats);
											})->get();   	  

		$rep_S = SemCourse::wherehas('registrations',function($q) use ($stId, $pid) {
					         $q->where('student_id', '=', $stId)
					         ->where('part_id', $pid);
					    })->get();		

		$this->regCorses = SemCourse::where('semester_id', $this->semester_id) 
	    	                        ->wherehas('course_part', function($q) use ($pid, $prId) {
	    	                        $q->where('part_id', $pid)
	    	                          ->where('programme_id', $prId); })
	    	                        ->get();
	    	                        //dd($stId, $pid, $prId);
	    $this->prpend = Arr::collapse([$rep_S->modelKeys(), $carry->modelKeys()]); 

    }

    public function courses()
    {
    	$num = $this->semcourses->count();		      
		    if($num < 1) {
	    		$this->emit('swal:modal', [
		            'type'  => 'error',
		            'title' => 'Sorry! Courses missing',
		            'text'  => 'Contact your department for assistance.',
		        ]);
					
			} else {   
		      $this->stat = 2; 
		    }
    }

    public function register() 
    {
    	$stId = Auth::user()->student->id;
		$pid = Auth::user()->student->part_id + 1;	    
    	 $stud = Student::where('id', $stId);  
	    foreach ($this->semcourses as $s_cos) {
    		$reg = new Registration;
			$reg->student_id = $stId;
		    $reg->part_id = $pid;
			$reg->sem_course_id = $s_cos->id;
    		$reg->save();
    		// Result::create([
    		// 	'registration_id' => $reg->id
    		// ]);
    	} 

        $stud->update(['part_id' => $pid]);

  		$this->stat = 3; 
  		$this->emit('swal:modal', [
            'type'  => 'success',
            'title' => 'Success',
            'text'  => 'You have successfully registered your courses.',
        ]); 
          	      
    }

    public function render()
    { 
	    return view('livewire.student.registration.register');
    }

     public function mount()
    {
    	$this->semesters = Semester::all()->filter(function($item) {
		  if (Carbon::now()->between($item->reg_from, $item->reg_to)) {
		    return $item;
		  }
		});
		$this->bal =  CCClient::where('Account', strtoupper(Auth::user()->student->gsu_id))->first();
	}	
}
