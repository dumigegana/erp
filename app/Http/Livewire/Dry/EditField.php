<?php

namespace App\Http\Livewire\Dry;

use Livewire\Component;

class EditField extends Component
{
    public $origName;
    public $entityId;
    public $newName; // dirty operation name state
    public $isName; // determines whether to display it in bold text
    public string $field; // this is can be column. It comes from the blade-view foreach($fields as $field)
    public string $model; // Eloquent model with full name-space
 
    public function mount($model, $entity, $field)
    {
        $this->entityId = $entity->id;
        $this->origName = $entity->{$field};
        $this->field = $field;

        $this->init($model, $entity); // initialize the component state
    }

    public function save()
    {
        $entity = $this->model::findOrFail($this->entityId);
        $newName = $this->newName; 
        $newName = $newName === $this->origName ? null : $newName; // don't save it as operation name it if it's identical to the short_id

        $entity->{$this->field} = $newName ?? null;
        $entity->save();
        $this->origName = $newName;
        $this->init($this->model, $entity); // re-initialize the component state with fresh data after saving
        // $this->dispatchBrowserEvent('notify', Str::studly($this->field).' successfully updated!');
    }

    private function init($model, $entity)
    {
        $this->model = $model;
        $this->newName = $this->origName;
        $this->isName = $entity->{$this->field} ?? false;
    }

    public function render()
    {
        return view('livewire.dry.edit-field');
    }
}
