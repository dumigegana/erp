<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Auth;

class Login extends Component
{
  public function username()
  {
      return 'username';
  }
  public $form = [
    'username' => '',
    'password' => ''
  ];

  public function submit(  )
  {
    $this->validate([
      'form.username' => 'required',
      'form.password' => 'required'
    ]);

    Auth::attempt($this->form);

    return redirect(route('admin/users'));
  }
    public function render()
    {
        return view('livewire.auth.login');
    }
}
