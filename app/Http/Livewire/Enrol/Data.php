<?php

namespace App\Http\Livewire\Enrol;

use Livewire\Component;

class Data extends Component
{
	
    public function render()
    {
        return view('livewire.enrol.data');
    }
}
