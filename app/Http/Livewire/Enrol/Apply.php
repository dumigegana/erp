<?php

namespace App\Http\Livewire\Enrol;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Applicant;
use App\Applicant_code;
use App\Entry_type;
use App\Study_program;
use App\Programme;
use App\Applicant_Programme;
use App\Disability;
use App\Cohort;
use App\Check;

Use App\Http\Controllers\ERP;
use Mail; 

class Apply extends Component
{
	use WithFileUploads;

	public $step = 0;
	public $aplicant ="";
	public $disabilities;
	public $programmes;
	public $entry_types;
	public $cohorts;
	public $study_programs;
	public $data = []; 
	
 	public function increment()
    {
       $this->step++;             
    }

    public function render()
    {  
    	$this->entry_types = Entry_type::all();
      $this->study_programs = Study_program::all();
      $this->disabilities = Disability::all();         
      $this->programmes = Programme::all();
      $this->cohorts = Cohort::where('active', true)->get();
    	return view('livewire.enrol.apply');
    	// ->extends('layouts.app_BK')
     //        ->section('body');
    }

    public function personal()
    {
    	$this->validate([
    		'data.first_name' => 'required',
    		'data.surname' => 'required',
    		'data.email' => 'required|email|confirmed',
    		'data.sex' => 'required',
    		'data.tittle' => 'required',
    		'data.marital_status' => 'required',
    		'data.home_phone' => 'required|phone:US,BE,ZW',
        	'data.cell_number' => 'required|phone:US,BE,ZW',
    		'data.home_address' => 'required',
    	]);
    	$old = Applicant::where('email', $this->data['email'])->first();
    	if ($old) {
    		$old->delete();
    	}
    	 // dd($this->data['first_name']);
    	$applicant = Applicant::create($this->data);
    	$check = new Check;
        $check->applicant_id = $applicant->id;
        $check->step = 1;
        $check->save();
    	 $to_name = $this->data['first_name'];
		 $to_email = $this->data['email'];
		 $data = array('name'=>"Gwanda State University", 'body' => "Use the following link to apply.");
		 Mail::send('emails.student', $data, function($message) use ($to_name, $to_email) {
		 $message->to($to_email, $to_name)
		 ->subject('Email Confirmation');		
		 });		 
    	 $this->step = 0;

    	 session()->flash('message', 'Account created successfully.');
    }

    public function connect($code)
    {
    	$app = Applicant_code::where('code', $code)->first();
    	if ($code){
    		$this->applicant = $app->applicant_id;
    		$applic = Applicant::find('$applic');
    		if ($applic->tittle);
    	}
    }

}
