<?php

namespace App\Http\Livewire\Enrol;

use Livewire\Component;

class Personal extends Component
{
    public function render()
    {
        return view('livewire.enrol.personal');
    }
}
