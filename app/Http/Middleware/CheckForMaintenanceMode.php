<?php

namespace App\Http\Middleware;

use closure;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Auth;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {   
        if (!$this->isAdmin() && $this->app->isDownForMaintenance()) {
            $data = json_decode(file_get_contents($this->app->storagePath() . '/framework/down'), true);

            throw new MaintenanceModeException($data['status']);
        }

        return $next($request);
    }

    private function isAdmin()
    {
        if (Auth::check()) {
            return optional(auth()->user()->hasRole('Admin'));
        }
        else {
            return false;
        }
    }
}
