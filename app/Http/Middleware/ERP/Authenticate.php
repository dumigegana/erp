<?php

namespace App\Http\Middleware\ERP;

use Closure;
use Auth;
use ERP;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            ERP::mustBeStaff(ERP::loggedInUser());
        } else {
            return redirect()->route('welcome')->with('error', 'You are not logged in');
        }
        
        return $next($request);
    }
}
