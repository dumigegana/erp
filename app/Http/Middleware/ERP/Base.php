<?php

namespace App\Http\Middleware\ERP;

use Closure;
use Auth;
use ERP;
use App;

class Base
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {

            $user = ERP::loggedInUser();

            if(!$user->active) {
                if(ERP::currentURL() != url('/logout')) {
                    return redirect()->route('welcome')->with('warning', 'Account De-activated, Contact ICTS for assistance.');                   
                }

            }
        } 
        return $next($request);
    }
}
