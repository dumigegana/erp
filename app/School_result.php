<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class School_result extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    public static function search($search) 
    {
        // return empty($search) ? static::query()
        // : static::query()->where('name','Like', '%'.$search.'%')->orwhere('type','Like', '%'.$search.'%');
    }

     protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'school_certificate_id', 'subject_id', 'grade_id',    
    ];  
    
    public function school_certificate()
    {
        return $this->belongsTo('App\School_certificate');
    } 

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    } 

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    } 
}
 