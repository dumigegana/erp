<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applicant_code extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    
    protected $fillable = [
        'applicant_id', 'code',  
    ];   

    
     public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    } 

   
}
