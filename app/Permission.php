<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    public function roles()
    {
    	return $this->belongsToMany('App\Role');
    }


    //migration permission_role

     public static function permission($type, $data)
    {
        if($type == 'id') {
            return Permission::find($data);
        }
        return Permission::where($type, $data)->first();
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('slug','Like', '%'.$search.'%');
    }

}
