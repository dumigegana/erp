<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegRedo extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

  protected $fillable = [
        'fail_id', 'sem_course_id', 'fin_clearnc', 'approved', 'status', 
    ]; 

    public function fail()
      {
      	$this->belongsTo('App\Fail');
      }

      public function sem_course()
      {
      	$this->belongsTo('App\Sem_course');
      }  
}
