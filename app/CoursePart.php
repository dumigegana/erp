<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoursePart extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'mysql'; 
    use SoftDeletes;

    protected $fillable = [
        'course_id', 'part_id', 'programme_id' 
    ];
    // protected $with = ['course', 'part'];
    protected $table = "course_parts";
    
    public function part()
    {
        return $this->belongsTo('App\Part', 'part_id');
    }        
    
    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }      
    
    public function programme()
    {
        return $this->belongsTo('App\Programme', 'programme_id');
    } 

     public function fails()
    {
        return $this->hasMany('App\Fails');
    } 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->wherehas('part', function($o) use ($search) {$o->where('semester',  'like', '%' . $search . '%');})
            ->orwherehas('part', function($p) use ($search) {$p->where('part',  'like', '%' . $search . '%');})                                        
            ->orwherehas('course', function($q) use ($search) {$q->where('name', 'like', '%' . $search . '%');})
            ->orwherehas('course', function($r) use ($search) {$r->where('code', 'like', '%' . $search . '%');})
            ->orwherehas('programme', function($s) use ($search) {$s->where('code', 'like', '%' . $search . '%');})
            ->orwherehas('programme', function($t) use ($search) {$t->where('name', 'like', '%' . $search . '%');});
    }
}
