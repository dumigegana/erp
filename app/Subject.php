<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';   
	use SoftDeletes;

    protected $fillable = [
        'name', 'type',  
    ]; 

    public function tertiary_subject()
    {
        return $this->hasOne('App\Tertiary_subject');
    }  

    public function school_results()
    {
        return $this->hasMany('App\School_result');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('type','Like', '%'.$search.'%');
    }
}
