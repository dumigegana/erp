<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
   protected $table = 'staffs';

      protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'gsu_number', 'title', 'sex', 'department_id', 'position', 'type',  
    ];

    // protected $with = ['user'];    

     public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function hods()
    {
        return $this->hasMany('App\Hod');
    }

    public function dean()
    {
        return $this->hasMany('App\Dean');
    }

     public function sem_courses()
    {
        return $this->hasMany('App\SemCourse');
    } 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('gsu_number','Like', '%'.$search.'%')->orwhere('title','Like', '%'.$search.'%');
    }

}
