<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outcome extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';   
    use SoftDeletes;

	protected $fillable = [
        'name', 'type',   
    ];   

    public function cohorts()
    {
        return $this->hasMany('App\Cohort');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('type','Like', '%'.$search.'%');
    }
 
}
