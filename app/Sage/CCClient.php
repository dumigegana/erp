<?php

namespace App\Sage;

use Illuminate\Database\Eloquent\Model;

class CCClient extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = '_bvCCClients';
}
 