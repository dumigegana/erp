<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Applicant_Proramme;
class Applicant extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $with = ['work_experiences', 'tertiary_infos.tertiary_subjects', 'school_certificates.school_results', 'programmes', 'Attended_schools', 'check'];
 
    protected $dates = [
        'created_at', 'updated_at', 'outcome_at',  
    ];

    protected $fillable = [
        'student_id', 'cohort_id', 'entry_type_id', 'study_program_id', 'programme_id', 'first_name', 'middle_names', 'surname', 'previous_surname', 'email', 'home_address', 'sex', 'national_id', 'tittle', 'marital_status', 'nationality', 'citizenship', 'perm_res_zim', 'permit', 'zim_res_period', 'cell_number', 'home_phone', 'date_of_birth', 'birth_cert_no', 'place_of_birth', 'has_dis', 'disability_id', 'prospective_sponsor', 'referee_1', 'admissions_outcome', 'ref1_address', 'referee_2', 'ref2_address', 'birth_certificate', 'id_card', 'marriage_certificate', 'cv', 'terms_agree', 'admissions_comment', 'fdean_outcome', 'fdean_comment', 'hod_outcome', 'hod_comment', 'bursar_outcome', 'bursar_comment', 'imprt'
    ];
    
    #create fullname from first_name, middle_names and surname attributes
     public function getFullNameAttribute()
    {
      return $this->first_name . " " . $this->middle_names . " " . $this->surname;
    }

    #create prefered programme from the provided list.
     public function getPreferedProgAttribute()
    { 
         if($this->imprt == false) {
            $program = Applicant_Programme::where('applicant_id', $this->id)->orderBy('id', 'desc')->first();
          return Programme::find($program->programme_id)->name;
           }else { return"";}
    }
    #create prefered programme id from the provided list.
     public function getPreferedProgIdAttribute()
    {
        if($this->imprt == false) {
            $program = Applicant_Programme::where('applicant_id', $this->id)->orderBy('id', 'desc')->first();
          return Programme::find($program->programme_id)->id;
      }else { return"";}
    }

    #Define relationship with student
     public function student()
    {
        return $this->belongsTo('App\Student');
    }
    
    #Define relationship with cohort
    public function cohort()
    {
        return $this->belongsTo('App\Cohort');
    }
    
    #Define relationship with Programmes
     public function programmes()
    {
        return $this->belongsToMany('App\Programme');
    }

     #Define relationship with Entry_type
     public function entry_type()
    {
        return $this->belongsTo('App\Entry_type');
    }
     #Define relationship with Check
     public function check()
    {
        return $this->hasOne('App\Check');
    }

     #Define relationship with study_program
     public function study_program()
    {
        return $this->belongsTo('App\Study_program');
    }

     #Define relationship with disability
     public function disability()
    {
        return $this->belongsTo('App\Disability');
    }
     
      #Define relationship with Applicant_code
     public function applicant_code()
        {
            return $this->hasOne('App\Applicant_code');
        }
     
      #Define relationship with work_experiences
     public function work_experiences()
    {
        return $this->hasMany('App\Work_experience');
    }


     #Define relationship with school_certificates 
     public function school_certificates()
    {
        return $this->hasMany('App\School_certificate');
    }

     #Define relationship with attended_schools 
     public function attended_schools()
    {
        return $this->hasMany('App\Attended_school');
    }

     #Define relationship with tertiary_infos 
     public function tertiary_infos()
    {
        return $this->hasMany('App\Tertiary_info');
    }
 
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()
                        //->where('imprt', false)
                        ->where('first_name','Like', '%'.$search.'%')
                        ->where('surname','Like', '%'.$search.'%')
                        ->orwhere('student_id','Like', '%'.$search.'%')
                        ->orwhere('previous_surname','Like', '%'. $search.'%')
                        ->orwhere('national_id','Like', '%'. $search.'%')
                        ->orwhere('cell_number','Like', '%'. $search.'%')
                        ->orwhere('home_phone','Like', '%'. $search.'%')
                        ->orwhere('birth_cert_no','Like', '%'. $search.'%')
                        ->orwhere('prospective_sponsor','Like', '%'. $search.'%');
    }


}
 