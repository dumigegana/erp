<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
	use SoftDeletes;

	protected $fillable = [
        'grade', 'type', 'points'
    ];

    public function school_results()
    {
        return $this->hasMany('App\School_result');
    } 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('grade','Like', '%'.$search.'%')
                        ->orwhere('type','Like', '%'.$search.'%')
                        ->orwhere('points','Like', '%'.$search.'%');
    }
}
