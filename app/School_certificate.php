<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class School_certificate extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    public static function search($search) 
    {
        // return empty($search) ? static::query()
        // : static::query()->where('name','Like', '%'.$search.'%')->orwhere('type','Like', '%'.$search.'%');
    }

    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'applicant_id', 'awaded_date', 'exam_body', 'level', 'file_loc',   
    ];   

    public function getAwadedMonthAttribute()
    {
        $date = new Carbon( $this->awaded_date );   
        return $date->format('Y M');
    }

     public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }  

    public function school_results()
    {
        return $this->hasMany('App\School_result');
    } 
}
