<?php

namespace App\Exports;

use App\Registration;
use Maatwebsite\Excel\Concerns\FromCollection;

class ResultsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection($id)
    {
    	  $regs = Registration::with('registrations')->find($id);
        return $regs;
    }
}
