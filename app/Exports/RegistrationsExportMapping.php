<?php

namespace App\Exports;

use App\Registration;
use App\SemCourse;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
Use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RegistrationsExportMapping implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize, WithEvents//, WithTitle
{
	protected $id;
	use Exportable, RegistersEventListeners;

    public function __construct($id)
    {
        $this->id = $id;
        $this->lines = Registration::with('student.user')->where('sem_course_id', $this->id)->count() + 1; 
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }

    /**
    * @return query
    */
    public function query()
    {
        return Registration::query()->with('student.user')->where('sem_course_id', $this->id);
    }
     public function map($registration) : array {
        $course_w = '';
        $exam = '';
        if ($registration->result()->exists()) {
          $course_w = $registration->result->course_w;
          $exam = $registration->result->exam;  
        }

        return [
            $registration->id, //dechex($registration->id) hexdec($registration->id)
            $registration->student->gsu_id,
            $registration->student->user->fullname,
            $course_w,
            $exam
        ] ;
    } 

    public function headings() : array {

        return [
           'Reg',
           'ID',
           'Name',
           'Course_Work',
           'Exam',
        ];
    }

    public function registerEvents(): array
    { 
      return [
    		AfterSheet::class => function(AfterSheet $event) {
		    	$semC = SemCourse::find($this->id);
		    	$tittl = $semC->semester->name." ". $semC->course_part->course->code ." ".$semC->course_part->course->name;
    			$cellRange = 'A1:F1';
    			$event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14)->setBold(true);
          $event->sheet->getDelegate()->getStyle('A1:A'. $this->lines)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
				$event->sheet->setCellValue('G1', $tittl);
	    		// Protect whole sheet.
	    		//$event->sheet->loadView('template');
				$event->sheet->getProtection()->setAlgorithm('SHA-512');
				$event->sheet->getProtection()->setSheet(true)
											->setSort(false)
								            ->setFormatCells(false)
								            ->setFormatRows(false)
								            ->setFormatColumns(false)
				 				            ->setSelectLockedCells(true)
											->setPassword('c1!a5sLi5t');
				// Now unprotect the areas,
                $event->sheet->getStyle('D2:F'. $this->lines)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
                $event->sheet->getColumnDimension('B')->setAutoSize(true);
                $event->sheet->getColumnDimension('C')->setAutoSize(true);
                $event->sheet->getColumnDimension('D')->setAutoSize(true);
	    	}
	    ];
    }

}

