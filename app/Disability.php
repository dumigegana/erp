<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disability extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
	use SoftDeletes;
    protected $fillable = ['description', 'code'];
    #Define relationship with Check
     public function applicant()
    {
        return $this->hasOne('App\Applicant');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('description','Like', '%'.$search.'%')->orwhere('code','Like', '%'.$search.'%');
    } 

}
