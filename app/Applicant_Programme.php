<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applicant_Programme extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;     
     use SoftDeletes;

     protected $connection = 'mysql';
     public $timestamps = false;
     protected $table = 'applicant_programme';
    
	protected $fillable = ['applicant_id', 'programme_id'];
    
}
