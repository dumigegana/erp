<?php

namespace App;

use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'mysql';
    protected $primaryKey = 'payment';
    
    protected $fillable = [
        'fullname', 'gsu_id', 'amount', 'program', 'phone', 'status', 'channel', 'reference', 'merchanttrace', 'hash', 'pollurl'
         ];

    public static function search($search) 
    {
         return empty($search) ? static::query()
        : static::query()->where('gsu_id',  'like', '%' . $search . '%')
            ->orwhere('fullname',  'like', '%' . $search . '%')  
            ->orwhere('phone',  'like', '%' . $search . '%')  
            ->orwhere('channel',  'like', '%' . $search . '%')  
            ->orwhere('reference',  'like', '%' . $search . '%');
    }
}
