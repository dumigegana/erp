<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bulletin extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
   protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'year_range',   
    ];   

    public function courses()
    {
        return $this->belongsToMany('App\Course');
    } 

    public function cohorts()
    {
        return $this->hasMany('App\Cohort');
    }

    public function hasCourse($id)
    {
        foreach($this->courses as $course) {
            if($course->id == $id) {
                return true;
            }
        }
        return false;
    }
         
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('year_range','Like', '%'. $search.'%');
    } 

}
