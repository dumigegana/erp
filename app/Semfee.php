<?php

namespace App;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Semfee extends Model implements Auditable
{
   use \OwenIt\Auditing\Auditable;
    protected $connection = 'mysql';

    protected $fillable = ['semester_id', 'programme_id', 'fee', 'registration', 'results'];

    protected $with = ['semester', 'programme'];

    public function semester()
    {
        return $this->belongsTo('App\Semester');
    }

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }

     public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->wherehas('semester', function($o) use ($search) {$o->where('name',  'like', '%' . $search . '%');})
            ->orwherehas('programme', function($s) use ($search) {
                $s->where('code', 'like', '%' . $search . '%')
                  ->orwhere('name', 'like', '%' . $search . '%');
            });
    }
}
