<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dean extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'faculty_id', 'staff_id', 'active', 'from_date', 'to_date',  
    ];
    
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%');
    }   

    /**
    * A accessor to get Dean's full name dean name. 
    *
    */
    public function getFullNameAttribute()
    { 
        return $this->staff->user->username;
    }

     public function faculty()
    {
        return $this->belongsTo('App\Faculty');
    } 

      public function staff()
    {
        return $this->belongsTo('App\Staff');
    } 

    public function facultyC()
    {
        return $this->hasOne('App\Faculty');
    }
}
