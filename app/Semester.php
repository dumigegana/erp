<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Semester extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
	use SoftDeletes;
	protected $fillables =[
	  	'name', 'reg_from', 'reg_to', 'active', 'billed'
	  ];
    public function sem_courses()
    {
        return $this->hasMany('App\SemCourse');
    }     

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%');
    }

}
