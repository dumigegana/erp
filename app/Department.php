<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'faculty_id', 'name', 'hod_id'
    ];   

    
   // protected $with = ['staff'];
     public function faculty()
    {
        return $this->belongsTo('App\Faculty');
    } 

     public function programmes()
    {
        return $this->hasMany('App\Programme');
    } 

     public function courses()
    {
        return $this->hasMany('App\Course');
    } 

     public function hods()
    {
        return $this->hasMany('App\Hod');
    } 
    
     public function current()
    {         
        return $this->belongsTo('App\Hod', 'hod_id');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('faculty_id','Like', '%'.$search.'%')->orwherehas('faculty', function($p) use ($search) {$p->where('name',  'like', '%' . $search . '%');}); 
    }
}
