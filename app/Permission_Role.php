<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Permission_Role extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('type','Like', '%'.      $search.'%');
    }

    public $timestamps = false;
    protected $table = 'permission_role';
}
