<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

     use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 'created_at', 'updated_at',  
    ];
    protected $softDelete = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'first_name', 'middle_names', 'surname', 'username', 'password',  'cell_number', 'profile_picture', 'active', 'su'  
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
    * A mutator to convert username to lower case.
    *
    */
    public function setUsernameAttribute($value)
    {
      $this->attributes['username'] = strtolower($value);
    }
   
    /**
    * A accessor to generate Users' emails from their username 
    *
    */
    public function getEmailAttribute()
    {
      return strtolower($this->username) . "@gsu.ac.zw";
    }

    /**
    * An accessor to display the Users' full name
    *
    */
    public function getFullNameAttribute()
    {
      return ucwords($this->first_name . " " . $this->middle_names . " " . $this->surname);
    }
    
    /**
    * Returns true if the user has the staff.access permission
    *
    */
    public function isStaff()
    {
        return $this->hasRole('Staff');
    }


    /**
    * Returns true if the user has the Student Role
    *
    */
    public function isStudent()
        {
            return $this->hasRole('Student');
        } 

   public function hasPermission($slug)
    {
        foreach($this->roles as $role) {
            foreach($role->permissions as $perm) {
                if($perm->slug == $slug) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
    * Returns true if the user has the role
    *
    * @param string $name
    */
    public function hasRole($name)
    {
        foreach($this->roles as $role) {
            if($role->name == $name) {
                return true;
            }
        }
        return false;
    }

    /**
    *Defining User Model Relationships with Student and Student models 
    *
    */
    public function student()
    {
        return $this->hasOne('App\Student');
    }

    

    public function staff()
    {
        return $this->hasOne('App\Staff');
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('first_name','Like', '%'.$search.'%')
                        ->orwhere('surname','Like', '%'.$search.'%')
                        ->orwhere('middle_names','Like', '%'.$search.'%')
                        ->orwhere('surname','Like', '%'.$search.'%')
                        ->orwhere('cell_number','Like', '%'.$search.'%');
    }
}
