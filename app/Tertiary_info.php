<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Tertiary_info extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';    
     protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'applicant_id', 'institution', 'period', 'programme', 'class', 'awaded', 'certificat_loc', 'transcript_loc', 
    ];   

    public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    }  

    public function tertiary_subjects()
    {
        return $this->hasmany('App\Tertiary_subject');
    } 
    
    public function getAwadedMonthAttribute()
    {
        $date = new Carbon( $this->awaded );   
        return $date->format('M/Y');
    }
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('institution','Like', '%'.$search.'%')->orwhere('programme','Like', '%'.$search.'%');
    }

}
