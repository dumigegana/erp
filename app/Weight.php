<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Weight extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
   use SoftDeletes;

	protected $fillable = [
        'course_w', 'exam',   
    ]; 

    public function oldresults()
    {
        return $this->hasMany('App\Oldresult');
    } 

    public function sem_courses()
    {
        return $this->hasMany('App\SemCourse');
    } 
}
