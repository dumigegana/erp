<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cohort extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
    protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',
    ];

    use SoftDeletes;

    protected $fillable = [
        'name', 'bulletin_id', 'active', 
    ];

    public function bulletin()
    {
        return $this->belongsTo('App\Bulletin');
    } 

    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    } 
     
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('active','Like', '%'.$search.'%');
    }
  

}
