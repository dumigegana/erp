<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tertiary_subject extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
   protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'tertiary_info_id', 'subject',   
    ];   

    public function tertiary_info()
    {
        return $this->belongsTo('App\Tertiary_info');
    } 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('subject','Like', '%'.$search.'%');
    }  
}
 