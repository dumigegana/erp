<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Contracts\Auditable;

class BulletinCourse extends Pivot implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'mysql';
    // protected $table = 'bulletin_courses';
}

