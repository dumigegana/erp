<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LecturerCourses extends Mailable
{
    use Queueable, SerializesModels;
    protected $details, $sem_courses;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details, $sem_courses)
    {
        $this->details = $details;
        $this->sem_courses = $sem_courses;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { dd($this->details)
        return $this->view('emails.lecturer_courses')->with(['details' => $this->details, 'sem_courses' => $this->sem_courses]);
    }
}
