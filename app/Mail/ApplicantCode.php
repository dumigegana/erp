<?php

namespace App\Mail;

use App\Applicant_code;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplicantCode extends Mailable implements ShouldQueue
{
  use Queueable, SerializesModels;

  /**
   * The applicant_code instance.
   *
   * @var \App\Applicant_code
   */
  protected $code;

  /**
   * Create a new message instance.
   *
   * @param  \App\Applicant_code $code
   * @return void
   */
  public function __construct(Applicant_code $code)
  {
    $this->code = $code;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    // dd($this->code);
    return $this->markdown('emails.applicant.code')
                ->with(['code' => $this->code->code]);
  }
}
