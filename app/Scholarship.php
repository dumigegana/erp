<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Scholarship extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';

    use SoftDeletes;

    protected $dates = [
        'created_at', 'updated_at',  
    ];

    protected $fillable = [
        'name', 'description',
    ]; 

    public function students()
    {
    	$this->hasMany('App/Student');
    }

     public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('description','Like', '%'.$search.'%');
    }

}
