<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    use SoftDeletes;

    protected $fillable = [
        'name', 'description'
    ];
    protected $table = "roles";

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    //migration permission_role
     public static function role_m($type, $data)
    {
        if($type == 'id') {
            return Role::find($data);
        }
        return Role::where($type, $data)->first();
    }

    public function permissions()
    {
    	return $this->belongsToMany('App\Permission');
    }

    public function hasPermission($slug)
    {
        foreach($this->permissions as $perm) {
            if($perm->slug == $slug) {
                return true;
            }
        }
        return false;
    }

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%');
    }
}
