<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fail extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
	use SoftDeletes;
    protected $fillables = [
                  'fail_result_id', 'repeat_result_id', 'sem_course_id', 'course_id', 'status'
                ];

    public function registration()
    {
      return $this->hasOne('App\Registration');
    }

    public function sem_course()
    {
      return $this->belongsTo('App\SemCourse');
    }

    public function course()
    {
      return $this->belongsTo('App\Course');
    }

    public function fail_result()
    {
      return $this->belongsTo('App\Result', 'fail_result_id');
    }

    public function repeat_result()
    {
      return $this->belongsTo('App\Result', 'repeat_result_id');
    }
}
