<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Check extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $connection = 'mysql';

	protected $fillable = [
    	'applicant_id', 'step', 'personal', 'academic', 'files', 'outcomes', 'status'
    ];
}
