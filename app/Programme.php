<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programme extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
      protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'name', 'department_id', 'code', 
    ];   
    
    public function department()
    {
        return $this->belongsTo('App\Department');
    } 

     public function course_parts()
    {
        return $this->hasMany('App\Course_Part', 'programme_id');
    } 

    #Define relationship with applicant
    public function applicants()
    {
        return $this->belongsToMany('App\Applicant');
    }

   
    /**
    * A accessor to generate full program name 
    *
    */
    public function getFullnameAttribute()
    {
      return $this->code . ":" . $this->name;
    }
    
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('code','Like', '%'.$search.'%');
    }

}
