<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registration extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

  protected $fillable = [
        'student_id', 'sem_course_id', 'part_id', 'fin_clearnc', 'approved', 'status', 'fail_id' 
    ];
 
    public function part()
    {
    	return $this->belongsTo('App\Part');
    }

    public function sem_course()
    {
    	return $this->belongsTo('App\SemCourse');
    }

    public function courses()
    {
        return $this->hasOneThrough(Course::class, CoursePart::class);
    }

    public function student()
    {
      return $this->belongsTo('App\Student');
    }

    public function result()
    {
      return $this->hasOne('App\Result');
    }

     #Define relationship with fail
    public function fails()
    {
        return $this->belongsTo('App\Fail');
    }

    public static function search($search) 
    {
         return empty($search) ? static::query()
        : static::query()->wherehas('part', function($o) use ($search) {$o->where('semester',  'like', '%' . $search . '%');})
            ->orwherehas('part', function($p) use ($search) {$p->where('part',  'like', '%' . $search . '%');})  
            ->orwherehas('student', function($r) use ($search) {$r->where('gsu_id', 'like', '%' . $search . '%');})
            ->orwherehas('student', function($s) use ($search) {$s->wherehas('user',function($a) use ($search) {$a->where('first_name', 'like', '%' . $search . '%');});})
            ->orwherehas('student', function($u) use ($search) {$u->wherehas('programme',function($b) use ($search) {$b->where('code', 'like', '%' . $search . '%');});})
            ->orwherehas('sem_course.course_part', function($v) use ($search) {$v->wherehas('course',function($c) use ($search) {$c->where('code', 'like', '%' . $search . '%');});});
    }
}
 