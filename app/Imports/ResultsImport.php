<?php

namespace App\Imports;

use App\Result;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ResultsImport implements ToCollection,WithHeadingRow, WithValidation, SkipsOnError, SkipsOnFailure
{
    Use Importable, SkipsErrors, SkipsFailures;

    private $data; 

     public function  __construct(array $data = [])
    {
        $this->data = $data; 
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function model(array $row)
    // {
    //     // dd($this->data);
    //     return new Result([
    //         'weight_id' => $this->data['weight_id'],
    //         'registration_id' => $row['reg'],
    //         'course_w'        => $row['course_work'], 
    //         'exam'            => $row['exam'],
    //     ]);
    // }

    public function collection(Collection $rows)
    {
        $rows->each(function($row, $key) {
            Result::updateOrCreate(['registration_id'   => $row['reg'] ],
                ['weight_id' => $this->data['weight_id'],                
                'course_w'  => $row['course_work'],
                'exam'  => $row['exam'],
                // Want it to be possible to add my package here
                //'package' => $package
            ]);
        });
    }

    public function rules(): array 
    {
        return [
            'reg' =>['required', 'integer', 'unique:results,id']
        ];
    }
}
