<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'gsu_id', 'programme_id', 'cohort_id', 'part_id', 'sex', 'disability', 'imprt', 'scholarship_id'
    ]; 
   
    
    protected $with = ['user'];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    } 

    public function oldresults()
    {
        return $this->hasMany('App\Oldresult');
    }
 

    public function applicant()
    {
        return $this->hasOne('App\Applicant');
    } 

    public function programme()
    {
        return $this->belongsTo('App\Programme');
    }

    public function part()
    {
        return $this->belongsTo('App\Part');
    }

    public function scholarship()
    {
        return $this->belongsTo('App\Scholarship');
    }

    public function registrations()
    {
        return $this->hasMany('App\Registration');
    }
    

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('gsu_id','Like', '%'.$search.'%')->orwhere('sex','Like', '%'.$search.'%')
                          ->orwhereHas('user', function($q) use ($search){
                            $q->where('surname', 'Like', '%'.$search.'%')->orwhere('first_name', 'Like', '%'.$search.'%');
                             });
    }

     public static function searchStud($id, $searchStud) 
    {
        return empty($search) ? static::query()->where('scholarship_id', $id)
        : static::query()->where('scholarship_id', $id)->where('gsu_id','Like', '%'.$search.'%')
                         ->orwhere('sex','Like', '%'.$search.'%')
                          ->orwhereHas('user', function($q) use ($search){
                            $q->where('surname', 'Like', '%'.$search.'%')->orwhere('first_name', 'Like', '%'.$search.'%');
                             });;
    }

}
