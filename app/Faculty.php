<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faculty extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'name', 'dean_id', 'code', 
    ];
       
    protected $with = ['departments'];
    
     public function deans()
    {
        return $this->hasMany('App\Dean');
    }
     public function current()
    { 
        $current = Dean::find($this->dean_id)->full_name;
        return $this->belongsTo('App\Dean', 'dean_id');
    }

    /**
    * A accessor to get Faculty full name. 
    *
    */
    public function getFullNameAttribute()
    { 
      return $this->code . " : " . $this->name; 
    }
    
    
    public function departments()
    {
        return $this->hasMany('App\Department');
    }  
    
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%')->orwhere('code','Like', '%'.$search.'%');
    }
    
}
