<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Oldresult extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    use SoftDeletes;
     protected $dates = [
        'created_at', 'updated_at',  
    ];
    protected $fillable = [ 
            'student_id', 'cours_w', 'exam', 'course_id', 'weight_id', 'year', 'part_id', 'comment'
        ];
    protected $appends = ['overall', 'grade'];
            
    protected $with = ['weight', 'part', 'course'];
     /**
    * An accessor to display the student's Overall Mark rounded to the nearest Integer.
    *
    */
    public function getOverallAttribute()
    {
        $ovral = $this->cours_w * $this->weight->course_w + $this->exam * $this->weight->exam;
      return number_format($ovral);
       // return $ovral;
    } 
     
    /**
    * An accessor to display the student's Grade.
    *
    */
    public function getGradeAttribute()
    {
        if ($this->overall > 74.5) { return '1'; }        
        elseif ($this->overall >=64.5 && $this->overall < 74.5) { return '2.1'; }
        elseif ($this->overall >=54.5 && $this->overall <=64.5) { return '2.2'; }
        elseif ($this->overall >=49.5 && $this->overall <=54.5) { return 'P'; }
        else { return 'F'; }

    }

    
     public function student()
    {
        return $this->belongsTo('App\Student');
    }
     
      public function course()
    {
        return $this->belongsTo('App\Course');
    }

      public function part()
    {
        return $this->belongsTo('App\Part');
    }

      public function weight()
    {
        return $this->belongsTo('App\Weight');
    } 
}
 