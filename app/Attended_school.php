<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attended_school extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $connection = 'mysql';
	protected $dates = [
        'created_at', 'updated_at',  
    ];

    protected $fillable = [
        'applicant_id', 'name', 'from_year', 'to_year', 'address',  
    ];
    
     public function applicant()
    {
        return $this->belongsTo('App\Applicant');
    } 

    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%');
    }

   
}
