<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hod extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    protected $dates = [
        'created_at', 'updated_at',  
    ];
    use SoftDeletes;

    protected $fillable = [
        'staff_id', 'department_id', 'active', 'from_date', 'to_date',
    ];
   
    
    protected $with = ['staff'];
  
    public static function search($search) 
    {
        return empty($search) ? static::query()
        : static::query()->where('name','Like', '%'.$search.'%');
    } 

    public function getFullNameAttribute()
    { 
        return $this->staff->user->fullname;
    }
    
     public function department()
    {
        return $this->belongsTo('App\Department');
    }
     public function staff()
    {
        return $this->belongsTo('App\Staff');
    }

}
