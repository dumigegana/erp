<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Result extends Model implements Auditable
{
        use \OwenIt\Auditing\Auditable;
protected $connection = 'mysql';
    use SoftDeletes;
    protected $table = 'results';

    protected $fillable = ['registration_id', 'course_w', 'exam', 'is_publish', 'remark'];
    /**
    * An accessor to display the student's Overall Mark rounded to the nearest Integer.
    *
    */
    public function getOverallAttribute()
    { 
      $c_w = $this->registration->sem_course->weight->course_w;
      $ex = $this->registration->sem_course->weight->exam;
      return number_format($this->course_w * $c_w + $this->exam * $ex);
    }
     
    /**
    * An accessor to display the student's Grade.
    *
    */
    public function getGradeAttribute()
    {
        switch ($this->overal) {
            case $this->overal < 49.5:
            return 'F';
            break;

            case $this->overal >= 49.5 && $this->overal <= 54.5:
            return 'P';
            break;

            case $this->overal >=  54.5 && $this->overal <= 64.5:
            return '2.2';
            break;

            case $this->overal >=  64.5 && $this->overal <= 74.5:
            return '2.1';
            break;
        
            default:
            return '1';
            break;
        }      
    }

    public function registration()
    {
        return $this->belongsTo('App\Registration');
    }   

    public function fail()
    {
        return $this->hasOne('App\Fail', 'fail_result_id');
    } 

    public function repeat()
    {
        return $this->hasOne('App\Fail', 'repeat_result_id');
    }
}
