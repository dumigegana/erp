# [**Gwanda State University Enteprise Resource Plan**](http://www.gsu.ac.zw)

## About GSU ERP

## Installation
composer dump-autoload
php artisan storage:link
php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider" --tag=config

## Main Modules
1. Roles and permissions
2. Admissions
3. Bulletin & Registration
4. Grades
5. Reports 
6. Payments

## Bulletin & Registration
### Bulletin
This module groups Program courses according to the order they should be taken by student at any given Part. To each semester course the Lecturer is assigned to indicate that the course is on offer in that particular semester.   

The academic officers create (submits), the full list of program course (Academics -> Courses). Then they group them according to Parts (Academics -> Program Courses).

The Records officer creates (submits) academic semester, giving it a name that clear indicates the year, study program and whether its 1st or second semester of that year. Also the opening and closing dates for that particular semester are submitted (Records -> Semesters).

The HODs (Academic departments) links the semester and Program courses, then assign Lecturers to courses (Records -> Semester Courses) 

### Registration
To register for a semester the student should login to the portal, Click on Registration and select the semester. The courses for that student for the selected semester will appear and below them a submit button to submit those courses.

## Roles permissions
### Dean -> admission_access, academic_access, registration_access academic_dean
### HOD  -> academic_access, academic_hod
### Lecturer  -> academic_access, academic_hod gsu_tutor
### Staff  -> dashboard_access,
### Registra  -> admission_access, academic_access, academic_admin, admission_admin registration_access academic_regestry,


